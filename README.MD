# IronMax prj.

> [DEPLOYMENT](DEPLOYMENT.MD)

## Инсталляция проекта

    перед установкой необходимо предоставить ssh publklic key менеджеру проекта

Выполнить:

> ./install.sh

## Запуск проекта

Выполнить:

> ./run.sh

## Повторная инсталляция проекта

Выполнить:

> ./rebuild.sh
    
----
2021 [daglob](http://daglob.ru)
