<?php

namespace App\Observers;

use App\Models\EventReports;
use App\Models\EventReportsPool;

class EventReportsObserver
{
    public function created(EventReports $event_report)
    {
        $event_report = EventReports::whereId($event_report->id)
            ->whereIn('code', array_merge(getWarningStatuses(), getCriticalStatuses()))
            ->with('report', 'facility')
            ->first();
        if (!is_null($event_report)) {
            if ($event_report->report === null) {
                $event_report->makeHidden('report');
                $event = EventReportsPool::firstOrNew(
                    [
                        'facility_code' => $event_report->facility_code,
                        'code' => $event_report->code,
                        'time_group' => $event_report->time_group,
                        'date' => $event_report->date,
                        'type' => 'facility',
                    ],
                    ['type' => 'facility', 'event_data' => $event_report, 'date' => $event_report->date]
                );
            } else {
                $event = EventReportsPool::firstOrNew(
                    [
                        'report_id' => $event_report->report_id,
                        'code' => $event_report->code,
                        'date' => $event_report->date,
                        'type' => 'report',
                    ],
                    ['type' => 'report', 'event_data' => $event_report, 'date' => $event_report->date]
                );
            }
            $event->save();

            if ($event->exists) {
                $event_report->processed = true;
                $event_report->save();
            }
        }
    }
}
