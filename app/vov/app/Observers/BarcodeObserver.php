<?php

namespace App\Observers;

use App\Models\Barcode;

class BarcodeObserver
{
    public function created(Barcode $barcode)
    {
        $this->freshLatestQrs($barcode);
    }

    public function deleted(Barcode $barcode)
    {
        $this->freshLatestQrs($barcode);
    }

    private function freshLatestQrs(Barcode $updatedBarcode)
    {
        /** @var Barcode[] $latestBarcodes */
        $latestBarcodes = Barcode::query()
            ->where('facility_id', $updatedBarcode->facility_id)
            ->latest()
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();

        foreach ($latestBarcodes as $barcode) {
            $barcode->updateQrString($barcode == $latestBarcodes->first());
            $barcode->updateQrImg();
            $barcode->save();
        }
    }
}
