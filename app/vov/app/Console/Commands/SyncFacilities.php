<?php

namespace App\Console\Commands;

use App\Models\Facility;
use App\Models\MongoFacilitiesModel;
use App\Models\User;
use Illuminate\Console\Command;

class SyncFacilities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'x-mongo-sync:facilities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация данных с Mongo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (MongoFacilitiesModel::all() as $facilitiesModel) {
            $f = Facility::findOrNew($facilitiesModel->owner_id);
            $f->data = $facilitiesModel->toJson(JSON_INVALID_UTF8_IGNORE);
            $f->save();
        }
    }
}
