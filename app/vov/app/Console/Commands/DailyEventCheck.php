<?php

namespace App\Console\Commands;

use App\Models\EventReports;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use stdClass;

class DailyEventCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reports:daily-event-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $events = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (User::all() as $user) {
            $time_new = [];
            for ($i = 9; $i >= 0; $i--) {
                $time = Carbon::now($user->timezone ?? 'UTC')->subMinutes($i);
                $time->second = 0;
                array_push($time_new, $time);
            }
            foreach ($time_new as $time_once) {
                if ($user->facilities->count()) {
                    $scheduling = $user->facilities->map(function ($item) use ($time_once) {
                        $object = new stdClass;
                        $object->owner_id = $item->owner_id;
                        $object->day_schedule = $item->weekly_schedule[strtolower($time_once->englishDayOfWeek)] ?? null;
                        unset($object->day_schedule['first_patrol'], $object->day_schedule['last_patrol'], $object->day_schedule['weekday_index'], $object->day_schedule['weekday']);
                        return $object;
                    });
                    $this->scheduling($time_once, $scheduling, $user);
                }
            }
        }
        if (!empty($this->events)) {
            $this->runCommand('send:events', [], $this->output);
        }
    }
    private function scheduling($time_once, $scheduling, $user)
    {
        foreach ($scheduling as $schedule) {
            if (!is_null($schedule->day_schedule)) {
                foreach ($schedule->day_schedule as $value) {
                    $time_array = explode(':', $value['end']);
                    $time = $time_array[0] . ':' . $time_array[1];
                    $time = Carbon::parse($time_once->toDateString() . ' ' . $time, $user->timezone ?? 'UTC');
                    if (!($time_array[0] >= 22 && $time_array[1] > 30)) {
                        $time->addMinutes(50);
                    }
                    if ($time->diffInMinutes($time_once) === 0) {
                        $time->subMinutes(50);
                        $data = DB::table('pdf_datas')->selectRaw('count(*) AS cnt,  JSON_ARRAYAGG(id) AS array_reports_ids')
                        ->whereIn('type', ['inspector_report', 'patrol_report'])
                        ->where('is_cheating', '!=', 1)
                        ->where('owner_id', $schedule->owner_id)
                            ->whereRaw('date(patrol_start_time) = ?', [$time->toDateString()])
                            ->whereRaw("timediff(time(patrol_start_time), ?) > 0", [$value['start']]) // правильное время начала и конца патруля вставить
                            ->whereRaw("timediff(time(patrol_start_time), ?) < 0", [$value['end']])
                            ->whereRaw("DAYNAME(patrol_start_time) = ?", [$time->englishDayOfWeek])
                            ->get();
                        $cnt = $data[0]->cnt; // колличество записей в выборке
                        $array_reports_ids = $data[0]->array_reports_ids ? json_decode($data[0]->array_reports_ids) : []; //json из id
                        $data = DB::table('event_reports')
                        ->selectRaw("if(count(*) > 0 AND code = 's_tb_em', 1, 0) AS has_empty, if(count(*) > 0 AND code = 's_tb_ls', 1,0) AS has_less")
                        ->where('date', $time->toDateString())
                            ->where('start', $value['start'] . ':00')
                            ->where('facility_code', $schedule->owner_id)
                            ->whereIn('code', ['s_tb_em', 's_tb_ls'])->get();
                        $has_empty = $data[0]->has_empty;
                        $has_less = $data[0]->has_less;

                        if ($cnt == 0 && $has_empty == 0) {
                            $origin = new DateTime($time->toDateString() . ' ' . $value['start']);
                            $target = new DateTime($time->toDateString() . ' ' . $value['end']);
                            $event = EventReports::updateOrCreate([
                                'utc_time_point' => Carbon::parse($time->toDateString() . ' ' . $value['end'], $user->timezone)->timezone('UTC'),
                                'facility_code' => $schedule->owner_id,
                                'date' => $time->toDateString(),
                                'start' => $value['start'],
                                'duration' => $origin->diff($target)->format('%H:%I:%S'),
                                'code' => 's_tb_em',
                                'time_group' => $value['start'] . '-' . $value['end'],
                                'expected_reports' => $value['expected_count']
                            ]);
                            array_push($this->events, $event);
                        } elseif (($cnt < $value['expected_count'] && $has_less == 0 && $has_empty != 1 )) {
                            $origin = new DateTime($time->toDateString() . ' ' . $value['start']);
                            $target = new DateTime($time->toDateString() . ' ' . $value['end']);
                            $event = EventReports::updateOrCreate([
                                'utc_time_point' => Carbon::parse($time->toDateString() . ' ' . $value['end'], $user->timezone)->timezone('UTC'),
                                'facility_code' => $schedule->owner_id,
                                'date' => $time->toDateString(),
                                'start' => $value['start'],
                                'duration' => $origin->diff($target)->format('%H:%I:%S'),
                                'code' => 's_tb_ls',
                                'time_group' => $value['start'] . '-' . $value['end'],
                                'expected_reports' => $value['expected_count'],
                                'reports_ids' => $array_reports_ids,
                            ]);
                            array_push($this->events, $event);
                        }
                    }
                }
            }
        }
    }
}
