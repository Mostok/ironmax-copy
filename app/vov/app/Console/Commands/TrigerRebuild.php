<?php

namespace App\Console\Commands;

use App\Models\EventReports;
use App\Models\Facility;
use App\Models\pdfData;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;

class TrigerRebuild extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rebuild:triger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $patrol_events;
    private $events = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->patrol_events = config('patrol_events');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (pdfData::unprocessed()->get()->all() as $pdf_data) {
            $facilyty = Facility::where('owner_id', $pdf_data->owner_id)->first();
            if (is_null($facilyty)) {
                $this->patrol_unknown_facility($pdf_data);
            } elseif (is_null($facilyty->timetable)) {
                $this->patrol_empty_timetable($pdf_data);
            } elseif (is_null($pdf_data->owner_id) || $pdf_data->owner_id == '') {
                $this->patrol_not_set_facility($pdf_data);
            } else {
                if ($pdf_data->is_cheating === 1) {
                    $this->patrol_cheat($pdf_data);
                } elseif ($pdf_data->type == 'incident_report') {
                    $this->patrol_incident($pdf_data);
                } elseif ($pdf_data->type == 'emergency_report') {
                    $this->patrol_emergency($pdf_data);
                } elseif ($pdf_data->type == 'inspector_report' || $pdf_data->type == 'patrol_report') {
                    if (is_null($pdf_data->patrol_start_time) || $pdf_data->patrol_start_time == '') {
                        $this->patrol_not_set_patrol_start_time($pdf_data);
                    } elseif ((Carbon::parse($pdf_data->patrol_end_time)->getTimestamp() - Carbon::parse($pdf_data->patrol_start_time)->getTimestamp()) < 0) {
                        $this->patrol_wrong_start_to_end_time_ratio($pdf_data);
                    } elseif ($pdf_data->barcodes_scanned != $pdf_data->barcodes_should_be_scanned) {
                        $this->patrol_barcodes($pdf_data);
                    } elseif (Carbon::parse($pdf_data->patrol_start_time)->dayName !== Carbon::parse($pdf_data->patrol_end_time)->dayName) {
                        $this->patrol_mistake_day($pdf_data);
                    } else {
                        $day_of_week = strtolower(Carbon::parse($pdf_data->patrol_end_time)->dayName);
                        $schedule_for_day_of_week = $facilyty->weekly_schedule[$day_of_week . 's'];
                        unset($schedule_for_day_of_week['first_patrol'], $schedule_for_day_of_week['last_patrol'], $schedule_for_day_of_week['weekday_index'], $schedule_for_day_of_week['weekday']);
                        if (empty($schedule_for_day_of_week)) {
                            $this->patrol_mistake_day($pdf_data);
                        } else {
                            $patrol_mistake_time = true;
                            foreach ($schedule_for_day_of_week as $schedule) {
                                $start_needed = Carbon::parse(Carbon::parse($pdf_data->patrol_start_time)->toDateString() . ' ' . $schedule['start'])->getTimestamp();
                                $end_needed = Carbon::parse(Carbon::parse($pdf_data->patrol_end_time)->toDateString() . ' ' . $schedule['end'])->getTimestamp();
                                $start = Carbon::parse($pdf_data->patrol_start_time)->getTimestamp();
                                $end = Carbon::parse($pdf_data->patrol_end_time)->getTimestamp();
                                if (($start_needed - $start < 0) && ($end_needed - $end > 0)) {
                                    $patrol_mistake_time = false;
                                    break;
                                }
                            }
                            if ($patrol_mistake_time) {
                                $this->patrol_mistake_time($pdf_data);
                            } else {
                                $this->patrol_success($pdf_data);
                            }
                        }
                    }
                }
            }
            $pdf_data->processed = true;
            $pdf_data->save();
        }

        if (!empty($this->events)) {
            $this->runCommand('send:events', [], $this->output);
        }
    }

    // errors block
    private function patrol_wrong_start_to_end_time_ratio($pdf_data)
    {
        EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->created_at),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->created_at)->toDateString(),
            'start' => Carbon::parse($pdf_data->created_at)->toTimeString(),
            'duration' => '00:00:00',
            'code' => $this->patrol_events['errors']['patrol_wrong_start_to_end_time_ratio'],
            'reports_ids' => [$pdf_data->id],
        ]);
    }

    private function patrol_empty_timetable($pdf_data)
    {
        EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->created_at),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->created_at)->toDateString(),
            'start' => Carbon::parse($pdf_data->created_at)->toTimeString(),
            'duration' => '00:00:00',
            'code' => $this->patrol_events['errors']['patrol_empty_timetable'],
            'reports_ids' => [$pdf_data->id]
        ]);
    }

    private function patrol_not_set_facility($pdf_data)
    {
        EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->created_at),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->created_at)->toDateString(),
            'start' => Carbon::parse($pdf_data->created_at)->toTimeString(),
            'duration' => '00:00:00',
            'code' => $this->patrol_events['errors']['patrol_not_set_facility'],
            'reports_ids' => [$pdf_data->id],
        ]);
    }

    private function patrol_not_set_patrol_start_time($pdf_data)
    {
        EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->created_at),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->created_at)->toDateString(),
            'start' => Carbon::parse($pdf_data->created_at)->toTimeString(),
            'duration' => '00:00:00',
            'code' => $this->patrol_events['errors']['patrol_not_set_patrol_start_time'],
            'reports_ids' => [$pdf_data->id],
        ]);
    }

    private function patrol_unknown_facility($pdf_data)
    {
        EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->created_at),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->created_at)->toDateString(),
            'start' => Carbon::parse($pdf_data->created_at)->toTimeString(),
            'duration' => '00:00:00',
            'code' => $this->patrol_events['errors']['patrol_unknown_facility'],
            'reports_ids' => [$pdf_data->id],
        ]);
    }
    //erros block ended

    // statuses block
    private function patrol_cheat($pdf_data)
    {
        $origin = new DateTime(Carbon::parse($pdf_data->patrol_start_time));
        $target = new DateTime(Carbon::parse($pdf_data->patrol_end_time));
        $event = EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->patrol_end_time, $pdf_data->user->timezone)->timezone('UTC'),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->patrol_start_time)->toDateString(),
            'start' => Carbon::parse($pdf_data->patrol_start_time)->toTimeString(),
            'duration' => $origin->diff($target)->format('%H:%I:%S'),
            'code' => $this->patrol_events['statuses']['patrol_cheat'],
            'reports_ids' => [$pdf_data->id],
        ]);
        array_push($this->events, $event);
    }

    private function patrol_emergency($pdf_data)
    {
        $event = EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->patrol_end_time, $pdf_data->user->timezone)->timezone('UTC'),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->created_at)->toDateString(),
            'start' => Carbon::parse($pdf_data->created_at)->toTimeString(),
            'duration' => '00:00:00',
            'code' => $this->patrol_events['statuses']['patrol_emergency'],
            'reports_ids' => [$pdf_data->id],
        ]);
        array_push($this->events, $event);
    }

    private function patrol_incident($pdf_data)
    {
        $event = EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->created_at->toDateTimeString(), $pdf_data->user->timezone)->timezone('UTC'),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => $pdf_data->created_at->toDateString(),
            'start' => $pdf_data->created_at->toTimeString(),
            'duration' => '00:00:00',
            'code' => $this->patrol_events['statuses']['patrol_incident'],
            'reports_ids' => [$pdf_data->id],
        ]);
        array_push($this->events, $event);
    }

    private function patrol_mistake_time($pdf_data)
    {
        $origin = new DateTime(Carbon::parse($pdf_data->patrol_start_time));
        $target = new DateTime(Carbon::parse($pdf_data->patrol_end_time));
        $event = EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->patrol_end_time, $pdf_data->user->timezone)->timezone('UTC'),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->patrol_start_time)->toDateString(),
            'start' => Carbon::parse($pdf_data->patrol_start_time)->toTimeString(),
            'duration' => $origin->diff($target)->format('%H:%I:%S'),
            'code' => $this->patrol_events['statuses']['patrol_mistake_time'],
            'reports_ids' => [$pdf_data->id],
        ]);
        array_push($this->events, $event);
    }

    private function patrol_mistake_day($pdf_data)
    {
        $origin = new DateTime(Carbon::parse($pdf_data->patrol_start_time));
        $target = new DateTime(Carbon::parse($pdf_data->patrol_end_time));
        $event = EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->patrol_end_time, $pdf_data->user->timezone)->timezone('UTC'),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->patrol_start_time)->toDateString(),
            'start' => '00:00:00',
            'duration' => $origin->diff($target)->format('%H:%I:%S'),
            'code' => $this->patrol_events['statuses']['patrol_mistake_day'],
            'reports_ids' => [$pdf_data->id],
        ]);
        array_push($this->events, $event);
    }

    private function patrol_barcodes($pdf_data)
    {
        $origin = new DateTime(Carbon::parse($pdf_data->patrol_start_time));
        $target = new DateTime(Carbon::parse($pdf_data->patrol_end_time));
        $event = EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->patrol_end_time, $pdf_data->user->timezone)->timezone('UTC'),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->patrol_start_time)->toDateString(),
            'start' => Carbon::parse($pdf_data->patrol_start_time)->toTimeString(),
            'duration' => $origin->diff($target)->format('%H:%I:%S'),
            'code' => $this->patrol_events['statuses']['patrol_barcodes'],
            'reports_ids' => [$pdf_data->id],
        ]);
        array_push($this->events, $event);
    }

    private function patrol_success($pdf_data)
    {
        $origin = new DateTime(Carbon::parse($pdf_data->patrol_start_time));
        $target = new DateTime(Carbon::parse($pdf_data->patrol_end_time));
        $event = EventReports::updateOrCreate([
            'utc_time_point' => Carbon::parse($pdf_data->patrol_end_time, $pdf_data->user->timezone)->timezone('UTC'),
            'report_id' => $pdf_data->id,
            'facility_code' => $pdf_data->owner_id,
            'date' => Carbon::parse($pdf_data->patrol_start_time)->toDateString(),
            'start' => Carbon::parse($pdf_data->patrol_start_time)->toTimeString(),
            'duration' => $origin->diff($target)->format('%H:%I:%S'),
            'code' => $this->patrol_events['statuses']['patrol_success'],
            'reports_ids' => [$pdf_data->id],
        ]);
        array_push($this->events, $event);
    }
}
