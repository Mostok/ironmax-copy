<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class SendEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:events';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send all events all users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (User::all() as $user) {
            broadcast(new \App\Events\Dashboard\MissingPatrolsEvent($user));
            broadcast(new \App\Events\Dashboard\EmergencyEventsEvent($user));
            broadcast(new \App\Events\Dashboard\IncidentReportsEvent($user));
            broadcast(new \App\Events\Dashboard\MissingBarcodesEvent($user));
            broadcast(new \App\Events\Dashboard\MissingInspectorBarcodesEvent($user));
            broadcast(new \App\Events\Dashboard\PointsEvent($user));
            broadcast(new \App\Events\Dashboard\ProfileEvent($user));
            broadcast(new \App\Events\Dashboard\NotificationsEvent($user));
            broadcast(new \App\Events\Dashboard\Reports\PatrolsEvent($user));
            broadcast(new \App\Events\Dashboard\Reports\ExceptionsEvent($user));
            broadcast(new \App\Events\Dashboard\Reports\EmergencyEvent($user));
            broadcast(new \App\Events\Dashboard\Reports\IncidentEvent($user));
            broadcast(new \App\Events\Dashboard\Reports\InspectorEvent($user));
        }
    }
}
