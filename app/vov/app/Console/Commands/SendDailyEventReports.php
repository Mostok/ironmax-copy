<?php

namespace App\Console\Commands;

use App\Notifications\DailyReport;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendDailyEventReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reports:send_daily_event_reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправка указанному пользователю отчета о событиях на объектах на текущий момент';

    /**
     * Execute the console command.
     *DashboardController
     * @return mixed
     */
    public function handle()
    {
        foreach (User::where('send_out_events', '=', true)->get() as $u) {
            $time_new = [];
            for ($i = 9; $i >= 0; $i--) {
                $time = Carbon::now($u->timezone ?? 'UTC')->subMinutes($i);
                $time->second = 0;
                array_push($time_new, $time);
            }
            if ($u->facilities->count()) {
                foreach ($time_new as $time_once) {
                    $time = $u->facilities->map(function ($val) {
                        return $val->weekly_schedule->mapToGroups(function ($item) {
                            return [$item['weekday'] => $item['last_patrol'] ?? null];
                        });
                    })->pluck(strtolower($time_once->englishDayOfWeek))->max()->first();
                    $time_array = explode(':', $time);
                    if (!($time_array[0] >= 22 && $time_array[1] > 30)) {
                        $time_array[0] = $time_array[0] + 1;
                    } else {
                        $time_once->subMinutes(10);
                    }
                    $time = $time_array[0] . ':' . $time_array[1];
                    $time = Carbon::parse($time_once->toDateString() . ' ' . $time, $u->timezone ?? 'UTC');
                    if ($time->diffInMinutes($time_once) === 0) {
                        try {
                            Log::info('Message: ' . $u->id . ' user id and for time ' . $time_once->toDateTimeString());
                            Log::info($this->sendMessage($u));
                        } catch (\Exception $e) {
                            Log::critical($e->getMessage());
                            continue;
                        }
                    }
                }
            }
        }
    }

    public function sendMessage($user)
    {
        $from = now($user->timezone)->setTime(0, 0, 0)->addDays(-1);
        $to = now($user->timezone)->setTime(0, 0, 0);

        $status_codes = array_values(array_merge(getWarningStatuses(), getCriticalStatuses()));

        $er = $user->events()
            ->whereIn('code', $status_codes)
            ->with(['report', 'reports'])
            ->today()
            ->get()->sortByDesc('start_date_time')->sortBy(function ($a) {

                return array_search($a->code, [
                    config('patrol_events.statuses.patrol_emergency'),
                    config('patrol_events.statuses.patrol_incident'),
                    config('patrol_events.statuses.patrol_cheat'),
                    config('patrol_events.statuses.time_block_empty'),
                    config('patrol_events.statuses.time_block_less'),
                    config('patrol_events.statuses.patrol_barcodes'),
                ]);
            })->groupBy('facility_code')->sortKeys()->toArray();
        $facilities = $user->facilities()->get()->toArray();

        $data = [];

        foreach ($facilities as $facility) {
            if (!isset($er[$facility['code']])) {
                continue;
            }
            $event_reports = $er[$facility['code']];
            if (!empty($event_reports)) {
                $data[] = array_merge($facility, ['event_reports' => $event_reports]);
            }
        }

        if (empty($data)) {
            return "not data for send reports to {$user->email}\n";
        }

        /**
         * отправка
         */
        $user->notify(new DailyReport(
            $from,
            $to,
            $data
        ));
    }
}
