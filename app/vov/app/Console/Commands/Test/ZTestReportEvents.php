<?php

namespace App\Console\Commands\Test;

use App\Models\Report;
use App\Models\ScannedBarcodes;
use Illuminate\Console\Command;

class ZTestReportEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'z-test:reports-events';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Only for testing!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        factory(Report::class, 5)->create();
        factory(ScannedBarcodes::class, 5)->create();
        $this->runCommand('sync:event-report-pool', [], $this->output);
    }
}
