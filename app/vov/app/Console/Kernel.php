<?php

namespace App\Console;

use App\ScannedBarcode;
use App\Models\User;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  Schedule  $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule, $query = null)
    {
        $schedule
            ->command('reports:daily-event-check')
            ->everyTenMinutes();

        /** Формирование уведомлений по новым отчетам */
        $schedule
            ->command('rebuild:triger')
            ->everyFiveMinutes()
            ->appendOutputTo(storage_path('logs/cron_rebuild_triger.log'));

        /** Синхронизация данных с MongoDB */
        $schedule
            ->command('x-mongo-sync:facilities')
            ->everyTenMinutes()
            ->withoutOverlapping(3);

        /**
         * Рассылка ежедневных отчетов
         * Динамическое формирование тасков в зависимости от настроек пользователя
         */
        $schedule->command('reports:send_daily_event_reports')
            ->everyTenMinutes()
            ->emailOutputOnFailure('daglob@mail.ru');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }
}
