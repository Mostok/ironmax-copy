<?php

namespace App\Providers;

use App\Models\Barcode;
use App\Models\EventReports;
use App\Observers\BarcodeObserver;
use App\Observers\UserObserver;
use App\Models\User;
use App\Observers\EventReportsObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Barcode::observe(BarcodeObserver::class);
        EventReports::observe(EventReportsObserver::class);
    }
}
