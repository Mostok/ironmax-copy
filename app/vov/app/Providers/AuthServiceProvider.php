<?php

namespace App\Providers;
// use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //		'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {

        $this->registerPolicies();
        Passport::routes();
        Passport::cookie('some_test_cookies');

        $gate_list = [
            'admin' => ['admin'],
            'big_boss' => ['big_boss'],
            'mini_boss' => ['mini_boss'],
            'security_officer' => ['security_officer'],
            'security_inspector' => ['security_inspector'],
            'boss' => ['big_boss', 'mini_boss'],
            'security' => ['security_officer', 'security_inspector'],
        ];

        foreach ($gate_list as $gate_name => $types) {
            Gate::define($gate_name, function ($user) use ($types) {

                return in_array($user->type, $types);
            });
        }

        //for passport
        // Passport::routes();
        // Passport::routes();
        // Passport::tokensExpireIn(Carbon::now()->addHours(24));
        // Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
    }
}
