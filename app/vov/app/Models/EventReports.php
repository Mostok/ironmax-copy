<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
use Staudenmeir\EloquentJsonRelations\Relations\BelongsToJson;

/**
 * Class EventReports
 *
 * @package App
 *
 * @property-read Report $report
 * @property-read MongoFacilitiesModel $facility
 *
 * @method Builder unprocessed() return only processed = null
 * @method Builder processed() return only processed = 1
 * @method Builder warning() return only warning codes
 * @method Builder critical() return only critical codes
 *
 * @method static Builder today($tz = null) filter today only
 * @method static Builder yesterday($tz = null) filter yesterday only
 * @method static Builder lastDay($tz = null) filter last day (last 24 hours from today) only
 * @method static Builder prevWeek($tz = null) filter previous week only
 * @method static Builder curWeek($tz = null) filter current week only
 * @method static Builder lastWeek($tz = null) filter last week (last 7 days from today) only
 * @method static Builder lastMonth($tz = null) filter last month only
 * @method static Builder lastYear($tz = null) filter last month only
 * @method static Builder betwixt(Carbon $from, Carbon $to, $tz = null) filter by between
 */
class EventReports extends Model
{
    use HybridRelations;
    use HasJsonRelationships;

    protected $table = 'event_reports';
    protected $connection = 'mysql';
    protected $casts = [
        'reports_ids' => 'json',
        'missed_points' => 'array',
    ];
    protected $appends = [
        'start_date_time',
        'short_time_group',
        'short_time_group_rtl',
        'timezone_time_point'
    ];

    protected $fillable = [
        'utc_time_point',
        'facility_code',
        'date',
        'start',
        'duration',
        'code',
        'time_group',
        'expected_reports',
        'reports_ids',
        'report_id',
    ];

    /**
     * @param  Builder  $query
     * @param  null  $tz
     * @return Builder
     *
     * @see today
     */
    public function scopeToday(Builder $query, $tz = null): Builder
    {
        return $this->setCondition(
            $query,
            Date::today($tz)->format('Y-m-d H:i:s'),
            Date::tomorrow($tz)->format('Y-m-d H:i:s'),
            $tz
        );
    }

    protected function setCondition(Builder $query, $from, $to, $tz = null): Builder
    {
        $from = new Carbon($from, $tz);
        $to = new Carbon($to, $tz);
        return $query->whereBetween('utc_time_point', [$from->setTimezone('UTC'), $to->setTimezone('UTC')]);
    }

    public function scopeLastDay(Builder $query, $tz = null): Builder
    {
        return $this->setCondition(
            $query,
            Date::now($tz)->addDays(-1)->format('Y-m-d H:i:s'),
            Date::now($tz)->format('Y-m-d H:i:s'),
            $tz
        );
    }

    /**
     * @param  Builder  $query
     * @param  null  $tz
     * @return Builder
     *
     * @see yesterday
     */
    public function scopeYesterday(Builder $query, $tz = null): Builder
    {
        return $this->setCondition(
            $query,
            Date::yesterday($tz)->format('Y-m-d H:i:s'),
            Date::today($tz)->format('Y-m-d H:i:s'),
            $tz
        );
    }

    /**
     * @param  Builder  $query
     * @param  null  $tz
     * @return Builder
     *
     * @see prevWeek
     */
    public function scopePrevWeek(Builder $query, $tz = null): Builder
    {
        return $this->setCondition(
            $query,
            Date::today($tz)->addDays(-7)->startOf('week')->format('Y-m-d H:i:s'),
            Date::today($tz)->addDays(-7)->endOfWeek()->format('Y-m-d H:i:s'),
            $tz
        );
    }

    /**
     * @param  Builder  $query
     * @param  null  $tz
     * @return Builder
     *
     * @see curWeek
     */
    public function scopeCurWeek(Builder $query, $tz = null): Builder
    {
        return $this->setCondition(
            $query,
            Date::today($tz)->startOf('week')->format('Y-m-d H:i:s'),
            Date::today($tz)->endOfWeek()->format('Y-m-d H:i:s'),
            $tz
        );
    }

    /**
     * @param  Builder  $query
     * @param  null  $tz
     * @return Builder
     *
     * @see lastWeek
     */
    public function scopeLastWeek(Builder $query, $tz = null): Builder
    {
        return $this->setCondition(
            $query,
            Date::now($tz)->addDays(-7)->format('Y-m-d H:i:s'),
            Date::now($tz)->format('Y-m-d H:i:s'),
            $tz
        );
    }

    /**
     * @param  Builder  $query
     * @param  null  $tz
     * @return Builder
     *
     * @see lastMonth
     */
    public function scopeLastMonth(Builder $query, $tz = null): Builder
    {
        return $this->setCondition(
            $query,
            Date::now($tz)->addMonths(-1)->format('Y-m-d H:i:s'),
            Date::now($tz)->format('Y-m-d H:i:s'),
            $tz
        );
    }

    /**
     * @param  Builder  $query
     * @param  null  $tz
     * @return Builder
     *
     * @see lastYear
     */
    public function scopeLastYear(Builder $query, $tz = null): Builder
    {
        return $this->setCondition(
            $query,
            Date::now($tz)->addYears(-1)->format('Y-m-d H:i:s'),
            Date::now($tz)->format('Y-m-d H:i:s'),
            $tz
        );
    }

    /**
     * @param  Builder  $query
     * @param  Carbon  $from
     * @param  Carbon  $to
     * @param  null  $tz
     * @return Builder
     *
     * @see betwixt
     */
    public function scopeBetwixt(Builder $query, Carbon $from, Carbon $to, $tz = null): Builder
    {
        if (empty($from) || empty($to)) {
            return $query;
        }

        return $this->setCondition(
            $query,
            $from->setTimezone($tz)->format('Y-m-d H:i:s'),
            $to->setTimezone($tz)->format('Y-m-d H:i:s'),
            $tz
        );
    }

    public function scopeFrom(Builder $query, Carbon $from, $tz = null): Builder
    {
        return $this->setCondition(
            $query,
            $from->setTimezone($tz)->format('Y-m-d H:i:s'),
            Date::now($tz)->format('Y-m-d H:i:s'),
            $tz
        );
    }
    public function scopeTo(Builder $query, Carbon $to, $tz = null): Builder
    {
        return $this->setCondition(
            $query,
            Date::parse('1970-01-01')->setTimezone($tz)->format('Y-m-d H:i:s'),
            $to->setTimezone($tz)->format('Y-m-d H:i:s'),
            $tz
        );
    }

    public function getStartDateTimeAttribute()
    {
        return "{$this->date} {$this->start}";
    }

    public function getTimezoneTimePointAttribute()
    {
        $time = Carbon::parse($this->utc_time_point, 'UTC')->timezone($this->facility->user->timezone ?? 'UTC');
        return $time->toDateTimeString();
    }

    public function getShortTimeGroupAttribute()
    {
        if ($this->time_group) {
            $ntg = $this->parseTimeGroup($this->time_group);

            return implode('-', $ntg);
        }

        return null;
    }

    protected function parseTimeGroup($tg): array
    {
        return array_map(function ($t) {
            $t = explode(':', $t);

            return "{$t[0]}:{$t[1]}";
        }, explode('-', $tg));
    }

    public function getShortTimeGroupRtlAttribute()
    {
        if ($this->time_group) {
            $ntg = $this->parseTimeGroup($this->time_group);

            return implode('-', array_reverse($ntg));
        }

        return null;
    }

    /**
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeUnprocessed(Builder $query): Builder
    {
        return $query->whereNull('processed');
    }

    /**
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeProcessed(Builder $query): Builder
    {
        return $query->whereNotNull('processed')->where('processed', '=', 1, 'and');
    }

    /**
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeWarning(Builder $query): Builder
    {
        return $query->whereNotNull('code')->whereIn('code', getWarningStatuses());
    }

    /**
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeCritical(Builder $query): Builder
    {
        return $query->whereNotNull('code')->whereIn('code', getCriticalStatuses());
    }

    /**
     * @return HasOne
     */
    public function report(): HasOne
    {
        return $this->hasOne(Report::class, 'id', 'report_id');
    }

    /**
     * @return BelongsTo|HasMany|BelongsToJson
     */
    public function reports(): BelongsToJson
    {
        return $this->belongsToJson(Report::class, 'reports_ids');
    }

    /**
     * @return HasOne
     */
    public function officer(): HasOne
    {
        return $this->hasOne(Officer::class, 'owner_id', 'facility_code');
    }

    public function facility(): HasOne
    {
        return $this->hasOne(MongoFacilitiesModel::class, 'owner_id', 'facility_code');
    }
}
