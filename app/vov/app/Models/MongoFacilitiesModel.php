<?php

namespace App\Models;

use DateTimeInterface;
use Exception;
use Illuminate\Support\Collection;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

/**
 * Class MongoFacilitiesModel
 *
 * @package App
 * @property-read null|Collection $weekly_schedule Делаем матрицу интервалов (для фри - один период, для "дневки" - от
 *                             одного, "неделька" - на каждый день от 0) {@link getWeeklyScheduleAttribute()}
 * @property-read string|false $sql             Формирует часть sql, позволяющую оптимизировать запрос по
 *                пользователю
 *                {@see getSqlAttribute}
 * @property-read string $name
 * @property User $user
 * @property array $timetable
 */
class MongoFacilitiesModel extends Eloquent
{
    use SoftDeletes;

    public $primaryKey = 'owner_id';
    public $connection = 'mongodb';
    public $collection = 'Facilities';
    protected $dates = ['deleted_at'];
    protected $appends = [
        'name',
        'code',
    ];
    protected $visible = [
        'name',
        'code',
        'owner_id',
        'manager',
        'company_name',
        'company_pic',
        'company_id',
        'country',
        'facility_language',
        'facility_language',
        'sql',
        'time_zone',
		'is_sayar',
        'timetable',
        'reports',
        'events',
        'eventReportsPool',
        'eventReportsPoolWarning',
        'eventReportsPoolCritical',
        'facility_barcodes_list',
        'facility_name',
        'facility_email',
        'facility_language',
        '_class',
        'emergency_email',
        'barcodes_num',
        'mini_boss_group_id',
        'big_boss_group_id',
        'manager_id',
        'total_req_patrols',
        'is_on_barcode_mode',
        'updated_at',
    ];
    protected $fillable = [
        'barcodes_num',
        'facility_barcodes_list',
    ];

    public function getNameAttribute()
    {
        return $this->facility_name;
    }

    public function getCodeAttribute()
    {
        return $this->owner_id;
    }

    public function reports()
    {
        return $this->hasMany(Report::class, 'owner_id');
    }

    public function events()
    {
        return $this->hasMany(EventReports::class, 'facility_code');
    }

    public function eventReportsPool()
    {
        return $this->hasMany(EventReportsPool::class, 'facility_code');
    }

    public function eventReportsPoolWarning()
    {
        return $this->hasMany(EventReportsPool::class, 'facility_code')
            ->whereIn('code', getWarningStatuses());
    }

    public function eventReportsPoolCritical()
    {
        return $this->hasMany(EventReportsPool::class, 'facility_code')
            ->whereIn('code', getCriticalStatuses());
    }

    public function mini_bosses()
    {
        return $this->hasMany(User::class, 'mini_boss_group_id', 'mini_boss_group_id')->where('type', '=', 'mini_boss');
    }

    public function big_boss() // big_boss_group_id
    {
        return $this->hasOne(User::class, 'big_boss_group_id', 'big_boss_group_id')->where('type', '=', 'big_boss');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'owner_id', 'owner_id');
    }

    public function manager()
    {
        return $this->hasOne(MongoManagersModel::class, 'manager_id', 'manager_id');
    }

    /**
     * @return Collection|null
     * @throws Exception
     * @internal
     */
    public function getWeeklyScheduleAttribute(): ?Collection
    {
        if (empty($this->timetable)) {
            return null;
        }
        $weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $interval_rules = collect();
        switch ($this->timetable['type']) {
            case 'weekly':
                foreach ($this->timetable['value'] as $weekday => $intervals) {
                    $weekday_one = ucfirst(substr($weekday, 0, -1));
                    $rules = array_reduce($intervals, function ($all, $interval) use ($weekday) {
                        [$start, $end] = $interval['interval'];
                        $all[] = [
                            'name' => $weekday.'-'.implode('-', $interval['interval']).'-'.$interval['count'],
                            'start' => $start,
                            'end' => $end,
                            'expected_count' => (int) $interval['count'],
                        ];

                        return $all;
                    }, []);
                    if ($rules = $this->calcPatrolsInterval($rules)) {
                        $interval_rules->put(strtolower($weekday_one), array_merge($rules, [
                            'weekday' => strtolower($weekday_one),
                            'weekday_index' => array_search($weekday_one, $weekdays, true),
                        ]));
                        continue;
                    }
                    $interval_rules->put(strtolower($weekday_one), array_merge($rules, [
                        'weekday' => strtolower($weekday_one),
                        'weekday_index' => array_search($weekday_one, $weekdays, true),
                    ]));
                }
                break;
            case 'daily':
                $rules = array_reduce($this->timetable['value'], function ($all, $interval) {
                    [$start, $end] = $interval['interval'];
                    $all[] = [
                        'name' => 'Daily-'.implode('-', $interval['interval']).'-'.$interval['count'],
                        'start' => $start,
                        'end' => $end,
                        'expected_count' => (int) $interval['count'],
                    ];

                    return $all;
                }, []);
                foreach ($weekdays as $weekday) {
                    if ($rules = $this->calcPatrolsInterval($rules)) {
                        $interval_rules->put(strtolower($weekday), array_merge($rules, [
                            'weekday' => strtolower($weekday),
                            'weekday_index' => array_search($weekday, $weekdays, true),
                        ]));
                        continue;
                    }
                    $interval_rules->put(strtolower($weekday), $rules);
                }
                break;
            case 'free':
                $interval = $this->timetable['value'];
                [$start, $end] = $interval['interval'];
                $rules[] = [
                    'name' => 'Free-'.implode('-', $interval['interval']).'-'.$interval['count'],
                    'start' => $start,
                    'end' => $end,
                    'expected_count' => (int) $interval['count'],
                ];
                foreach ($weekdays as $weekday) {
                    if ($rules = $this->calcPatrolsInterval($rules)) {
                        $interval_rules->put(strtolower($weekday), array_merge($rules, [
                            'weekday' => strtolower($weekday),
                            'weekday_index' => array_search($weekday, $weekdays, true),
                        ]));
                        continue;
                    }
                    $interval_rules->put(strtolower($weekday), $rules);
                }
                break;
            default:
                throw new Exception('Undefined timetable type: '.$this->timetable['type']);
                break;
        }

        return $interval_rules;
    }

    /**
     * Рассчитываем время первого патруля и последнего
     *
     * @param  null  $rules
     *
     * @return array|null
     */
    protected function calcPatrolsInterval($rules = null): ?array
    {
        if (isset($rules)) {
            $starts = array_column($rules, 'start');
            $ends = array_column($rules, 'end');
            if (!$starts || !$ends) {
                return $rules;
            }

            return array_merge($rules, [
                'first_patrol' => count($starts) > 1 ? min(...$starts) :
                    array_shift($starts),
                'last_patrol' => count($ends) > 1 ? max(...$ends) :
                    array_shift($ends),
            ]);
        }

        return $rules;
    }

    /**
     * @return bool|string
     * @internal
     * @see sql
     */
    public function getSqlAttribute()
    {
        if ($weekly_schedule = $this->weekly_schedule) {
            $sql = 'case COALESCE(weekday(patrol_start_time), \'not_set\')';
            $sql .= " when 'not_set' then 'not set patrol_start_time'";
            foreach ($this->weekly_schedule as $weekday => $day_schedule) {
                $index = $day_schedule['weekday_index'] ?? null;
                unset($day_schedule['first_patrol'], $day_schedule['last_patrol'], $day_schedule['weekday_index'], $day_schedule['weekday']);
                if (empty($day_schedule)) {
                    $sql .= " when {$index} then 'mistake day'";
                } else {
                    $sql .= " when {$index} then case";
                    foreach ($day_schedule as $schedule) {
                        $sql .= " when timediff(time(patrol_start_time), '{$schedule['start']}') > 0 and timediff(time(patrol_start_time), '{$schedule['end']}') < 0 then if(num_of_patrols < {$schedule['expected_count']}, 'few patrols','OK')";
                    }
                    $sql .= " else 'mistake time' end";
                }
            }
            $sql .= ' else \'err not weekday\'';
            $sql .= ' end';

            return $sql;
        }

        return false;
    }

    protected function patrolTimeFormat(DateTimeInterface $dateTime): string
    {
        return $dateTime->format('H:i:s');
    }
}
