<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Facility extends Model
{
    protected $keyType = 'string';
    protected $primaryKey = 'owner_id';

    protected $casts = [
        'owner_id' => 'string',
        'timetable' => 'json'
    ];

    protected $appends = [
        'name',
        'code',
    ];

    public function getNameAttribute()
    {
        return $this->facility_name;
    }

    public function getCodeAttribute()
    {
        return $this->owner_id;
    }

    public function reports()
    {
        return $this->hasMany(Report::class, 'owner_id');
    }

    public function events()
    {
        return $this->hasMany(EventReports::class, 'facility_code');
    }

    public function eventReportsPoolWarning()
    {
        return $this->hasMany(EventReportsPool::class, 'facility_code', 'owner_id')
            ->whereIn('code', getWarningStatuses());
    }

    public function eventReportsPoolCritical()
    {
        return $this->hasMany(EventReportsPool::class, 'facility_code', 'owner_id')
            ->whereIn('code', getCriticalStatuses());
    }

    /**
     * @return Collection|null
     * @throws Exception
     * @internal
     */
    public function getWeeklyScheduleAttribute(): ?Collection
    {
        if (empty($this->timetable)) {
            return null;
        }
        $weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $interval_rules = collect();
        switch ($this->timetable['type']) {
            case 'weekly':
                foreach ($this->timetable['value'] as $weekday => $intervals) {
                    $weekday_one = ucfirst(substr($weekday, 0, -1));
                    $rules = array_reduce($intervals, function ($all, $interval) use ($weekday) {
                        [$start, $end] = $interval['interval'];
                        $all[] = [
                            'name' => $weekday.'-'.implode('-', $interval['interval']).'-'.$interval['count'],
                            'start' => $start,
                            'end' => $end,
                            'expected_count' => (int) $interval['count'],
                        ];

                        return $all;
                    }, []);
                    if ($rules = $this->calcPatrolsInterval($rules)) {
                        $interval_rules->put($weekday, array_merge($rules, [
                            'weekday' => strtolower($weekday_one),
                            'weekday_index' => array_search($weekday_one, $weekdays, true),
                        ]));
                        continue;
                    }
                    $interval_rules->put($weekday, array_merge($rules, [
                        'weekday' => strtolower($weekday_one),
                        'weekday_index' => array_search($weekday_one, $weekdays, true),
                    ]));
                }
                break;
            case 'daily':
                $rules = array_reduce($this->timetable['value'], function ($all, $interval) {
                    [$start, $end] = $interval['interval'];
                    $all[] = [
                        'name' => 'Daily-'.implode('-', $interval['interval']).'-'.$interval['count'],
                        'start' => $start,
                        'end' => $end,
                        'expected_count' => (int) $interval['count'],
                    ];

                    return $all;
                }, []);
                foreach ($weekdays as $weekday) {
                    if ($rules = $this->calcPatrolsInterval($rules)) {
                        $interval_rules->put($weekday, array_merge($rules, [
                            'weekday' => strtolower($weekday),
                            'weekday_index' => array_search($weekday, $weekdays, true),
                        ]));
                        continue;
                    }
                    $interval_rules->put($weekday, $rules);
                }
                break;
            case 'free':
                $interval = $this->timetable['value'];
                [$start, $end] = $interval['interval'];
                $rules[] = [
                    'name' => 'Free-'.implode('-', $interval['interval']).'-'.$interval['count'],
                    'start' => $start,
                    'end' => $end,
                    'expected_count' => (int) $interval['count'],
                ];
                foreach ($weekdays as $weekday) {
                    if ($rules = $this->calcPatrolsInterval($rules)) {
                        $interval_rules->put($weekday, array_merge($rules, [
                            'weekday' => strtolower($weekday),
                            'weekday_index' => array_search($weekday, $weekdays, true),
                        ]));
                        continue;
                    }
                    $interval_rules->put($weekday, $rules);
                }
                break;
            default:
                throw new Exception('Undefined timetable type: '.$this->timetable['type']);
                break;
        }

        return $interval_rules;
    }

    /**
     * Рассчитываем время первого патруля и последнего
     *
     * @param null $rules
     *
     * @return array|null
     */
    protected function calcPatrolsInterval($rules = null): ?array
    {
        if (isset($rules)) {
            $starts = array_column($rules, 'start');
            $ends = array_column($rules, 'end');
            if (!$starts || !$ends) {
                return $rules;
            }

            return array_merge($rules, [
                'first_patrol' => count($starts) > 1 ? min(...$starts) :
                    array_shift($starts),
                'last_patrol' => count($ends) > 1 ? max(...$ends) :
                    array_shift($ends),
            ]);
        }

        return $rules;
    }
}
