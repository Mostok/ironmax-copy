<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventReportsPoolUserStatus extends Model {

	protected $table = 'event_reports_pool_user_status';
}
