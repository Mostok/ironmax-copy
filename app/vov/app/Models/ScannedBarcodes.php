<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

/**
 * Class ScannedBarcodes
 *
 * @package App
 */
class ScannedBarcodes extends Model
{

    use HybridRelations;

    public $timestamps = false;
    protected $hidden = ['big_boss_group_id', 'mini_boss_group_id'];
    protected $connection = 'mysql';

    /**
     * @return HasOne|HasOne
     * @see facility
     */
    public function facility(): HasOne
    {
        return $this->hasOne(MongoFacilitiesModel::class, 'owner_id', 'owner_id');
    }
}
