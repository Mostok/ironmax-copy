<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EventReportsPool extends Model {

    protected $table      = 'event_reports_pool';
    protected $fillable   = [ 'type', 'event_data', 'status' ];
    protected $connection = 'mysql';
    protected $casts      = [
        'event_data' => 'array',
        'facility_code' => 'string',
    ];

    /**
     * @return HasMany
     */
    public function status(): HasMany {

        return $this->hasMany( EventReportsPoolUserStatus::class );
    }

    /**
     * @return belongsTo
     */
    public function facility() {

        return $this->belongsTo(Facility::class, 'facility_code');
    }
}
