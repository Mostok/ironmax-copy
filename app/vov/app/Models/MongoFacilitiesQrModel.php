<?php

namespace App\Models;

use App\Traits\HebrewFields;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Illuminate\Support\Collection;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class MongoFacilitiesQrModel extends Eloquent {

    use HebrewFields;

    public    $primaryKey = 'owner_id';
    public    $connection = 'mongodb';
    public    $collection = 'Facilities';
    protected $visible    = [
        'facility_name',
        'is_sayar'
    ];

    protected function mbStrrev(string $string, string $encoding = null) {
        $chars = mb_str_split($string, 1, $encoding ?: mb_internal_encoding());
        return implode('', array_reverse($chars));
    }

    protected function isRtl($string) {
        $rtlCharsPattern = '/[\x{0590}-\x{05ff}\x{0600}-\x{06ff}]/u';
        return preg_match($rtlCharsPattern, $string);
    }


    public function getFacilityNamePdfAttribute() {
        return $this->hebrewField($this->facility_name);
    }

}
