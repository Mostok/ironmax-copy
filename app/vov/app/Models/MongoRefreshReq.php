<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

/**
 * Class MongoRefreshReq
 */
class MongoRefreshReq extends Eloquent
{
    use SoftDeletes;

    public $connection = 'mongodb';
    public $collection = 'RefreshReq';
    public $fillable = ['is_refresh_req'];
    public $timestamps = false;

    public function facility(): HasOne
    {
        return $this->hasOne(MongoFacilitiesModel::class, 'owner_id', 'owner_id');
    }
}
