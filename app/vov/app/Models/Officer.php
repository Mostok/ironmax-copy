<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Officer extends Model {

    protected $table   = 'users';
    protected $visible = [ 'big_boss_group_id', 'type', 'owner_id', 'name', 'mini_boss_group_id', 'eventReports' ];

    public function miniBosses(): HasMany {

        return $this->hasMany( User::class, 'big_boss_group_id', 'big_boss_group_id' )
                    ->where( 'type', '=', User::TYPE_MINI_BOSS );
    }

    public function bigBosses(): HasMany {

        return $this->hasMany( User::class, 'big_boss_group_id', 'big_boss_group_id' )
                    ->where( 'type', '=', User::TYPE_BIG_BOSS );
    }

    public function newQuery() {

        return parent::newQuery()->whereIn( 'type', [
            User::TYPE_SECURITY_OFFICER,
            User::TYPE_SECURITY_INSPECTOR,
        ] );
    }

    public function eventReports(): HasMany {

        return $this->hasMany( EventReportsPool::class, 'facility_code', 'owner_id' );
    }

    /**
     * @return MongoFacilitiesModel|HasOne|HasOne
     * @see facility
     */
    public function facility() {

        return $this->hasOne( MongoFacilitiesModel::class, 'owner_id', 'owner_id' );
    }
}
