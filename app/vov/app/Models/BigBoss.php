<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BigBoss extends Model {

	protected $table = 'users';

	public function newQuery() {

		return parent::newQuery()->where( 'type', '=', User::TYPE_BIG_BOSS );
	}
}
