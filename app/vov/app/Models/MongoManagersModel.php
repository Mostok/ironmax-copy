<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Relations\HasMany;
use Jenssegers\Mongodb\Relations\HasOne;

class MongoManagersModel extends Eloquent
{

    use SoftDeletes;

    public $connection = 'mongodb';
    public $collection = 'Managers';
    protected $dates = ['deleted_at'];
    protected $hidden = [
        'email',
        'manager_id',
        'mini_boss_group_id',
        'big_boss_group_id',
        'is_send_byemail',
        'manager_name',
        'time_zone',
        'last_patrol_time',
        'company_name',
        'country',
        'language',
        'daily_reports_schedule',
        'updated_at',
        'company_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|HasMany|HasOne
     */
    public function facilities()
    {

        return $this->hasMany(MongoFacilitiesModel::class, 'manager_id', 'manager_id');
    }
}
