<?php

namespace App\Models;

use App\Notifications\IronmaxResetPassword;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Jenssegers\Mongodb\Relations\HasMany as MongoHasMany;
use Jenssegers\Mongodb\Relations\HasOne as MongoHasOne;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 *
 * @package  App
 *
 * @property HasOne|MongoFacilitiesModel $facility
 * @property HasMany|MongoHasMany|MongoFacilitiesModel $facilities
 * @property MongoManagersModel $manager
 * @property MongoManagersModel $managers
 * @property string $firstname
 * @property string $lastname
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $type
 * @property string $language
 * @property string $timezone
 * @property array $config
 *
 * @property-read HasMany $reports
 *
 * @method static Builder whereEmail
 * @method static Builder whereName
 * @method static Builder wherePassword
 * @method static Builder whereType
 * @method static Builder whereRememberToken
 */
class User extends Authenticatable
{
    use HybridRelations;
    use Notifiable;
    use HasApiTokens;

    public const TYPE_ADMIN = 'admin';
    public const TYPE_SECURITY_OFFICER = 'security_officer';
    public const TYPE_BIG_BOSS = 'big_boss';
    public const TYPE_MINI_BOSS = 'mini_boss';
    public const TYPE_SECURITY_INSPECTOR = 'security_inspector';
    protected $connection = 'mysql';
    protected $appends = ['firstname', 'lastname'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'phone',
        'password',
        'type',
        'remember_token',
        'language',
        'timezone',
        'send_out_events',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'name',
        'password',
        'remember_token',
        'email_verified_at',
        'created_at',
        'updated_at',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'config' => 'array'
    ];

    public function pdfs()
    {
        return $this->hasMany(pdfData::class);
    }

    /**
     * @return HasOne|HasOne
     * @see facility
     */
    public function facility(): HasOne
    {
        return $this->hasOne(MongoFacilitiesModel::class, 'owner_id', 'owner_id');
    }

    /**
     * @return HasMany
     * @see facilities
     */
    public function facilities(): HasMany
    {
        switch ($this->type) {
            case 'big_boss':
                return $this->hasMany(MongoFacilitiesModel::class, 'big_boss_group_id', 'big_boss_group_id');
            case 'mini_boss':
                return $this->hasMany(MongoFacilitiesModel::class, 'mini_boss_group_id', 'mini_boss_group_id');
            case 'security_officer':
            case 'security_inspector':
            default:
                return $this->hasMany(MongoFacilitiesModel::class, 'owner_id', 'owner_id');
        }
    }

    /**
     * @return HasMany
     * @see facilities
     */
    public function localFacilities(): HasMany
    {
        switch ($this->type) {
            case 'big_boss':
                return $this->hasMany(Facility::class, 'big_boss_group_id', 'big_boss_group_id');
            case 'mini_boss':
                return $this->hasMany(Facility::class, 'mini_boss_group_id', 'mini_boss_group_id');
            case 'security_officer':
            case 'security_inspector':
            default:
                return $this->hasMany(Facility::class, 'owner_id', 'owner_id');
        }
    }

    /**
     * Получаем менеджера по mini_boss_group_id
     *
     * @return HasOne|MongoHasOne
     */
    public function manager()
    {
        return $this->hasOne(MongoManagersModel::class, 'mini_boss_group_id', 'mini_boss_group_id');
    }

    /**
     * Получаем менеджеров по big_boss_group_id
     *
     * @return HasMany|MongoHasMany
     */
    public function managers()
    {
        return $this->hasMany(MongoManagersModel::class, 'big_boss_group_id', 'big_boss_group_id');
    }

    /**
     * @return HasMany|MongoHasMany
     */
    public function reports()
    {
        switch ($this->type) {
            case 'big_boss':
                return $this
                    ->hasMany(Report::class, 'big_boss_group_id', 'big_boss_group_id');
            case 'mini_boss':
                return $this
                    ->hasMany(Report::class, 'mini_boss_group_id', 'mini_boss_group_id');
            case 'security_officer':
            case 'security_inspector':
            default:
                return $this->hasMany(Report::class, 'owner_id', 'owner_id');
        }
    }

    public function barcodes()
    {
        // ScannedBarcodes
        switch ($this->type) {
            case 'big_boss':
                return $this
                    ->hasMany(ScannedBarcodes::class, 'big_boss_group_id', 'big_boss_group_id');
            case 'mini_boss':
                return $this
                    ->hasMany(ScannedBarcodes::class, 'mini_boss_group_id', 'mini_boss_group_id');
            case 'security_officer':
            case 'security_inspector':
            default:
                return $this->hasMany(ScannedBarcodes::class, 'owner_id', 'owner_id');
        }
    }

    public function bigBosses()
    {
        return $this->hasMany(BigBoss::class, 'big_boss_group_id', 'big_boss_group_id');
    }

    public function miniBosses()
    {
        return $this->hasMany(MiniBoss::class, 'big_boss_group_id', 'big_boss_group_id');
    }

    public function officers()
    {
        return $this->hasMany(Officer::class, 'big_boss_group_id', 'big_boss_group_id');
    }

    public function events_pool()
    {
        return $this->belongsToMany(EventReportsPool::class, (new EventReportsPoolUserStatus)->getTable())
            ->withTimestamps();
    }

    public function events()
    {
        switch ($this->type) {
            case 'big_boss':
                return $this->belongsToMany(EventReports::class, 'facilities', 'big_boss_group_id', 'owner_id',
                    'big_boss_group_id', 'facility_code');
            case 'mini_boss':
                return $this->belongsToMany(EventReports::class, 'facilities', 'mini_boss_group_id', 'owner_id',
                    'mini_boss_group_id', 'facility_code');
            case 'security_officer':
            case 'security_inspector':
            default:
                return $this->hasMany(EventReports::class, 'facility_code', 'owner_id');
        }
    }

    /**
     * todo[daglob] 13.04.2020 12:35: decrypt( $this->password ) === $password -- заменить в дальнейшем
     *
     * @param $password
     *
     * @return bool
     */
    public function validateForPassportPasswordGrant($password): bool
    {
        return decrypt($this->password) === $password;
    }

    /**
     * @return string
     */
    public function getFirstnameAttribute(): string
    {
        if ($this->name) {
            $name = explode(' ', $this->name, 2);

            return isset($name[0]) ? $name[0] : '';
        }

        return '';
    }

    /**
     * @param  string  $firstname
     */
    public function setFirstnameAttribute(string $firstname): void
    {
        if ($this->name) {
            $name = explode(' ', $this->name, 2);
            $name[0] = $firstname;
        }
        $this->name = implode(' ', $name);
    }

    /**
     * @return string
     */
    public function getLastnameAttribute(): string
    {
        if ($this->name) {
            $name = explode(' ', $this->name, 2);

            return count($name) > 1 ? $name[1] : '';
        }

        return '';
    }

    /**
     * @param  string  $lastname
     */
    public function setLastnameAttribute(string $lastname): void
    {
        if ($this->name) {
            $name = explode(' ', $this->name, 2);
            $name[1] = $lastname;
        }
        $this->name = implode(' ', $name);
    }

    /**
     * @param  string  $token
     */
    public function sendPasswordResetNotification($token)
    {
        $rp = new IronmaxResetPassword($token);
        $rp->name = $this->name;
        $rp->lang = $this->language ?? 'en';
        $this->notify($rp);
    }

    public function routeNotificationForMail(): string
    {
        return $this->email;
    }
}
