<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MiniBoss extends Model {

	protected $table = 'users';

	public function newQuery() {

		return parent::newQuery()->where( 'type', '=', User::TYPE_MINI_BOSS );
	}
}
