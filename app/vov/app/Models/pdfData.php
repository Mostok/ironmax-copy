<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class pdfData extends Model
{

    const AVAIL_PATROL_REPORT    = 'patrol_report';
    const AVAIL_INSPECTOR_REPORT = 'inspector_report';
    const AVAIL_DAILY_REPORT     = 'daily_report';
    const AVAILABLE_LIST         = [
        self::AVAIL_PATROL_REPORT,
        self::AVAIL_INSPECTOR_REPORT,
        self::AVAIL_DAILY_REPORT,
    ];
    protected $fillable = [
        'file',
        'type',
        'user_id',
        'report',
        'owner_id',
    ];
    protected $hidden   = [
        'category',
        'type',
    ];
    protected $table    = 'pdf_datas';

    public function user()
    {
        return $this->hasOne(User::class, 'owner_id', 'owner_id');
    }

    /**
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeUnprocessed(Builder $query): Builder
    {
        return $query->whereNotIn('type', ['daily_report', 'summary_report', 'android'])->whereNull('processed');
    }
}
