<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Date;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

/**
 * Class Report
 *
 * @param  HasOne|MongoFacilitiesModel  $facility
 *
 * @package App
 *
 * @property-read string $weekday
 * @property-read Date $entry
 * @property-read EventReports $events
 */
class Report extends Model
{
    use HybridRelations;

    protected $table = 'pdf_datas';
    protected $connection = 'mysql';
    protected $hidden = [
        'big_boss_group_id',
        'mini_boss_group_id',
        'file',
        'number_of_patrols_should_be_made',
        'updated_at',
    ];
    protected $casts = [
        'missed_points' => 'array',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id', 'owner_id');
    }

    public function facility(): HasOne
    {
        return $this->hasOne(MongoFacilitiesModel::class, 'owner_id', 'owner_id');
    }

    /**
     * @return HasMany
     */
    public function events(): hasOne
    {
        return $this->hasOne(EventReports::class, 'report_id', 'id');
    }
}
