<?php

namespace App\Models;

use Illuminate\Support\Str;
use App\Traits\HebrewFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Illuminate\Database\Eloquent\Builder;
use QrCode as QrGenerator;

class Barcode extends Model
{
    use HybridRelations;
    use HebrewFields;
    //
    protected $casts = [
        'additional' => 'JSON'
    ];

    protected $table = 'barcodes';
    protected $connection = 'mysql';
    protected $visible = [
        'id', 'name', 'location', 'facility_id', 'facility', 'additional', 'qr_string', 'start_patrol_name'
    ];

    protected $fillable = [
        'name', 'location', 'user_id', 'facility_id', 'additional', 'start_patrol_name'
    ];

    public function facility()
    {
        return $this->belongsTo(MongoFacilitiesQrModel::class,'facility_id','owner_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('user_id', function (Builder $builder) {
            $currentUserId = Auth::id();
            $builder->where('user_id', $currentUserId);
        });

    }

    public function updateQrString($isLast = false)
    {
        $hebrewPercents = [
            0 => 'אאא',
            10 => 'בבב',
            20 => 'גגג',
            30 => 'דדד',
            40 => 'ההה',
            50 => 'ווו',
            60 => 'זזז',
            70 => 'חחח',
            80 => 'טטט',
            90 => 'ייי',
            100 => 'כככ'
        ];

        $qrString['name'] = '_'.$this->name;
        $qrString['syr'] = '_';
        $qrString['open_questions'] = '';
        $qrString['predefined_questions'] = '';
        $qrString['reminder'] = '';

        if($this->facility->is_sayar){
            $qrString['syr'] = '*SYR*<'.$this->facility->facility_name.'>_';
        }

        if($this->is_last || $isLast) {
            $this->qr_string = $qrString['name'].$qrString['syr'].'*END*';
            return;
        }

        $questions = $this->additional;

        $haveOpenQuestions = isset($questions['open_questions']) && count($questions['open_questions']) > 0;
        $havePredefinedQuestions = isset($questions['predefined_questions']) && count($questions['predefined_questions']) > 0;

        if($haveOpenQuestions){
            $qs = '';
            $hints = '';
            foreach ($questions['open_questions'] as $open_question) {
                $qs .= $open_question['question'].';';
                if(isset($open_question['hint']) && $open_question['hint'] !== ''){
                    $hints .= $open_question['hint'].';';
                } else {
                    $hints .= ';';
                }
            }
            $qs = rtrim($qs, ';');
            $hints = rtrim($hints, ';');

            $delimiter = '&-&';

            if($havePredefinedQuestions) {
                $delimiter = '/';
            }

            $qrString['open_questions'] = '{Q:'.$qs . $delimiter . 'Hn:'.$hints.'}';
        }

        if($havePredefinedQuestions) {

            $questionsAll = [];
            $answersAll = [];

            foreach ($questions['predefined_questions'] as $questionIndex => $predefined_question) {

                $isHebrew = $this->isHebrew($predefined_question['question']);

                $questionsAll[] = $predefined_question['question'];

                $questionAnswers = [];

                foreach ($predefined_question['answers'] as $answerIndex => $answer) {

                    $percentVal = $isHebrew ? $hebrewPercents[round($answer['percent']/10)*10] : '('.$answer['percent'].'%)';

                    $answerText = $isHebrew ?
                        $this->makeHebrewLine($answer['answer'], $percentVal) :
                        $this->makeDefaultLine($answer['answer'], $percentVal);

                    if(!$answerIndex && !$questionIndex) {
                        if($isHebrew) $answerText = 'מחק.זה;' . $answerText;
                        else $answerText = 'delete.this;' . $answerText;
                    }

                    $questionAnswers[] = $answerText;
                }

                $answersAll[] = 'An' . ($questionIndex + 1) . ':' . implode(';', $questionAnswers);
            }

            if($this->isHebrew($questionsAll[0])) $questionsAll = array_reverse($questionsAll);

            $questionsAll = implode(';', $questionsAll);
            $answersAll = '/' . implode('/', $answersAll);

            $qrString['predefined_questions'] = $questionsAll . $answersAll;
        }

        $qrQuestions = '';

        if($haveOpenQuestions && $havePredefinedQuestions) {
            $qrQuestions = '[reg_check/Q:' . $qrString['predefined_questions'] . ']';
            $qrQuestions .= $qrString['open_questions'];
        }
        elseif($haveOpenQuestions) {
            $qrQuestions = '[reg_check/$$$' . $qrString['open_questions'] . ']';
        }
        elseif($havePredefinedQuestions) {
            $qrQuestions = '[reg_check/Q:' . $qrString['predefined_questions'] . ']';
        }

        if(isset($questions['reminder']) && $questions['reminder'] !== ''){
            $qrString['reminder'] = '[remind_check/R:'.$questions['reminder'].']';
        }

        if(isset($this->start_patrol_name) && $this->start_patrol_name == 1){
            $this->qr_string = $qrString['name'].'_[start_patrol_name]';
        } else {
            $this->qr_string = $qrString['name'] . $qrString['syr'] . $qrQuestions . $qrString['reminder'];
        }
    }

    private function isHebrew($string)
    {
        return preg_match("/\p{Hebrew}/u", $string);
    }

    private function makeDefaultLine($answer, $percent)
    {
        return $answer . $percent;
    }

    private function makeHebrewLine($answer, $percent)
    {
        return hebrev("{$answer} {$percent}");
    }

    public function checkSize() {
        return mb_strlen($this->qr_string, '8bit')<=2500;
    }

    public function updateQrImg($isCurrent = false)
    {
        if($this->checkSize()) {
            $qr = QrGenerator::format('svg')->encoding('UTF-8')->size(400)->generate($this->qr_string);
            $name = $isCurrent ? 'current' . $this->user_id : 'qr_' . $this->id;
            \Storage::disk('public')->put('/qrs/'.$name.'.svg', $qr);
        }
    }

    public function getQrCodeImgName()
    {
        return 'qr_'.$this->id.'.svg';
    }

    public function getNamePdfAttribute() {
        return $this->hebrewField($this->name);
    }

    public function getLocationPdfAttribute() {
        return $this->hebrewField($this->location);
    }

    public function getIsLastAttribute() {
        $lastBarcode = Barcode::where('facility_id', $this->facility_id)->orderBy('id', 'desc')->first();
        return $lastBarcode && $lastBarcode->id==$this->id;
    }
}
