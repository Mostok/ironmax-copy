<?php

namespace App\Listeners;

use App\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Cookie;

class UserLogin
{

    /**
     * Handle the event.
     *
     * @param Login $event
     * @return void
     */
    public function handle(Login $event)
    {
        $cookie_name = config('cookies_name_list.locale');
        if (Cookie::has($cookie_name)) {
            $cookie_value = Cookie::get($cookie_name);
            /** @var User $user */
            $user = $event->user;
            $user->language === $cookie_value ?? ($user->language = $cookie_value && $user->save());
        } else {
            Cookie::queue($cookie_name, $event->user->language ?? config('app.locale'));
        }
    }
}
