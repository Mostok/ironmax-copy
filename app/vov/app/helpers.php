<?php
if (!function_exists('checkwebp')) {
    function checkwebp($type = 0)
    {
        if (isset(getallheaders()["Accept"])) {
            $accept = explode(',', getallheaders()["Accept"]);
            if (in_array('image/webp', $accept)) {
                return $type === 0 ? ' webp ' : 'webp';
            } elseif ($type !== 0) {
                return $type;
            }
        } elseif ($type === 0) {
            return '';
        } elseif ($type !== 0) {
            return $type;
        }
    }
}
if (!function_exists('getWarningStatuses')) {
    /**
     * @return array
     */
    function getWarningStatuses()
    {
        return collect(config('patrol_events.statuses'))->except(
            'patrol_mistake_time',
            'patrol_mistake_day',
            'patrol_emergency',
            'patrol_incident',
            'patrol_success'
        )->toArray();
    }
}
if (!function_exists('getMistakeStatuses')) {
    /**
     * @return array
     */
    function getMistakeStatuses()
    {
        return [
            config('patrol_events.statuses.patrol_mistake_time'),
            config('patrol_events.statuses.patrol_mistake_day'),
            config('patrol_events.errors.patrol_wrong_start_to_end_time_ratio'),
            config('patrol_events.errors.patrol_empty_timetable'),
            config('patrol_events.errors.patrol_not_set_facility'),
            config('patrol_events.errors.patrol_not_set_patrol_start_time'),
            config('patrol_events.errors.patrol_unknown_facility'),
        ];
    }
}
if (!function_exists('getCriticalStatuses')) {
    /**
     * @return array
     */
    function getCriticalStatuses()
    {
        return [
            config('patrol_events.statuses.patrol_emergency'),
            config('patrol_events.statuses.patrol_incident'),
        ];
    }
}
if (!function_exists('getSuccessStatus')) {
    /**
     * @return array
     */
    function getSuccessStatus()
    {
        return [
            config('patrol_events.statuses.patrol_success'),
        ];
    }
}
