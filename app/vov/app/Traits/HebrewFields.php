<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 24.09.2020
 * Time: 15:07
 */
namespace App\Traits;

trait HebrewFields
{
    private function mbStrrev(string $string, string $encoding = null) {
        $chars = mb_str_split($string, 1, $encoding ?: mb_internal_encoding());
        return implode('', array_reverse($chars));
    }

    private function isRtl($string) {
        $rtlCharsPattern = '/[\x{0590}-\x{05ff}\x{0600}-\x{06ff}]/u';
        return preg_match($rtlCharsPattern, $string);
    }

    protected function hebrewField($value) {
        if($this->isRtl($value)) {
            return $this->mbStrrev($value);
        } else {
            return $value;
        }
    }

}
