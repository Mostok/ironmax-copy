<?php

namespace App\Events\Dashboard;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\EventReportsPool;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class NotificationsEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;

        $facilities = $this->user
            ->facilities()->get()->all();
        $events = [];
        foreach ($facilities as $facility) {
            $facility_events = $facility->eventReportsPool()
                ->whereIn('code', array_merge(getWarningStatuses(), getCriticalStatuses()))
                ->whereDoesntHave(
                    'status',
                    static function (Builder $query) use ($user) {
                        $query->where('user_id', $user->id);
                    }
                )
                ->get()->all();
            $events = array_merge($events, $facility_events);
        }

        usort($events, function ($item_1, $item_2) {
            return $item_1->id < $item_2->id;
        });

        $new_events = [];

        foreach ($events as $key => $event) {
            if ($key <= 14) {
                array_push($new_events, $event);
            } else {
                $this->user->events_pool()->syncWithoutDetaching($event);
            }
        }
        $this->data = $new_events;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("dashboard.{$this->user->id}");
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return ['data' => $this->data];
    }
}
