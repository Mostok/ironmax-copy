<?php

namespace App\Events\Dashboard;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MissingBarcodesEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $tz = $user->timezone ?? 'UTC';
        $this->data = $this->user->events()->lastDay($tz)
            ->whereIn('code', [config('patrol_events.statuses.patrol_barcodes')])
            ->with([
                'report' => static function (HasOne $query) {
                    $query->select('id')
                        ->addSelect(DB::raw("barcodes_should_be_scanned - barcodes_scanned as cnt"));
                }
            ])
            ->whereHas('report', function ($query) {
                $query->where('type', 'patrol_report');
            })
            ->get()->sum('report.cnt');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("dashboard.{$this->user->id}");
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return ['data' => $this->data];
    }

}
