<?php

namespace App\Events\Dashboard\Reports;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ExceptionsEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $tz = $user->timezone ?? 'UTC';
        /**
         * Делим на 2 группы:
         * Выполнено частично (баркодов не хватает: 's_bc_ls')
         * Не выполнено совсем (читер/ошибка уровня временной группы: 's_ch', 's_tb_ls', 's_tb_em')
         * - если s_ch - кол-во баркодов/ошибку не выводим в details
         */
        $partially_completed = 'partially_completed';
        $not_done_at_all = 'not_done_at_all';
        $partially_completed_statuses = collect(config('patrol_events.statuses'))
            ->filter(function ($_, $status_name) {
                return $status_name === 'patrol_barcodes';
            })->values()->add($partially_completed)->toArray();
        $not_done_at_all_statuses = collect(config('patrol_events.statuses'))
            ->filter(function ($_, $status_name) {
                return in_array($status_name, [
                    'patrol_cheat',
                    'time_block_less',
                    'time_block_empty',
                ], true);
            })->values()->add($not_done_at_all)->toArray();
        $event_reports = $user
            ->events()
            ->with(['reports', 'facility', 'report'])
            ->whereIn('event_reports.code', getWarningStatuses())
            ->addSelect('event_reports.*')
            ->selectRaw('JSON_ARRAYAGG(event_reports.code) AS codes')
            ->selectRaw('if(event_reports.code in ('.
                implode(', ', array_slice(array_fill(0, count($partially_completed_statuses), '?'), 1)).
                '), ?, if(event_reports.code in ('.
                implode(', ', array_slice(array_fill(0, count($not_done_at_all_statuses), '?'), 1)).
                '), ?, null)) as type', array_merge($partially_completed_statuses, $not_done_at_all_statuses))
            ->addSelect(
                DB::raw("CONVERT_TZ(event_reports.utc_time_point, 'UTC', (SELECT time_zone FROM facilities WHERE facilities.owner_id = event_reports.facility_code)) AS sort_date")
            );
        $from = Carbon::now($tz)->subMonths(2);
        $to = Carbon::now($tz);
        $event_reports->whereBetween('event_reports.utc_time_point', [$from->setTimezone('UTC'), $to->setTimezone('UTC')]);
        $event_reports->orderByDesc('sort_date');
        $event_reports->groupByRaw('event_reports.report_id, event_reports.facility_code, event_reports.date, event_reports.time_group');
        $this->data = $event_reports->get();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("dashboard.{$this->user->id}");
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return ['data' => $this->data];
    }
}
