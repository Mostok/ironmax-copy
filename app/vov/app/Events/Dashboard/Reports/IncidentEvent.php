<?php

namespace App\Events\Dashboard\Reports;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class IncidentEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $tz = $user->timezone ?? 'UTC';

        $from = Carbon::now($tz)->subMonths(2);
        $to = Carbon::now($tz);

        $this->data = $this->user->events()->with('facility', 'report')
            ->whereHas('report', function ($query) {
                $query->where('type', 'incident_report');
            })
            ->orderByDesc('event_reports.created_at')
            ->whereNotIn('code', getMistakeStatuses())
            ->whereBetween('event_reports.created_at', [$from->setTimezone('UTC'), $to->setTimezone('UTC')])
            ->get();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("dashboard.{$this->user->id}");
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return ['data' => $this->data];
    }
}
