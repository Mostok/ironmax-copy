<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class BarcodeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $words = ['.','---','_','[',']','{','}','*SYR*','*syr*', '$$$', '&-&', '*END*', '*end*', ';'];

        return [
            'name' => ['required', Rule::notIn($words)]
        ];
    }

//    public function failedValidation(Validator $validator)
//    {
//        throw new \Exception($validator->errors(), 422);
//    }
}
