<?php

namespace App\Http\Controllers\Api;

use App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\BarcodeRequest;
use App\Models\Barcode;
use Illuminate\Http\Request;
use PDF;
use App\Http\Controllers\Controller;
use App\Models\MongoFacilitiesModel;

class BarcodeController extends Controller
{
    private function prepare_index(Request $request)
    {
        $qrs = Barcode::with('facility');
        $facility_id = $request->get('facility_id');
        if ($facility_id) {
            $qrs->where('facility_id', $facility_id);
            sleep(2);
        }
        return $qrs;
    }

    public function index(Request $request)
    {

        $qrs = $this->prepare_index($request);
        return $qrs->get();
    }

    public function indexPrint(Request $request, $lang = 'en', $download = null)
    {
        App::setLocale($lang);
        $barcodes = $this->prepare_index($request);
        if ($request->get('page') == 'all') {
            $barcodes = $barcodes->get();
        } else {
            $barcodes = $barcodes->paginate(16);
        }

        $direction = $request->get('direction', 'ltr');
        if ($download && $download === 'download') {
            $facility_name = MongoFacilitiesModel::whereOwnerId($request->get('facility_id'))->first()->facility_name;
            $viewName = $direction == 'rtl' ? 'barcodes.pdf_rtl' : 'barcodes.pdf';
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'chroot'  => base_path(),])
                ->loadView($viewName, ['barcodes' => $barcodes, 'direction' => $direction]);
            return $pdf->download($facility_name . '.pdf');
            //            return view('barcodes.pdf2', ['barcodes' => $barcodes, 'direction' => $direction]);
        } else if (!$download) {
            return view('barcodes.print', ['barcodes' => $barcodes, 'direction' => $direction]);
        } else {
            return redirect('/');
        }
    }

    public function store(BarcodeRequest $request)
    {
        return DB::transaction(function () use ($request) {

            $input = $request->post();
            $input['user_id'] = Auth::id();

            $barcode = Barcode::query()->create($input);

            return response()->json([
                'message' => 'Success',
                'id' => $barcode->id
            ]);
        });
    }

    public function updateCurrent(Request $request)
    {
        if ($request['name'] == null) $request['name'] = '123';

        $barcode =  new Barcode($request->post());
        $barcode->user_id = Auth::id();
        $barcode->updateQrString($request->post('is_last'));
        $barcode->updateQrImg(true);
        if ($barcode) {
            if ($barcode->checkSize()) {
                return response()->json(['message' => 'Success']);
            } else {
                return response()->json(['message' => 'Error'], 413);
            }
        }
        return response()->json(['message' => 'Error'], 400);
    }

    public function update(BarcodeRequest $request, Barcode $barcode)
    {

        if ($barcode) {

            $barcode->fill($request->post());
            $barcode->updateQrString();
            $barcode->updateQrImg();
            $barcode->save();
            if ($barcode) {
                return response()->json(['message' => 'Success']);
            }
        }
        return response()->json(['message' => 'Error'], 200);
    }

    public function delete($id)
    {
        //$user_id = \Auth::id();
        $barcode = Barcode::where('id', $id);
        if ($barcode) {
            $barcode->delete();
            return response()->json(['message' => 'Success', 'id' => $id]);
        }
        return response()->json(['message' => 'Error'], 400);
    }

    public function show($id)
    {
        $barcode = Barcode::where('id', $id)->with('facility')->first();
        if ($barcode) {
            return $barcode;
        } else {
            return response()->json(['message' => 'Error'], 400);
        }
    }

    public function makeBarcodes()
    {
        /**
         * @var $barcode Barcode
         */
        $barcodes = factory(Barcode::class, 100)->create();
        foreach ($barcodes as $barcode) {
            $barcode->updateQrString();
            $barcode->updateQrImg();
            $barcode->save();
        }
        return ['message' => '100 barcodes was added'];
    }
}
