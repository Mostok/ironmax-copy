<?php

namespace App\Http\Controllers\Api;

use App\Models\MongoFacilitiesModel;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MongoRefreshReq;

class FacilitiesController extends Controller
{
    public function index(Request $request)
    {
        return auth()->user()->facilities;
    }

    public function barcodes(Request $request)
    {
        return auth()->user()->facilities()->where('is_on_barcode_mode', '=', true)->get();
    }

    public function show(Request $request, $code)
    {
        $facility = auth()->user()->facilities()->where('owner_id', '=', $code)
            ->first();

        return $facility ? $facility->timetable : abort(404, 'not found');
    }

    public function save(Request $request, $code)
    {
        /** @var User $user */
        $user = auth()->user();
        /** @var MongoFacilitiesModel $facility */
        $facility = $user->facilities()->where('owner_id', '=', $code)
                ->first() ?? abort(404, 'not found facilities');
        $manager = $facility->manager ?? abort(404, 'not found manager');

        $facility->timetable = $request->post();

        if ($facility->save()) {
            $daily_reports_schedule = [];
            if ($manager->facilities()->count()) {
                foreach ($manager->facilities as $f) {
                    foreach ($f->weekly_schedule as $schedule) {
                        if (isset($schedule['first_patrol'])) {
                            if (isset($daily_reports_schedule[$schedule['weekday']])) {
                                if ($daily_reports_schedule[$schedule['weekday']]['first_patrol'] > $schedule['first_patrol']) {
                                    $daily_reports_schedule[$schedule['weekday']]['first_patrol'] = $schedule['first_patrol'];
                                }
                                if ($daily_reports_schedule[$schedule['weekday']]['last_patrol'] < $schedule['last_patrol']) {
                                    $daily_reports_schedule[$schedule['weekday']]['last_patrol'] = $schedule['last_patrol'];
                                }
                            } else {
                                $daily_reports_schedule[$schedule['weekday']] = [
                                    'first_patrol' => $schedule['first_patrol'],
                                    'last_patrol' => $schedule['last_patrol'],
                                ];
                            }
                        }
                    }
                }
                $manager->daily_reports_schedule = $daily_reports_schedule;
                $manager->save();
            }
        }

        return ['complete' => 2];
    }

    public function saveComplex(Request $request)
    {
        return ['status' => auth()->user()->facilities()->update(['timetable' => $request->post()])];
    }

    public function savePoints(Request $request, $code)
    {
        $list = $request->post('list', []);

        if($refrsh_req = MongoRefreshReq::where('owner_id', $code)->first()) {
            $refrsh_req->update([
                'is_refresh_req' => true
            ]);
        }

        return [
            'status' => auth()->user()->facilities()->find($code)->update([
                'barcodes_num' => count($list),
                'facility_barcodes_list' => $list
            ])
        ];
    }
}
