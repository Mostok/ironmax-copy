<?php

namespace App\Http\Controllers\Api;

use App\Models\EventReportsPool;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class EventsController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();

        $facilities = $user
            ->facilities()->get()->all();
        $events = [];
        foreach ($facilities as $facility) {
            $facility_events = $facility->eventReportsPool()
                ->whereIn('code', array_merge(getWarningStatuses(), getCriticalStatuses()))
                ->whereDoesntHave(
                    'status',
                    static function (Builder $query) use ($user) {
                        $query->where('user_id', $user->id);
                    }
                )
                ->get()->all();
            $events = array_merge($events, $facility_events);
        }

        usort($events, function ($item_1, $item_2) {
            return $item_1->id < $item_2->id;
        });

        $new_events = [];

        foreach ($events as $key => $event) {
            if ($key <= 14) {
                array_push($new_events, $event);
            } else {
                $user->events_pool()->syncWithoutDetaching($event);
            }
        }
        return $new_events;
    }

    public function read(Request $request)
    {
        try {
            $event = EventReportsPool::find($request->post('event_id'));
            $request->user()->events_pool()->syncWithoutDetaching($event);

            $status = true;
        } catch (\Throwable $th) {
            $status = $th->getMessage();
        };

        return ['status' => $status];
    }

    public function read_all(Request $request)
    {
        try {
            $user = $request->user();
            $facilities = $user->facilities()
                ->with(['eventReportsPool' => static function($query) use ($user) {
                    $query->whereDoesntHave( 'status', static function( Builder $query ) use ( $user ) {
                        $query->whereIn( 'user_id', [ $user->id ] );
                    });
                },
            ])->get();
            foreach ($facilities as $facility) {
                if ($facility->eventReportsPool->count()) {
                    foreach ($facility->eventReportsPool as $event) {
                        $user->events_pool()->syncWithoutDetaching($event);
                    }
                }
            }

            $status = true;
        } catch (\Throwable $th) {
            $status = $th->getMessage();
        };

        return ['status' => $status];

    }
}
