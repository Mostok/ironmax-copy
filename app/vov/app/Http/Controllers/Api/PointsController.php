<?php
namespace App\Http\Controllers\Api;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

/**
 * Class PointsController
 *
 * @package App\Http\Controllers
 */
class PointsController extends Controller
{
    /**
     * @param  Request  $request
     *
     * @return Builder[]|Collection
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        return $user->barcodes()->with([
            'facility' => function ($query) {
                $query->select(['name', 'code', 'facility_name', 'owner_id']);
            },
        ])
            ->where('scanned_time', '>=',
                now()->timezone($user->timezone)->addHours(-24)->format('Y-m-d H:i:s')
            )->get(['location', 'scanned_time', 'owner_id'])->makeHidden(['owner_id']);
    }
}
