<?php
namespace App\Http\Controllers\Api;

use App;
use App\Models\pdfData;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class PdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function downlaod($id)
    {
        $file = PdfData::findOrFail($id);
        if ($file->report) {
            return response($file->report)
                ->header('Cache-Control', 'no-cache private')
                ->header('Content-Description', 'File Transfer')
                ->header('Content-Type', 'application / octet-stream')
                ->header('Content-length', strlen($file->report))
                ->header('Content-Disposition', 'attachment; filename='.$file->file);
        } elseif ($file->drive_file_id) {
            $file_drive = Storage::disk('google')->get($file->drive_file_id);

            return response($file_drive)
                ->header('ContentType', 'application / octet-stream')
                ->header('Content-Disposition', "attachment; filename=report.pdf");
        } else {
            return "<b><h1 style='text-align: center;margin-top: 250px;'>FILE NOT AVAILABLE</h1></b>";
        }
    }

    public function search(Request $request, $lang = 'en')
    {
        App::setLocale($lang);
        if ($request->date) {
            $date = $request->date;
            if (!App::isLocale('en')) {
                $date_chunks = explode('/', $date);
                $date_check = $date_chunks[1].'/'.$date_chunks[0].'/'.$date_chunks[2];
            } else {
                $date_check = $date;
            }
        } else {
            $date = date('m/d/Y');
            $date_check = $date;
            if (!App::isLocale('en')) {
                $date_chunks = explode('/', $date);
                $date = $date_chunks[1].'/'.$date_chunks[0].'/'.$date_chunks[2];
            }
        }
        $dateP = Carbon::parse($date_check)->format('Y-m-d H:i:s');
        $type = $request->category ?: 'Daily patrol report';
        $facility = $request->facility ?: (Auth::user()->type == 'security_officer' ? Auth::user()->owner_id : 'all');
        if (in_array($type, ['Incident report', 'Petrol report'])) {
            $category = $type == 'Incident report' ? 'android' : 'patrol_report';
        } elseif ($type == 'Daily patrol report') {
            $category = 'daily_report';
        }
        $owner_ids = [Auth::user()->owner_id];
        if (Auth::user()->type == "big_boss") {
            $files_all = PdfData::where('big_boss_group_id', Auth::user()->big_boss_group_id)
                ->get();
        } elseif (Auth::user()->type == "mini_boss") {
            $files_all = PdfData::where('mini_boss_group_id', Auth::user()->mini_boss_group_id)
                ->get();
        } else {
            $files_all = PdfData::where('owner_id', Auth::user()->owner_id)
                ->get();
        }
        if ($facility != 'all') {
            $all_files = PdfData::whereDate('created_at', '=', $dateP)
                ->where([['type', $category], ['owner_id', $facility]])
                ->where('drive_file_id', '!=', '')
                ->get();
        } else {
            if (Auth::user()->type == "big_boss") {
                $all_files = PdfData::whereDate('created_at', '=', $dateP)
                    ->where('type', $category)
                    ->where('big_boss_group_id', Auth::user()->big_boss_group_id)
                    ->where('drive_file_id', '!=', '')
                    ->get();
            } elseif (Auth::user()->type == "mini_boss") {
                $all_files = PdfData::whereDate('created_at', '=', $dateP)
                    ->where('type', $category)
                    ->where('mini_boss_group_id', Auth::user()->mini_boss_group_id)
                    ->where('drive_file_id', '!=', '')
                    ->get();
            }
        }
        $files = [];
        $facilities = User::whereIn('owner_id', $files_all->pluck('owner_id')->toArray())->get();
        foreach ($facilities as $f) {
            $files[] = ['name' => $f->name, 'files' => $all_files->where('owner_id', $f->owner_id)];
        }

        return view('user.index', compact('files', 'date', 'facility', 'type', 'facilities'));
    }

    public function addNewPdf()
    {
        return view('admin.pdf.add_pdf');
    }

    public function index()
    {
        $pdfs = pdfData::orderBy('created_at', 'desc')->paginate(10);

        return view('admin.pdf.pdfs', compact('pdfs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'pdf' => 'required|mimes:pdf',
        ]);
        //return $request->all();
        if ($request->hasFile('pdf')) {
            // $fileNameWithExt = $request->file('pdf')->getClientOrignalImage();
            $fileNameWithExt = Input::file('pdf')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            //$extention = Input::file('pdf')->getClientOrignalExtention();
            $extention = File::extension($fileName);
            $fileNameToStore = $fileName.'_'.time().'.'.$extention;
            $path = $request->file('pdf')->store('pdfs');
            $pdf = pdfData::create([
                'file' => $path,
                'type' => 'android',
                'user_id' => Auth::user()->id,
            ]);
            if ($pdf) {
                return redirect('/pdf')->with('add_success', 'File add successfully ');
            }
        } else {
            $fileNameToStore = 'nothing.pdf';
        }
        //  User::create([
        //     'name' => $request['name'],
        //     'email' => $request['email'],
        //     'password' => Hash::make($request['password']),
        //     'type' => $request['type'],
        //     'bio' => $request['bio'],
        //     'photo' => $request['photo'],
        // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        pdfData::find($id)->delete();

        return redirect('/pdf')->with('del_success', 'File deleted successfully ');
    }

    public function deleteOldPdfs()
    {
        // $timeLimit = date('Y-m-d H:i:s', strtotime('-2 weeks'));
        // pdfData::where('created_at', '<=', $timeLimit)->delete();
        $timeLimit = date('Y-m-d H:i:s', strtotime('-2 weeks'));
        $pdfs = pdfData::where([['created_at', '<=', $timeLimit], ['type', '=', 'android']])
            ->orWhere([['created_at', '<=', $timeLimit], ['type', '=', 'java']])
            ->orWhere([['created_at', '<=', $timeLimit], ['type', '=', 'emergency']])->get();
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
        foreach ($pdfs as $pdf) {
            if ($pdf['driveFileId'] !== '') {
                Storage::disk('google')->delete($pdf['driveFileId']);
            }
        }
        $pdfs = pdfData::where([['created_at', '<=', $timeLimit], ['type', '=', 'android']])
            ->orWhere([['created_at', '<=', $timeLimit], ['type', '=', 'java']])
            ->orWhere([['created_at', '<=', $timeLimit], ['type', '=', 'emergency']])->delete();
        //shifter files
        $timeLimit = date('Y-m-d H:i:s', strtotime('-2 months'));
        $pdfs_shifter = pdfData::where([['created_at', '<=', $timeLimit], ['type', '=', 'shifter']])->get();
        foreach ($pdfs_shifter as $pdf) {
            if ($pdf['driveFileId'] !== '') {
                Storage::disk('google')->delete($pdf['driveFileId']);
            }
        }
        $pdfs_shifter = pdfData::where([['created_at', '<=', $timeLimit], ['type', '=', 'shifter']])->delete();

        return redirect('/pdf')->with('del_success', 'Files deleted successfully!');
    }
}
