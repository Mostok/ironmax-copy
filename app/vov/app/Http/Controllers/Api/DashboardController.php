<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

/**
 * Class DashboardController
 *
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    public function allEvents(Request $request) {
        $user = $request->user();
        broadcast(new \App\Events\Dashboard\MissingPatrolsEvent($user));
        broadcast(new \App\Events\Dashboard\EmergencyEventsEvent($user));
        broadcast(new \App\Events\Dashboard\IncidentReportsEvent($user));
        broadcast(new \App\Events\Dashboard\MissingBarcodesEvent($user));
        broadcast(new \App\Events\Dashboard\MissingInspectorBarcodesEvent($user));
        broadcast(new \App\Events\Dashboard\PointsEvent($user));
        broadcast(new \App\Events\Dashboard\ProfileEvent($user));
        broadcast(new \App\Events\Dashboard\NotificationsEvent($user));
        broadcast(new \App\Events\Dashboard\Reports\PatrolsEvent($user));
        broadcast(new \App\Events\Dashboard\Reports\ExceptionsEvent($user));
        broadcast(new \App\Events\Dashboard\Reports\EmergencyEvent($user));
        broadcast(new \App\Events\Dashboard\Reports\IncidentEvent($user));
        broadcast(new \App\Events\Dashboard\Reports\InspectorEvent($user));
    }

    public function missing_patrols()
    {
        /** @var User $user */
        $user = auth()->user();
        return $user->events()->lastDay($user->timezone ?? 'UTC')->whereIn('code', getWarningStatuses())->count();
    }

    public function emergency_events()
    {
        /** @var User $user */
        $user = auth()->user();
        return $user->events()
            ->lastDay($user->timezone ?? 'UTC')
            ->where('code', 's_em')->count();
    }

    public function missing_barcodes()
    {
        /** @var User $user */
        $user = auth()->user();
        return $user->events()->lastDay($user->timezone ?? 'UTC')
            ->whereIn('code', [config('patrol_events.statuses.patrol_barcodes')])
            ->with([
                'report' => static function (HasOne $query) {
                    $query->select('id')
                        ->addSelect(DB::raw("barcodes_should_be_scanned - barcodes_scanned as cnt"));
                }
            ])
            ->whereHas('report', function ($query) {
                $query->where('type', 'patrol_report');
            })
            ->get()->sum('report.cnt');
    }

    public function missing_inspector_barcodes()
    {
        /** @var User $user */
        $user = auth()->user();
        return $user->events()->lastDay($user->timezone ?? 'UTC')
        ->whereIn('code', [config('patrol_events.statuses.patrol_barcodes')])
        ->with([
            'report' => static function (HasOne $query) {
                $query->select('id')
                    ->addSelect(DB::raw("barcodes_should_be_scanned - barcodes_scanned as cnt"));
            }
        ])
        ->whereHas('report', function ($query) {
            $query->where('type', 'inspector_report');
        })
        ->get()->sum('report.cnt');
    }

    public function incident_reports()
    {
        /** @var User $user */
        $user = auth()->user();
        return $user->events()->lastDay($user->timezone ?? 'UTC')->whereIn('code', [config('patrol_events.statuses.patrol_incident')])->count();
    }

    public function inspector()
    {
        /** @var User $user */
        $user = auth()->user();

        $tz = $user->timezone ?? 'UTC';

        $from = Carbon::now($tz)->subMonths(2);
        $to = Carbon::now($tz);
        return $user->events()->with('facility', 'report')
            ->whereHas('report', function ($query) {
                $query->where('type', 'inspector_report');
            })
            ->whereNotIn('code', getMistakeStatuses())
            ->orderByDesc('event_reports.utc_time_point')
            ->whereBetween('event_reports.utc_time_point', [$from->setTimezone('UTC'), $to->setTimezone('UTC')])
            ->get();
    }

    public function emergency()
    {
        /** @var User $user */
        $user = auth()->user();
        $tz = $user->timezone ?? 'UTC';
        $from = Carbon::now($tz)->subMonths(2);
        $to = Carbon::now($tz);

        return $user->events()->with('facility', 'report')
            ->whereHas('report', function ($query) {
                $query->where('type', 'emergency_report');
            })
            ->orderByDesc('event_reports.created_at')
            ->whereNotIn('code', getMistakeStatuses())
            ->whereBetween('event_reports.created_at', [$from->setTimezone('UTC'), $to->setTimezone('UTC')])
            ->get();
    }

    public function incident()
    {
        /** @var User $user */
        $user = auth()->user();
        $tz = $user->timezone ?? 'UTC';
        $from = Carbon::now($tz)->subMonths(2);
        $to = Carbon::now($tz);

        return $user->events()->with('facility', 'report')
            ->whereHas('report', function ($query) {
                $query->where('type', 'incident_report');
            })
            ->orderByDesc('event_reports.created_at')
            ->whereNotIn('code', getMistakeStatuses())
            ->whereBetween('event_reports.created_at', [$from->setTimezone('UTC'), $to->setTimezone('UTC')])
            ->get();
    }

    public function patrol()
    {
        /** @var User $user */
        $user = auth()->user();
        $tz = $user->timezone ?? 'UTC';
        $from = Carbon::now($tz)->subMonths(2);
        $to = Carbon::now($tz);

        return $user->events()->with('facility', 'report')
            ->whereHas('report', function ($query) {
                $query->where('type', 'patrol_report');
            })
            ->whereNotIn('code', getMistakeStatuses())
            ->orderByDesc('event_reports.utc_time_point')
            ->whereBetween('event_reports.utc_time_point', [$from->setTimezone('UTC'), $to->setTimezone('UTC')])
            ->get();
    }

    public function exceptions()
    {
        /** @var User $user */
        $user = auth()->user();
        $tz = $user->timezone ?? 'UTC';
        /**
         * Делим на 2 группы:
         * Выполнено частично (баркодов не хватает: 's_bc_ls')
         * Не выполнено совсем (читер/ошибка уровня временной группы: 's_ch', 's_tb_ls', 's_tb_em')
         * - если s_ch - кол-во баркодов/ошибку не выводим в details
         */
        $partially_completed = 'partially_completed';
        $not_done_at_all = 'not_done_at_all';
        $partially_completed_statuses = collect(config('patrol_events.statuses'))
        ->filter(function ($_, $status_name) {
            return $status_name === 'patrol_barcodes';
        })->values()->add($partially_completed)->toArray();
        $not_done_at_all_statuses = collect(config('patrol_events.statuses'))
        ->filter(function ($_, $status_name) {
            return in_array($status_name, [
                'patrol_cheat',
                'time_block_less',
                'time_block_empty',
            ], true);
        })->values()->add($not_done_at_all)->toArray();
        $event_reports = $user
            ->events()
            ->with(['reports', 'facility', 'report'])
            ->whereIn('event_reports.code', getWarningStatuses())
            ->addSelect('event_reports.*')
            ->selectRaw('JSON_ARRAYAGG(event_reports.code) AS codes')
            ->selectRaw('if(event_reports.code in (' .
            implode(', ', array_slice(array_fill(0, count($partially_completed_statuses), '?'), 1)) .
            '), ?, if(event_reports.code in (' .
            implode(', ', array_slice(array_fill(0, count($not_done_at_all_statuses), '?'), 1)) .
            '), ?, null)) as type', array_merge($partially_completed_statuses, $not_done_at_all_statuses))
            ->addSelect(
                DB::raw("CONVERT_TZ(event_reports.utc_time_point, 'UTC', (SELECT time_zone FROM facilities WHERE facilities.owner_id = event_reports.facility_code)) AS sort_date")
            );
        $from = Carbon::now($tz)->subMonths(2);
        $to = Carbon::now($tz);
        $event_reports->whereBetween('event_reports.utc_time_point', [$from->setTimezone('UTC'), $to->setTimezone('UTC')]);
        $event_reports->orderByDesc('sort_date');
        $event_reports->groupByRaw('event_reports.report_id, event_reports.facility_code, event_reports.date, event_reports.time_group');

        return $event_reports->get();
    }
}
