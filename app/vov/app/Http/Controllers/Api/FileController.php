<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    public function index(Request $request, $code = null)
    {
        $file_drive = Storage::disk('google')->get($code);

        return response($file_drive)
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-Disposition', 'attachment; filename=report.pdf');
    }
}
