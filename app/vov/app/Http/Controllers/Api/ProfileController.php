<?php
namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function update(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        $user->email = $request->post('email');
        $user->phone = $request->post('phone');
        $user->firstname = $request->post('firstname');
        $user->lastname = $request->post('lastname');
        $user->language = $request->post('language');
        $user->send_out_events = $request->post('send_out_events');
        $user->timezone = $request->post('timezone');

        broadcast(new \App\Events\Dashboard\ProfileEvent($user));

        return ['status' => $user->save()];
    }

    public function load(Request $request)
    {
        return $request->user();
    }

    public function lang(Request $request)
    {
        $lang = $request->post('lang');
        /** @var User $user */
        $user = auth()->user();
        $user->language = $lang;
        cookie()->queue(config('cookies_name_list.locale'), $lang);

        return ['status' => $user->save()];
    }

    public function changePassword(Request $request)
    {
        $user = $request->user();
        $cur_pass = $request->post('cur_pass');
        if (decrypt($user->password) !== $cur_pass) {
            return ['status' => 'encored_pass'];
        }
        $user->password = encrypt($request->post('new_pass'));

        broadcast(new \App\Events\Dashboard\ProfileEvent($user));

        return ['status' => $user->save()];
    }

    public function getConfig(Request $request)
    {
        return $request->user()->config;
    }

    public function setConfig(Request $request)
    {
        $user = $request->user();
        $conf = $user->config;
        foreach ($request->post() as $key => $value) {
            Arr::set($conf, $key, $value);
        }
        $user->config = $conf;

        return ["status" => $user->save()];
    }
}
