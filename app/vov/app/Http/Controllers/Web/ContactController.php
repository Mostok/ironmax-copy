<?php
namespace App\Http\Controllers\Web;

use App\Models\Contact;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use App\Http\Controllers\Controller;


class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $messages = Contact::paginate(10);

        return view('admin.messages')->with('messages', $messages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return JsonResponse|Response|null
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|email|string|max:191|',
            'subject' => 'required|string',
            'message' => 'required|string',
            'g-recaptcha-response' => 'required',
        ]);
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = [
            'secret' => '6LeITcUUAAAAAHRbjYVVbPGjn1VACsabChItLVSr',
            'response' => $request->input('g-recaptcha-response'),
        ];
        // use key 'http' even if you send the request to https://...
        $options = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data),
            ],
        ];
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === false) {
            return response('Recapture check failed', 500);
        }
        $result = json_decode($result, true);
        if ($result['success']) {
            Contact::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'subject' => $request['subject'],
                'message' => $request['message'],
            ]);
            $data = [
                'from' => $request['email'],
                'name' => $request['name'],
                'body' => $request['message'],
                'subject' => $request['subject'],
            ];
            Mail::send('emails.mail', $data, function (Message $message) use ($request) {
                $message->to('support@ironmax.tech')
                    ->subject($request['subject'])
                    ->from($request['email'], $request['name']);
            });

            return response()->json(['ok' => 'ok']);
        }

        return response('Wrong recapture', 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return void
     */
    public function destroy($id)
    {
        $msg = Contact::findOrFail($id);
        if ($msg->delete()) {
            redirect('/contact')->with('res', 'Message Deleted');
        }
    }
}
