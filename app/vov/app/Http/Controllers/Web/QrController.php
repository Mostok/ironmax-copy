<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;

class QrController extends Controller
{
    //
    public function index($id)
    {
        $id = intval($id);
        try{
            $contents = Storage::disk('public')->get('/qrs/qr_'.$id.'.svg');
        } catch (Exception $e){
            return response()->json(['message'=>'Error'], 400);
        }
        return response($contents)->header('Content-type','image/svg+xml');
    }

    public function current()
    {
        try{
            $contents = Storage::disk('public')->get('/qrs/current' . auth()->user()->id . '.svg');
        } catch (Exception $e){
            return response()->json(['message'=>'Error'], 400);
        }
        return response($contents)->header('Content-type','image/svg+xml');
    }
}
