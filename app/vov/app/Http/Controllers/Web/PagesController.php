<?php
namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function index($lang = null)
    {
        $app_locale = app()->getLocale();
        if ($app_locale == "ru") {
            $alt1 = "Приложение по мониторингу патрулей";
        } elseif ($app_locale == "he") {
            $alt1 = "אפליקציה לסיורי אבטחה";
        } else {
            $alt1 = "Patrols monitoring app";
        }
        if ($app_locale == "ru") {
            $alt5 = "Умная сигнализация";
        } elseif ($app_locale == "he") {
            $alt5 = "מערכת אזעקה חכמה";
        } else {
            $alt5 = "Smart alarm system";
        }
        if ($app_locale == "ru") {
            $alt2 = "Умная кнопка экстренного вызова";
        } elseif ($app_locale == "he") {
            $alt2 = "לחצן מצוקה חכם";
        } else {
            $alt2 = "Smart emergency call button";
        }
        if ($app_locale == "ru") {
            $alt6 = "Программное обеспечение для мониторинга рабочего времени";
        } elseif ($app_locale == "he") {
            $alt6 = "תוכנת שעון נוכחות";
        } else {
            $alt6 = "Working time monitoring software";
        }
        if ($app_locale == "ru") {
            $alt43 = "Программное обеспечение для мониторинга входа посетителей";
        } elseif ($app_locale == "he") {
            $alt43 = "תוכנה לבקרת כניסת מבקרים";
        } else {
            $alt43 = "Software for Monitoring Visitors";
        }

        return view('pages.home', [
            'slides' => [
                [
                    'title' => 'slider.title_1', 'subtitle' => 'slider.subtitle_1', 'image' => $app_locale.'/1.svg',
                    'alt' => $alt1
                ],
                [
                    'title' => 'slider.title_2', 'subtitle' => 'slider.subtitle_2', 'image' => $app_locale.'/2.svg',
                    'alt' => $alt2
                ],
                [
                    'title' => 'slider.title_3', 'subtitle' => 'slider.subtitle_3', 'image' => $app_locale.'/3.svg',
                    'alt' => $alt43
                ],
                [
                    'title' => 'slider.title_4', 'subtitle' => 'slider.subtitle_4', 'image' => $app_locale.'/4.svg',
                    'alt' => $alt43
                ],
                [
                    'title' => 'slider.title_5', 'subtitle' => 'slider.subtitle_5', 'image' => $app_locale.'/5.svg',
                    'alt' => $alt5
                ],
                [
                    'title' => 'slider.title_6', 'subtitle' => 'slider.subtitle_6', 'image' => $app_locale.'/6.svg',
                    'alt' => $alt6
                ],
            ],
            'lang' => $lang,
        ]);
    }

    public function product_static_page($product_static_page, $lang = null)
    {
        return view('pages.'.$product_static_page, ['lang' => $lang]);
    }

    public function product_privacy_page($product_privacy_page, $lang = null)
    {
        $params = ['type' => '', 'title' => ''];
        switch ($product_privacy_page) {
            case 'patrol':
                $params = ['type' => '_patrol', 'title' => 'Privacy Policy Patrol'];
                break;
            case 'sos':
                $params = ['type' => '_sos', 'title' => 'Privacy Policy SOS'];
                break;
            case 'miniguard':
                $params = ['type' => '_miniguard', 'title' => 'Privacy Policy Miniguard'];
                break;
            case 'shifter':
                $params = ['type' => '_shifter', 'title' => 'Privacy Policy Shifter'];
                break;
        }
        $params['lang'] = $lang;

        return view('pages.privacy_policy', $params);
    }

    public function privacy_policy($lang = null)
    {
        return view('pages.privacy_policy', ['type' => '', 'title' => 'Privacy Policy', 'lang' => $lang]);
    }

    public function termofuse($lang = null)
    {
        return view('pages.privacy_policy', ['type' => 'tos', 'title' => 'Terms of Use', 'lang' => $lang]);
    }
}
