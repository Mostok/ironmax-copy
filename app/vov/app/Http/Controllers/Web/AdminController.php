<?php
namespace App\Http\Controllers\Web;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Show the application dashboard. (OLD)
     *
     * @param  Request  $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        return view('admin.index', [
            'token' => $request->user()->remember_token,
            'logo' => '',
        ]);
    }

    /**
     * Show the application dashboard. (NEW)
     *
     * @param  Request  $request
     * @return Application|Factory|View
     */
    public function dashboard(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $logo = null;
        if ($facility = $user->facilities()->first()) {
            $logo = "{$facility->company_id}/logo.png";
        }
        app()->setLocale($user->language);

        return view('dashboard', [
            'token' => $user->remember_token,
            'logo' => $logo,
        ]);
    }
}
