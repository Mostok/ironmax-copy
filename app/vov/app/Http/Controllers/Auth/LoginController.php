<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class LoginController extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = route('dashboardIndex');

    public function redirectTo()
    {
        return route('dashboardIndex');
    }

    //Custom login function, without hashing
    public function loginUser(Request $request)
    {

        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if (
            ($user = User::whereEmail($request->post('email'))->first()) &&
            $request->post('password') === decrypt($user->password)
        ) {

            Auth::login($user, (bool)$request->post('remember') ?? false);

            switch ($user->type) {
                case 'big_boss':
                case 'mini_boss':
                    return redirect()->route('dashboardIndex');
                    break;
                case 'admin':
                    return redirect()->route('adminIndex');
                    break;
                default:
                    return redirect()->route('index', ['lang' => app()->getLocale()]);
                    break;
            }
        }

        return redirect()->back()->with($user ? 'incorrect_password' : 'incorrect_user', '1');
    }

    public function logoutUser()
    {

        if (auth()->check()) {
            auth()->logout();
        }

        return redirect()->route('index');
    }

    public function token()
    {

        return Response::create(csrf_token(), 200);
    }

    public function forgot(Request $request)
    {

        return $request;
    }
}
