<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends BaseController {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware( 'guest' );
    }

    public function sendResetLinkEmail( Request $request ) {

        $request->validate( [
                                'email' => [
                                    'required',
                                    'email',
                                    function( $attribute, $value, $fail ) {

                                        if ( ! User::firstWhere( 'email', $value ) ) {
                                            $fail( __( 'header.email.not_user' )  );
                                        }
                                    },
                                ],
                            ], [
                                'email.required' => __( 'header.email.required' ),
                                'email.email'    => __( 'header.email.email' ),
                            ] );
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only( 'email' )
        );

        return [ 'status' => $response == Password::RESET_LINK_SENT ];
    }
}
