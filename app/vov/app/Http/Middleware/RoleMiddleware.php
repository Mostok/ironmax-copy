<?php


namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RoleMiddleware
{
    public function handle(Request $request, Closure $next, ...$roles)
    {
        $roles = array_filter($roles);
        if (empty($roles)) {
            return redirect()->route('index')->with(['open_login_form' => true]);
        }

        if (in_array($request->user()->type, $roles, true)) {
            return $next($request);
        }

        return abort('404');
    }
}
