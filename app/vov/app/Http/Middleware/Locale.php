<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class Locale
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cookie_name = config('cookies_name_list.locale');
        $route_param = 'lang';
        $route = $request->route();
        /** @var string $route_lang [en,he,ru,null] */
        if (!$request->isXmlHttpRequest()) {
            if (!$lang = $route->parameter($route_param)) {
                $lang = (auth()->check() ? auth()->user()->language : null) ?? Cookie::get($cookie_name) ?? $this->getLang();
                /**
                 * Исключаем админки и дашборды, т.к. эта хрень с указанием локали в УРЛ должна работать на ЛП... и только!
                 */
                if (!in_array($route->getName(), ['dashboardIndex', 'adminIndex'])) {
                    return redirect(route(
                        $route->action['as'], array_merge($route->parameters, [
                            $route_param => $lang,
                        ])
                    ))->cookie($cookie_name, $lang);
                }
            } elseif (auth()->check() && auth()->user()->language != $lang) {
                auth()->user()->update(['language' => $lang]);
            }

            Cookie::queue($cookie_name, $lang);
            session(['locale' => $lang]);
        }

        app()->setLocale(session('locale', 'en'));
        return $next($request);
    }

    protected function getLang()
    {

        $lang = 'en';
        if (function_exists('curl_init')) {
            /** @var string $ip - значение присваиваем с учетом наличия фронтенд cloudflare */
            $ip = $_SERVER['HTTP_CF_CONNECTING_IP'] ?? $_SERVER['HTTP_X_FORWARDED_FOR'] ?? $_SERVER['REMOTE_ADDR'];
            $url = "https://api.ipinfodb.com/v3/ip-city/?key=345f70aec9a0975ea4290c4cf8c4276dbf8ce326c3946825467d68ea27bb185d&format=json&ip={$ip}";
            $channel = curl_init();
            curl_setopt($channel, CURLOPT_URL, $url);
            curl_setopt($channel, CURLOPT_RETURNTRANSFER, 1);
            if ($json_response = curl_exec($channel)) {
                $response = json_decode($json_response, true);
                if ($response['statusCode'] === 'OK') {
                    switch ($response['countryName']) {
                        case 'Israel':
                            $lang = 'he';
                            break;
                        case 'Russian Federation':
                        case 'Belarus':
                        case 'Kyrgyzstan':
                        case 'Kazakhstan':
                            $lang = 'ru';
                            break;
                        default:
                            $lang = 'en';
                            break;
                    }
                }
            }
            curl_close($channel);
        }

        return $lang;
    }
}
