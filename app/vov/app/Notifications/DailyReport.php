<?php
namespace App\Notifications;

use App\Models\User;
use DateTime;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class DailyReport extends Notification implements ShouldQueue
{
    public $data;
    public $from;
    public $to;
    public $online_view = false;
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @param  null|DateTime  $from
     * @param  null|DateTime  $to
     * @param  array  $data
     * @param  bool  $online_view
     */
    public function __construct($from = null, $to = null, $data = [], $online_view = false)
    {
        $this->data = $data;
        $this->from = $from;
        $this->to = $to;
        $this->online_view = $online_view;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed|User  $notifiable
     * @return MailMessage
     * @throws Exception
     */
    public function toMail($notifiable)
    {
        if (!$notifiable->send_out_events) {
            throw new Exception('Нельзя отправлять пользователю, параметр send_out_events которого не равен true');
        }
        /**
         * формируем к отправке
         */
        /** @var User $notifiable */
        $mail_message =  (new MailMessage)
            ->subject(__('mails/events_report_on_end_day.subject', [], $notifiable->language))
            ->bcc(['daglob@mail.ru', 'konevdaniel@gmail.com', 'daglob@yandex.ru', 'ironmaxtech@gmail.com'])
            ->view('emails.daily_event_report', [
                'table_header_date_time' => __('mails/events_report_on_end_day.table_header_date_time', [],
                    $notifiable->language),
                'table_header_exception' => __('mails/events_report_on_end_day.table_header_exception', [],
                    $notifiable->language),
                'lang' => $notifiable->language,
                'title' => __('mails/events_report_on_end_day.title', [], $notifiable->language),
                'preview' => __('mails/events_report_on_end_day.preview', [], $notifiable->language),
                'invitation_to_dashboard' => __('mails/events_report_on_end_day.invitation_to_dashboard', [],
                    $notifiable->language),
                'link_to_dashboard' => url(route('dashboardIndex')),
                // ?
                'class_rtl' => $notifiable->language === 'he' ? 'rtl' : null,
                'direction' => $notifiable->language === 'he' ? 'rtl' : 'ltr',
                'body' => __('mails/events_report_on_end_day.body', ['name' => $notifiable->name],
                    $notifiable->language),
                'thank_you' => __('mails/events_report_on_end_day.thank_you', [], $notifiable->language),
                'facilities' => $this->data,
                'online_view' => $this->online_view,
            ]);
        return $mail_message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray(User $notifiable)
    {
        return [
            'notifiable' => $notifiable,
            'data' => $this->data,
            'from' => $this->from,
            'to' => $this->to,
            'online_view' => $this->online_view,
        ];
    }
}
