<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword as DefaultResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class IronmaxResetPassword extends DefaultResetPassword
{

    public $name;
    public $lang;

    /**
     * Build the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }
        $url = url(route('password.reset', [
            'token' => $this->token,
            'email' => $notifiable->getEmailForPasswordReset(),
        ], false));

        return (new MailMessage)
            ->view('emails.restore_password', [
                'lang' => $this->lang,
                'class_rtl' => $this->lang === 'he' ? 'rtl' : null,
                'title' => __('mails/restore_password.title', [], $this->lang),
                'preview' => __('mails/restore_password.preview', [], $this->lang),
                'reset_url' => $url,
                'body' => __('mails/restore_password.body', ['name' => $this->name], $this->lang),
                'reset_btn_text' => __('mails/restore_password.reset_btn_text', [], $this->lang),
                'thank_you' => __('mails/restore_password.thank_you', [], $this->lang),
            ])
            ->subject(__('mails/restore_password.subject'));
    }
}
