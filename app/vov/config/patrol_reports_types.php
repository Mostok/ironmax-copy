<?php
return [
    /** стандартный отчет патруля */
    'patrol' => 'patrol_report',
    /** стандартный отчет инспекции */
    'inspector' => 'inspector_report',
    /** ежедневный отчет для ББ и МБ*/
    'daily' => 'daily_report',
    /** суммарный отчет для ББ и МБ */
    'summary' => 'summary_report',
    /** события моб.приложения */
    'android' => 'android',
    /** событие инцидент */
    'incident' => 'incident_report',
    /** событие тревога */
    'emergency' => 'emergency_report',
];
