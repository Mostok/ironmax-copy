<?php
return [
    'statuses' => [
        /** emergency */
        'patrol_emergency' => 's_em',
        /** incident */
        'patrol_incident' => 's_in',
        /** патруль выполнен в неожидаемое время */
        'patrol_mistake_time' => 's_mt_t', // не выводим
        /** патруль выполнен в неожидаемый день */
        'patrol_mistake_day' => 's_mt_d', // не выводим
        /** читер */
        'patrol_cheat' => 's_ch',
        /** в ожидаемый временной период выполнено патрулей меньше чем ожидалось */
        'time_block_less' => 's_tb_ls',
        /** в ожидаемый временной период не выполнено патрулей */
        'time_block_empty' => 's_tb_em',
        /** недостаточно баркодов */
        'patrol_barcodes' => 's_bc_ls',
        /** успешный патруль */
        'patrol_success' => 's_sc',
    ],
    'errors' => [
        'patrol_wrong_start_to_end_time_ratio' => 'er_pt_ste',
        'patrol_empty_timetable' => 'er_pt_ett',
        'patrol_not_set_facility' => 'er_pt_nsf',
        'patrol_not_set_patrol_start_time' => 'er_pt_nsp',
        'patrol_unknown_facility' => 'er_pt_uf',
    ],

];
