const mix = require('laravel-mix')
    /*
     |--------------------------------------------------------------------------
     | Mix Asset Management
     |--------------------------------------------------------------------------
     |
     | Mix provides a clean, fluent API for defining some Webpack build steps
     | for your Laravel application. By default, we are compiling the Sass
     | file for the application as well as bundling up all the JS files.
     |
     */

// app
mix.scripts(
        [
            "resources/js/wowslider_engine.js",
            "resources/js/wowslider.js",
            "resources/js/product.js"
        ],
        "public/js/product.js"
    )
    .js("resources/js/index.js", "public/js")
    .js("resources/js/main.js", "public/js")
    .sass("resources/sass/fonts.scss", "public/css")
    .sass("resources/views/emails/style.scss", "resources/views/emails")
    .sass("resources/sass/app.scss", "public/css")
    .copyDirectory("resources/svg", "public/svg")
    .copyDirectory("resources/images", "public/images")
    .copyDirectory("resources/mail", "public/mail");