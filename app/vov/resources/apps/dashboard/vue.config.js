module.exports = {
    'transpileDependencies': [
        'vuetify'
    ],
    devServer: {
        proxy: 'http://localhost:5680'
    },
    // output built static files to Laravel's public dir.
    // note the "build" script in package.json needs to be modified as well.
    outputDir: '../../../public/assets/dashboard',

    publicPath: '/dashboard',
    assetsDir: '../../assets/dashboard/',
    // modify the location of the generated HTML file.
    indexPath: '../../../resources/views/dashboard.blade.php',
    pluginOptions: {
        i18n: {
            locale: 'en',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableInSFC: false
        }
    },

    lintOnSave: process.env.NODE_ENV !== 'production',
    parallel: false/*,
    chainWebpack: config => {
        // Загрузчик GraphQL
        config.module
            .rule('css').
            test(/\.css$/).
            use('postcss-loader').
            loader('postcss-loader').
            end()
    }*/
}
