import appIconPatrolsPref from './app-icon-patrols-pref'
import appIconPatrolRep from './app-icon-patrol-rep'
import appIconPatrolExc from './app-icon-patrol-exc'
import appIconDashboard from './app-icon-dashboard'
import appIconRepIncident from './app-icon-rep-incident'
import appIconRepEmergency from './app-icon-rep-emergency'
import appIconRepInspector from './app-icon-rep-inspector'
import appIconMissingPatrols from './app-icon-missing-patrols'
import appIconEmergency from './app-icon-emergency'
import appIconIncident from './app-icon-incident'
import appIconMissBarcodes from './app-icon-miss-barcodes'
import appIconLangEn from './app-icon-lang-en'
import appIconLangHe from './app-icon-lang-he'
import appIconLangRu from './app-icon-lang-ru'
import appIconLogo from './app-icon-logo'
import appIconFileDownload from './app-icon-file-download'
import appIconQr from './app-icon-qr'

export default {
    'app-icon-file-download': {
        component: appIconFileDownload,
        props: []
    },
    'app-icon-logo': {
        component: appIconLogo,
        props: []
    },
    'app-icon-dashboard': {
        component: appIconDashboard,
        props: []
    },
    'app-icon-patrols-pref': {
        component: appIconPatrolsPref,
        props: []
    },
    'app-icon-patrol-rep': {
        component: appIconPatrolRep,
        props: []
    },
    'app-icon-patrol-exc': {
        component: appIconPatrolExc,
        props: []
    },
    'app-icon-rep-incident': {
        component: appIconRepIncident,
        props: []
    },
    'app-icon-rep-emergency': {
        component: appIconRepEmergency,
        props: []
    },
    'app-icon-rep-inspector': {
        component: appIconRepInspector,
        props: []
    },
    'app-icon-missing-patrols': {
        component: appIconMissingPatrols,
        props: []
    },
    'app-icon-emergency': {
        component: appIconEmergency,
        props: []
    },
    'app-icon-incident': {
        component: appIconIncident,
        props: []
    },
    'app-icon-miss-barcodes': {
        component: appIconMissBarcodes,
        props: []
    },
    'app-icon-lang-en': {
        component: appIconLangEn,
        props: []
    },
    'app-icon-lang-he': {
        component: appIconLangHe,
        props: []
    },
    'app-icon-lang-ru': {
        component: appIconLangRu,
        props: []
    },
    'app-icon-qr' :         {
        component : appIconQr,
        props :     []
    }
}
