import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../components/Dashboard'
import Patrol from '../components/reports/Patrol'
import Incident from '../components/reports/Incident'
import Inspector from '../components/reports/Inspector'
import Emergency from '../components/reports/Emergency'
import Exceptions from '../components/reports/Exceptions'
import Contacts from '../components/Contacts'
import ProfileEdit from '../components/profile/ProfileEdit'
import Reports from '../components/Reports'
import Patrols from '../components/Patrols'
import Facility from '../components/patrols/Facility'
import BarcodesIndex from "../components/barcodes/BarcodesIndex"

Vue.use(VueRouter)
const routes = [{
        path: '/dashboard/',
        meta: {},
        name: 'sidebar.dashboard',
        component: Dashboard
    },
    {
        path: '/dashboard/contacts',
        meta: {},
        name: 'sidebar.contacts',
        component: Contacts
    },
    {
        path: '/dashboard/reports',
        meta: {},
        name: 'reports',
        component: Reports,
        redirect: '/dashboard/reports/patrol',
        children: [{
                path: 'emergency',
                meta: {},
                name: 'sidebar.reports.emergency',
                component: Emergency
            },
            {
                path: 'inspector',
                meta: {},
                name: 'sidebar.reports.inspector',
                component: Inspector
            },
            {
                path: 'incident',
                meta: {},
                name: 'sidebar.reports.incident',
                component: Incident
            },
            {
                path: 'patrol',
                meta: {},
                name: 'sidebar.reports.patrol',
                component: Patrol
            },
            {
                path: 'exceptions',
                meta: {},
                name: 'sidebar.reports.exceptions',
                component: Exceptions
            }
        ]
    },
    {
        path: '/dashboard/profile',
        name: 'sidebar.profile',
        component: ProfileEdit
    },
    {
        path: '/dashboard/patrols',
        name: 'sidebar.patrols.index',
        component: Patrols,
        children: [{
            path: ':id',
            meta: {},
            name: 'sidebar.patrols.facility',
            component: Facility
        }]
    },
    {
        path: '/dashboard/barcodes',
        name: 'sidebar.barcodes',
        component: BarcodesIndex,
    }
]
const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes
})
export default router