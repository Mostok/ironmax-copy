import Vue from 'vue'
import Vuex from 'vuex'
import config from './modules/config'
import dashboardBlocks from './modules/dashboard/blocks'
import dashboardMap from './modules/dashboard/map'
import patrols from './modules/patrols'
import profile from './modules/profile'
import notification from './modules/notification'
import barcodes from "./modules/barcodes"
// import reports_emergency from './modules/reports/reports_emergency'
// import reports_incident from './modules/reports/reports_incident'
// import reports_inspector from './modules/reports/reports_inspector'
// import reports_exceptions from './modules/reports/reports_exceptions'
// import reports_patrol from './modules/reports/reports_patrol'
import createPlugin from 'logrocket-vuex'
import LogRocket from 'logrocket'
import reports_patrol from './modules/reports/patrols'
import reports_emergency from './modules/reports/emergency'
import reports_incident from './modules/reports/incident'
import reports_inspector from './modules/reports/inspector'
import reports_exceptions from './modules/reports/exceptions'

import i18n from '../i18n'

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        lang: null,
        token: null,
        logo: null,
        overlay: false,
        errors: [],
        appHeader: true,
        profile_id: null
    },
    mutations: {
        SET_LOGO(state, value) {
            state.logo = value
        },
        SET_LANG(state, value) {
            state.lang = value
        },
        TOGGLE_OVERLAY(state, value) {
            state.overlay = value
        },
        ADD_ERROR(state, value) {
            state.errors.push(value)
        },
        SET_ERRORS(state, value) {
            state.errors = value
        },
        SET_APP_HEADER(state, value) {
            state.appHeader = value
        },
        SET_PROFILE_ID(state, value) {
            state.profile_id = value
        }
    },
    actions: {
        removeError({ commit, state }, index) {
            commit(
                'SET_ERRORS',
                state.errors.filter((el, i) => i !== index)
            )
        },
        async changeLang({ commit }, lang) {
            await Vue.axios.post('/api/profile/lang', { lang }).
            then(async() => {
                await commit('SET_LANG', lang)
            }).
            catch(err => console.error(err))
        },
        initApplication: {
            handler(ctx) {
                Vue.axios.get('/api/profile')
                    .then(response => {
                        const profile = response.data
                        store.commit('SET_PROFILE_ID', profile.id)
                        ctx.dispatch('initProfile');
                        ctx.dispatch('initMap');
                        ctx.dispatch('initBlocks');
                        ctx.dispatch('initPatrols');
                        ctx.dispatch('initExceptions');
                        ctx.dispatch('initEmergency');
                        ctx.dispatch('initIncident');
                        ctx.dispatch('initInspector');
                        ctx.dispatch('initNotification');
                        ctx.dispatch("config/init");
                    })
                    .catch(err => console.error(err))

                ctx.commit("SET_LANG", i18n.locale);
            },
            root: true
        },
    },
    modules: {
        config,
        dashboardBlocks,
        dashboardMap,
        patrols,
        profile,
        notification,
        reports_patrol,
        reports_emergency,
        reports_incident,
        reports_inspector,
        reports_exceptions,
        barcodes
    },
    plugins: [createPlugin(LogRocket)]
})

export default store;