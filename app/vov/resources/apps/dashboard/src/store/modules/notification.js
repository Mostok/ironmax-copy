import Vue from 'vue';
export default {
    namespaced: true,
    state: () => ({
        notifications: [],
        changes_in_notifications: false,
        show_events_board: false,
        auto_open: false
    }),
    getters: {
        notifications_count: (state) => {
            return state.notifications.length;
        },

        notifications: state => {
            return state.notifications;
        }
    },
    mutations: {
        SET_NOTIFICATIONS(state, value) {
            state.notifications = value;
        },
        DEL_NOTIFICATION(state, notification_id) {
            let notification = state.notifications.find(item => item.id === notification_id);
            if (!!notification) {
                state.notifications.splice(state.notifications.indexOf(notification), 1);
            }
        },
        SET_SHOW_EVENTS_BOARD(state, val) {
            state.show_events_board = val;
        }
    },
    actions: {
        initNotification: {
            handler({ dispatch }) {
                dispatch("loadNotifications");
            },
            root: true
        },
        async loadNotifications({ commit, rootState }) {
            await Vue.axios.get('/api/events').then((e) => {
                commit("SET_NOTIFICATIONS", e.data);
            }).catch(err => console.error(err));

            window.Echo.private(`dashboard.${rootState.profile_id}`).listen(
                "Dashboard\\NotificationsEvent",
                e => {
                    commit("SET_NOTIFICATIONS", e.data);
                }
            );
        },
        toggleEventBoard({ state, commit }) {
            commit("SET_SHOW_EVENTS_BOARD", !state.show_events_board);
        },
        changeEventBoard({ commit }, value) {
            commit("SET_SHOW_EVENTS_BOARD", value);
        },
        async read({ commit }, event_id) {
            await Vue.axios
                .post("/api/events/read", { event_id })
                .then(response => {
                    if (response.data.status) {
                        commit("DEL_NOTIFICATION", event_id);
                    }
                })
                .catch(err => console.error(err));
        },
        async readAll({ commit }) {
            await Vue.axios
                .get("/api/events/read_all")
                .then(response => {
                    if (response.data.status) {
                        commit("SET_NOTIFICATIONS", []);
                    }
                })
                .catch(err => console.error(err));
        }
    }
};