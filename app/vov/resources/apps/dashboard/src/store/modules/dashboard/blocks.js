import Vue from 'vue';
export default {
    namespaced: true,
    state: {
        missing_patrols: 0,
        emergency_events: 0,
        incident_reports: 0,
        missing_barcodes: 0,
        missing_inspector_barcodes: 0,
        overlays: {
            missing_patrols: false,
            emergency_events: false,
            incident_reports: false,
            missing_barcodes: false,
            missing_inspector_barcodes: false
        }
    },
    getters: {
        missing_all_barcodes(state) {
            return state.missing_inspector_barcodes + state.missing_barcodes
        }
    },
    mutations: {
        SET_MISSING_PATROLS(state, value) {
            state.missing_patrols = value
        },
        SET_EMERGENCY_EVENTS(state, value) {
            state.emergency_events = value
        },
        SET_INCIDENT_REPORTS(state, value) {
            state.incident_reports = value
        },
        SET_MISSING_BARCODES(state, value) {
            state.missing_barcodes = value
        },
        SET_MISSING_INSPECTOR_BARCODES(state, value) {
            state.missing_inspector_barcodes = value
        },
        TOGGLE_OVERLAY(state, { type, value }) {
            state.overlays[type] = value
        }
    },
    actions: {
        initBlocks: {
            handler({ dispatch }) {
                dispatch('loadMissingPatrols');
                dispatch('loadEmergencyEvents');
                dispatch('loadIncidentReports');
                dispatch('loadMissingBarcodes');
                dispatch('loadMissingInspectorBarcodes');
            },
            root: true
        },
        async loadMissingPatrols({ commit, rootState }) {
            commit('TOGGLE_OVERLAY', { type: 'missing_patrols', value: true })

            window.Echo.private(`dashboard.${rootState.profile_id}`).listen('Dashboard\\MissingPatrolsEvent', e => {
                commit('SET_MISSING_PATROLS', e.data);
            })

            await Vue.axios.get('/api/dashboard/missing_patrols').then((e) => {
                commit('TOGGLE_OVERLAY', { type: 'missing_patrols', value: false })
                commit('SET_MISSING_PATROLS', e.data);
            }).catch(err => console.error(err));
        },
        async loadEmergencyEvents({ commit, rootState }) {
            commit('TOGGLE_OVERLAY', { type: 'emergency_events', value: true })

            window.Echo.private(`dashboard.${rootState.profile_id}`).listen('Dashboard\\EmergencyEventsEvent', e => {
                commit('SET_EMERGENCY_EVENTS', e.data);
            })

            await Vue.axios.get('/api/dashboard/emergency_events').then((e) => {
                commit('TOGGLE_OVERLAY', { type: 'emergency_events', value: false })
                commit('SET_EMERGENCY_EVENTS', e.data);
            }).catch(err => console.error(err));
        },
        async loadIncidentReports({ commit, rootState }) {
            commit('TOGGLE_OVERLAY', { type: 'incident_reports', value: true })

            window.Echo.private(`dashboard.${rootState.profile_id}`).listen('Dashboard\\IncidentReportsEvent', e => {
                commit('SET_INCIDENT_REPORTS', e.data);
            })

            await Vue.axios.get('/api/dashboard/incident_reports').then((e) => {
                commit('TOGGLE_OVERLAY', { type: 'incident_reports', value: false })
                commit('SET_INCIDENT_REPORTS', e.data);
            }).catch(err => console.error(err));
        },
        async loadMissingBarcodes({ commit, rootState }) {
            commit('TOGGLE_OVERLAY', { type: 'missing_barcodes', value: true })

            window.Echo.private(`dashboard.${rootState.profile_id}`).listen('Dashboard\\MissingBarcodesEvent', e => {
                commit('SET_MISSING_BARCODES', e.data);
            })

            await Vue.axios.get('/api/dashboard/missing_barcodes').then((e) => {
                commit('TOGGLE_OVERLAY', { type: 'missing_barcodes', value: false })
                commit('SET_MISSING_BARCODES', e.data);
            }).catch(err => console.error(err));
        },
        async loadMissingInspectorBarcodes({ commit, rootState }) {
            commit('TOGGLE_OVERLAY', { type: 'missing_inspector_barcodes', value: true })

            window.Echo.private(`dashboard.${rootState.profile_id}`).listen('Dashboard\\MissingInspectorBarcodesEvent', e => {
                commit('SET_MISSING_INSPECTOR_BARCODES', e.data);
            })

            await Vue.axios.get('/api/dashboard/missing_inspector_barcodes').then((e) => {
                commit('TOGGLE_OVERLAY', { type: 'missing_inspector_barcodes', value: false })
                commit('SET_MISSING_INSPECTOR_BARCODES', e.data);
            }).catch(err => console.error(err));
        }
    }
}