import Vue from 'vue'

export default {
    namespaced: true,
    state: {
        config: {
            ui: {
                events: {
                    auto_open: true
                },
                map: {
                    center: {
                        lat: 0,
                        lng: 0
                    },
                }
            }
        }
    },
    mutations: {
        SET_CONFIG(state, value) {
            state.config = value
        }
    },
    actions: {
        init({commit}) {
            Vue.axios.get('/api/profile/config').then(response => {
                commit('SET_CONFIG', response.data)
            }).catch(err => console.error(err))
        },
        async update({dispatch}, value) {
            await Vue.axios.post('/api/profile/config', value).then(() => {
                dispatch('init')
            }).catch(err => console.error(err))
        }
    }
}
