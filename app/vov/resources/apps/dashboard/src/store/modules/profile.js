import Vue from "vue";
import LogRocket from "logrocket";

export default {
    namespaced: true,
    state: () => ({
        id: null,
        email: null,
        firstname: null,
        language: null,
        lastname: null,
        phone: null,
        role: null,
        send_out_events: null,
        timezone: null
    }),
    mutations: {
        SET_PROFILE(state, value) {
            state.id = value.id;
            state.email = value.email;
            state.firstname = value.firstname;
            state.language = value.language;
            state.lastname = value.lastname;
            state.phone = value.phone;
            state.role = value.role;
            state.send_out_events = value.send_out_events;
            state.timezone = value.timezone;

            if (process.env.NODE_ENV === "production") {
                LogRocket.identify(value.id, {
                    type: value.type,
                    uid: value.id,
                    email: value.email,
                    language: value.language,
                    timezone: value.timezone
                });
            }
        },
        SET_EMAIL(state, value) {
            state.email = value;
        },
        SET_FIRSTNAME(state, value) {
            state.firstname = value;
        },
        SET_LANGUAGE(state, value) {
            state.language = value;
        },
        SET_LASTNAME(state, value) {
            state.lastname = value;
        },
        SET_PHONE(state, value) {
            state.phone = value;
        },
        SET_TIMEZONE(state, value) {
            state.timezone = value;
        },
        SET_SEND_OUT_EVENTS(state, value) {
            state.send_out_events = value;
        }
    },
    actions: {
        initProfile: {
            handler({ dispatch }) {
                dispatch("getProfile");
            },
            root: true
        },
        async getProfile(ctx) {
            ctx.rootState.overlay = true;
            await Vue.axios.get('/api/profile').then((e) => {
                ctx.commit("SET_PROFILE", e.data);
            }).catch(err => console.error(err));

            window.Echo.private(`dashboard.${ctx.rootState.profile_id}`).listen(
                "Dashboard\\ProfileEvent",
                e => {
                    ctx.commit("SET_PROFILE", e.data);
                    ctx.rootState.overlay = false;
                }
            );
            ctx.rootState.overlay = false;
        },
        save({ state, rootState }) {
            rootState.overlay = true;
            Vue.axios
                .post("/api/profile", state)
                .then(() => {})
                .catch(err => console.error(err));
        }
    }
};