import Vue from 'vue';
export default {
    namespaced: true,
    state: {
        inspectors: []
    },
    getters: {
        inspectors: state => dates => {
            return state.inspectors.filter(item => {
                if (
                    new Date(dates.from) <= new Date(item.timezone_time_point) &&
                    new Date(dates.to) >= new Date(item.timezone_time_point)
                )
                    return item;
            })
        }
    },
    mutations: {
        SET_INSPECTORS(state, value) {
            state.inspectors = value
        },
    },
    actions: {
        initInspector: {
            handler({ dispatch }) {
                dispatch('loadInspector');
            },
            root: true
        },
        async loadInspector({ commit, rootState }) {

            await Vue.axios.get('/api/reports/inspector').then((e) => {
                commit('SET_INSPECTORS', e.data);
            }).catch(err => console.error(err));

            window.Echo.private(`dashboard.${rootState.profile_id}`).listen('Dashboard\\Reports\\InspectorEvent', e => {
                commit('SET_INSPECTORS', e.data);
            })
        },
    }
}
