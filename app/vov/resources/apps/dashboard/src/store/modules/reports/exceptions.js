import Vue from 'vue';
export default {
    namespaced: true,
    state: {
        exceptions: []
    },
    getters: {
        exceptions: state => dates => {
            return state.exceptions.filter(item => {
                if (
                    new Date(dates.from) <= new Date(item.timezone_time_point) &&
                    new Date(dates.to) >= new Date(item.timezone_time_point)
                )
                    return item;
            })
        }
    },
    mutations: {
        SET_EXCEPTIONS(state, value) {
            state.exceptions = value
        },
    },
    actions: {
        initExceptions: {
            handler({ dispatch }) {
                dispatch('loadExceptions');
            },
            root: true
        },
        async loadExceptions({ commit, rootState }) {
            await Vue.axios.get('/api/reports/exceptions').then((e) => {
                commit('SET_EXCEPTIONS', e.data);
            }).catch(err => console.error(err));
            window.Echo.private(`dashboard.${rootState.profile_id}`).listen('Dashboard\\Reports\\ExceptionsEvent', e => {
                commit('SET_EXCEPTIONS', e.data);
            })
        },
    }
}
