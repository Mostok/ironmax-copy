import Vue from 'vue';
export default {
    namespaced: true,
    state: {
        patrols: []
    },
    getters: {
        patrols: state => dates => {
            return state.patrols.filter(item => {
                if (
                    new Date(dates.from) <= new Date(item.timezone_time_point) &&
                    new Date(dates.to) >= new Date(item.timezone_time_point)
                )
                    return item;
            });
        }
    },
    mutations: {
        SET_PATROLS(state, value) {
            state.patrols = value;
        }
    },
    actions: {
        initPatrols: {
            handler({ dispatch }) {
                dispatch("loadPatrols");
            },
            root: true
        },
        async loadPatrols({ commit, rootState }) {
            await Vue.axios.get('/api/reports/patrol').then((e) => {
                commit('SET_PATROLS', e.data);
            }).catch(err => console.error(err));

            window.Echo.private(`dashboard.${rootState.profile_id}`).listen(
                "Dashboard\\Reports\\PatrolsEvent",
                e => {
                    commit("SET_PATROLS", e.data);
                }
            );
        }
    }
};