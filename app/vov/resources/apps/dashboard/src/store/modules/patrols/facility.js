import Vue from 'vue'
import i18n from '../../../i18n'

export default {
    namespaced: true,
    state: {
        id: null,
        facility: {},
        tmp: null,
        tmp_type: null,
        tmp_value: null
    },
    mutations: {
        SET_ID (state, value) {
            state.id = value
        },
        SET_FACILITY (state, value) {
            state.facility = value
        },
        SET_FACILITY_TYPE (state, value) {
            state.facility.type = value
        },
        SET_TMP_TYPE (state, value) {
            state.tmp_value = null
            state.tmp_type = value
        },
        SET_TMP_VALUE (state, value) {
            state.tmp_value = value
            state.tmp_type = state.tmp_type || state.facility.type
        },
        RESET_TMP (state) {
            state.tmp_type = state.tmp_value = null
        }
    },
    actions: {
        init ({ commit, rootState }, id) {
            rootState.overlay = true
            commit('RESET_TMP')
            Vue.axios.get(`/api/facilities/${id}`).then(response => {
                commit('SET_FACILITY', response.data)
                commit('SET_ID', id)
                rootState.overlay = false
            }).catch(err => console.error(err))
        },
        save ({ state, commit, dispatch, rootState }) {
            rootState.overlay = true
            if (!state.tmp_type || !state.tmp_value) {
                commit(
                    'ADD_ERROR',
                    {
                        color: 'red',
                        text: !state.tmp_type
                            ? i18n.t('errors.not_set_type')
                            : i18n.t('errors.missing_schedule')
                    },
                    { root: true }
                )
                rootState.overlay = false
                return false
            }
            const body = {
                type: state.tmp_type,
                value: state.tmp_value
            }
            commit('SET_FACILITY', body)
            Vue.axios.post(`/api/facilities/${state.id}`, body).
                then(response => {
                    rootState.overlay = false
                    switch (response.status) {
                        case 200:
                            dispatch('init', state.id)
                            break
                        case 401:
                            commit(
                                'ADD_ERROR',
                                {
                                    color: 'red',
                                    text: response.error
                                },
                                { root: true }
                            )
                            break
                    }
                }).
                catch(err => console.error(err))
        },
        saveComplex ({ state, commit, dispatch, rootState }) {
            rootState.overlay = true
            if (!state.tmp_type || !state.tmp_value) {
                commit(
                    'ADD_ERROR',
                    {
                        color: 'red',
                        text: !state.tmp_type
                            ? i18n.t('errors.not_set_type')
                            : i18n.t('errors.missing_schedule')
                    },
                    { root: true }
                )
                rootState.overlay = false
                return false
            }
            const body = {
                type: state.tmp_type,
                value: state.tmp_value
            }
            commit('SET_FACILITY', body)
            Vue.axios.post(`/api/facilities/complex`, body).then(response => {
                rootState.overlay = false
                switch (response.status) {
                    case 200:
                        dispatch('init', state.id)
                        break
                    case 401:
                        commit(
                            'ADD_ERROR',
                            {
                                color: 'red',
                                text: response.error
                            },
                            { root: true }
                        )
                        break
                }
            }).catch(err => console.error(err))
        }
    },
    getters: {
        isChange: state => {
            return state.tmp_type !== null || state.tmp_value !== null
        },
        tabletime: state => {
            if (state.tmp_type) {
                return {
                    type: state.tmp_type,
                    value: state.tmp_value
                }
            }
            return state.facility
        }
    },
    modules: {}
}
