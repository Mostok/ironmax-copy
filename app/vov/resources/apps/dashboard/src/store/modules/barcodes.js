import Vue from 'vue'

export default {
    namespaced : true,
    state :      {
        list: [],
        listChanged: [],
        facilities: [],
        maxPages: 5,
        changeCount: 0,
        dataSizeError: false
    },
    mutations :  {
        SAVE_REMINDER(state, data) {
            state.list[data.curBarcode].additional.reminder = data.text;
        },
        SET_CURBARCODE_OQ_HINT(state, data) {
            state.list[data.curBarcode].additional.open_questions[data.currentQuestion].hint = data.value;
            state.listChanged[data.curBarcode] = true;
        },
        SET_CURBARCODE_OQ_QUESTION(state, data) {
            state.list[data.curBarcode].additional.open_questions[data.currentQuestion].question = data.value;
            state.listChanged[data.curBarcode] = true;
        },
        SET_CURBARCODE_CHANGED(state, index) {
            state.listChanged[index] = true;
        },
        CLEAR_ADDITIONAL(state, curBarcode) {
            state.list[curBarcode].additional = [];
        },
        DELETE_QUESTION(state, data) {
            state.list[data.index].additional[data.question.type+'s'].splice(data.question.index, 1);
        },
        CLEAR_REMINDER(state, curBarcode) {
            state.list[curBarcode].additional.reminder = '';
        },
        REMINDER_INIT(state, curBarcode) {
            if(state.list[curBarcode].additional.length==0) {
                state.list[curBarcode].additional = {
                    reminder: ''
                }
            }
        },
        OQ_INIT(state, curBarcode) {
            if(state.list[curBarcode].additional.length==0) {
                state.list[curBarcode].additional = {
                    open_questions: []
                }
            } else if(state.list[curBarcode].additional.open_questions==undefined) {
                state.list[curBarcode].additional.open_questions = [];
            }
        },
        PQ_INIT(state, curBarcode) {
            if(state.list[curBarcode].additional.length==0) {
                state.list[curBarcode].additional = {
                    predefined_questions: []
                }
            } else if(state.list[curBarcode].additional.predefined_questions==undefined) {
                state.list[curBarcode].additional.predefined_questions = [];
            }
        },
        EDIT_PREDEFINED_QUESTION(state, data) {
            state.list[data.curBarcode].additional.predefined_questions[data.index].question = data.question.trim();
            state.list[data.curBarcode].additional.predefined_questions[data.index].answers = data.answers;
        },
        ADD_PREDEFINED_QUESTION(state, data) {
            state.list[data.curBarcode].additional.predefined_questions.push({
                question: data.question.trim(),
                answers: data.answers
            });
        },
        EDIT_OPEN_QUESTION(state, data) {
            state.list[data.curBarcode].additional.open_questions[data.index].question = data.question.trim();
            state.list[data.curBarcode].additional.open_questions[data.index].hint = data.hint.trim();
        },
        ADD_OPEN_QUESTION(state, data) {
            state.list[data.curBarcode].additional.open_questions.push({
                question: data.question.trim(),
                hint: data.hint.trim()
            });
        },
        ADD_EMPTY_OQ(state, curBarcode) {
            state.list[curBarcode].additional.open_questions.push({
                question: '',
                hint: ''
            });
        },
        ADD_EMPTY_BARCODE(state, facility) {
            state.list.push({
                id: 0,
                name: "",
                location: "",
                facility_id: facility.code,
                facility: {
                        facility_name: facility.name,
                        is_sayar: facility.is_sayar
                },
                start_patrol_name: false,
                additional: {}
            });
        },
        CLEAR_LIST(state) {
            state.list = [];
        },
        SET_LIST(state, value) {
            state.list     = value
        },
        SET_FACILITIES(state, value) {
            state.facilities     = value
        },
        SET_START_PATROL_NAME(state, value) {
            state.list[0].start_patrol_name = value;
            state.listChanged[0] = true;
        },
        SET_CURBARCODE_ID(state, data) {
            state.list[data.index].id     = data.value;
        },
        SET_CURBARCODE_NAME(state, data) {
            state.list[data.index].name     = data.value;
            state.listChanged[data.index] = true;
        },
        SET_CURBARCODE_LOCATION(state, data) {
            state.list[data.index].location     = data.value;
            state.listChanged[data.index] = true;
        },
        SET_CURBARCODE_FACILITY(state, data) {
            state.list[data.barcodeIndex].facility_id     = data.facilityCode;
            let facility = state.facilities.find(item => item.code == data.facilityCode, data);
            state.list[data.barcodeIndex].facility.facility_name     = facility.name;
            state.list[data.barcodeIndex].facility.is_sayar = facility.is_sayar;
            state.listChanged[data.barcodeIndex] = true;
        },
        DELETE_BARCODE(state, index) {
            state.list.splice(index, 1);
        },
        CHANGES_INC(state) {
            state.changeCount++;
        },
        DATA_SIZE_ERROR(state, value) {
            state.dataSizeError = value;
        }
    },
    getters :    {
        listCount : state => state.list.length,
        additionalCount: (state, getters) => index =>  {
            var sum = 0;
            if (getters.listCount > 0) {
                if (state.list[index].additional.reminder != undefined &&
                        state.list[index].additional.reminder != '') {
                    sum++;
                } else {
                    if (state.list[index].additional.open_questions != undefined) {
                        sum += state.list[index].additional.open_questions.length;
                    }
                    if (state.list[index].additional.predefined_questions != undefined) {
                        sum += state.list[index].additional.predefined_questions.length;
                    }
                }
            }
            return sum;
        },
        isQuestionExist : state => data => {
            var result = false;
            if(state.list[data.index].additional!=undefined) {
                if(state.list[data.index].additional.open_questions!=undefined) {
                    result = result || state.list[data.index].additional.open_questions.find(
                            (item, index) => {
                                if(item.question == data.question.trim()) {
                                    if(data.exclude!=undefined) {
                                        if(data.exclude.type=='open_question' && data.exclude.index==index) {
                                            return false;
                                        } else {
                                            return true;
                                        }
                                    } else {
                                        return true;
                                    }
                                } else {
                                    return false;
                                }
                            }, data)!=undefined;
                }
                if(!result && state.list[data.index].additional.predefined_questions!=undefined) {
                    result = result || state.list[data.index].additional.predefined_questions.find(
                            (item, index) => {
                                if(item.question == data.question.trim()) {
                                    if(data.exclude!=undefined) {
                                        if(data.exclude.type=='predefined_question' && data.exclude.index==index) {
                                            return false;
                                        } else {
                                            return true;
                                        }
                                    } else {
                                        return true;
                                    }
                                } else {
                                    return false;
                                }
                            }, data)!=undefined;
                }
            }
            return result;
        },
        hasOpenQuestions : (state, getters) => index => {
            return (getters.listCount > 0) && (state.list[index].additional.open_questions != undefined);
        },
        facilities : state => state.facilities,
        isBarcodeChanged : state => index => (state.listChanged[index]!=undefined),
        questions : state => index => {
            var result = [];
            if(state.list[index].additional!=undefined) {
                if(state.list[index].additional.open_questions!=undefined) {
                    result = state.list[index].additional.open_questions.reduce((result, current, index) => { result.push({type: 'open_question', index: index, text: current.question}); return result; }, result);
                }
                if(state.list[index].additional.predefined_questions!=undefined) {
                    result = state.list[index].additional.predefined_questions.reduce((result, current, index) => { result.push({type: 'predefined_question', index: index, text: current.question}); return result; }, result);
                }
            }
            return result;
        }
    },
    actions :    {
        async listInit({ state, commit }, facilityId) {
            Vue.axios
               .get('/api/barcode', {
                    params: {
                      facility_id: facilityId
                    }
                })
               .then((response) => {
                   commit('SET_LIST', response.data);
                   if(response.data.length==0) {
                       let facility = state.facilities.find(item => item.code == facilityId, facilityId);
                       commit('ADD_EMPTY_BARCODE', facility);
                   }
               })
               .catch(err => console.error(err))
        },
        async facilitiesInit({ commit, rootState }) {
            rootState.overlay = true;
            Vue.axios
//               .get('/api/facilities')
               .get('/api/facilities/barcodes')
               .then((response) => {
                   commit('SET_FACILITIES', response.data)
                   rootState.overlay = false
               })
               .catch(err => console.error(err))
        },
        async updateBarcode({ state }, index) {
            Vue.axios
               .put('/api/barcode/' + state.list[index].id, state.list[index])
               .catch(err => console.error(err));
        },
        async updateCurrentBarcode({ state, commit }, index) {
            if(state.list[index]!=undefined) {
                let data = state.list[index];
                if(state.list[index + 1] == undefined) {
                    data.is_last = true;
                } else {
                    data.is_last = false;
                }
                Vue.axios
                .put('/api/barcode/current', data)
                .then((response) => {
                    if(response.data.message=='Success') {
                        commit('CHANGES_INC');
                    }
                })
//                .catch(err => console.error(err));
                .catch(function(err) {
                    if(err.response.status==413) {
                        commit('DATA_SIZE_ERROR', true);
                    }
                });
            }
        },
        async addBarcode({ state, commit }, index) {
            Vue.axios
               .post('/api/barcode', state.list[index])
//               .then((response) => index => {
               .then((response) => {
                   if(response.data.id!=undefined) {
                       let index = state.list.length-1;
                       if(state.list[index-1].id==0) {
                           index--;
                       }
                       commit('SET_CURBARCODE_ID', {
                           index: index,
                           value: response.data.id
                       });
                   }
               })
               .catch(err => console.error(err));
        },
        async delBarcode({ state, commit, dispatch, rootState }, index) {
            rootState.overlay = true;
            Vue.axios
               .delete('/api/barcode/' + state.list[index].id)
//               .then((response) => index => {
//                    commit('DELETE_BARCODE', index);
//               })
               .then((response) => {
                   if(response.data.id!=undefined) {
                       let index = state.list.findIndex(item => item.id == response.data.id, response);
                       commit('DELETE_BARCODE', index);
                       dispatch('savePoints');
                   }
                   rootState.overlay = false;
               })
               .catch(err => console.error(err));
        },
        async savePoints({ state }) {

            let names = state.list.filter(function(item){
                return !!item.name.length;
            });

            names = names.map(function (item) {
                return item.name
            })

            const data = {
                list: names
            }

            Vue.axios
               .post('/api/facilities/' + state.list[0].facility_id + '/points', data)
               .catch(err => console.error(err));
        }
    }
}
