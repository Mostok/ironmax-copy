import _ from "lodash";
import Vue from 'vue';

export default {
    namespaced: true,
    state: {
        points: []
    },
    getters: {
        points: state =>
            state.points.filter(
                point => point.location !== "" && point.location !== "---,---"
            )
    },
    mutations: {
        SET_POINTS(state, value) {
            state.points = value.map(point => {
                let coords = point.location.split(",", 2);
                return _.extend(point, {
                    position: {
                        lat: parseFloat(coords[0]),
                        lng: parseFloat(coords[1])
                    }
                });
            });
        }
    },
    actions: {
        initMap: {
            handler({ dispatch }) {
                dispatch("getPoints");
            },
            root: true
        },
        async getPoints({ commit, rootState }) {
            window.Echo.private(`dashboard.${rootState.profile_id}`).listen(
                "Dashboard\\PointsEvent",
                e => {
                    commit("SET_POINTS", e.data);
                }
            );
            await Vue.axios.get('/api/points').then((e) => {
                commit('SET_POINTS', e.data);
            }).catch(err => console.error(err));
        }
    },
    modules: {}
};