import Vue from 'vue';
export default {
    namespaced: true,
    state: {
        emergencies: []
    },
    getters: {
        emergencies: state => dates => {
            return state.emergencies
                .filter(item => {
                    if ((new Date(dates.from) <= new Date(item.timezone_time_point)) && (new Date(dates.to) >= new Date(item.timezone_time_point))) return item;
                })
        }
    },
    mutations: {
        SET_EMERGENCY(state, value) {
            state.emergencies = value
        },
    },
    actions: {
        initEmergency: {
            handler({ dispatch }) {
                dispatch('loadEmergency');
            },
            root: true
        },
        async loadEmergency({ commit, rootState }) {
            await Vue.axios.get('/api/reports/emergency').then((e) => {
                commit('SET_EMERGENCY', e.data);
            }).catch(err => console.error(err));

            window.Echo.private(`dashboard.${rootState.profile_id}`).listen('Dashboard\\Reports\\EmergencyEvent', e => {
                commit('SET_EMERGENCY', e.data);
            })
        },
    }
}
