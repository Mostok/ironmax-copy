import Vue from 'vue';
export default {
    namespaced: true,
    state: {
        incidents: []
    },
    getters: {
        incidents: state => dates => {
            return state.incidents.filter(item => {
                if ((new Date(dates.from) <= new Date(item.timezone_time_point)) && (new Date(dates.to) >= new Date(item.timezone_time_point))) return item;
            })
        }
    },
    mutations: {
        SET_INCIDENTS(state, value) {
            state.incidents = value
        },
    },
    actions: {
        initIncident: {
            handler({ dispatch }) {
                dispatch('loadIncident');
            },
            root: true
        },
        async loadIncident({ commit, rootState }) {
            await Vue.axios.get('/api/reports/incident').then((e) => {
                commit('SET_INCIDENTS', e.data);
            }).catch(err => console.error(err));

            window.Echo.private(`dashboard.${rootState.profile_id}`).listen('Dashboard\\Reports\\IncidentEvent', e => {
                commit('SET_INCIDENTS', e.data);
            })
        },
    }
}