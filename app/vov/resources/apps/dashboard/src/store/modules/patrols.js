import facility from './patrols/facility'
import Vue from 'vue'

export default {
    namespaced: true,
    state: {
        list: [],
        facilities: []
    },
    mutations: {
        SET_LIST (state, value) {
            state.list = value
        },
        SET_FACILITIES (state, value) {
            state.facilities = value
        }
    },
    actions: {
        init ({ commit, rootState }) {
            rootState.overlay = true
            Vue.axios.get('/api/facilities').then(response => {
                commit('SET_FACILITIES', response.data)
                rootState.overlay = false
            }).catch(err => console.error(err))
        }
    },
    modules: {
        facility
    }
}
