import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import ru from 'vuetify/es5/locale/ru'
import he from 'vuetify/es5/locale/he'
import en from 'vuetify/es5/locale/en'
import app_icons from '../ui/icons/app_icons'

const lang = document.querySelector('html').lang || 'en'
Vue.use(Vuetify)
const light = {
    primary: '#607d8b',
    secondary: '#673ab7',
    accent: '#00bcd4',
    error: '#ff5722',
    warning: '#ffc107',
    info: '#03a9f4',
    success: '#8bc34a'
}
export default new Vuetify({
    rtl: lang === 'he',
    theme: {
        options: {
            customProperties: !window.MSInputMethodContext
        },
        themes: {
            light
        }
    },
    icons: {
        values: app_icons
    },
    lang: {
        locales: { ru, en, he },
        current: lang
    }
})
