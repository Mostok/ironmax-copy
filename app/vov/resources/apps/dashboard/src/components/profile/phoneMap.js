import importAll from './importAll'
// todo[daglob] 08.12.2019 16:50: https://www.npmjs.com/package/i18n-iso-countries  - может быть когда-нибудь
const phoneMap = importAll(require.context('./', false, /phoneMap\.json$/))[
    'phoneMap'
    ]
export default phoneMap
