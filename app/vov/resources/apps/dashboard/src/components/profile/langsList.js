import importAll from './importAll'

const langsList = importAll(require.context('./', false, /langsList\.json$/))[
    'langsList'
    ]
export default langsList
