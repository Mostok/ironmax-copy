const importAll = r => {
    const data = {}
    r.keys().forEach(key => {
        const matched = key.match(/([A-Za-z0-9-_]+)\./i)
        if (matched && matched.length > 1) {
            data[matched[1]] = r(key)
        }
    })
    return data
}
// https://datahub.io/core/country-codes#data-cli
export default importAll
