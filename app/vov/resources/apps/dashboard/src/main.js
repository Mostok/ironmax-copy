import '@babel/polyfill'
import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import i18n from './i18n'
import vuetify from './plugins/vuetify'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import VueTheMask from 'vue-the-mask'
import Echo from 'laravel-echo'
import axios from './axios'
import VueAxios from 'vue-axios'
import LogRocket from 'logrocket'
import EventBus from './plugins/event_bus'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
    load: {
        // region :    'RU',
        language: i18n.locale,
        key: 'AIzaSyDtMf_JidX-0U3PCkTYP1FivwRlZnkzbXo',
        libraries: 'places' // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)
        //// If you want to set the version, you can do so:
        // v: '3.26',
    },
    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    // autobindAllEvents: false,
    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then disable the following:
    installComponents: true
})

Vue.use(EventBus)

if (process.env.NODE_ENV === 'production'){
    LogRocket.init('cltbq7/ironmax')
}

Vue.use(VueAxios, axios)

window.Pusher = require('pusher-js')

/** @see app/vov/node_modules/pusher-js/types/src/core/options.d.ts tats all options
 *
 * @type {Echo}
 */

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.VUE_APP_PUSHER_APP_KEY,
    wsHost: process.env.VUE_APP_WEBSOCKETS_DOMAIN,
    wsPort: process.env.VUE_APP_WEBSOCKETS_PORT,
    wssPort: process.env.VUE_APP_WEBSOCKETS_PORT_WSS,
    enabledTransports: process.env.VUE_APP_PUSHER_ENABLED_TRANSPORTS.split(','),
    enableStats: false,
    ignoreNullOrigin: true,
    useTLS: false,
    forceTLS: process.env.VUE_APP_PUSHER_FORCE_TLS === 'true'
})

router.beforeEach((to, from, next) => {
    if (to.path === '/') {
        location.href = '/'
    }
    document.title = 'IRONMAX USER PANEL: ' + i18n.t(to.name)
    next()
})
setTimeout(() => {
    location.href = "/logout";
}, 5 * 60 * 60 * 1000);
Vue.use(VueTheMask)

Vue.config.productionTip = false

store.dispatch("initApplication");

new Vue({
    store,
    router,
    i18n,
    vuetify,
    render: h => h(App)
}).$mount('#app')
