import Vue from 'vue'

/**
 * @package downloadFile
 */
export default {
    data: () => ({
        isLoad: false,
        isError: false,
        isComplete: false,
        isFinished: false
    }),
    methods: {
        /**
         * Метод загрузки файла по коду
         * @param id
         * @param beforeCallback
         * @param afterCallback
         */
        async download(id, beforeCallback, afterCallback) {
            this.isLoad = true
            typeof beforeCallback === 'function' ? beforeCallback() : null
            await Vue.axios.get(`/api/files/${id}`, {
                responseType: 'arraybuffer'
            }).then(response => {
                this.isLoad = false
                this.isFinished = this.isComplete = true
                const filename = response.headers[
                    'content-disposition'
                    ].split('filename=')[1]
                const blob = new Blob([response.data], {type: 'application/pdf'});
                if (window.navigator.msSaveOrOpenBlob) {
                    // IE11
                    window.navigator.msSaveBlob(blob, filename);
                } else {
                    const file = window.URL.createObjectURL(blob)
                    const a = document.createElement('a')
                    a.href = file
                    a.download = filename
                    // we need to append the element to the dom -> otherwise it will not work in firefox
                    document.body.appendChild(a)
                    a.click()
                    a.remove()
                }


                typeof afterCallback === 'function'
                    ? afterCallback()
                    : null
            }).catch(() => {
                this.isLoad = false
                this.isFinished = this.isError = true
                this.$store.commit('ADD_ERROR', {
                    color: 'red',
                    text: this.$i18n.t('errors.get_file')
                })
            })
        },
        /**
         * Метод открытия файла в новой вкладке по коду
         * @param id
         */
        async open(id) {
            this.isLoad = true
            await Vue.axios.get(`/api/files/${id}`, {
                responseType: 'arraybuffer'
            }).then(response => {
                this.isLoad = false
                this.isFinished = this.isComplete = true

                const blob = new Blob([response.data], {type: 'application/pdf'});
                if (window.navigator.msSaveOrOpenBlob) {
                    const filename = response.headers[
                        'content-disposition'
                        ].split('filename=')[1]
                    // IE11
                    window.navigator.msSaveOrOpenBlob(blob, filename);
                } else {
                    window.open(
                        URL.createObjectURL(blob)
                    )
                }

            }).catch(() => {
                this.isLoad = false
                this.isFinished = this.isError = true
                this.$store.commit('ADD_ERROR', {
                    color: 'red',
                    text: this.$i18n.t('errors.get_file')
                })
            })
        }
    }
}
