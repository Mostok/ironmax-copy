import moment from 'moment'
import i18n from '../i18n'

const lang_def = i18n.locale | 'en'
const lang_weekdays = {
    en: [
        'sundays',
        'mondays',
        'tuesdays',
        'wednesdays',
        'thursdays',
        'fridays',
        'saturdays'
    ],
    ru: [
        'mondays',
        'tuesdays',
        'wednesdays',
        'thursdays',
        'fridays',
        'saturdays',
        'sundays'
    ],
    he: [
        'sundays',
        'mondays',
        'tuesdays',
        'wednesdays',
        'thursdays',
        'fridays',
        'saturdays'
    ]
}
const lang_date_format = {
    en: 'YYYY-MM-DD',
    ru: 'YYYY-MM-DD',
    he: 'DD/MM/YYYY'
}
const lang_date_time_format = {
    en: 'YYYY-MM-DD HH:mm:ss',
    ru: 'YYYY-MM-DD HH:mm:ss',
    he: 'HH:mm:ss DD/MM/YYYY'
}
const lang_short_date_time_format = {
    en: 'YYYY-MM-DD HH:mm',
    ru: 'YYYY-MM-DD HH:mm',
    he: 'HH:mm DD/MM/YYYY'
}
const lang_full_date_time_format = {
    en: 'HH:mm:ss, DD MMMM YYYY, dddd',
    ru: 'HH:mm:ss, DD MMMM YYYY, dddd',
    he: 'יום dddd, DD בMMMM YYYY, HH:mm:ss'
}

export const sort_weekdays = (lang = lang_def) => lang_weekdays[lang]
export const format_date = (strDate, lang = lang_def) =>
    moment(strDate).locale(lang).format(lang_date_format[lang])
export const format_date_time = (strDate, lang = lang_def) =>
    moment(strDate).locale(lang).format(lang_date_time_format[lang])
export const format_short_date_time = (strDate, lang = lang_def) =>
    moment(strDate).locale(lang).format(lang_short_date_time_format[lang])
export const format_full_date_time = (strDate, lang = lang_def) =>
    moment(strDate).locale(lang).format(lang_full_date_time_format[lang])
