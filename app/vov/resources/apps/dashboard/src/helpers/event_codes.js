/** emergency */
const patrol_emergency = 's_em';
const patrol_incident = 's_in';
const patrol_mistake_time = 's_mt_t';
const patrol_mistake_day = 's_mt_d';
const patrol_cheat = 's_ch';
const time_block_less = 's_tb_ls';
const time_block_empty = 's_tb_em';
const patrol_barcodes = 's_bc_ls';

const all = [
    patrol_emergency,
    patrol_incident,
    patrol_mistake_time,
    patrol_mistake_day,
    patrol_cheat,
    time_block_less,
    time_block_empty,
    patrol_barcodes
]

const hidden = [patrol_mistake_time, patrol_mistake_day]
const visible = all.filter(c => !hidden.includes(c))

const critical = [patrol_emergency, patrol_incident]
const warn = all.filter(c => visible.includes(c) && !critical.includes(c))

const report = [patrol_barcodes, patrol_cheat, patrol_mistake_day, patrol_mistake_time]
const facility = [time_block_empty, time_block_less]

const by_type = {report, facility}

export default {
    all,
    hidden,
    visible,
    critical,
    warn,
    by_type
}

