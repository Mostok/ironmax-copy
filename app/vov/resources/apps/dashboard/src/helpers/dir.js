export const wrap = (dir, str) => {
    if (dir === 'rtl') return '\u202B' + str + '\u202C';
    return '\u202A' + str + '\u202C';
}
