import axios from 'axios'

axios.interceptors.response.use(
    function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        return response
    },
    function (error) {
        // TODO[DaGlob] 27.06.2020 21:07: return Promise.reject(error)
        if (error.response.status > 399 && error.response.status < 500 && error.response.status !== 422) {
            location.href = "/logout";
            return null
        }
        return Promise.reject(error)
    }
)
export default axios
