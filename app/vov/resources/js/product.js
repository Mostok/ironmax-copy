
// require('./wowslider_engine')
// require('./wowslider')
$('[data-accordion-controler=\'expand\']').on('click', function () {
    toggle_accordion($('[data-accordion-trigger][aria-expanded="false"]'))
})
$('[data-accordion-controler=\'collapse\']').on('click', function () {
    toggle_accordion($('[data-accordion-trigger][aria-expanded="true"]'))
})
$('[data-accordion-trigger]').on('click', function () {
    toggle_accordion($(this))
})

function toggle_accordion (el) {
    el.next().toggle()
    el.attr('aria-expanded', function (index, attr) {
        return attr === 'true' ? 'false' : 'true'
    })
    el.next().attr('data-accordion-state', function (index, attr) {
        return attr === 'expanded' ? 'collapsed' : 'expanded'
    })
    el.next().attr('aria-hidden', function (index, attr) {
        return attr === 'true' ? 'false' : 'true'
    })
}
