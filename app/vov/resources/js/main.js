import "slick-carousel";
import { animateScroll } from "./libs";
require("./bootstrap");
require("jquery-validation");
require("./scrollHandler");

const loginForm = $("#login-form");
const forgotForm = $("#forgot-form");
const error_text = $("#error_text");
const complete_forgot = $("#complete");
complete_forgot.hide(0);

$(function() {
    // Слайдер логотипов партнеров на главной странице
    $("#partner_logo").slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    // Слайдер отзывы клиентов партнеров на главной странице
    $(".client-slider").slick({
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        responsive: [{
                breakpoint: 1024,
                settings: {}
            },
            {
                breakpoint: 600,
                settings: {}
            },
            {
                breakpoint: 480,
                settings: {}
            }
        ]
    });

    loginForm.validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                maxlength: 191
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $("#contact-form").validate({
        ignore: ".ignore",
        rules: {
            name: {
                required: true,
                maxlength: 191
            },
            email: {
                required: true,
                email: true
            },
            subject: {
                required: true,
                maxlength: 191
            },
            message: {
                required: true,
                maxlength: 5000
            }
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            if (crecapture) {
                $.ajax({
                    type: "POST",
                    url: "/action/sendmessage",
                    data: $(form).serialize(),
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    timeout: 3000,
                    success: function() {
                        $("#contact-form").hide();
                        $("#contact-success").addClass("d-flex");
                        setTimeout(function() {
                            $("form input").val("");
                            $("form textarea").val("");
                            grecaptcha.reset(grecapturecontact);
                            $("#contact-form").show();
                            $("#contact-success").removeClass("d-flex");
                        }, 4000);
                    },
                    error: function() {
                        $("#alert-error").show();
                    }
                });
            }
            return false;
        }
    });
    $(".closeAlert").on("click", function() {
        $(".alert-wrapper").hide();
    });
});
/** Elements */
const header = $("header");
const myBtn1 = document.getElementById("myBtn1");
myBtn1.style.transition = "300ms all";
const toTopBtn = $("#toTopBtn");

$(document).ready(function() {
    $(document).click(function(event) {
        var clickover = $(event.target);
        let opened = $(".navbar-toggler.hamburger").attr("aria-expanded");
        if (opened == "true" && !clickover.is("#navbarDropdownFAQ")) {
            $("button.navbar-toggler")
                .get(0)
                .click();
            $("button.navbar-toggler").addClass("is-active");
        } else if (opened == "false") {
            $("button.navbar-toggler").removeClass("is-active");
        }
        if ($("button.navbar-toggler").hasClass("collapsed")) {
            $("button.navbar-toggler").removeClass("is-active");
        }
    });
});

$(window).on({
    scroll() {
        if (window.pageYOffset > 200) {
            header.addClass("moved");
            myBtn1.style.bottom = "110px";
            toTopBtn.stop().fadeTo(100, 0.7);
        } else {
            header.removeClass("moved");
            myBtn1.style.bottom = "20px";
            toTopBtn
                .stop()
                .fadeTo(100, 0)
                .hide();
        }
    },
    load() {
        $(".preloaderLogo").fadeOut("slow");
        // кнопка "верх"
        $(".moveup").on("click", function() {
            const target = $("#about-us");
            if (target.length) {
                animateScroll(target.offset().top - header.height(), 500);
            }
        });
        // при загрузке окна браузера скролл на элемент с id из хеша, если есть таковой
        if ("" !== window.location.hash) {
            const target = $(window.location.hash);
            if (target.length) {
                //animateScroll(target.offset().top - header.height(), 500)
            }
        }
        // Мягкий скролл к элементу по ИД
        $('[data-scroll-to^="#"]').on({
            click() {
                const $link = $(this);
                $link
                    .parents(".navbar-nav")
                    .find(".dropdown-menu.show-menu")
                    .removeClass("show-menu");
                if ($link.hasClass("dropdown-toggle")) {
                    $link.next().addClass("show-menu");
                }

                const anchor =
                    "#" +
                    $(this)
                    .data("scrollTo")
                    .split("#")[1];
                const target = $(anchor);
                if (target.length) {
                    animateScroll(target.offset().top - 100, 500);
                    // location.hash = anchor
                }
            }
        });

        $(".testfade").fadeIn("slow");
        toTopBtn.on("click", function(e) {
            e.preventDefault();
            animateScroll();
        });

        $(".modal").on({
            "show.bs.modal": () => {
                $("html").css("overflow-y", "hidden");
            },
            "hide.bs.modal": () => {
                $("html").css("overflow-y", "scroll");
            }
        });

        const loginPopUp = $("#loginPopUp");
        console.log(loginPopUp);
        loginPopUp.on({
            "show.bs.modal": () => {
                console.log("oooon");
                loginPopUp.find('[name="password"]').val("");
            }
        });

        $("#forgotPopUp").on({
            "show.bs.modal": () => {
                forgotForm.show(0);
                complete_forgot.hide(0);
            }
        });

        forgotForm.on({
            submit(e) {
                e.preventDefault();
                error_text.html("");
                complete_forgot.hide(0);
                axios
                    .post(forgotForm.attr("action"), forgotForm.serialize())
                    .then(() => {
                        forgotForm.hide(0);
                        complete_forgot.show(0);
                    })
                    .catch(error => {
                        if (error.response.status === 422) {
                            const errors = error.response.data.errors;
                            const txt = Object.keys(errors)
                                .reduce(
                                    (list, key) => (
                                        list.push(...errors[key]), list
                                    ), []
                                )
                                .join("<br/>");
                            error_text.html(txt);
                        }
                        if (error.response.status === 500) {
                            error_text.html("system error");
                            complete_forgot.hide(0);
                        }
                    });
            }
        });
        const pass_error_text = $("#pass_error_text");
        pass_error_text.css({ color: "red" });
        $("#resetPassword").on({
            submit(e) {
                e.preventDefault();
                const resetPasswordForm = $("#resetPasswordForm");
                pass_error_text.html("");
                axios
                    .post("/reset_password", resetPasswordForm.serialize())
                    .then(response => {
                        location.replace("/");
                    })
                    .catch(error => {
                        if (error.response.status === 422) {
                            const erros = error.response.data.errors;
                            const txt = Object.keys(erros)
                                .reduce(
                                    (list, key) => (
                                        list.push(...erros[key]), list
                                    ), []
                                )
                                .join("<br/>");
                            pass_error_text.html(txt);
                        }
                    });
            }
        });
    }
});