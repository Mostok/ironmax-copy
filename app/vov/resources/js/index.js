// import counterUp from 'counterup2'
import { CountUp } from 'countup.js';

require('waypoints/lib/noframework.waypoints.js')
const swiper = require('swiper/swiper-bundle.min.js')
const el = document.querySelectorAll('.counter')

const options = {
    duration: 1,
    useEasing: false,
    separator: ''
};

if (el.length > 0) {
    [...el].forEach(counter => {
        new Waypoint({
            element: counter,
            handler: function () {
                let demo = new CountUp(counter, Number(counter.getAttribute('data-number')), options);
                if (!demo.error) {
                    demo.start();
                } else {
                    console.error(demo.error);
                }
                this.destroy();
            },
            offset: 'bottom-in-view'
        })
    })
}

$(function () {
    // Основной слайдер на главной странице
    new swiper('.swiper-container', {
        effect: 'fade',
        // autoHeight: true,
        preventInteractionOnTransition: true,
        followFinger: false,
        allowTouchMove: true,
        fadeEffect: {
            crossFade: true
        },
        speed: 2000,
        loop: true,
        autoplay: {
            delay: 5000
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        on: {
            slideChangeTransitionStart() {
                // Уход со слайда: анимация заголовков слайда
                const slide = $(this.slides[this.previousIndex])
                slide.find('.txt-wrap h1').
                    addClass('slideOutDown').
                    removeClass('slideInUp').
                    delay(500).
                    hide(0)
                slide.find('.txt-wrap h5').
                    addClass('slideOutDown').
                    removeClass('slideInUp').
                    delay(500).
                    hide(0)
            },
            slideChangeTransitionEnd() {
                // Вход на слайд: анимация заголовков слайда
                const slide = $(this.slides[this.activeIndex])
                slide.find('.txt-wrap h1').
                    addClass('slideInUp').
                    removeClass('slideOutDown').
                    show(0)
                slide.find('.txt-wrap h5').
                    addClass('slideInUp').
                    removeClass('slideOutDown').
                    delay(500).
                    show(0)
            }
        }
    })
})
