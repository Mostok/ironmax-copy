const messages = {
  ru : {
    $vuetify : {
      login : {
        title :         'Вход',
        loginLabel :    'Введите адрес эл.почты',
        passwordLabel : 'Введите пароль',
        rememberMe :    'Запомнить меня',
        errors :        {
          required :    'Поле обязательно для заполнения',
          emailFormat : 'Укажите корректный формат адреса эл.почты'
        },
        cancel :        'Отмена',
        ok :            'Войти'
      }
    }
  },
  en : {
    $vuetify : {
      login : {
        title :         'Login',
        loginLabel :    '',
        passwordLabel : '',
        rememberMe :    '',
        errors :        {
          required :    '',
          emailFormat : ''
        },
        cancel :        '',
        ok :            ''
      }
    }
  },
  he : {
    $vuetify : {
      login : {
        title :         'התחבר',
        loginLabel :    '',
        passwordLabel : '',
        rememberMe :    '',
        errors :        {
          required :    '',
          emailFormat : ''
        },
        cancel :        '',
        ok :            ''
      }
    }
  }
}
export default messages