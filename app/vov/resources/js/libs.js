import $ from 'jquery'

const html                 = $('html, body')
export const animateScroll = (top, ms) => {
  html.stop()
      .animate({
                 scrollTop : top || 0
               }, ms || 1000, 'swing')
}
