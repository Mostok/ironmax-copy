/*!
 * scrollHandler jQuery plugin
 *
 * © DaGlob 09.04.2020
 * @author: DaGlob <daglob@mail.ru>
 * @link: https://daglob.ru
 * @licenses
 * Copyright (c) 2020 Daniel Konev (nickname & TM: DaGlob) and contributors
 *
 * Released under the MIT license
 */
/**
 *
 * use scss for visible change
 ///////////////////////////////////////////////
 .in_view {
    background-color: orange;

    // &.went_top_top {
    //     background-color: orangered;
    // }
    //
    // &.went_bottom_bottom {
    //     background-color: darkred;
    // }

    &.full_in_view {
        background-color: green;
    }

    &.full_view {
        background-color: cornflowerblue;
    }
}
 ///////////////////////////////////////////////
 */
(function ($) {
  /**
   *
   * @param {Object=} options
   * @param {String=} [options.triggerPrefix=sh] - префикс имен триггеров
   * @param {String=} [options.topLineBlockKey=tl] - ключ имени в формируемом названии триггера, связанный с верхней
   * границей элемента
   * @param {String=} [options.bottomLineBlockKey=bl] - ключ имени в формируемом названии триггера, связанный с нижней
   * границей элемента
   * @param {String=} [options.allBlockKey=all] - ключ имени в формируемом названии триггера, связанный с полность.
   * попавшем во вьюборд эелементом
   * @param {Object=} options.classes
   * @param {String=} [options.classes.in_view=in_view] - виден на экране
   * @param {String=} [options.classes.full_view=full_view] - занимает 100% экрана
   * @param {String=} [options.classes.full_in_view=full_in_view] - полностью поместился в экране
   * @param {String=} [options.classes.went_top_top=went_top_top] - верхняя граница вышла за верх экрана
   * @param {String=} [options.classes.went_bottom_bottom=went_bottom_bottom] - нижняя граница вышла за низ экрана
   * @param {Array|null=} [options.share=[50]] - список процентных долей [20, 70] - разделит экран на 3 горизонтальные
   * доли: 0-20,
   * 20-70, 70-100, проходя которые будет сробатывать триггер на вход и выход для верхней и нижних границ: <ol>
   *   <li>**sh:tl:0:20:in** - верхняя граница блока вошла в диапазон 0-20</li>
   *   <li>**sh:bl:70:100:out** - нижняя граница блока вышла из дипазона 70-100</li>
   *   <li>**sh:all:0:80:in** - элемент полностью вошел в дипазон 0-80</li>
   * </ol>
   * __установите значение в null, если хотите отключить данный функционал__
   *
   * @returns {*}
   */
  jQuery.fn.scrollHandler = function (options) {
    options         = $.extend({
                                 triggerPrefix :      'sh',
                                 topLineBlockKey :    'tl',
                                 bottomLineBlockKey : 'bl',
                                 allBlockKey :        'all',
                                 classes :            {
                                   in_view :      'in_view',
                                   full_view :    'full_view',
                                   full_in_view : 'full_in_view'
                                   // went_top_top :       'went_top_top',
                                   // went_bottom_bottom : 'went_bottom_bottom'
                                 },
                                 share :              [50]
                               }, options)
    const share     = options.share || [],
          win       = $(window),
          key       = options.triggerPrefix || 'sh',
          tl        = options.topLineBlockKey || 'tl',
          bl        = options.bottomLineBlockKey || 'bl',
          all       = options.allBlockKey || 'all'
    let top_line    = 0,
        bottom_line = window.innerHeight
    win.on('resize', () => bottom_line = window.innerHeight)
    let triggerList = []
    if (Array.isArray(share) && 0 < share.length) {
      const makeTriggerName = (a, b, c) => {
        return b === c.length - 1 ? undefined : [a, c[b + 1]]
      }
      share.sort((a, b) => a - b)
      0 > share.indexOf(0) ? share.unshift(0) : null
      0 > share.indexOf(100) ? share.push(100) : null
      triggerList = [
        ...share.map((a, b, c) => makeTriggerName(a, b, c))
      ].filter(v => typeof v !== 'undefined')
    }
    return this.each(function () {
      const el             = $(this),
            wrap_class_job = (className, add, hard) => {
              add !== false ?
              !el.hasClass(className) || hard ? el.addClass(className).trigger(`${key}:${className}`) : null :
              el.hasClass(className) || hard ? el.removeClass(className).trigger(`${key}:${className}:out`) : null
            },
            classClean     = () => {
              el.removeClass(...Object.values(options.classes))
            }
      let prevStatusShare  = []
      win.on({
               scroll : function () {
                 const el_top    = el.offset().top - window.pageYOffset
                 const el_bottom = el.offset().top + el.height() - window.pageYOffset
                 if (el_top < el_bottom && top_line < bottom_line) {
                   if (el_top < bottom_line - 1 && el_bottom >= top_line) {
                     const full_view    = el_top <= top_line && el_bottom >= bottom_line
                     const full_in_view = el_top >= top_line && el_bottom <= bottom_line
                     wrap_class_job(options.classes.in_view)
                     wrap_class_job(options.classes.full_in_view, full_in_view)
                     // wrap_class_job(options.classes.went_top_top, !full_in_view && el_top < top_line)
                     // wrap_class_job(options.classes.went_bottom_bottom, !full_in_view && el_bottom > bottom_line)
                     wrap_class_job(options.classes.full_view, full_view)
                     if (!full_view) {
                       let t_top_line, t_bottom_line
                       triggerList.forEach((a, b, c) => {
                         t_top_line    = bottom_line * a[0] / 100
                         t_bottom_line = bottom_line * a[1] / 100
                         /** top line */
                         const t_cod   = `${key}:${tl}:${a[0]}:${a[1]}`,
                               b_cod   = `${key}:${bl}:${a[0]}:${a[1]}`,
                               a_cod   = `${key}:${all}:${a[0]}:${a[1]}`
                         if (el_top >= t_top_line && el_top < t_bottom_line &&
                         (typeof prevStatusShare[t_cod] === 'undefined' || prevStatusShare[t_cod] === false)) {
                           el.trigger(`${t_cod}:in`)
                           prevStatusShare[t_cod] = true
                         }
                         if ((el_top < t_top_line || el_top >= t_bottom_line) &&
                         (typeof prevStatusShare[t_cod] !== 'undefined' && prevStatusShare[t_cod] === true)) {
                           el.trigger(`${t_cod}:out`)
                           prevStatusShare[t_cod] = false
                         }
                         if (el_bottom > t_top_line && el_bottom <= t_bottom_line &&
                         (typeof prevStatusShare[b_cod] === 'undefined' || prevStatusShare[b_cod] === false)) {
                           el.trigger(`${b_cod}:in`)
                           prevStatusShare[b_cod] = true
                         }
                         if ((el_bottom < t_top_line || el_bottom >= t_bottom_line) &&
                         (typeof prevStatusShare[b_cod] !== 'undefined' && prevStatusShare[b_cod] === true)) {
                           el.trigger(`${b_cod}:out`)
                           prevStatusShare[b_cod] = false
                         }
                         if (prevStatusShare[t_cod] && prevStatusShare[b_cod] &&
                         (typeof prevStatusShare[a_cod] === 'undefined' || prevStatusShare[a_cod] === false)) {
                           el.trigger(`${a_cod}:in`)
                           prevStatusShare[a_cod] = true
                         }
                         if ((!prevStatusShare[t_cod] || !prevStatusShare[b_cod]) &&
                         (typeof prevStatusShare[a_cod] !== 'undefined' && prevStatusShare[a_cod] === true)) {
                           el.trigger(`${a_cod}:out`)
                           prevStatusShare[a_cod] = false
                         }
                       })
                     }
                   } else {
                     wrap_class_job(options.classes.in_view, false)
                     prevStatusShare = []
                     classClean()
                   }
                 }
               },
               load() {
                 wrap_class_job(options.classes.in_view, false, true)
                 prevStatusShare = []
                 classClean()
               }
             })
    })
  }
})(jQuery)