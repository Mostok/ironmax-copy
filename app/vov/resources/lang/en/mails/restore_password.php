<?php
return [
    'body'           => "Hello, :name,<br/>We`ve received a request to reset your password. If you didn`t make the request, just ignore this email. Otherwise, you can reset your password using this link:",
    'reset_btn_text' => "Click here to reset your password",
    "thank_you"      => "Thank you,",
    "subject"        => "Reset Password Notification",
    "preview"        => "Password change request from https://ironmax.tech/",
    "title"          => "Change Password",
];
