<?php
return [
    'subject' => 'Daily report',
    'table_header_date_time' => 'Date/Time',
    'table_header_exception' => 'Exception',
    'title' => 'Your daily exceptions report is ready',
    'preview' => 'Your daily exceptions report is ready',
    'invitation_to_dashboard' => 'For more information enter your dashboard by the following link:',
    'body' => 'Hello, :name,<br/>Your daily exceptions report is ready',
    'thank_you' => 'Thank you,',
    'events' => [
        's_tb_ls' => '{0} null|{1} :num patrol has not been done at<br/>":facility"|[2,*] :num patrols have not been done at<br/>":facility"',
    ]
];
