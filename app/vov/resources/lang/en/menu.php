<?php
return [
    'home'        => 'Home', // HOME
    'products'    => 'Products', // PRODUCTS
    'patrol'      => 'Patrol', // VAR21
    'sos'         => 'SOS', // VAR23
    'mini_guard'  => 'Miniguard', // VAR22
    'shifter'     => 'Shifter', // VAR20
    'gate_keeper' => 'Gatekeeper', // VAR24
    'digital_id'  => 'Digital ID', // VAR25
    'contact'     => 'Contact', // CONTACT
    'faqs'        => 'FAQ', // FAQs
    'login'       => 'Login', // LOGIN
    'logout'      => 'Logout',
    'lang'        => 'Language',
    'dashboard'   => 'Dashboard',
    'admin_panel' => 'Panel',
];
