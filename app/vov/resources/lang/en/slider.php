<?php
$br = '<br class="d-block d-lg-none"/><span class="d-none d-lg-inline">&nbsp;</span>';

return [
    'title_1'    => 'IRONMAX PATROL',
    'subtitle_1' => 'Keep your facility safe',
    'title_2'    => 'IRONMAX SOS',
    'subtitle_2' => 'Call for help within a second',
    'title_3'    => 'IRONMAX GateKeeper',
    'subtitle_3' => "Modern way to manage{$br}your facility visitors entries",
    'title_4'    => 'IRONMAX Digital ID',
    'subtitle_4' => "Provide the digital id`s{$br}for the facility visitors",
    'title_5'    => 'IRONMAX MiniGuard',
    'subtitle_5' => "Get an alert when{$br}your facility in danger",
    'title_6'    => 'IRONMAX Shifter',
    'subtitle_6' => 'Manage your work place shifts',
];
