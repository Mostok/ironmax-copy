<?php
return [
    "s_bc_ls" => "On facility \":facility\"<br>not all points were visited",
    "s_ch" => "An attempted deception event occurred at the \":facility\"<br> facility",
    "s_em" => "Emergency event has occured at the <br>\":facility\" facility",
    "s_in" =>  "Incident event has occured at the <br>\":facility\" facility",
    "s_mt_d" => "On facility \":facility\"<br>the patrol was accomplished on a non-working day",
    "s_mt_t" => "On facility \":facility\"<br>the patrol was accomplished at the wrong time",
    "s_tb_em" => "The patrol wasn't accomplished at the <br>\":facility\" facility",
    "s_tb_ls" => "On facility \":facility\"<br>not all the patrols were accomplished",
];
