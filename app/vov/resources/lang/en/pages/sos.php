<?php
return [
    'title' => 'SOS',
    'points' => [
        'A combination of an application and a hardware device that communicate over the Internet and together form an emergency System.',
        'Instantly send an emergency alert (up to 3 seconds) directly to the smartphone on which the app is installed.',
        'Direct navigation to the location of the incident where the emergency event was triggered.',
        'Option to link multiple smartphones to a single emergency button.',
        'Option to link multiple emergency buttons to a single smartphone.',
        'Recent history of the emergency events and their details including locations.',
        'Easy and quick installation (no need for infrastructure and cables deployment)',
    ],
    'faq' => [
        'header' => 'Frequently Asked Questions',
        'blocks' => [
            [
                'question' => 'How do I sign up for a service?',
                'answer' => 'The registration for a service is done in three simple steps:<br>
                                    1) To get the SOS device/ devices contact us by email at support@ironmax.tech<br>
                                    2) Download the "IRONMAX SOS" app from Google Play market.<br>
                                    3) Create a new user account. When first time getting into the app you will be
                                    redirected to the login screen.',
            ],
            [
                'question' => 'I want to link several SOS devices to my account. Is this possible?',
                'answer' => 'Yes.<br>
All you have to do is to contact us for the desired number of devices and we will
take care of the rest.<br>
In the event of activating SOS by pressing the SOS button, phones that linked to the
SOS device will be notified with siren and message including the location of the SOS
device and additional data.',
            ],
            [
                'question' => 'I want to connect a single SOS device to a number of smartphones. Is this possible?',
                'answer' => 'Yes.<br>
Simply login to the same account on each of the smartphones. Automatically all of
your smartphones will be linked to the same SOS device.',
            ],
            [
                'question' => 'I changed the name of the Wi-Fi network that SOS device was
                                connected to or I moved the SOS device to another place where the Wi-Fi network name is
                                different from the network to which the device was previously connected to, what should
                                I do?',
                'answer' => 'Because the name of the Wi-Fi network is different, the SOS device will not be able
to connect to the network, in this case the SOS device is programmed to open a
secure access point. So via this access point device can be connected from any
computer or smart device. Just select the new network name to which the alarm device
will be connected.<br>
To get the password to connect to the access point, you must identify yourself with
IRONMAX customer service (support@ironmax.tech).',
            ],
            [
                'question' => 'I forgot the password for changing application’s settings. What should I do?',
                'answer' => 'In case of password loss please contact us by email at support@ironmax.tech.',
            ],
        ],
    ],
];






