<?php
return [
    'title' => 'Patrol',
    'points' => [
        'Monitoring patrol by scanning barcodes located at key points throughout the facility.',
        'Full documentation about time and location of the security guard after barcode has been scanned.',
        'Ability to add notes and forms to fill out that will be displayed to the security guard right after barcode has been scanned.',
        'Receiving notifications and reports (emergency/patrol/incident) in the user dashboard',
        'Possibility for creating a special report about the unusual activity in the facility can be made by the security guard.',
        'Monitoring patrol by using phone location.',
        'Emergency button for making an emergency call to a predefined number.',
        'Monitoring the guards’ shifts and producing a monthly shifts summary report.',
    ],
    'faq' => [
        'header' => 'Frequently Asked Questions',
        'blocks' => [
            [
                'question' => 'How do I sign up for a service?',
                'answer' => '1) For the “facility code” contact us by email at support@ironmax.tech.<br>
2) Enter the “facility code” and login to the app.',
            ],
            [
                'question' => 'How do I create barcodes for one of the facilities?',
                'answer' => 'It’s simple:<br>Use the “Barcode generator” tool from your dashboard.',
            ],
            [
                'question' => 'How do I view reports issued by the app?',
                'answer' => 'All reports (emergency/incident/patrol) are always available on your dashboard. If you want, we can send them directly to your Email.',
            ],
            [
                'question' => 'What is the difference between "regular” and "special" barcodes?',
                'answer' => 'Regular barcodes contain the point information unique for each barcode. Whereas in
special barcodes in addition to the above information also questions / comments for
the security guard are included in the barcode.<br>
The security guard\'s answers for the provided questions and images attached by the
guard will be added to the patrol’s summary report.',
            ],
            [
                'question' => 'How can I be sure that all the facility barcodes have been scanned by security guard during the patrol?',
                'answer' => 'If the security guard cheated and scanned the
                                    barcodes from the same place then the following note will appear: "Guard tried to
                                    cheat" next to the patrol report.
                                    You can check for more details by clicking on the open/download button of the
                                    matched patrol report.
                                    (You can always use the app version that based on the device location)',
            ],
            [
                'question' => 'How do I know that security guard did not made whole the patrol?',
                'answer' => 'You will receive notification for the exception in your dashboard. Additionally you can check this in the exceptions category in your dashboard.',
            ],
            [
                'question' => 'I forgot the password for changing application’s settings. What should I do?',
                'answer' => 'In case of password loss please contact us by email at support@ironmax.tech.',
            ],
        ],
    ],
];






