<?php
return [
    'title' => 'Shifter',
    'points' => [
        'This application will help you to manage and track employee’s shifts.',
        'Generate a summary monthly shifts report for each of the employee’s.',
        'Option for editing employee’s data via IRONMAX website.',
    ],
    'faq' => [
        'header' => 'Frequently Asked Questions',
        'blocks' => [
            [
                'question' => 'When shifts summary report will be send to me?',
                'answer' => 'On the first day of each month you will receive a detailed shifts summary report
                                    where all the shifts of the previous month for each of the company\'s employees will
                                    be represented.',
            ],
            [
                'question' => 'Can the shifts summary report be edited during the month?',
                'answer' => 'Yes.<br>
Each employee\'s summary report is sent to the Employee Manager\'s email on 1st of
each month and can also be edited in the Employee Manager\'s personal panel. To get
there Employee Manager should login to the IRONMAX Web site.',
            ],
            [
                'question' => 'New employees joined the company. How do I add them to the list
                                of employees?',
                'answer' => 'You can edit the list of employees who appear in the app by clicking the "Contacts"
                                    icon that locates at the top of the app\'s main screen.',
            ],
            [
                'question' => 'The number of employees working at the same shift is increased / decreased. How can I change it in the app as well?',
                'answer' => 'You can edit the number of employees working on the same shift by logging in to the
app’s settings and changing the "number of positions”.<br>
If you have been notified that you have exceeded the maximum number of employees
allowed in your program, please contact us by email at support@ironmax.tech.',
            ],
            [
                'question' => 'I forgot the password for changing application’s settings. What should I do?',
                'answer' => 'In case of password loss please contact us by email at support@ironmax.tech.',
            ],
        ],
    ],
];






