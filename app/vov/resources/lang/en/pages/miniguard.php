<?php
return [
    'title' => 'Miniguard',
    'points' => [
        'A combination of an application and a hardware device that communicate over the Internet and together form a silent alarm system.',
        'The hardware device that can be installed on the door / window / safe-box or anything else that can be opened. In case of burglar alarm system is triggered in the application on mobile phone only and thus the burglar does not know that he was caught.',
        'Instantly send a burglar alert (up to 3 seconds) directly to the smartphone on which the app is installed.',
        'Set the alarm systems operating hours through the application’s preferences.',
        'The hardware device is about the size of a matchbox and can be hidden even in the small places.',
        'Option to link multiple smartphones to a single Miniguard alarm system.',
        'Option to link multiple Miniguard alarm systems to a single smartphone.',
        'Option to place Miniguard device inside the drawer / cash desk / safe-box (when it connected to rechargeable battery).',
    ],
    'faq' => [
        'header' => 'Frequently Asked Questions',
        'blocks' => [
            [
                'question' => 'How do I sign up for a service?',
                'answer' => 'The registration for a service is done in three simple steps:<br>1) To get the alarm
device(s) contact us by email at support@ironmax.tech<br>
2) Download the "IRONMAX MiniGuard" app from Google Play market.<br>
3) Create a new user account. When first time getting into the app you will be
redirected to the login screen.',
            ],
            [
                'question' => 'I want to link several alarm devices to my account. Is this possible?',
                'answer' => 'Yes.<br>
All you have to do is to contact us for the desired number of devices and we will
take care of the rest.<br>
In the event of a burglary to one of the facilities where the Miniguard alarm
devices are installed, you will be notified of that case via Miniguard app including
information about the place location where hacked Miniguard device is installed.',
            ],
            [
                'question' => 'I want to connect a single alarm device to a number of smartphones. Is this possible?',
                'answer' => 'Yes.
<br> Simply login to the same account on each of the smartphones. Automatically all of your smartphones will be linked to the same alarm device.',
            ],
            [
                'question' => 'How to setup the alarm device work mode or specific hours?',
                'answer' => 'The way the alarm device will work can be setup or changed through the app settings,
where two possible modes of operation are offered:<br>
1) “Set time mode" – setting the exact time when alarm will work (specific days and
hours of activity) at which the alarm device will be active, and also alert in the
event of a burglary will be send.<br>
2) "Full control" - turns the device on and off at the push of the button when
needed.',
            ],
            [
                'question' => 'I changed the name of the Wi-Fi network that the alarm device
                                was connected to or I moved the alarm device to another place where the Wi-Fi network
                                name is different from the network to which the device was previously connected to, what
                                should I do?',
                'answer' => 'Because the name of the Wi-Fi network is different, the alarm device will not be able
to connect to the network, in this case the alarm device is programmed to open a
secure access point. So via this access point device can be connected from any
computer or smart device. Just select the new network name to which the alarm device
will be connected.<br>
To get the password for connection to the access point, you must identify yourself
with IRONMAX customer service (support@ironmax.tech).',
            ],
            [
                'question' => 'I forgot the password for changing application’s settings. What should I do?',
                'answer' => 'In case of password loss please contact us by email at support@ironmax.tech.',
            ],
        ],
    ],
];






