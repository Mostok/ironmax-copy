<?php
return [
    'title'  => 'Gatekeeper and Digital ID',
    'points' => [
        'Real-time tracking of the facility’s visitors',
        'Add, remove and edit entry permissions through the manager`s personal dashboard',
        'Checking the entry permission by scanning the driver`s license or any other ID',
        'Checking the entry permission by scanning a digital certificate that is issued by the IRONMAX Digital ID application in a secured way. So ID cannot be transferred to someone else or falsified',
        'Checking the entry permission by manually entered ID',
    ],
    'faq'    => [
        'header' => 'Frequently Asked Questions',
        'blocks' => [
            [
                'question' => 'How do i sign up for a service?',
                'answer'   => 'Registering for the service is completed in three simple steps:<br/>
1) To add the site’s visitors and update the company dashboard information - contact us by the following email: support@ironmax.tech<br/>
2) Download the "IRONMAX GateKeeper" application from the Google Play store<br/>
3) Create a company account via application’s login screen and click on login button<hr/>
<small>* If you would like to issue the digital certificate ID’s for the site, please contact us at email support@ironmax.tech</small>',
            ],
            [
                'question' => 'How do I update my Visitors List?',
                'answer'   => 'Just login to the dashboard of the IRONMAX site and add / remove / edit the list of the site’s visitors.',
            ],
            [
                'question' => 'How do I prevent the outdated visitors from entering the site?',
                'answer'   => 'When filling in the visitor\'s ID details, you can also add validity to the ID.<br/>
The application checks permission’s validity for each of the visitors.<br/>
In addition, the visitor\'s permission for entering the site can always be removed from the list of visitors through the company’s dashboard',
            ],
            [
                'question' => 'I have decided to use the IRONMAX DIGITAL ID application to generate the digital ID’s for the site I manage.
How can I be sure that digital ID’s are not passed to someone else or copied?',
                'answer'   => 'The integration between the IRONMAX GateKeeper and IRONMAX Digital ID software systems is designed so that the system cannot be "cheated" by copying or transferring the digital ID certificate.<br/>
For reasons of confidentiality and security, we do not explain on how it exactly works, but we would certainly love to demonstrate its reliability
',
            ],
        ],
    ],
];






