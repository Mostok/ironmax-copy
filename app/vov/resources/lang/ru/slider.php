<?php
$br = '<br class="d-block d-lg-none"/><span class="d-none d-lg-inline">&nbsp;</span>';

return [
    'title_1'    => 'СИСТЕМА IRONMAX ПАТРУЛЬ',
    'subtitle_1' => 'Создайте безопасность на вашем объекте',
    'title_2'    => 'СИСТЕМА ТРЕВОГИ IRONMAX СОС',
    'subtitle_2' => 'Вызов помощи в течение секунды ',
    'title_3'    => "СИСТЕМА{$br}ПРОВЕРКИ ПОСЕТИТЕЛЕЙ{$br}ГЕЙТКИПЕР",
    'subtitle_3' => 'Современный менеджмент посетителей',
    'title_4'    => 'IRONMAX ЦИФРОВОЙ ПАССПОРТ',
    'subtitle_4' => "Цифровые пасспорта{$br}для посетителей объекта",
    'title_5'    => "СИГНАЛЬНАЯ СИСТЕМА{$br}IRONMAX МИНИГАРД",
    'subtitle_5' => 'Мгновенное уведомление в случии взлома',
    'title_6'    => "IRONAX СИСТЕМА{$br}УПРАВЛЕНИЯ СМЕНАМИ",
    'subtitle_6' => 'Управляйте сменами ваших работников',
];
