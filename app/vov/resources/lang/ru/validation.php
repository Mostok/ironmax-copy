<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'accepted'             => ':attribute должен быть принят.',
    'active_url'           => ':attribute недействительный URL.',
    'after'                => ':attribute должен быть датой после :date.',
    'after_or_equal'       => ':attribute должен быть датой после или равная :date.',
    'alpha'                => ':attribute может содержать только буквы.',
    'alpha_dash'           => ':attribute может содержать только буквы, цифры, тире и подчеркивания.',
    'alpha_num'            => ':attribute может содержать только буквы и цифры.',
    'array'                => ':attribute должен быть массивом.',
    'before'               => ':attribute должен быть датой до :date.',
    'before_or_equal'      => ':attribute должен быть датой до или равна :date.',
    'between'              => [
        'numeric' => ':attribute должен быть между :min и :max.',
        'file'    => ':attribute должен быть между :min и :max килобайтами.',
        'string'  => ':attribute должен быть между :min и :max символами.',
        'array'   => ':attribute должен быть между :min и :max предметами.',
    ],
    'boolean'              => ':attribute поле должно быть истинным или ложным.',
    'confirmed'            => ':attribute подтверждение атрибута не совпадает.',
    'date'                 => ':attribute это недействительная дата.',
    'date_equals'          => ':attribute должен быть датой, равной :date.',
    'date_format'          => ':attribute не соответствует формату :format.',
    'different'            => ':attribute и :other должны быть разными.',
    'digits'               => ':attribute должен быть :digits цифрами.',
    'digits_between'       => ':attribute должен быть между :min и :max цифрами.',
    'dimensions'           => ':attribute имеет недопустимые размеры изображения.',
    'distinct'             => ':attribute поле имеет повторяющееся значение.',
    'email'                => ':attribute адрес эл. почты должен быть действительным.',
    'ends_with'            => ':attribute должен заканчиваться одним из следующих: :values',
    'exists'               => 'выбранный :attribute является недействительным.',
    'file'                 => ':attribute должен быть файлом.',
    'filled'               => ':attribute поле должно иметь значение.',
    'gt'                   => [
        'numeric' => ':attribute должен быть больше чем :value.',
        'file'    => ':attribute должен быть больше чем :value килобайтов.',
        'string'  => ':attribute должен быть больше чем :value символов.',
        'array'   => ':attribute должен быть больше чем :value предметов.',
    ],
    'gte'                  => [
        'numeric' => ':attribute должен быть больше или равен :value.',
        'file'    => ':attribute должен быть больше или равен :value килобайтов.',
        'string'  => ':attribute должен быть больше или равен :value символов.',
        'array'   => ':attribute должен иметь количество :value или больше.',
    ],
    'image'                => ':attribute должно быть изображением.',
    'in'                   => ' выбранный :attribute является недействительным.',
    'in_array'             => ':attribute поле не существует в :other.',
    'integer'              => ':attribute должен быть целым числом.',
    'ip'                   => ':attribute должен быть действительный IP-адресом.',
    'ipv4'                 => ':attribute должен быть действительным адресом IPv4.',
    'ipv6'                 => ':attribute должен быть действительным адресом IPv6.',
    'json'                 => ':attribute должна быть допустимой строкой JSON.',
    'lt'                   => [
        'numeric' => ':attribute должно быть меньше чем :value.',
        'file'    => ':attribute должен быть больше чем :value килобайтов.',
        'string'  => ':attribute должен быть больше чем :value символов.',
        'array'   => ':attribute должно быть меньше чем :value предметов.',
    ],
    'lte'                  => [
        'numeric' => ':attribute должен быть больше чем or equal :value.',
        'file'    => ':attribute должен быть больше чем or equal :value килобайтов.',
        'string'  => ':attribute должен быть больше чем or equal :value символов.',
        'array'   => ':attribute должно быть больше чем :value предметов.',
    ],
    'max'                  => [
        'numeric' => ':attribute не может быть больше чем :max.',
        'file'    => ':attribute не может быть больше чем :max килобайтов.',
        'string'  => ':attribute не может быть больше чем :max символов.',
        'array'   => ':attribute не может иметь больше, чем :max предметов.',
    ],
    'mimes'                => ':attribute должен быть файлом типа: :values.',
    'mimetypes'            => ':attribute должен быть файлом типа: :values.',
    'min'                  => [
        'numeric' => ':attribute должен быть не менее :min.',
        'file'    => ':attribute должен быть не менее :min килобайтов.',
        'string'  => ':attribute должен быть не менее :min символов.',
        'array'   => ':attribute должен иметь по крайней мере :min предметов.',
    ],
    'not_in'               => 'Выбранный :attribute является недействительным.',
    'not_regex'            => ':attribute неверный формат.',
    'numeric'              => ':attribute должен быть числом.',
    'present'              => 'Поле :attribute должно присутствовать.',
    'regex'                => ':attribute неверный формат.',
    'required'             => 'Поле :attribute обязательно для заполнения.',
    'required_if'          => 'Поле :attribute требуется, когда :other яаляется :value.',
    'required_unless'      => 'Поле :attribute требуется, если :other находится в :values.',
    'required_with'        => 'Поле :attribute требуется, если :values присутствует.',
    'required_with_all'    => 'Поле :attribute требуется, если :values присутствуют.',
    'required_without'     => 'Поле :attribute требуется, если :values не присутствуют.',
    'required_without_all' => 'Поле :attribute требуется, если ни один из :values не присутствует.',
    'same'                 => ':attribute и :other должны соответствовать.',
    'size'                 => [
        'numeric' => ':attribute должен быть :size.',
        'file'    => ':attribute должен быть :size килобайтов.',
        'string'  => ':attribute должен быть :size символов.',
        'array'   => ':attribute должен содержать :size предметов.',
    ],
    'starts_with'          => ':attribute должен начинаться с одного из следующих: :values',
    'string'               => ':attribute должен быть текстом.',
    'timezone'             => ':attribute должен быть действительной зоной.',
    'unique'               => ':attribute уже использовуется.',
    'uploaded'             => ':attribute не удалось загрузить.',
    'url'                  => ':attribute неверный формат.',
    'uuid'                 => ':attribute должен быть действительным UUID.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom'               => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */
    'attributes'           => [],
];
