<?php
return [
    'home'        => 'Главная', // HOME
    'products'    => 'Продукты', // PRODUCTS
    'patrol'      => 'Патруль', // VAR21
    'sos'         => 'СОС', // VAR23
    'mini_guard'  => 'Минигард', // VAR22
    'shifter'     => 'Шифтер', // VAR20
    'gate_keeper' => 'Гейткипер', // VAR24
    'digital_id'  => 'Цифровой ID', // VAR25
    'contact'     => 'Контакты', // CONTACT
    'faqs'        => 'Частые вопросы', // FAQs
    'login'       => 'Вход', // LOGIN
    'logout'      => 'Выход',
    'lang'        => 'Выбрать язык',
    'dashboard'   => 'Личный кабинет',
    'admin_panel' => 'Админ. панель',
];
