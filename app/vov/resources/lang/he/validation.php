<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'accepted'             => ':attribute חייב להתקבל.',
    'active_url'           => ':attribute כתובת אינטרנט לא תקינה.',
    'after'                => ':attribute חייב להיות לאחר התאריך :date.',
    'after_or_equal'       => ':attribute חייב להיות לאחר או באותו התאריך :date.',
    'alpha'                => ':attribute צריך להכיל אותיות בלבד.',
    'alpha_dash'           => ':attribute צריך להכיל אותיות, מספרים, קו תחתון ומקו בלבד.',
    'alpha_num'            => ':attribute צריך להכיל אותיות ומספרים בלבד.',
    'array'                => 'The :attribute must be an array.',
    'before'               => ':attribute חייב להיות לפני התאריך :date.',
    'before_or_equal'      => ':attribute חייב להיות לפני או באותו התאריך :date.',
    'between'              => [
        'numeric' => ':attribute חייב להיות בין :min ל :max.',
        'file'    => ':attribute חייב להיות בין :min ל :max קילו-בתים.',
        'string'  => ':attribute חייב להיות בין :min ל :max תווים.',
        'array'   => ':attribute חייב להיות בין :min ל :max פריטים.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => ':attribute האישור לא מתאים.',
    'date'                 => ':attribute הוא לא תאריך תקין.',
    'date_equals'          => ':attribute חייב להיות תאריך זהה לתאריך :date.',
    'date_format'          => ':attribute לא מתאים לתבנית :format.',
    'different'            => ':attribute ו- :other חייבים להיות שונים.',
    'digits'               => ':attribute חייב להיות בעל :digits ספרות.',
    'digits_between'       => ':attribute חייב להכיל בין :min ל :max ספרות.',
    'dimensions'           => ':attribute בעל מימדי תמונה לא חוקיים.',
    'distinct'             => ':attribute מכיל ערך משוכפל.',
    'email'                => ':attribute חייב להיות כתובת דואר אלקטרוני תקינה.',
    'ends_with'            => ':attribute חייב להסתיים באחד מהבאים: :values',
    'exists'               => ':attribute לא חוקי.',
    'file'                 => ':attribute חייב להיות קובץ.',
    'filled'               => ':attribute חייב להכיל ערך.',
    'gt'                   => [
        'numeric' => ':attribute חייב להיות גדול מ :value.',
        'file'    => ':attribute חייב להיות גדול מ :value קילו-בתים.',
        'string'  => ':attribute חייב להיות גדול מ :value תווים.',
        'array'   => ':attribute חייב להיות בעל יותר מ :value פריטים.',
    ],
    'gte'                  => [
        'numeric' => ':attribute חייב להיות גדול או שווה ל :value.',
        'file'    => ':attribute חייב להיות בגודל גדול או שווה ל :value קילו-בתים.',
        'string'  => ':attribute חייב להיות גדול או שווה ל :value תווים.',
        'array'   => ':attribute חייב להיות בעל :value פריטים או יותר.',
    ],
    'image'                => ':attribute חייב להיות תמונה.',
    'in'                   => ':attribute לא חוקי.',
    'in_array'             => ':attribute לא קיים ב:other.',
    'integer'              => ':attribute חייב להיות מספר שלם.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'lt'                   => [
        'numeric' => ':attribute חייב להיות פחות מ :value.',
        'file'    => ':attribute חייב להיות פחות מ :value קילו-בתים.',
        'string'  => ':attribute חייב להיות פחות מ :value תווים.',
        'array'   => ':attribute חייב להיות בעל פחות מ :value פריטים.',
    ],
    'lte'                  => [
        'numeric' => ':attribute חייב להיות קטן או שווה ל :value.',
        'file'    => ':attribute חייב להיות קטן או שווה ל :value קילו-בתים.',
        'string'  => ':attribute חייב להיות קטן או שווה ל :value תווים.',
        'array'   => ':attribute -אסור שיהיו יותר מ :value פריטים.',
    ],
    'max'                  => [
        'numeric' => ':attribute צריך להיות קטן או שווה ל- :max.',
        'file'    => ':attribute צריך להיות בגודל קטן או שווה ל- :max kilobytes.',
        'string'  => ':attribute צריך להיות באורך קטן או שווה ל :max תווים.',
        'array'   => ':attribute צריך להיות בעל פחות מ :max פריטים.',
    ],
    'mimes'                => ':attribute חייב להיות קובץ מסוג: :values.',
    'mimetypes'            => ':attribute חייב להיות קובץ מסוג: :values.',
    'min'                  => [
        'numeric' => ':attribute חייב להיות לפחות :min.',
        'file'    => ':attribute חייב להיות לפחות בגודל :min קילו-בתים.',
        'string'  => ':attribute חייב להכיל לפחות :min תווים.',
        'array'   => ':attribute חייב להכיל לפחות :min פריטים.',
    ],
    'not_in'               => ':attribute הנבחר אינו חוקי.',
    'not_regex'            => ':attribute פורמט לא חוקי.',
    'numeric'              => ':attribute חייב להיות מספר.',
    'present'              => ':attribute הינו שדה שחייב להיות מוצג.',
    'regex'                => ':attribute בעל פורמט לא תקין.',
    'required'             => ':attribute הינו שדה חובה.',
    'required_if'          => ':attribute הינו שדה חובה כאשר :other הוא :value.',
    'required_unless'      => ':attribute הינו שדה חובה, אלא אם כן :other הוא ב :values.',
    'required_with'        => ':attribute הינו שדה חובה כאשר :values מוצג.',
    'required_with_all'    => ':attribute הינו שדה חובה כאשר :values מוצגים.',
    'required_without'     => ':attribute הוא שדה חובה כאשר :values לא מוצג.',
    'required_without_all' => ':attribute הוא שדה חובה כאשר אף אחד מה- :values לא מוצגים.',
    'same'                 => ':attribute ו- :other חייבים להתאים.',
    'size'                 => [
        'numeric' => ':attribute חייב להיות :size.',
        'file'    => ':attribute  חייב להיות בגודל :size קילו-בתים.',
        'string'  => ':attribute חייב להכיל :size תווים.',
        'array'   => ':attribute חייב להכיל :size פריטים.',
    ],
    'starts_with'          => ':attribute חייב להתחיל באחד מהבאים: :values',
    'string'               => 'The :attribute must be a string.',
    'timezone'             => ':attribute חייב להיות איזור זמן תקין.',
    'unique'               => ':attribute כבר נבחר קודם.',
    'uploaded'             => ':attribute נכשל בהעלאה.',
    'url'                  => ':attribute בעל פורמט לא תקין.',
    'uuid'                 => 'The :attribute must be a valid UUID.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom'               => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */
    'attributes'           => [],
];
