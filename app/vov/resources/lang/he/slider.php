<?php
$br = '<br class="d-block d-lg-none"/><span class="d-none d-lg-inline">&nbsp;</span>';

return [
    'title_1'    => 'IRONMAX PATROL',
    'subtitle_1' => 'מערכת תוכנה למעקב וניהול מערך האבטחה במתקן',
    'title_2'    => 'IRONMAX SOS',
    'subtitle_2' => 'לחצן מצוקה ואפליקציה לזיהוי וטיפול באירועי חירום',
    'title_3'    => 'IRONMAX GateKeeper',
    'subtitle_3' => 'מערכת תוכנה לניהול כניסת מבקרים למתקנים',
    'title_4'    => 'IRONMAX Digital ID',
    'subtitle_4' => 'אפליקציה להנפקת תעודות דיגיטליות',
    'title_5'    => 'IRONMAX MiniGuard',
    'subtitle_5' => 'מערכת אזעקה שקטה עם התראה בזמן אמת',
    'title_6'    => 'IRONMAX Shifter',
    'subtitle_6' => 'אפליקציה לניהול ומעקב אחר משמרות העובדים',
];
