<?php
return [
    'subject' => 'דוח חריגות יומי',
    'table_header_date_time' => 'תאריך ושעה',
    'table_header_exception' => 'חריגה',
    'title' => 'דוח החריגות היומי מוכן לצפייה',
    'preview' => 'דוח החריגות היומי מוכן לצפייה',
    'invitation_to_dashboard' => 'למידע נוסף היכנס לאיזור האישי על ידי לחיצה על הקישור הבא:',
    'body' => 'שלום :name,<br/> דוח החריגות היומי במתקניך מוכן לצפייה',
    'thank_you' => 'תודה,',
    'events' => [
        's_tb_ls' => '{0} null|{1} :num סיור לא התבצע במתקן <br/>":facility"|[2,*] :num סיורים לא התבצעו במתקן <br/>":facility"',
    ]
];
