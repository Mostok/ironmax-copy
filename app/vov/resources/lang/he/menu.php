<?php
return [
    'home'        => 'ראשי', // HOME
    'products'    => 'מוצרים', // PRODUCTS
    'patrol'      => 'Patrol', // VAR21
    'sos'         => 'SOS', // VAR23
    'mini_guard'  => 'Miniguard', // VAR22
    'shifter'     => 'Shifter', // VAR20
    'gate_keeper' => 'Gatekeeper', // VAR24
    'digital_id'  => 'Digital ID', // VAR25
    'contact'     => 'צור קשר', // CONTACT
    'faqs'        => 'שאלות נפוצות', // FAQs
    'login'       => 'התחבר', // LOGIN
    'logout'      => 'התנתק',
    'lang'        => 'שפה',
    'dashboard'   => 'לאיזור האישי',
    'admin_panel' => 'הגדרות מנהל',
];
