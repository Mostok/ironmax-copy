<!DOCTYPE html>
<html lang={{auth()->user()->getAttribute('language') ?? app()->getLocale()}}>
<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <meta name=token content="{{$token}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name=logo content="{{$logo}}">
    <link rel=icon href={{asset('images/fav-icon/icon1.png')}}>
    <title>IRONMAX USER PANEL</title>
    <link href="{{mix('/css/font.css')}}" rel=preload as=style>
    <link href="{{mix('/css/font.css')}}" rel=stylesheet>
    <link href="{{mix('/js/main.js')}}" rel=preload as=script>
</head>
<body>
<noscript><strong>We're sorry but ironmax-vm doesn't work properly without JavaScript enabled.
        Please enable it to continue.</strong></noscript>
<div id=app></div>
<script src="{{mix('/js/main.js')}}"></script>
@if(config('app.env') == 'local')
    <script src="http://localhost:35729/livereload.js"></script>
@endif
</body>
</html>

