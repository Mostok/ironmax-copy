<!DOCTYPE html>
<html lang="en" style="overflow: hidden;">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IRONMAX</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="{{asset('images/fav-icon/icon1.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animat.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/msdropdown/sample.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/msdropdown/dd.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/msdropdown/flags.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/msdropdown/skin2.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/msdropdown/sprite.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-datepicker.min.css')}}">
    <style type="text/css">
        .arrow {
            text-align: center;
            margin: 8% 0;
        }

        .bounce {
            -webkit-animation: bounce 4s infinite;
            animation: bounce 4s infinite;
        }

        @-webkit-keyframes bounce {
            0%,
            20%,
            50%,
            80%,
            100% {
                -webkit-transform: translateY(0);
                transform: translateY(0);
            }
            60% {
                -webkit-transform: translateY(+10px);
                transform: translateY(+10px);
            }
        }

        @keyframes bounce {
            0%,
            20%,
            50%,
            80%,
            100% {
                -webkit-transform: translateY(0);
                transform: translateY(0);
            }
            60% {
                -webkit-transform: translateY(+10px);
                transform: translateY(+10px);
            }
        }
    </style>
    <!-- Fix Internet Explorer ______________________________________-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="vendor/html5shiv.js"></script>
    <script src="vendor/respond.js"></script>
    <![endif]-->
    <style type="text/css">
        body {
            font-family: 'Nunito Sans', sans-serif;
            overflow-x: hidden;
        }

        h2 {
            color: #000;
            font-size: 26px;
            font-weight: 300;
            text-align: center;
            text-transform: uppercase;
            position: relative;
            margin: 30px 0 70px;
        }

        h2::after {
            content: "";
            width: 100px;
            position: absolute;
            margin: 0 auto;
            height: 4px;
            border-radius: 1px;
            left: 0;
            right: 0;
            bottom: -20px;
        }

        .carousel {
            margin: 50px auto;
            padding: 0 70px;
        }

        .carousel .item {
            color: #999;
            overflow: hidden;
            min-height: 120px;
            font-size: 13px;
        }

        .carousel .media img {
            width: 80px;
            height: 80px;
            display: block;
            border-radius: 50%;
        }

        .carousel .testimonial {
            padding: 0 15px 0 60px;
            position: relative;
        }

        .carousel .testimonial::before {
            content: '\93';
            color: #e2e2e2;
            font-weight: bold;
            font-size: 68px;
            line-height: 54px;
            position: absolute;
            left: 15px;
            top: 0;
        }

        .carousel .overview b {
            text-transform: uppercase;
            color: #1c47e3;
        }

        .carousel .carousel-indicators {
            bottom: -40px;
        }

        .carousel-indicators li, .carousel-indicators li.active {
            width: 18px;
            height: 18px;
            border-radius: 50%;
            margin: 1px 3px;
        }

        .carousel-indicators li {
            background: #e2e2e2;
            border: 4px solid #fff;
        }

        .carousel-indicators li.active {
            color: #fff;
            background: #1c47e3;
            border: 5px double;
        }

        .theme-main-menu .navbar-nav > li.active > a {
            color: #97b4e4;
        }
    </style>
    <style>
        .fa-login {
            font-size: 33px;
            display: block;
            margin-top: -22.5px;
        }

        .login-icon {
            width: 18px;
            display: block;
            margin-left: 1.5px;
            margin-top: -26px;
        }

        .login-icon.arrow {
            margin-top: -16px;
            position: absolute;
            transition: .3s;
        }

        .log:hover .login-link .login-icon.arrow {
            margin-left: 13px;
            transition: .3s;
        }

        .num_count {
            width: 24%;
            float: left;
            text-align: center;
        }

        .num_count1 {
            padding: 70px;
            padding-bottom: 160px;
        }

        #service1 {
            width: 28%;
            margin-left: 31px;
            margin-right: 31px;
        }

        @media screen and (max-width: 600px) {
            .num_count {
                width: 100%;
                text-align: center;
                padding-bottom: 50px;
            }

            .num_count1 {
                padding-bottom: 600px;
            }

            #service1 {
                width: 100%;
                margin-bottom: 10px;
                margin-left: 0px;
                margin-right: 0px;
            }

            .containervid {
                float: unset;
                margin-left: 0px;
                padding-right: 0px;
                margin-bottom: 40px;
                padding-left: 0px;
            }

            /* Modal Content/Box */
            .containervid {
                float: unset !important;
                margin-right: 0px !important;
            }

            #partner-section .item {
                border: none;
                margin-right: 0px;
            }
        }

        @media screen and (min-width: 600px) {
            .containervid {
                height: 400px;
                width: 600px;
                float: right;
                margin-left: 50px;
            }

            /* Modal Content/Box */
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
    <script type="text/javascript">//paste this code under the head tag or in a separate js file.
    // Wait for window load
    $(window).load(function () {
      // Animate loader off screen
      $("html").css("overflow", "visible")
      // $("html").removeClass("notvisible");
      //  $("html").addClass("svisible");
      $(".theme-main-header").fadeIn("slow")
    })</script>
    <style>
        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .preloaderLogo {
            display: flex;
            align-items: center;
            justify-content: center;
            
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #FFFFFF;
        }
    </style>
    <style>
        /* Full-width input fields */
        @if(App::isLocale('he'))
input[type=text], input[type=password], input[type=email], textarea, select {
            width: 100%;
            border-radius: .3rem !important;
            font-size: 20px !important;
            padding: 5px 20px;
            margin: 0px;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
            direction: rtl;
        }

        #setpadd {
            padding-left: 17%;
        }

        h2, dt, dd {
            direction: rtl;
            margin-left: auto;
            margin-right: auto;
        }

        #CFont {
            direction: rtl;
        }

        span.psw a {
            direction: rtl;
        }

        @elseif(App::isLocale('ru'))
@media (max-width: 430px) {
            #decSize {
                font-size: 15px;
            }

            #decSize1 {
                font-size: 13px;
            }
        }

        input[type=text], input[type=password], input[type=email], textarea, select {
            width: 100%;
            border-radius: .3rem !important;
            font-size: 20px !important;
            padding: 5px 20px;
            margin: 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }

        @else
input[type=text], input[type=password], input[type=email], textarea, select {
            width: 100%;
            border-radius: .3rem !important;
            font-size: 20px !important;
            padding: 5px 20px;
            margin: 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }

        @endif
       /* Set a style for all buttons */
        button.mybutton {
            font-size: 22px;
            border-radius: 20px;
            background-color: #000000;
            color: white;
            padding: 8px 8px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        button.mybutton:hover {
            background-color: #97b4e4;
        }

        /* Extra styles for the cancel button */
        .cancelbtn {
            width: auto;
            padding: 10px 18px;
            background-color: #f44336;
        }

        /* Center the image and position the close button */
        .imgcontainer {
            text-align: center;
            padding: 10px 0 10px 0;
            position: relative;
            background-color: black;
            border-radius: 25px 25px 0px 0px;
        }

        .container1 {
            padding: 16px;
        }

        span.psw {
            float: right;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
            padding-top: 60px;
        }

        /* Add Zoom Animation */
        .animate {
            -webkit-animation: animatezoom 0.6s;
            animation: animatezoom 0.6s
        }

        @-webkit-keyframes animatezoom {
            from {
                -webkit-transform: scale(0)
            }
            to {
                -webkit-transform: scale(1)
            }
        }

        @keyframes animatezoom {
            from {
                transform: scale(0)
            }
            to {
                transform: scale(1)
            }
        }

        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
            span.psw {
                display: block;
                float: none;
            }

            .cancelbtn {
                width: 100%;
            }
        }

        .sign-in-1-img {
            position: absolute;
            right: -15px;
            top: -15px;
            z-index: 111;
            cursor: pointer;
        }
    </style>
    <style>
        .container7 {
            height: 200px;
            position: relative;
        }

        .vertical-center {
            margin: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }

        #user-dashboard button, #user-dashboard select {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
            border-radius: .3rem !important;
        }

        @media (min-width: 1500px) {
            input[type="text"], input[type="password"], input[type="email"], textarea, select {
                padding: 5px 20px !important;
            }
        }

        .download-btn {
            display: inline-block;
            width: 25%;
            margin-bottom: 20px;
        }

        select option:hover, select option:checked {
            background-color: #97b4e4 !important;
            box-shadow: 0 0 10px 100px #97b4e4 inset;
        }

        @media (max-width: 768px) {
            .download-btn {
                width: 49%;
            }
        }
    </style>
</head>
<body>
<div class="preloaderLogo">
    <svg id="preloader-spinner" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 61.5" height="61.5" width="120" ><g data-name="Layer 2"><path d="M60 58.5A60 60 0 010 0v1.5a60 60 0 00120 0V0a60 60 0 01-60 58.5z" data-name="Layer 1"/></g></svg>
    <svg id="preloader-logo" xmlns="http://www.w3.org/2000/svg" width="64" height="69.466"><g data-name="Group 55"><g data-name="Group 53"><path data-name="Path 107" d="M31.487 53.893s24.5-10.411 24.059-46.087c-2.2-2.01-22.928-15.954-47.55-.628 0 1.047-5.154 29.574 23.491 46.715z" fill="#5f5f5f"/><path data-name="Path 108" d="M31.497 51.274s22.344-9.495 21.941-42.032c-2.007-1.833-20.91-14.55-43.365-.573 0 .955-4.697 26.972 21.424 42.605z" fill="#fff"/></g><path data-name="Path 109" d="M32.127 39.827l10.163-2.956-.239-.5-10.457 3.455z" fill="#5f5f5f"/><path data-name="Path 110" d="M42.142 36.404l-10.163 2.954.239.5 10.457-3.455z" fill="#5f5f5f"/><path data-name="Path 111" d="M46.472 36.603l1.923-.963v-.649l-1.593.806z" fill="#5f5f5f"/><path data-name="Path 112" d="M9.585 7.647c-3.854 14.949 5.73 28.83 5.73 28.83l1.106.117a1.33 1.33 0 01.064-.15 1.273 1.273 0 01.882-2.144 1.3 1.3 0 01.452.078.958.958 0 01.37.156 7.138 7.138 0 011.272 1.22c.093.108.186.215.287.315.024.023.143.115.171.142a1.506 1.506 0 01.145.077 2.421 2.421 0 00.337.067 1.081 1.081 0 01.822.746l.633.067 1.318-12.374-2.277-2.41.309-3.541 1.194-.057-.419 2.374 2.638-.7.073-2.83 2.063-.353-.252 1.759.639.59 1.564-.511-.025-2.675 1.423-.5 3.183.419-.168 2.345 2.68.925-.072-.42 1.9.642-.064.36 2.135.44.006-.1.907.307.243-2.149.729.243.5 3.518-8.291-2.844-2.195.834 7.806 4.333 1.508 10.858 3.119 1.956 2.492-1.571s.085.022.233.068a1.274 1.274 0 01.252-.369 1.287 1.287 0 01.7-.348c.036-.013.072-.028.11-.038l.133-.018a5.321 5.321 0 01.758-.915 1.211 1.211 0 01.923-.355c3.778-8.448 4.893-22.148 4.893-24.4C32.533-6.673 10.801 6.894 9.585 7.647z" fill="#e7ebee"/><g data-name="Group 54"><path data-name="Path 113" d="M17.053 38.508l1.131-1.759 1.507 2.387 1.816-1.628-4.345-3.3z" fill="none"/><path data-name="Path 114" d="M29.363 18.849l-3.077 1v-2.073l-1.508.314v2.268l-3.077.817-.063-2.01-.314.063-.377 2.889.251.377 7.663-1.822-6.47 2.764.943 1.256-1.315 12.333.027.018a2.041 2.041 0 011.1-.417 14.231 14.231 0 001.319-1.256l2.01.879 4.02-4.459.059-15.388-1 .251z" fill="none"/><path data-name="Path 115" d="M45.642 40.206a8.585 8.585 0 00-.329-1.007l-.518 1.014z" fill="none"/><path data-name="Path 116" d="M10.073 8.669a40.73 40.73 0 005.472 26.824 26.75 26.75 0 001.633-1.947v.166l4.17 2.85 1.3-11.872-2.261-2.01.628-3.831 1.821-.565v2.074l1.57-.5v-2.2l2.638-.753v2.2l1.633-.691v-2.2l1.885-.628V6.792h.879v.943a5.459 5.459 0 012.387-.063v.753s1.884 1 4.4-1.13a4.983 4.983 0 01-4.522 3.894h-.754l-1.13.565s-.189 3.518-.126 3.832 1.822.879 1.822.879v2.01l2.135.691-.125-2.2 2.7.69v2.2l1.633.691v-2.2l1.822.566.565 3.58-9.242-2.135 7.914 3.58-1.005.754 1.256 10.615 3.2 1.57 2.826-1.256-.5.879-2.324 1.068-2.827-.942.5-.251-2.324-1.131-2.073 1.131-6.218-4.837-.251 6.03 1.57-2.2 2.135 3.2-.125.377-2.638.879s-.251.377.628.314 6.124-1.382 6.124-1.382l-1.476.943 7.286-.817.628.314s8.768-10.8 8.121-29.957c-22.399-15.571-43.36-.57-43.36-.57z" fill="none"/><path data-name="Path 117" d="M55.542 7.806S34.11-9.33 7.992 7.178c0 0-2.551 17.407 6.487 30.43a4.289 4.289 0 00.94-1.6 14.471 14.471 0 001.912 4.445l-.722.006 1.466 1.259c.323.534.544.877.544.877l.465-.011 12.348 10.605 12.382-11.158h.054l.046-.089 1.764-1.59a5.13 5.13 0 01.158 1.161c10.64-13.272 9.706-33.707 9.706-33.707zm-35.855 31.33l-1.508-2.386-1.13 1.758.108-4.3 4.345 3.3zm10.8-7.348l-4.02 4.459-2.01-.879a14.231 14.231 0 01-1.319 1.256 2.041 2.041 0 00-1.1.417l-.027-.018 1.315-12.333-.943-1.256 6.47-2.764-7.663 1.822-.251-.377.377-2.889.314-.063.063 2.01 3.077-.817v-2.261l1.508-.314v2.077l3.077-1 .189-2.2 1-.251zm14.823 7.411a8.585 8.585 0 01.329 1.007l-.847.007.518-1.014-.628-.314-7.286.817 1.476-.943s-5.245 1.319-6.124 1.382-.628-.314-.628-.314l2.638-.879.125-.377-2.135-3.2-1.57 2.2.251-6.03 6.218 4.837 2.073-1.131 2.324 1.131-.5.251 2.827.942 2.324-1.068.5-.879-2.826 1.256-3.2-1.57-1.256-10.615 1.005-.754-7.914-3.58 9.233 2.136-.565-3.58-1.822-.566v2.2l-1.633-.691v-2.2l-2.7-.69.125 2.2-2.135-.691v-2.021s-1.759-.565-1.822-.879.126-3.832.126-3.832l1.13-.565h.754a4.983 4.983 0 004.522-3.894c-2.512 2.135-4.4 1.13-4.4 1.13v-.753a5.459 5.459 0 00-2.387.063v-.943h-.879v8.794l-1.885.628v2.2l-1.633.691v-2.2l-2.638.753v2.2l-1.57.5v-2.072l-1.821.565-.628 3.831 2.261 2.01-1.3 11.872-4.17-2.85v-.166s-.753 1.005-1.633 1.947a40.73 40.73 0 01-5.472-26.824s20.961-15 43.365.573c.658 19.165-8.11 29.962-8.11 29.962z" fill="#5f5f5f"/></g><path data-name="Path 118" d="M14.436 36.373l1.507.523.235-1.1-1.454-.249z" fill="#5f5f5f"/><path data-name="Path 119" d="M21.365 39.624l3.405-3.251-.324 2.344 3.892-2.041-1.784 2.949-2.594.832z" fill="#fff"/></g><text transform="translate(0 66.466)" fill="#5f5f5f" font-size="12.32" font-family="CalistoMT, Calisto MT"><tspan x="0" y="0">IRONMAX</tspan></text></svg>
</div>
<div class="main-page-wrapper">
    <!--
            =============================================
                Theme Header
            ==============================================
            -->
	<?php
	$android = strpos( strtolower( $_SERVER['HTTP_USER_AGENT'] ), "mob" );
	if($android){
	?>
    <div class="preloaderLogo">
        <svg id="preloader-spinner" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 61.5" height="61.5" width="120" ><g data-name="Layer 2"><path d="M60 58.5A60 60 0 010 0v1.5a60 60 0 00120 0V0a60 60 0 01-60 58.5z" data-name="Layer 1"/></g></svg>
        <svg id="preloader-logo" xmlns="http://www.w3.org/2000/svg" width="64" height="69.466"><g data-name="Group 55"><g data-name="Group 53"><path data-name="Path 107" d="M31.487 53.893s24.5-10.411 24.059-46.087c-2.2-2.01-22.928-15.954-47.55-.628 0 1.047-5.154 29.574 23.491 46.715z" fill="#5f5f5f"/><path data-name="Path 108" d="M31.497 51.274s22.344-9.495 21.941-42.032c-2.007-1.833-20.91-14.55-43.365-.573 0 .955-4.697 26.972 21.424 42.605z" fill="#fff"/></g><path data-name="Path 109" d="M32.127 39.827l10.163-2.956-.239-.5-10.457 3.455z" fill="#5f5f5f"/><path data-name="Path 110" d="M42.142 36.404l-10.163 2.954.239.5 10.457-3.455z" fill="#5f5f5f"/><path data-name="Path 111" d="M46.472 36.603l1.923-.963v-.649l-1.593.806z" fill="#5f5f5f"/><path data-name="Path 112" d="M9.585 7.647c-3.854 14.949 5.73 28.83 5.73 28.83l1.106.117a1.33 1.33 0 01.064-.15 1.273 1.273 0 01.882-2.144 1.3 1.3 0 01.452.078.958.958 0 01.37.156 7.138 7.138 0 011.272 1.22c.093.108.186.215.287.315.024.023.143.115.171.142a1.506 1.506 0 01.145.077 2.421 2.421 0 00.337.067 1.081 1.081 0 01.822.746l.633.067 1.318-12.374-2.277-2.41.309-3.541 1.194-.057-.419 2.374 2.638-.7.073-2.83 2.063-.353-.252 1.759.639.59 1.564-.511-.025-2.675 1.423-.5 3.183.419-.168 2.345 2.68.925-.072-.42 1.9.642-.064.36 2.135.44.006-.1.907.307.243-2.149.729.243.5 3.518-8.291-2.844-2.195.834 7.806 4.333 1.508 10.858 3.119 1.956 2.492-1.571s.085.022.233.068a1.274 1.274 0 01.252-.369 1.287 1.287 0 01.7-.348c.036-.013.072-.028.11-.038l.133-.018a5.321 5.321 0 01.758-.915 1.211 1.211 0 01.923-.355c3.778-8.448 4.893-22.148 4.893-24.4C32.533-6.673 10.801 6.894 9.585 7.647z" fill="#e7ebee"/><g data-name="Group 54"><path data-name="Path 113" d="M17.053 38.508l1.131-1.759 1.507 2.387 1.816-1.628-4.345-3.3z" fill="none"/><path data-name="Path 114" d="M29.363 18.849l-3.077 1v-2.073l-1.508.314v2.268l-3.077.817-.063-2.01-.314.063-.377 2.889.251.377 7.663-1.822-6.47 2.764.943 1.256-1.315 12.333.027.018a2.041 2.041 0 011.1-.417 14.231 14.231 0 001.319-1.256l2.01.879 4.02-4.459.059-15.388-1 .251z" fill="none"/><path data-name="Path 115" d="M45.642 40.206a8.585 8.585 0 00-.329-1.007l-.518 1.014z" fill="none"/><path data-name="Path 116" d="M10.073 8.669a40.73 40.73 0 005.472 26.824 26.75 26.75 0 001.633-1.947v.166l4.17 2.85 1.3-11.872-2.261-2.01.628-3.831 1.821-.565v2.074l1.57-.5v-2.2l2.638-.753v2.2l1.633-.691v-2.2l1.885-.628V6.792h.879v.943a5.459 5.459 0 012.387-.063v.753s1.884 1 4.4-1.13a4.983 4.983 0 01-4.522 3.894h-.754l-1.13.565s-.189 3.518-.126 3.832 1.822.879 1.822.879v2.01l2.135.691-.125-2.2 2.7.69v2.2l1.633.691v-2.2l1.822.566.565 3.58-9.242-2.135 7.914 3.58-1.005.754 1.256 10.615 3.2 1.57 2.826-1.256-.5.879-2.324 1.068-2.827-.942.5-.251-2.324-1.131-2.073 1.131-6.218-4.837-.251 6.03 1.57-2.2 2.135 3.2-.125.377-2.638.879s-.251.377.628.314 6.124-1.382 6.124-1.382l-1.476.943 7.286-.817.628.314s8.768-10.8 8.121-29.957c-22.399-15.571-43.36-.57-43.36-.57z" fill="none"/><path data-name="Path 117" d="M55.542 7.806S34.11-9.33 7.992 7.178c0 0-2.551 17.407 6.487 30.43a4.289 4.289 0 00.94-1.6 14.471 14.471 0 001.912 4.445l-.722.006 1.466 1.259c.323.534.544.877.544.877l.465-.011 12.348 10.605 12.382-11.158h.054l.046-.089 1.764-1.59a5.13 5.13 0 01.158 1.161c10.64-13.272 9.706-33.707 9.706-33.707zm-35.855 31.33l-1.508-2.386-1.13 1.758.108-4.3 4.345 3.3zm10.8-7.348l-4.02 4.459-2.01-.879a14.231 14.231 0 01-1.319 1.256 2.041 2.041 0 00-1.1.417l-.027-.018 1.315-12.333-.943-1.256 6.47-2.764-7.663 1.822-.251-.377.377-2.889.314-.063.063 2.01 3.077-.817v-2.261l1.508-.314v2.077l3.077-1 .189-2.2 1-.251zm14.823 7.411a8.585 8.585 0 01.329 1.007l-.847.007.518-1.014-.628-.314-7.286.817 1.476-.943s-5.245 1.319-6.124 1.382-.628-.314-.628-.314l2.638-.879.125-.377-2.135-3.2-1.57 2.2.251-6.03 6.218 4.837 2.073-1.131 2.324 1.131-.5.251 2.827.942 2.324-1.068.5-.879-2.826 1.256-3.2-1.57-1.256-10.615 1.005-.754-7.914-3.58 9.233 2.136-.565-3.58-1.822-.566v2.2l-1.633-.691v-2.2l-2.7-.69.125 2.2-2.135-.691v-2.021s-1.759-.565-1.822-.879.126-3.832.126-3.832l1.13-.565h.754a4.983 4.983 0 004.522-3.894c-2.512 2.135-4.4 1.13-4.4 1.13v-.753a5.459 5.459 0 00-2.387.063v-.943h-.879v8.794l-1.885.628v2.2l-1.633.691v-2.2l-2.638.753v2.2l-1.57.5v-2.072l-1.821.565-.628 3.831 2.261 2.01-1.3 11.872-4.17-2.85v-.166s-.753 1.005-1.633 1.947a40.73 40.73 0 01-5.472-26.824s20.961-15 43.365.573c.658 19.165-8.11 29.962-8.11 29.962z" fill="#5f5f5f"/></g><path data-name="Path 118" d="M14.436 36.373l1.507.523.235-1.1-1.454-.249z" fill="#5f5f5f"/><path data-name="Path 119" d="M21.365 39.624l3.405-3.251-.324 2.344 3.892-2.041-1.784 2.949-2.594.832z" fill="#fff"/></g><text transform="translate(0 66.466)" fill="#5f5f5f" font-size="12.32" font-family="CalistoMT, Calisto MT"><tspan x="0" y="0">IRONMAX</tspan></text></svg>
    </div>
    <div class="main-page-wrapper">
        <!--
                    =============================================
                        Theme Header
                    ==============================================
                    -->
        <header style="display: none;" class="theme-main-header">
            <div class="container">
                <a style="padding-top: 6px;" href="{{asset('index')}}" class="logo float-left tran4s"><img
                            src="{{asset('images/logo/logo4.png')}}" style="width: 65px" alt="Logo"></a>
                <!-- ========================= Theme Feature Page Menu ======================= -->
                <nav class="navbar float-right theme-main-menu one-page-menu">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" id="btn-menu" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        {{-- LANGUAGE SELECTOR --}}
                        <div class="dropdown" style="float: left; margin-right: 25px;">
                            <a class="dropbtn" onclick="myFunction2()" href="#" id="navbardrop">
                                @if(App::isLocale('ru'))
                                    <img style="width: 26px;margin-top: 7px;"
                                         src="{{asset('images/flag/countries_russia.png')}}" width="40px" alt="">
                                @elseif(App::isLocale('he'))
                                    <img style="width: 26px;margin-top: 7px;"
                                         src="{{asset('images/flag/countries_israel.png')}}" width="40px" alt="">
                                @else
                                    <img style="width: 26px;margin-top: 7px;"
                                         src="{{asset('images/flag/countries_english.png')}}" width="40px" alt="">
                                @endif
                            </a>
                            <div id="dropdown-content" class="dropdown-content">
                                <a href="{{ route('search.data', 'en') }}" style="width: 110px;height: 30px"><p
                                            style="display: inline;    float: left;">&nbsp&nbsp&nbspEnglish &nbsp</p>
                                    <img
                                            style="    width: 30px;
    height: 30px;
    margin-top: -3px;
    margin-right: 8px;
    float: right;
    display: inline;" src="{{asset('images/flag/countries_english.png')}}"></a>
                                <a href="{{ route('search.data', 'he') }}" style="width: 110px;height: 30px"><p
                                            style="display: inline;    float: left;">&nbsp&nbsp&nbspעברית
                                        &nbsp&nbsp&nbsp&nbsp</p><img style="    width: 30px;
    height: 30px;
    margin-top: -3px;
    margin-right: 8px;
    float: right;
    display: inline;" src="{{asset('images/flag/countries_israel.png')}}"></a>
                                <a href="{{ route('search.data', 'ru') }}" style="width: 110px;height: 30px"><p
                                            style="display: inline;    float: left;">&nbsp&nbsp&nbspРусский
                                    </p><img style="    width: 30px;
    height: 30px;
    margin-top: -3px;
    margin-right: 8px;
    float: right;
    display: inline;" src="{{asset('images/flag/countries_russia.png')}}"></a>
                            </div>
                        </div>
                        {{-- END LANGUAGE SELECTOR --}}
                        @guest
                            <div class="log" style="float: left; cursor: pointer;"
                                 onclick="document.getElementById('loginPopUp').style.display='block'">
                                <div class="login"
                                     style="float: left;color: white;padding-top: 30px;padding-left: 5px ;font-weight: 600;font-size: 15px;margin-right: 5px;line-height: 22.5px;">{{__('header.LOGIN')}}</div>
                                <li style="display: inline-block;margin-top: 38px;line-height: 22.5px;
    font-size: 15px;" class="nav-item  pl-35 login-link"><a class="mybutton" style="color: #ffffff">
                                        <img src="https://ironmax.tech/images/frontend/login_unmoved_part.png"
                                             alt="login" width="30" class="login-icon">
                                        <img src="https://ironmax.tech/images/frontend/login_arrow.png" alt="login"
                                             width="30" class="login-icon arrow">
                                    </a></li>
                            </div>
                        @else
                            <div class="dropdown" style="    float: left; margin-right: 25px;margin-top: 28px">
                                <a style="color: #fff !important" class="dropbtn" href="#"
                                   id="navbardrop">{{ Auth::user()->name }}</a>
                                <div class="dropdown-content">
                                    <ul id="sub-menu1" class="sub-menu">
                                        <li style="padding: 8px 12px" id="sub-menu-sub"><a
                                                    href="{{route('search.data')}}"
                                                    class="tran3s">{{__('header.dashboard')}}</a></li>
                                        <li style="padding: 8px 12px" id="sub-menu-sub"><a href="{{route('logout')}}"
                                                                                           class="tran3s">{{__('header.logout')}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endguest
                        <ul class="nav navbar-nav" style="font-size: 16px;margin-top: 50px;">
                            <li><a onclick="myFunction()">{{__('header.PRODUCTS')}}</a>
                                <ul id="sub-menu1" class="sub-menu">
                                    <li id="sub-menu-sub"><img
                                                style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                src="{{asset('images/logo/pro1.png')}}"><a href="{{asset('patrol')}}"
                                                                                           class="tran3s">PATROL</a>
                                    </li>
                                    <li id="sub-menu-sub"><img
                                                style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                src="{{asset('images/logo/pro2.png')}}"><a href="{{asset('sos')}}"
                                                                                           class="tran3s">SOS</a></li>
                                    <li id="sub-menu-sub"><img
                                                style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                src="{{asset('images/logo/pro3.png')}}"><a href="{{asset('miniguard')}}"
                                                                                           class="tran3s">MINIGUARD</a>
                                    </li>
                                    <li id="sub-menu-sub"><img
                                                style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                src="{{asset('images/logo/pro4.png')}}"><a href="{{asset('shifter')}}"
                                                                                           class="tran3s">SHIFTER</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a id="btn-contact" href="#contact-section">{{__('header.CONTACT')}}</a></li>
                            <li><a onclick="myFunction1()">{{__('header.FAQs')}}</a>
                                <ul id="sub-menu2" class="sub-menu">
                                    <li id="sub-menu-sub"><img
                                                style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                src="{{asset('images/logo/pro1.png')}}"><a
                                                href="{{route('patrol').'#FAQ'}}"
                                                class="tran3s">PATROL</a>
                                    </li>
                                    <li id="sub-menu-sub"><img
                                                style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                src="{{asset('images/logo/pro2.png')}}"><a
                                                href="{{route('sos').'#FAQ'}}"
                                                class="tran3s">SOS</a></li>
                                    <li id="sub-menu-sub"><img
                                                style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                src="{{asset('images/logo/pro3.png')}}"><a
                                                href="{{route('miniguard').'#FAQ'}}" class="tran3s">MINIGUARD</a></li>
                                    <li id="sub-menu-sub"><img
                                                style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                src="{{asset('images/logo/pro4.png')}}"><a
                                                href="{{route('shifter').'#FAQ'}}" class="tran3s">SHIFTER</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="log" style="float: left; cursor: pointer;">
                        </div>
                    </div><!-- /.navbar-collapse -->
                </nav> <!-- /.theme-feature-menu -->
            </div>
        </header> <!-- /.theme-main-header -->
		<?php
		}
		else{
		?>
        <div class="preloaderLogo">
            <svg id="preloader-spinner" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 61.5" height="61.5" width="120" ><g data-name="Layer 2"><path d="M60 58.5A60 60 0 010 0v1.5a60 60 0 00120 0V0a60 60 0 01-60 58.5z" data-name="Layer 1"/></g></svg>
            <svg id="preloader-logo" xmlns="http://www.w3.org/2000/svg" width="64" height="69.466"><g data-name="Group 55"><g data-name="Group 53"><path data-name="Path 107" d="M31.487 53.893s24.5-10.411 24.059-46.087c-2.2-2.01-22.928-15.954-47.55-.628 0 1.047-5.154 29.574 23.491 46.715z" fill="#5f5f5f"/><path data-name="Path 108" d="M31.497 51.274s22.344-9.495 21.941-42.032c-2.007-1.833-20.91-14.55-43.365-.573 0 .955-4.697 26.972 21.424 42.605z" fill="#fff"/></g><path data-name="Path 109" d="M32.127 39.827l10.163-2.956-.239-.5-10.457 3.455z" fill="#5f5f5f"/><path data-name="Path 110" d="M42.142 36.404l-10.163 2.954.239.5 10.457-3.455z" fill="#5f5f5f"/><path data-name="Path 111" d="M46.472 36.603l1.923-.963v-.649l-1.593.806z" fill="#5f5f5f"/><path data-name="Path 112" d="M9.585 7.647c-3.854 14.949 5.73 28.83 5.73 28.83l1.106.117a1.33 1.33 0 01.064-.15 1.273 1.273 0 01.882-2.144 1.3 1.3 0 01.452.078.958.958 0 01.37.156 7.138 7.138 0 011.272 1.22c.093.108.186.215.287.315.024.023.143.115.171.142a1.506 1.506 0 01.145.077 2.421 2.421 0 00.337.067 1.081 1.081 0 01.822.746l.633.067 1.318-12.374-2.277-2.41.309-3.541 1.194-.057-.419 2.374 2.638-.7.073-2.83 2.063-.353-.252 1.759.639.59 1.564-.511-.025-2.675 1.423-.5 3.183.419-.168 2.345 2.68.925-.072-.42 1.9.642-.064.36 2.135.44.006-.1.907.307.243-2.149.729.243.5 3.518-8.291-2.844-2.195.834 7.806 4.333 1.508 10.858 3.119 1.956 2.492-1.571s.085.022.233.068a1.274 1.274 0 01.252-.369 1.287 1.287 0 01.7-.348c.036-.013.072-.028.11-.038l.133-.018a5.321 5.321 0 01.758-.915 1.211 1.211 0 01.923-.355c3.778-8.448 4.893-22.148 4.893-24.4C32.533-6.673 10.801 6.894 9.585 7.647z" fill="#e7ebee"/><g data-name="Group 54"><path data-name="Path 113" d="M17.053 38.508l1.131-1.759 1.507 2.387 1.816-1.628-4.345-3.3z" fill="none"/><path data-name="Path 114" d="M29.363 18.849l-3.077 1v-2.073l-1.508.314v2.268l-3.077.817-.063-2.01-.314.063-.377 2.889.251.377 7.663-1.822-6.47 2.764.943 1.256-1.315 12.333.027.018a2.041 2.041 0 011.1-.417 14.231 14.231 0 001.319-1.256l2.01.879 4.02-4.459.059-15.388-1 .251z" fill="none"/><path data-name="Path 115" d="M45.642 40.206a8.585 8.585 0 00-.329-1.007l-.518 1.014z" fill="none"/><path data-name="Path 116" d="M10.073 8.669a40.73 40.73 0 005.472 26.824 26.75 26.75 0 001.633-1.947v.166l4.17 2.85 1.3-11.872-2.261-2.01.628-3.831 1.821-.565v2.074l1.57-.5v-2.2l2.638-.753v2.2l1.633-.691v-2.2l1.885-.628V6.792h.879v.943a5.459 5.459 0 012.387-.063v.753s1.884 1 4.4-1.13a4.983 4.983 0 01-4.522 3.894h-.754l-1.13.565s-.189 3.518-.126 3.832 1.822.879 1.822.879v2.01l2.135.691-.125-2.2 2.7.69v2.2l1.633.691v-2.2l1.822.566.565 3.58-9.242-2.135 7.914 3.58-1.005.754 1.256 10.615 3.2 1.57 2.826-1.256-.5.879-2.324 1.068-2.827-.942.5-.251-2.324-1.131-2.073 1.131-6.218-4.837-.251 6.03 1.57-2.2 2.135 3.2-.125.377-2.638.879s-.251.377.628.314 6.124-1.382 6.124-1.382l-1.476.943 7.286-.817.628.314s8.768-10.8 8.121-29.957c-22.399-15.571-43.36-.57-43.36-.57z" fill="none"/><path data-name="Path 117" d="M55.542 7.806S34.11-9.33 7.992 7.178c0 0-2.551 17.407 6.487 30.43a4.289 4.289 0 00.94-1.6 14.471 14.471 0 001.912 4.445l-.722.006 1.466 1.259c.323.534.544.877.544.877l.465-.011 12.348 10.605 12.382-11.158h.054l.046-.089 1.764-1.59a5.13 5.13 0 01.158 1.161c10.64-13.272 9.706-33.707 9.706-33.707zm-35.855 31.33l-1.508-2.386-1.13 1.758.108-4.3 4.345 3.3zm10.8-7.348l-4.02 4.459-2.01-.879a14.231 14.231 0 01-1.319 1.256 2.041 2.041 0 00-1.1.417l-.027-.018 1.315-12.333-.943-1.256 6.47-2.764-7.663 1.822-.251-.377.377-2.889.314-.063.063 2.01 3.077-.817v-2.261l1.508-.314v2.077l3.077-1 .189-2.2 1-.251zm14.823 7.411a8.585 8.585 0 01.329 1.007l-.847.007.518-1.014-.628-.314-7.286.817 1.476-.943s-5.245 1.319-6.124 1.382-.628-.314-.628-.314l2.638-.879.125-.377-2.135-3.2-1.57 2.2.251-6.03 6.218 4.837 2.073-1.131 2.324 1.131-.5.251 2.827.942 2.324-1.068.5-.879-2.826 1.256-3.2-1.57-1.256-10.615 1.005-.754-7.914-3.58 9.233 2.136-.565-3.58-1.822-.566v2.2l-1.633-.691v-2.2l-2.7-.69.125 2.2-2.135-.691v-2.021s-1.759-.565-1.822-.879.126-3.832.126-3.832l1.13-.565h.754a4.983 4.983 0 004.522-3.894c-2.512 2.135-4.4 1.13-4.4 1.13v-.753a5.459 5.459 0 00-2.387.063v-.943h-.879v8.794l-1.885.628v2.2l-1.633.691v-2.2l-2.638.753v2.2l-1.57.5v-2.072l-1.821.565-.628 3.831 2.261 2.01-1.3 11.872-4.17-2.85v-.166s-.753 1.005-1.633 1.947a40.73 40.73 0 01-5.472-26.824s20.961-15 43.365.573c.658 19.165-8.11 29.962-8.11 29.962z" fill="#5f5f5f"/></g><path data-name="Path 118" d="M14.436 36.373l1.507.523.235-1.1-1.454-.249z" fill="#5f5f5f"/><path data-name="Path 119" d="M21.365 39.624l3.405-3.251-.324 2.344 3.892-2.041-1.784 2.949-2.594.832z" fill="#fff"/></g><text transform="translate(0 66.466)" fill="#5f5f5f" font-size="12.32" font-family="CalistoMT, Calisto MT"><tspan x="0" y="0">IRONMAX</tspan></text></svg>
        </div>
        <div class="main-page-wrapper">
            <!--
                            =============================================
                                Theme Header
                            ==============================================
                            -->
            <header style="display: none;" class="theme-main-header">
                <div class="container">
                    <a style="padding-top: 6px;" href="{{asset('index')}}" class="logo float-left tran4s"><img
                                src="{{asset('images/logo/logo4.png')}}" style="width: 65px" style="width: 65px"
                                alt="Logo"></a>
                    <!-- ========================= Theme Feature Page Menu ======================= -->
                    <nav class="navbar float-right theme-main-menu one-page-menu">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                Menu
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            @if(App::isLocale('ru'))
                                <ul style="margin-right: 200px;" id="new-margin-for-navbar" class="nav navbar-nav"
                                    style="font-size: 16px;">
                                    <li class="active"><a id="new1" style="font-size: 18px;"
                                                          href="{{asset('index')}}">{{__('header.HOME')}}</a></li>
                                    <li><a id="new2" style="font-size: 18px;"
                                           href="#PRODUCTS">{{__('header.PRODUCTS')}}</a>
                                        <ul class="sub-menu">
                                            <li id="sub-menu-sub"><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro1.png')}}"><a
                                                        href="{{asset('patrol')}}"
                                                        class="tran3s">{{__('header.VAR21')}}</a>
                                            </li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro2.png')}}"><a
                                                        href="{{asset('sos')}}"
                                                        class="tran3s">{{__('header.VAR23')}}</a>
                                            </li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro3.png')}}"><a
                                                        href="{{asset('miniguard')}}"
                                                        class="tran3s">{{__('header.VAR22')}}</a></li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro4.png')}}"><a
                                                        href="{{asset('shifter')}}"
                                                        class="tran3s">{{__('header.VAR20')}}</a></li>
                                        </ul>
                                    </li>
                                    <li><a id="new3" style="font-size: 18px;"
                                           href="#contact-section">{{__('header.CONTACT')}}</a></li>
                                    <li><a id="new4" style="font-size: 18px;" href="#">{{__('header.FAQs')}}</a>
                                        <ul class="sub-menu">
                                            <li id="sub-menu-sub"><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro1.png')}}"><a
                                                        href="{{route('patrol').'#FAQ'}}"
                                                        class="tran3s">{{__('header.VAR21')}}</a>
                                            </li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro2.png')}}"><a
                                                        href="{{route('sos').'#FAQ'}}"
                                                        class="tran3s">{{__('header.VAR23')}}</a></li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro3.png')}}"><a
                                                        href="{{route('miniguard').'#FAQ'}}"
                                                        class="tran3s">{{__('header.VAR22')}}</a></li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro4.png')}}"><a
                                                        href="{{route('shifter').'#FAQ'}}"
                                                        class="tran3s">{{__('header.VAR20')}}</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            @else
                                <ul class="nav navbar-nav" style="font-size: 16px;">
                                    <li class="active"><a id="new1" href="{{asset('index')}}">{{__('header.HOME')}}</a>
                                    </li>
                                    <li><a id="new2" href="#PRODUCTS">{{__('header.PRODUCTS')}}</a>
                                        <ul class="sub-menu">
                                            <li id="sub-menu-sub"><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro1.png')}}"><a
                                                        href="{{asset('patrol')}}" class="tran3s">PATROL</a>
                                            </li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro2.png')}}"><a
                                                        href="{{asset('sos')}}"
                                                        class="tran3s">SOS</a>
                                            </li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro3.png')}}"><a
                                                        href="{{asset('miniguard')}}" class="tran3s">MINIGUARD</a></li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro4.png')}}"><a
                                                        href="{{asset('shifter')}}" class="tran3s">SHIFTER</a></li>
                                        </ul>
                                    </li>
                                    <li><a id="new3" href="#contact-section">{{__('header.CONTACT')}}</a></li>
                                    <li><a href="#">{{__('header.FAQs')}}</a>
                                        <ul class="sub-menu">
                                            <li id="sub-menu-sub"><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro1.png')}}"><a
                                                        href="{{route('patrol').'#FAQ'}}" class="tran3s">PATROL</a>
                                            </li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro2.png')}}"><a
                                                        href="{{route('sos').'#FAQ'}}" class="tran3s">SOS</a></li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro3.png')}}"><a
                                                        href="{{route('miniguard').'#FAQ'}}"
                                                        class="tran3s">MINIGUARD</a>
                                            </li>
                                            <li><img
                                                        style="width: 30px; height: 20px;margin-top: 3px; display: inline; float: right;padding-right: 10px;"
                                                        src="{{asset('images/logo/pro4.png')}}"><a
                                                        href="{{route('shifter').'#FAQ'}}" class="tran3s">SHIFTER</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            @endif
                            {{-- LANGUAGE SELECTOR --}}
                            <div class="dropdown" style="    float: left; margin-right: 25px;">
                                <a class="dropbtn" href="#" id="navbardrop">
                                    @if(App::isLocale('ru'))
                                        <img style="width: 26px;margin-top: 8px;"
                                             src="{{asset('images/flag/countries_russia.png')}}" width="40px" alt="">
                                    @elseif(App::isLocale('he'))
                                        <img style="width: 26px;margin-top: 8px;"
                                             src="{{asset('images/flag/countries_israel.png')}}" width="40px" alt="">
                                    @else
                                        <img style="width: 26px;margin-top: 8px;"
                                             src="{{asset('images/flag/countries_english.png')}}" width="40px" alt="">
                                    @endif
                                </a>
                                <div class="dropdown-content">
                                    <a href="{{ route('search.data', 'en') }}" style="width: 110px;height: 30px"><p
                                                style="display: inline;    float: left;">&nbsp&nbsp&nbspEnglish
                                            &nbsp</p>
                                        <img style="    width: 30px;
    height: 30px;
    margin-top: -3px;
    margin-right: 8px;
    float: right;
    display: inline;" src="{{asset('images/flag/countries_english.png')}}"></a>
                                    <a href="{{ route('search.data', 'he') }}" style="width: 110px;height: 30px"><p
                                                style="display: inline;    float: left;">&nbsp&nbsp&nbspעברית
                                            &nbsp&nbsp&nbsp&nbsp</p><img style="    width: 30px;
    height: 30px;
    margin-top: -3px;
    margin-right: 8px;
    float: right;
    display: inline;" src="{{asset('images/flag/countries_israel.png')}}"></a>
                                    <a href="{{ route('search.data', 'ru') }}" style="width: 110px;height: 30px"><p
                                                style="display: inline;    float: left;">&nbsp&nbsp&nbspРусский
                                        </p><img style="    width: 30px;
    height: 30px;
    margin-top: -3px;
    margin-right: 8px;
    float: right;
    display: inline;" src="{{asset('images/flag/countries_russia.png')}}"></a>
                                </div>
                            </div>
                            {{-- END LANGUAGE SELECTOR --}}
                            @guest
                                <div class="log" style="float: left; cursor: pointer;"
                                     onclick="document.getElementById('loginPopUp').style.display='block'">
                                    <div class="login"
                                         style="float: left;color: white;padding-top: 30px;padding-left: 5px ;font-weight: 600;font-size: 15px;margin-right: 5px;line-height: 22.5px;">{{__('header.LOGIN')}}</div>
                                    <li style="display: inline-block;margin-top: 38px;line-height: 22.5px;
    font-size: 15px;" class="nav-item  pl-35 login-link"><a class="mybutton" style="color: #ffffff">
                                            <img src="https://ironmax.tech/images/frontend/login_unmoved_part.png"
                                                 alt="login" width="30" class="login-icon">
                                            <img src="https://ironmax.tech/images/frontend/login_arrow.png" alt="login"
                                                 width="30" class="login-icon arrow">
                                        </a></li>
                                </div>
                            @else
                                <div class="dropdown" style="    float: left; margin-right: 25px;margin-top: 28px">
                                    <a style="color: #fff !important" class="dropbtn" href="#"
                                       id="navbardrop">{{ Auth::user()->name }}</a>
                                    <div class="dropdown-content">
                                        <ul id="sub-menu1" class="sub-menu">
                                            <li style="padding: 8px 12px" id="sub-menu-sub"><a
                                                        href="{{route('search.data')}}"
                                                        class="tran3s">{{__('header.dashboard')}}</a></li>
                                            <li style="padding: 8px 12px" id="sub-menu-sub"><a
                                                        href="{{route('logout')}}"
                                                        class="tran3s">{{__('header.logout')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            @endguest
                        </div><!-- /.navbar-collapse -->
                    </nav> <!-- /.theme-feature-menu -->
                </div>
            </header>
			<?php
			}
			?>
            <div class="row" style="margin-top: 10%">
                <div class="col-md-2">
                </div>
                <div class="col-md-8" id="user-dashboard">
                    <h4 class="text-uppercase font-weight-bolder form-group text-center">@lang('SEARCH RESULTS')</h4>
                    <form method="post" action="{{ route('search.data') }}">
                        @csrf
                        <div class="row">
                            @if(App::isLocale('he'))
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button
                                                class="btn btn-secondary btn-block search-btn {{ App::isLocale('he') ? 'rtl' : 'text-center' }}"
                                                style="font-size: 20px;border-radius: 20px"> @lang('Search')</button>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-3">
                                <div class="form-group ">
                                    <div class="input-group">
                                        <input type="text" id="datepicker" value="{{ $date }}" name="date"
                                               class="datepicker {{ $errors->has('subject') ? ' is-invalid' : '' }} {{ App::isLocale('he') ? 'rtl' : '' }}"
                                               placeholder="@lang('Select Date')" autocomplete="off" required/>
                                        <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                    @if ($errors->has('date'))
                                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="facility" class=" {{ App::isLocale('he') ? 'rtl' : 'text-center' }}"
                                            name="" id="" required>
                                        @if(Auth::user()->type != 'security_officer')
                                            <option value="all"
                                                    @if($facility == 'all') selected @endif>@lang('All')</option>
                                        @endif
                                        @foreach($facilities as $f)
                                            @if(Auth::user()->type != 'security_officer' && $f->owner_id == Auth::user()->owner_id) @continue @endif
                                            <option value="{{ $f->owner_id }}"
                                                    @if($facility == $f->owner_id) selected @endif>{{ $f->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('facility'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('facility') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select name="category"
                                            class=" {{ $errors->has('subject') ? ' is-invalid' : '' }} {{ App::isLocale('he') ? 'rtl' : 'text-center' }}"
                                            name="" id="" required>
                                        <option value="daily_report"
                                                @if($type == 'Daily patrol report') selected @endif>@lang('Daily Patrols Report')</option>
                                        <option value="patrol_report"
                                                @if($type == 'Petrol report') selected @endif>@lang('Patrol Report')</option>
                                        <option value="inspector_report"
                                                @if($type == 'Incident report') selected @endif>@lang('Incident Report')</option>
                                    </select>
                                    @if ($errors->has('category'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('category') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            @if(!App::isLocale('he'))
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button
                                                class="btn btn-secondary btn-block search-btn {{ App::isLocale('he') ? 'rtl' : 'text-center' }}"
                                                style="font-size: 20px;border-radius: 20px">@lang('Search')</button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-12">
                    <h4 class=" text-uppercase font-weight-bolder text-center {{ App::isLocale('he') ? 'rtl' : '' }}">
                        @lang('Reports for') {{ $date }}
                    </h4>
                    <hr/>
                </div>
                @php $flag = false @endphp
                @foreach($files as $data)
                    @if(count($data['files']))
                        <div class="col-md-12 form-group">
                            <h4 class=" text-uppercase font-weight-bolder text-center {{ App::isLocale('he') ? 'rtl' : '' }}">
                                {{ $data['name'] }}
                            </h4>
                        </div>
                        <div class="col-md-12 form-group">
                            @foreach($data['files'] as $file)
                                @php $flag = true @endphp
                                <div class="download-btn">
                                    <a class="btn btn-primary btn-lg theme-bg"
                                       style="background-color: #000101;border-color: #000101"
                                       href="{{route('downloadPdf',$file->id)}}"
                                       target="_blank">{{ date('H:i:s', strtotime($file->patrol_end_time)) }} <i
                                                class="fa fa-download"></i></a>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-12">
                            <hr/>
                        </div>
                    @endif
                @endforeach
            </div>
            @if(!$flag)
                <div class="row" style="margin-bottom: 4%">
                    <div class="col-md-12 form-group">
                        <h4 class="text-uppercase font-weight-bolder text-center {{ App::isLocale('he') ? 'rtl' : '' }}">
                            @lang('NO DOCUMENTS HAVE FOUND FOR THIS DATE')
                        </h4>
                    </div>
                </div>
            @endif
        <!--
            =====================================================
                Footer
            =====================================================
            -->
            <footer>
                <div class="container">
                    <a href="{{asset('index')}}" class="logo"><img src="{{asset('images/logo/logo4.png')}}"
                                                                   style="width: 65px" style="width: 65px"
                                                                   alt="Logo"></a>
                    <ul style="margin-bottom: 30px;">
                        <li><a href="https://www.facebook.com/Ironmax-Technologies-2070530093028121/" target="_blank"
                               class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="skype:live:ironmaxtech?chat" class="tran3s round-border"><i class="fa fa-skype"
                                                                                                 aria-hidden="true"></i></a>
                        </li>
                        <li><a href="https://www.linkedin.com/in/iron-max-21aa5817b/" target="_blank"
                               class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="https://www.youtube.com/channel/UCDzflJRI2I6y7hEFmBGO4bw/" target="_blank"
                               class="tran3s round-border"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                    @if(App::isLocale('ru'))
                        <p style="display: inline;"><a style="color: #ffffff"
                                                       href="{{asset('Privacy_Policy')}}">{{__('header.VAR13')}}</a></p>
                    @else
                        <p style="display: inline;"><a style="color: #ffffff" href="{{asset('Privacy_Policy')}}">{{__('header.VAR13')}}&emsp;&emsp;</a>
                        </p>
                    @endif
                    <p style="margin-top: 20px;display: inline;"><a style="color: #ffffff;"
                                                                    href="{{route('termofuse')}}">{{__('header.VAR14')}}</a>
                    </p>
                    <p style="margin-top: 20px;">Copyright @ 2019 IRONMAX TECHNOLOGIES</p>
                </div>
            </footer>
            <!-- =============================================
                                Loading Transition
                            ============================================== -->
            <!-- Scroll Top Button -->
            <div style="display:none;" class="testfade">
                <button onclick="location.href='https://accounts.google.com/'"
                        style="display:block;background: #1f5bc1;" id="myBtn1" class="scroll-top tran3s">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                </button>
                <button onclick="topFunction()" class="scroll-top tran3s p-color-bg" id="toTopBtn">
                    <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
                </button>
            </div>
            <!-- Email Button
                        <a href="https://accounts.google.com/" target="_blank">
                            <img id="myBtn" border="0" alt="W3Schools" src="images/logo/3.png" width="75" height="75">
            </a>-->
            <!-- =============================================
                                        popup login
                                    ============================================== -->
            <div id="loginPopUp" class="modal" style="overflow-y: hidden;">
                <form class="modal-content animate" action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="imgcontainer">
                        <div style="text-align: center; color: white; font-size: 24px">{{__('header.LOGIN')}}</div>
                        <img src="{{asset('images/logo/sign-in-1-x.png')}}" class="sign-in-1-img"
                             onclick="document.getElementById('loginPopUp').style.display='none'">
                    </div>
                    <div class="container1">
                        @if(App::isLocale('he'))
                            <input style="background: url(http://www.vsdigital.com/EN/img/sign-in-2.jpg) no-repeat center right;
    text-indent: 20px;" type="text" placeholder="{{__('Email')}}" name="email" required>
                            <input style="background: url(http://www.vsdigital.com/EN/img/sign-in-3.jpg) no-repeat center right;
    text-indent: 20px;" type="password" placeholder="{{__('header.Password')}}" name="password" required>
                        @else
                            <input style="background: url(http://www.vsdigital.com/EN/img/sign-in-2.jpg) no-repeat center left;
        text-indent: 20px;" type="text" placeholder="{{__('Email')}}" name="email" required>
                            <input style="background: url(http://www.vsdigital.com/EN/img/sign-in-3.jpg) no-repeat center left;
        text-indent: 20px;" type="password" placeholder="{{__('header.Password')}}" name="password" required>
                        @endif
                        <button class="mybutton" type="submit">{{__('header.LOGIN')}}</button>
                        <label style="font-weight: 100;">
                            <input type="checkbox" checked="checked" name="remember"> {{__('header.Remember Me')}}
                        </label>
                        <span class="psw"><a style="color: #365388;"
                                             href="#">{{__('header.Forgot password?')}}</a></span>
                    </div>
                </form>
            </div>
            <script>
            // Get the modal
            var modal      = document.getElementById('loginPopUp')
            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
              if (event.target == modal) {
                modal.style.display = "none"
              }
            }

            /* When the user clicks on the button,
             toggle between hiding and showing the dropdown content */
            function myFunction() {
              document.getElementById("sub-menu1").classList.toggle("show")
            }

            function myFunction1() {
              document.getElementById("sub-menu2").classList.toggle("show")
            }

            // function myFunction2() {
            //   document.getElementById("sub-menu2").classList.toggle("show");
            // }
            function myFunction2() {
              var x = document.getElementById("dropdown-content")
              if (x.style.display === "flex") {
                x.style.display = "none"
              } else {
                x.style.display = "flex"
              }
            }
            </script>
            <!-- Js File_________________________________ -->
            <!-- j Query -->
            <script type="text/javascript" src="{{asset('vendor/jquery.2.2.3.min.js')}}"></script>
            <!-- Bootstrap JS -->
            <script type="text/javascript" src="{{asset('vendor/bootstrap/bootstrap.min.js')}}"></script>
            <!-- Vendor js _________ -->
            <!-- revolution -->
            <script src="{{asset('vendor/revolution/jquery.themepunch.tools.min.js')}}"></script>
            <script src="{{asset('vendor/revolution/jquery.themepunch.revolution.min.js')}}"></script>
            <script type="text/javascript"
                    src="{{asset('vendor/revolution/revolution.extension.slideanims.min.js')}}"></script>
            <script type="text/javascript"
                    src="{{asset('vendor/revolution/revolution.extension.layeranimation.min.js')}}"></script>
            <script type="text/javascript"
                    src="{{asset('vendor/revolution/revolution.extension.navigation.min.js')}}"></script>
            <script type="text/javascript"
                    src="{{asset('vendor/revolution/revolution.extension.kenburn.min.js')}}"></script>
            <script type="text/javascript"
                    src="{{asset('vendor/revolution/revolution.extension.actions.min.js')}}"></script>
            <script type="text/javascript"
                    src="{{asset('vendor/revolution/revolution.extension.video.min.js')}}"></script>
            {{-- <script>
                            var $j = jQuery.noConflict();
                        </script> --}}
            {{--Number Counter JQuery--}}
            /* <script>
            $(document).ready(function ($) {
              $('.counter').counterUp({
                                        delay : 10,
                                        time :  1000
                                      })
            })
            </script> */
            {{--End Number Counter JQuery--}}
            {{-- <script>
                            var $j = jQuery.noConflict();
                        </script> --}}
            <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
            <script src="{{asset('js/jquery.counterup.min.js')}}"></script>
            {{-- <script>
                            var $j = jQuery.noConflict();
                        </script> --}}
        <!-- Google map js -->
            <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ8VrXgGZ3QSC-0XubNhuB2uKKCwqVaD0&callback=goMap"
                    type="text/javascript"></script> <!-- Gmap Helper -->
            <script src="{{asset('vendor/gmaps.min.js')}}"></script>
            <!-- owl.carousel -->
            <script type="text/javascript" src="{{asset('vendor/owl-carousel/owl.carousel.min.js')}}"></script>
            <!-- mixitUp -->
            <script type="text/javascript" src="{{asset('vendor/jquery.mixitup.min.js')}}"></script>
            <!-- Progress Bar js -->
            <script type="text/javascript" src="{{asset('vendor/skills-master/jquery.skills.js')}}"></script>
            <!-- Validation -->
            <script type="text/javascript" src="{{asset('vendor/contact-form/validate.js')}}"></script>
            <script type="text/javascript" src="{{asset('vendor/contact-form/jquery.form.js')}}"></script>
            <!-- Theme js -->
            <script type="text/javascript" src="{{asset('js/theme.js')}}"></script>
            <script type="text/javascript" src="{{asset('js/map-script.js')}}"></script>
            {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script>
                var $j = jQuery.noConflict();
            </script> --}}
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            {{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> --}}
            <script src="{{asset('js/msdropdown/jquery.dd.js')}}"></script>
            <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
        <!-- <script src="{{asset('js/scroll-down.js')}}"></script> -->
            {{-- BANNER SCRIPTS --}}
            @if(App::isLocale('ru'))
                <script type="text/javascript">
                //banner_img
                function newPicture() {
                  document.getElementById("image").src = "{{asset('images/banner_img/1_patrol-RU.png')}}"
                }

                function oldPicture() {
                  document.getElementById("image").src = "{{asset('images/banner_img/1_patrol_rus.png')}}"
                }
                </script>
                <script type="text/javascript">
                //banner_img
                function newPicture1() {
                  document.getElementById("image1").src = "{{asset('images/banner_img/2_shifter-RU.png')}}"
                }

                function oldPicture1() {
                  document.getElementById("image1").src = "{{asset('images/banner_img/2_shifter_rus.png')}}"
                }
                </script>
                <script type="text/javascript">
                //banner_img
                function newPicture2() {
                  document.getElementById("image2").src = "{{asset('images/banner_img/3_miniguard-RU.png')}}"
                }

                function oldPicture2() {
                  document.getElementById("image2").src = "{{asset('images/banner_img/3_miniguard_rus.png')}}"
                }
                </script>
                <script type="text/javascript">
                //banner_img
                function newPicture13() {
                  document.getElementById("image13").src = "{{asset('images/banner_img/4_sos-RU.png')}}"
                }

                function oldPicture13() {
                  document.getElementById("image13").src = "{{asset('images/banner_img/4_sos_rus.png')}}"
                }
                </script>
            @elseif(App::isLocale('he'))
                <script type="text/javascript">
                //banner_img
                function newPicture() {
                  document.getElementById("image").src = "{{asset('images/banner_img/1_patrol-HE.png')}}"
                }

                function oldPicture() {
                  document.getElementById("image").src = "{{asset('images/banner_img/1.png')}}"
                }
                </script>
                <script type="text/javascript">
                //banner_img
                function newPicture1() {
                  document.getElementById("image1").src = "{{asset('images/banner_img/2_shifter-HE.png')}}"
                }

                function oldPicture1() {
                  document.getElementById("image1").src = "{{asset('images/banner_img/2.png')}}"
                }
                </script>
                <script type="text/javascript">
                //banner_img
                function newPicture2() {
                  document.getElementById("image2").src = "{{asset('images/banner_img/3_miniguard-HE.png')}}"
                }

                function oldPicture2() {
                  document.getElementById("image2").src = "{{asset('images/banner_img/3.png')}}"
                }
                </script>
                <script type="text/javascript">
                //banner_img
                function newPicture13() {
                  document.getElementById("image13").src = "{{asset('images/banner_img/4_sos-HE.png')}}"
                }

                function oldPicture13() {
                  document.getElementById("image13").src = "{{asset('images/banner_img/4.png')}}"
                }
                </script>
            @else
                <script type="text/javascript">
                //banner_img
                function newPicture() {
                  document.getElementById("image").src = "{{asset('images/banner_img/1_patrol-EN.png')}}"
                }

                function oldPicture() {
                  document.getElementById("image").src = "{{asset('images/banner_img/1.png')}}"
                }
                </script>
                <script type="text/javascript">
                //banner_img
                function newPicture1() {
                  document.getElementById("image1").src = "{{asset('images/banner_img/2_shifter-EN.png')}}"
                }

                function oldPicture1() {
                  document.getElementById("image1").src = "{{asset('images/banner_img/2.png')}}"
                }
                </script>
                <script type="text/javascript">
                //banner_img
                function newPicture2() {
                  document.getElementById("image2").src = "{{asset('images/banner_img/3_miniguard-EN.png')}}"
                }

                function oldPicture2() {
                  document.getElementById("image2").src = "{{asset('images/banner_img/3.png')}}"
                }
                </script>
                <script type="text/javascript">
                //banner_img
                function newPicture13() {
                  document.getElementById("image13").src = "{{asset('images/banner_img/4_sos-EN.png')}}"
                }

                function oldPicture13() {
                  document.getElementById("image13").src = "{{asset('images/banner_img/4.png')}}"
                }
                </script>
            @endif
            {{-- END BANNER SCRIPTS --}}
            {{-- scroll to top Jquery --}}
            <script type="text/javascript">
            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function () {
              scrollFunction()
              scrollFunction1()
              scrollFunction2()
            }

            function scrollFunction() {
              if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
                document.getElementById("myBtn1").style.bottom = "80px"
              } else {
                document.getElementById("myBtn1").style.bottom = "20px"
              }
            }

            function scrollFunction1() {
              var testDiv  = document.getElementById("PRODUCTS")
              var X        = testDiv.offsetTop
              var y        = X - 100
              var testDiv1 = document.getElementById("contact-section")
              var X1       = testDiv.offsetTop
              var y1       = X1 - 100
              console.log(y)
              if (document.body.scrollTop > y || document.documentElement.scrollTop > y) {
                $('#new1').css('color', 'white')
                $('#new2').css('color', '#97b4e4')
              } else {
                $('#new1').css('color', '#97b4e4')
                $('#new2').css('color', 'white')
              }
            }

            function scrollFunction2() {
              var testDiv1 = document.getElementById("contact-section")
              var X1       = testDiv1.offsetTop
              var y1       = X1 - 100
              console.log(y1)
              if (document.body.scrollTop > y1 || document.documentElement.scrollTop > y1) {
                $('#new2').css('color', 'white')
                $('#new3').css('color', '#97b4e4')
              } else {
                //$('#new2').css('color', '#97b4e4');
                $('#new3').css('color', 'white')
              }
            }

            function topFunction() {
              document.body.scrollTop            = 0 // For Safari
              document.documentElement.scrollTop = 0 // For Chrome, Firefox, IE and Opera
            }

            var btnMenu    = document.getElementById("btn-menu")
            var btnLogin   = document.getElementById("btn-loginn")
            // var btnProduct = document.getElementById("btn-product");
            var btnContact = document.getElementById("btn-contact")
            var navBar     = document.getElementById("navbar-collapse-1")
            btnMenu.addEventListener("click", function (e) {
              if (navBar.style.display === "none") {
                navBar.style.display = "block"
              } else {
                navBar.style.display = "none"
              }
            })
            btnLogin.addEventListener("click", function (e) {
              navBar.style.display = "none"
            })
            // btnProduct.addEventListener("click",function(e){  navBar.style.display="none"       });
            btnContact.addEventListener("click", function (e) {
              navBar.style.display = "none"
            })
            </script>
            {{-- END scroll to top Jquery --}}
            <script type="text/javascript"
                    src="https://cdn.rawgit.com/jsmodules/ddslick/master/dist/jquery.ddslick.min.js">
            </script>
        </div> <!-- /.main-page-wrapper -->
        <script type="text/javascript">
        $('#datepicker').datepicker({
                                        @if(!App::isLocale('en'))
                                        format : 'dd/mm/yyyy'
                                        @else
                                        format : 'mm/dd/yyyy'
                                        @endif
                                    })

        function preloadImage(url) {
          var img = new Image()
          img.src = url
        }

        preloadImage("{{asset('images/banner_img/1_patrol-EN.png')}}")
        preloadImage("{{asset('images/banner_img/2_shifter-EN.png')}}")
        preloadImage("{{asset('images/banner_img/3_miniguard-EN.png')}}")
        preloadImage("{{asset('images/banner_img/4_sos-EN.png')}}")
        preloadImage("{{asset('images/banner_img/1_patrol-RU.png')}}")
        preloadImage("{{asset('images/banner_img/2_shifter-RU.png')}}")
        preloadImage("{{asset('images/banner_img/3_miniguard-RU.png')}}")
        preloadImage("{{asset('images/banner_img/4_sos-RU.png')}}")
        preloadImage("{{asset('images/banner_img/1_patrol-HE.png')}}")
        preloadImage("{{asset('images/banner_img/2_shifter-HE.png')}}")
        preloadImage("{{asset('images/banner_img/3_miniguard-HE.png')}}")
        preloadImage("{{asset('images/banner_img/4_sos-HE.png')}}")
        </script>
</body>
</html>
