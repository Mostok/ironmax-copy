<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel=icon href={{asset('images/fav-icon/icon1.png')}}>
    @php
        if(!isset($page_title) or $page_title=="")
        {
         echo '<title>IRONMAX</title>';
        }
        else{
         echo '<title>'.$page_title.' - IRONMAX</title>';
        }
    @endphp
    <style>
        @page{
            margin: 0;
        }
        body{
            font-family: firefly, DejaVu Sans, sans-serif;
            font-size: 10px;
            /*direction: {{ $direction }};*/
        }
        .qrs{
            width: 90%;
            margin: 0px auto;
        }
        .qr{
            border: 1px solid black;
            height: 235px;
            width: 23%;
            padding: 0px 10px;
            margin-left: 5px;
            margin-right: 5px;
            vertical-align: top;
            direction: {{ $direction }};
        }
        .qr-empty {
            border-color: #fff;
        }
        .qr-img img{
            width: 92px;
            height: auto;
        }
        .logo {
            text-align: center;
            /*margin: 9px 0px 12px;*/
            /*margin: 15px 0px;*/
            margin: 14px 0px 17px;
        }
        .qr-line {
            margin-top: 22px;
            height: 0;
            border-top: 1px solid #000;
        }
        .qr-img{
            text-align: center;
        }
        .facility img, .qr-name img, .location img{
            padding-right: 5px;
        }
        .facility, .qr-name, .location{
            /*text-align: justify;*/
        }
        .qr-name, .location{
            padding-top: 5px;
            line-height: 0.9;
        }
        .two-lines {
            display: block;  or inline-block
            text-overflow: ellipsis;
            word-wrap: break-word;
            overflow: hidden;
            max-height: 2.7em;
            line-height: 0.9em;
        }
    </style>
</head>
<body>
<table class="qrs" cellspacing="10" style="margin-top: 20px;">

    <tr>
        @php
            $i = 0;
        @endphp
        @foreach($barcodes as $barcode)
            <td class="qr">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <div class="logo">
                                <img width="92" src="{{public_path('images/qr/logo.png')}}" alt="">
                            </div>
                            <div class="qr-img">
                                <img src="{{public_path('/storage/qrs/'.$barcode->getQrCodeImgName())}}" alt="">
                            </div>
                            <!--<hr/>-->
                            <div class="qr-line"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; height: 82px;">
                            <table style="padding: 0; margin: -3px auto 0px;border: none; vertical-align: top; text-align: right;">
                                <tbody>
                                <tr>
                                    <td>{{$barcode->facility->facility_name_pdf}}</td>
                                    <td style="text-align: center; padding: 3px 0;"><img width="10" height="11" src="{{public_path('images/qr/icon-1.png')}}" alt=""/></td>
                                </tr>
                                <tr>
                                    <td><div class="two-lines" style="hyphens: auto;word-break: break-all;">@if($barcode->is_last)({{__('barcodes.last')}}) @endif{{$barcode->name_pdf}}</div></td>
                                    <td style="text-align: center; padding: 3px 0; padding-top: 5px;"><img width="10" height="10" src="{{public_path('/images/qr/icon-2.png')}}" alt=""/></td>
                                </tr>
                                @if($barcode->location!='')
                                    <tr>
                                        <td><div class="two-lines">{{$barcode->location_pdf}}</div></td>
                                        <td style="text-align: center; padding: 3px 0; vertical-align: top;"><img width="8" height="11" src="{{public_path('/images/qr/icon-3.png')}}" alt=""/></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            @php
                $i++;
            @endphp
            @if($i === count($barcodes) && $i % 4 !== 0)
                @for($j=0; $j<(4 - ($i % 4)); $j++)
                    <td class="qr qr-empty"></td>
                @endfor
            @endif

            @if($i % 4 === 0)
    </tr>
    @if($i < count($barcodes))
        <tr>
            @endif
            @endif
            @endforeach
        </tr>
</table>
</body>
</html>
