<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel=icon href={{asset('images/fav-icon/icon1.png')}}>
    @php
        if(!isset($page_title) or $page_title=="")
        {
         echo '<title>IRONMAX</title>';
        }
        else{
         echo '<title>'.$page_title.' - IRONMAX</title>';
        }
    @endphp
    <style>
        @page{
            margin: 0;
        }
        body{
            font-family: firefly, DejaVu Sans, sans-serif;
            font-size: 10px;
        }
        .page {
            height: 29.7cm;
        }
        .qrs{
            width: 90%;
            margin: 0 auto;
        }
        .qr{
            border: 1px solid black;
            height: 235px;
            width: 23%;
/*            padding: 10px;
            padding-top: 5px;
            padding-bottom: 5px;*/
            padding: 0 10px;
            margin-left: 5px;
            margin-right: 5px;
            vertical-align: top;
            direction: {{ $direction }};
        }
        .qr-empty {
            border-color: #fff;
        }
        .qr-img img{
            width: 92px;
            height: auto;
        }
        .logo {
            text-align: center;
            /*padding-bottom: 5px;*/
            /*margin: 15px 0px;*/
            margin: 13px 0px 14px;
        }
        .qr hr {
            margin-top: 14px;
        }
        .qr-line {
            margin-top: 18px;
            height: 0;
            border-top: 1.5px solid #000;
        }
        .qr-img{
            text-align: center;
        }
        .facility img, .qr-name img, .location img{
            padding-right: 5px;
        }
        .facility, .qr-name, .location{
            /*text-align: justify;*/
        }
        .qr-name, .location{
            padding-top: 5px;
            line-height: 0.9;
        }
        .two-lines {
            -webkit-line-clamp: 2;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-box-orient: vertical;
        }

        @media print {
            body, table {
                margin: 0;
                box-shadow: unset;
            }
        }

    </style>
</head>
<body onload="window.print()" style="margin: 0">
{{--<table class="qrs" cellspacing="10" cellpadding="0" style="margin-top: 20px;">--}}
<div class="page">
<table class="qrs" cellspacing="10" cellpadding="0">

    <tr>
        @php
            $i = 0;
        @endphp
        @foreach($barcodes as $barcode)
            <td class="qr">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <div class="logo">
                                <img width="92" src="{{asset('images/qr/logo.svg')}}" alt="">
                            </div>
                            <div class="qr-img">
                                <img src="/qr/{{$barcode->id}}" alt="">
                            </div>
                            <!--<hr/>-->
                            <div class="qr-line"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;  height: 85px; ">
                            <table style="padding: 0; margin: 0 auto;border: none; vertical-align: top;">
                                <tbody>
                                <tr>
                                    <td style="text-align: center; padding: 3px 0; vertical-align: top;"><img width="10" height="11" src="{{asset('images/qr/icon-1.svg')}}" alt=""/></td>
                                    <td >{{$barcode->facility->facility_name}}</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; padding: 3px 0; vertical-align: top;"><img width="10" height="10" src="{{asset('images/qr/icon-2.svg')}}" alt=""/></td>
                                    <td><div class="two-lines" style="hyphens: auto;word-break: break-all;">{{$barcode->name}}@if($barcode->is_last) ({{__('barcodes.last_print')}})@endif</div></td>
                                </tr>
                                @if($barcode->location!='')
                                <tr>
                                    <td style="text-align: center; padding: 3px 0; vertical-align: top;"><img width="8" height="11" src="{{asset('images/qr/icon-3.svg')}}" alt=""/></td>
                                    <td><div class="two-lines">{{$barcode->location}}</div></td>
                                </tr>
                                @endif
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            @php
                $i++;
            @endphp
        @if($i === count($barcodes) && $i % 4 !== 0)
            @for($j=0; $j<(4 - ($i % 4)); $j++)
                <td class="qr qr-empty"></td>
            @endfor
        @endif
            @if($i % 4 === 0)
    </tr>
    @if($i < count($barcodes))
        <!-- @php echo $i @endphp -->
        @if($i % 16 === 0)
        </table>
</div>
<div class="page">
        <table class="qrs" cellspacing="10" cellpadding="0">
{{--        <table class="qrs" cellspacing="10" cellpadding="0" style="margin-top: 20px;">--}}
        @endif
        <tr>
        @endif
    @endif
            @endforeach
</table>
</div>
</body>
</html>
