<div class="logotype">
    <img src="{{asset('images/product/miniguard.png')}}"
         alt="{{__('pages/miniguard.title')}}">
</div>

<div class="content_block {{(App::getLocale()=="he")?"rtl":""}} {{(App::getLocale()=="ru")?"ru":""}}">
    <ul class="features-list">
    @foreach(__('pages/miniguard.points') as $item)
        <li>
            <p>{!! $item !!}</p>
        </li>
    @endforeach
    </ul>
</div>
