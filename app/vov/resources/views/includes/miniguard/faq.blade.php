@if(app()->getLocale() === 'he')
    <div class="content_block rtl" id="FAQ">
        <h1 class="text-center">{{__('pages/miniguard.faq.header')}}</h1>
        <div class="article__paragraphs">
            <div class="entity entity-paragraphs-item paragraphs-item-accordion-tabs entity-paragraphs-item--narrow">
                <ul class="accordion accordion--clear" data-accordion-controls="true">
                    <div class="accordion-controller-wrapper">
                        <button data-accordion-controler="expand" tabindex="0"
                                class="button button--secondary">{{__('header.VAR15')}}</button>
                        <button data-accordion-controler="collapse" tabindex="0"
                                class="button button--secondary">{{__('header.VAR16')}}</button>
                    </div>
                    @foreach(__('pages/miniguard.faq.blocks') as $item)
                        <li class="accordion__child">
                            <div data-accordion-trigger="" class="accordion__toggle" tabindex="0" role="button"
                                 aria-expanded="false" aria-controls="accordion-a1-panel" style="direction: rtl;">
                                <h2 class="accordion__label">{{$item['question']}}</h2>
                            </div>
                            <div class="accordion__inner" data-accordion-state="collapsed"
                                 aria-labelledby="accordion-a1"
                                 aria-hidden="true" role="region" style="direction: rtl;">
                                <div class="accordion__inner-wrapper">
                                    <p>{!! $item['answer'] !!}</p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@else
    <div class="content_block" id="FAQ">
        <h1 class="text-center">{{__('pages/miniguard.faq.header')}}</h1>
        <div class="article__paragraphs">
            <div class="entity entity-paragraphs-item paragraphs-item-accordion-tabs entity-paragraphs-item--narrow">
                <ul class="accordion accordion--clear" data-accordion-controls="true">
                    <div class="accordion-controller-wrapper">
                        <button data-accordion-controler="expand" tabindex="0"
                                class="button button--secondary">{{__('header.VAR15')}}</button>
                        <button data-accordion-controler="collapse" tabindex="0"
                                class="button button--secondary">{{__('header.VAR16')}}</button>
                    </div>
                    @foreach(__('pages/miniguard.faq.blocks') as $item)
                        <li class="accordion__child">
                            <div data-accordion-trigger="" class="accordion__toggle" tabindex="0" role="button"
                                 aria-expanded="false" aria-controls="accordion-a1-panel">
                                <h2 class="accordion__label">{{$item['question']}}</h2>
                            </div>
                            <div class="accordion__inner" data-accordion-state="collapsed"
                                 aria-labelledby="accordion-a1"
                                 aria-hidden="true" role="region">
                                <div class="accordion__inner-wrapper">
                                    <p>{!! $item['answer'] !!}</p>
                                </div>
                            </div>
                        </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
@endif
