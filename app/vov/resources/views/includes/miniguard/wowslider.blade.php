@php
    $locale = app()->getLocale();
$slides = [
    'en' => [
        asset("images/pages/slider/miniguard/en/0.png"),
        asset("images/pages/slider/miniguard/en/1.png"),
        asset("images/pages/slider/miniguard/en/2.png"),
        asset("images/pages/slider/miniguard/en/3.png"),
        asset("images/pages/slider/miniguard/en/4.png"),
        asset("images/pages/slider/miniguard/en/5.png"),
        asset("images/pages/slider/miniguard/en/6.png"),
        asset("images/pages/slider/miniguard/en/7.png"),
    ],
    'ru' => [
        asset("images/pages/slider/miniguard/ru/0.png"),
        asset("images/pages/slider/miniguard/ru/1.png"),
        asset("images/pages/slider/miniguard/ru/2.png"),
        asset("images/pages/slider/miniguard/ru/3.png"),
        asset("images/pages/slider/miniguard/ru/4.png"),
        asset("images/pages/slider/miniguard/ru/5.png"),
        asset("images/pages/slider/miniguard/ru/6.png"),
        asset("images/pages/slider/miniguard/ru/7.png"),
    ],
    'he' => [
        asset("images/pages/slider/miniguard/he/0.png"),
        asset("images/pages/slider/miniguard/he/1.png"),
        asset("images/pages/slider/miniguard/he/2.png"),
        asset("images/pages/slider/miniguard/he/3.png"),
        asset("images/pages/slider/miniguard/he/4.png"),
        asset("images/pages/slider/miniguard/he/5.png"),
        asset("images/pages/slider/miniguard/he/6.png"),
        asset("images/pages/slider/miniguard/he/7.png"),
    ],
]

@endphp
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            @foreach($slides[$locale] as $image)
                <li><img src="{{$image}}" alt="Slide" title=""/></li>
            @endforeach
        </ul>
    </div>
    <div class="ws_shadow"></div>
</div>
