@php
    $locale = app()->getLocale();
$slides = [
    'en' => [
        asset("images/pages/slider/gatekeeper_and_digital_id/en/1.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/en/2.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/en/3.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/en/4.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/en/5.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/en/6.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/en/7.jpg"),
    ],
    'ru' => [
        asset("images/pages/slider/gatekeeper_and_digital_id/ru/1.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/ru/2.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/ru/3.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/ru/4.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/ru/5.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/ru/6.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/ru/7.jpg"),
    ],
    'he' => [
        asset("images/pages/slider/gatekeeper_and_digital_id/he/1.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/he/2.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/he/3.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/he/4.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/he/5.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/he/6.jpg"),
        asset("images/pages/slider/gatekeeper_and_digital_id/he/7.jpg"),
    ],
]

@endphp
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            @foreach($slides[$locale] as $image)
                <li><img src="{{$image}}" alt="Slide" title=""/></li>
            @endforeach
        </ul>
    </div>
    <div class="ws_shadow"></div>
</div>
