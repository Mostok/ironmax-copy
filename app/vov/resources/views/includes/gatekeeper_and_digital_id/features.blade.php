<div class="logotype">
    <img src="{{asset('images/pages/icons/Gatekeeper_Digital_ID.png')}}"
         alt="{{__('pages/gatekeeper_and_digital_id.title')}}">
</div>

<div class="content_block {{(App::getLocale()=="he")?"rtl":""}} {{(App::getLocale()=="ru")?"ru":""}}">
    <ul class="features-list">
    @foreach(__('pages/gatekeeper_and_digital_id.points') as $item)
        <li>
            <p>{!! $item !!}</p>
        </li>
    @endforeach
    </ul>
</div>