@php
    $class_bg_position = app()->getLocale() === 'he' ? 'he' : null
@endphp
<div class="modal fade" id="loginPopUp" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    {{__('header.auth.login.header')}}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('svg/icons/close_menu.svg')}}" class="sign-in-1-img" data-dismiss="loginPopUp"
                         aria-label="Close" alt="close">
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('login') }}" method="post" id="login-form">
                    {{ csrf_field() }}
                    <input class="{{$class_bg_position}}" type="email"
                           placeholder="{{__('Email')}}" name="email" required>
                    <input class="{{$class_bg_position}}" type="password"
                           placeholder="{{__('header.Password')}}" name="password" autocomplete="off" required>
                    <button class="mybutton" type="submit">{{__('header.auth.login.submit')}}</button>
                    <div class="text-center">
                        <label style="font-weight: 100;margin-bottom:16px;">
                            <input type="checkbox" checked="checked" name="remember"> {{__('header.Remember Me')}}
                        </label>
                        <div class="psw">
                            <a href="#" data-dismiss="modal" data-toggle="modal"
                               data-target="#forgotPopUp">{{__('header.Forgot password?')}}</a>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="forgotPopUp" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    {{__('header.auth.forgot.header')}}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('svg/icons/close_menu.svg')}}" class="sign-in-1-img" data-dismiss="loginPopUp"
                         aria-label="Close" alt="close">
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('forgot') }}" method="post" id="forgot-form">
                    {{ csrf_field() }}
                    <input class="{{$class_bg_position}}" type="email" placeholder="{{__('Email')}}" name="email"
                           required>
                    <div id="error_text" class="text-center"></div>

                    <button class="mybutton" type="submit">{{__('header.auth.forgot.submit')}}</button>
                    <div class="text-center">
                        <div class="psw">
                            <a href="#" data-dismiss="modal" data-toggle="modal"
                               data-target="#loginPopUp">{{__('header.return_to_auth')}}</a>
                        </div>
                    </div>
                </form>
                <div id="complete" class="text-center hidden-lg">

                    {{__('header.forgot_password_complete_message')}}

                    {{--                    <button class="mybutton" data-dismiss="modal" data-toggle="modal">--}}
                    {{--                        {{__('header.restore')}}--}}
                    {{--                    </button>--}}
                </div>
            </div>
        </div>
    </div>
</div>
