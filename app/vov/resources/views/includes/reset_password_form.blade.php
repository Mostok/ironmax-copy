@php
    $alignment = app()->getLocale() === 'he' ? 'right' : 'left'
@endphp

@section('header_assets')
    <style type="text/css" rel="stylesheet">
        body footer {
            position: fixed;
            bottom: 0;
            display: block;
            width: 100% !important;
        }
    </style>
@endsection

<div id="resetPassword">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    {{__('header.auth.reset.header')}}
                </h5>
            </div>
            <div class="modal-body">
                <form class="modal-content animate" action="{{ route('password.reset.post',[
	'token'=>$token,
	'email'=>$email,
	'lang'=>$lang ?? 'en',
]) }}" method="post" id="resetPasswordForm">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" id="token" value="{{$token}}">
                    <input type="hidden" name="email" id="email" value="{{$email}}">

                    <input type="password"
                           placeholder="{{__('header.auth.reset.new_password')}}" name="password"
                           required>
                    <input type="password"
                           placeholder="{{__('header.auth.reset.confirm_password')}}"
                           name="password_confirmation"
                           required>

                    <div class="text-center" id="pass_error_text" style="color:red">
                        @if($errors = session('errors'))
                            @foreach(json_decode($errors, true) as $field => $error_list)
                                {!! implode('<br/>', $error_list) !!}
                            @endforeach
                        @endif
                    </div>


                    <button class="mybutton" type="submit">{{__('header.auth.reset.submit')}}</button>
                    <div class="text-center">
                        <div class="psw">
                            <a href="#" data-dismiss="modal" data-toggle="modal"
                               data-target="#loginPopUp">{{__('header.auth.login.header')}}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
