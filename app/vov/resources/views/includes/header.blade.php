@php

    $lang = $lang ?? null;
    $product_active = $index = false;
    $lang_link_ru = route('index', ['lang' => 'ru']);
    $lang_link_he = route('index', ['lang' => 'he']);
    $lang_link_en = route('index', ['lang' => 'en']);
    $index = false;
    if (($current_rout = Illuminate\Support\Facades\Route::current()) && isset($current_rout->action['as'])) {
        $product_active = $current_rout->action['as'] === 'product_static_page';
        $index = $current_rout->action['as'] === 'index';
        $lang_link_ru = route($current_rout->action['as'], array_merge(
            $current_rout->originalParameters(), ['lang' => 'ru']
        ));
        $lang_link_he = route($current_rout->action['as'], array_merge(
            $current_rout->originalParameters(), ['lang' => 'he']
        ));
        $lang_link_en = route($current_rout->action['as'], array_merge(
            $current_rout->originalParameters(), ['lang' => 'en']
        ));
    }
    $rtlClass = app()->getLocale() === 'he' ? 'rtl' : null;
    $patrol = asset('svg/icons/product/patrol_short_logo.svg');
    $sos = asset('svg/icons/product/sos_icon_short.svg');
    $mini_guard = asset('svg/icons/product/miniguard_short_logo.svg');
    $shifter = asset('svg/icons/product/shifter_logo.svg');
    $gate_keeper = asset('svg/icons/product/gatekeeper_icon.svg');
    $digital_id = asset('svg/icons/product/digital_id_icon.svg');

@endphp
<header id="header">
    <nav class="navbar navbar-expand-lg bg-dark fixed-top" id="main_nav">
        <div class="container">
            <a class="navbar-brand" href="{{route('index',['lang'=>$lang])}}">
                <img src="{{asset('images/logo/logo4.png')}}" style="width: 65px" alt="Logo"></a>
            <button class="navbar-toggler hamburger hamburger--squeeze {{$lang}}" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
            <div class="collapse navbar-collapse {{$lang}}" id="navbarSupportedContent">
                <ul class="navbar-nav top-menu mx-auto">
                    <li class="nav-item">
                        <a
                            class="nav-link"
                            href="{{$index? null :'/'}}#home"
                            data-target="{{$index? null :'/'}}#home"
                            data-scroll-to="#home">
                            {{__('menu.home')}}
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a
                            class="nav-link dropdown-toggle"
                            href="{{ $index ? '#products' : '#product'}}"
                            data-target="{{ $index ? '#products' : '#product'}}"
                            data-scroll-to="{{ $index ? '#products' : '#product'}}"
                            id="navbarDropdownProduct"
                            data-toggle="dropdown1"
                            role="button"
                            aria-haspopup="false"
                            aria-expanded="false"
                        >
                            {{__('menu.products')}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownProduct">
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'patrol','lang'=>$lang])}}">
                                <span>{{__('menu.patrol')}}</span>
                                <img src="{{$patrol}}" alt="Patrol"/>
                            </a>
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'sos','lang'=>$lang])}}">
                                <span>{{__('menu.sos')}}</span>
                                <img src="{{$sos}}" alt="SOS"/>
                            </a>
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'miniguard','lang'=>$lang])}}">
                                <span>{{__('menu.mini_guard')}}</span>
                                <img src="{{$mini_guard}}" alt="Miniguard"/>
                            </a>
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'shifter','lang'=>$lang])}}">
                                <span>{{__('menu.shifter')}}</span>
                                <img src="{{$shifter}}" alt="Shifter"/>
                            </a>
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'gatekeeper_and_digital_id','lang'=>$lang])}}">
                                <span>{!! __('menu.gate_keeper') !!}</span>
                                <img src="{{$gate_keeper}}" alt="Shifter"/>
                            </a>
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'gatekeeper_and_digital_id','lang'=>$lang])}}">
                                <span>{!! __('menu.digital_id') !!}</span>
                                <img src="{{$digital_id}}" alt="Shifter"/>
                            </a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{$index? null : '/'}}#contacts"
                           data-scroll-to="{{$index? null : '/'}}#contacts">{{__('menu.contact')}}</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a
                            class="nav-link dropdown-toggle"
                            data-scroll-to="#FAQ"
                            data-target="#FAQ"
                            id="navbarDropdownFAQ"
                            role="button"
                            data-toggle="dropdown2"
                            aria-haspopup="true"
                            aria-expanded="false">
                            {{__('menu.faqs')}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownFAQ">
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'patrol','lang'=>$lang]).'#FAQ'}}">
                                <span>{{__('menu.patrol')}}</span>
                                <img src="{{$patrol}}" alt="patrol"/>
                            </a>
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'sos','lang'=>$lang]).'#FAQ'}}">
                                <span>{{__('menu.sos')}}</span>
                                <img src="{{$sos}}" alt="sos"/>
                            </a>
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'miniguard','lang'=>$lang]).'#FAQ'}}">
                                <span>{{__('menu.mini_guard')}}</span>
                                <img src="{{$mini_guard}}" alt="miniguard"/>
                            </a>
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'shifter','lang'=>$lang]).'#FAQ'}}">
                                <span>{{__('menu.shifter')}}</span>
                                <img src="{{$shifter}}" alt="shifter"/>
                            </a>
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'gatekeeper_and_digital_id','lang'=>$lang]).'#FAQ'}}">
                                <span>{!! __('menu.gate_keeper') !!}</span>
                                <img src="{{$gate_keeper}}" alt="Shifter"/>
                            </a>
                            <a class="dropdown-item"
                               href="{{route('product_static_page',['product_static_page'=>'gatekeeper_and_digital_id','lang'=>$lang]).'#FAQ'}}">
                                <span>{!! __('menu.digital_id') !!}</span>
                                <img src="{{$digital_id}}" alt="Shifter"/>
                            </a>
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown langdrop nav-item-icon-btn">
                        <a class="nav-link dropdown-toggle <?= (app()->getLocale() !== 'he') ? 'rtl-mobile' : 'he-align-end' ?>" href="#" id="navbarDropdownl" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @switch(app()->getLocale())
                                @case('ru')
                                <img class="langflag" src="{{asset('images/flag/countries_russia.png')}}"
                                     alt="russian"/>
                                @break
                                @case('he')
                                <img class="langflag he" src="{{asset('images/flag/countries_israel.png')}}"
                                     alt="hebrew"/>
                                @break
                                @case('en')
                                @default
                                <img class="langflag" src="{{asset('images/flag/countries_english.png')}}"
                                     alt="english"/>
                                @break
                            @endswitch
                            <span class="d-inline-block d-lg-none m-0">{{__('menu.lang')}}</span>
                        </a>
                        <div class="dropdown-menu lang" aria-labelledby="navbarDropdownl">
                            <a class="dropdown-item" href="{{$lang_link_en}}">English
                                <img class="img-flag" src="{{asset('images/flag/countries_english.png')}}"
                                     alt="english"/>
                            </a>
                            <a class="dropdown-item" href="{{$lang_link_he}}">עברית
                                <img class="img-flag" src="{{asset('images/flag/countries_israel.png')}}"
                                     alt="hebrew"/>
                            </a>
                            <a class="dropdown-item" href="{{$lang_link_ru}}">Русский
                                <img class="img-flag" src="{{asset('images/flag/countries_russia.png')}}"
                                     alt="russian"/>
                            </a>
                        </div>
                    </li>
                    @guest
                        <li class="nav-item login-item">
                            <a class="nav-link " href="#" data-toggle="modal"
                               data-target="#loginPopUp">{{__('menu.login')}}
                                <span style="margin-bottom: 2px;">
                                    <img src="{{asset('/images/frontend/login_unmoved_part.png')}}" alt="login"
                                         class="login-icon">
                                    <img src="{{asset('/images/frontend/login_arrow.png')}}" alt="login"
                                         class="login-icon arrow">
                            </span>
                            </a>
                        </li>
                    @endguest
                    @auth
                        <li style="color: white" class="{{$rtlClass}}">
                            @can('boss')
                                <a class="nav-link" href="{{route('dashboardIndex')}}">{{__('menu.dashboard')}}</a>
                            @endcan
                            @can('admin')
                                <a class="nav-link" href="{{route('adminIndex')}}">{{__('menu.admin_panel')}}</a>
                            @endcan
                        </li>
                        <li class="nav-item nav-item-icon-btn {{$rtlClass}}" data-target="home" id="logout_block">
                            <a class="nav-link" href="{{route('logout')}}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16.721"
                                     height="18.393" viewBox="0 0 16.721 18.393">
                                    <rect width="2.125" height="10.833" rx="1.062" transform="translate(7.298)"/>
                                    <path
                                        d="M123.431,98.019a1.006,1.006,0,0,0-.463-.168l-.022,0a.889.889,0,0,0-.133,0A1.063,1.063,0,0,0,121.8,98.9a.886.886,0,0,0,.06.3l-.005.007c.007.021.021.035.029.055a1.209,1.209,0,0,0,.095.178c.014.021.023.049.036.068a.958.958,0,0,0,.127.146,2.079,2.079,0,0,0,.356.309,6.215,6.215,0,1,1-7.379.116.238.238,0,0,1,.032-.026s.327-.257.674-.59a1.023,1.023,0,0,0,.194-.575A1.055,1.055,0,0,0,115,97.842c-.028,0-.058-.005-.085,0a1.926,1.926,0,0,0-.676.278,8.355,8.355,0,1,0,9.187-.1Z"
                                        transform="translate(-110.545 -94.995)"/>
                                </svg>
                                <span class="d-inline-block d-lg-none">{{__('menu.logout')}}</span>
                            </a>
                        </li>
                    @endauth
                </ul>
            </div>
        </div>
    </nav>
</header>
