<div class="logotype">
    <img src="{{asset('images/product/4sos.png')}}"
         alt="{{__('pages/sos.title')}}">
</div>
<div class="content_block {{(App::getLocale()=="he")?"rtl":""}} {{(App::getLocale()=="ru")?"ru":""}}">
    <ul class="features-list">
    @foreach(__('pages/sos.points') as $item)
        <li>
            <p>{!! $item !!}</p>
        </li>
    @endforeach
    </ul>
</div>