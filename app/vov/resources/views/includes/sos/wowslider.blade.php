@php
    $locale = app()->getLocale();
$slides = [
    'en' => [
        asset("images/pages/slider/sos/en/0.png"),
        asset("images/pages/slider/sos/en/1.jpg"),
        asset("images/pages/slider/sos/en/2.jpg"),
        asset("images/pages/slider/sos/en/3.jpg"),
        asset("images/pages/slider/sos/en/4.jpg"),
    ],
    'ru' => [
        asset("images/pages/slider/sos/ru/0.png"),
        asset("images/pages/slider/sos/ru/1.jpg"),
        asset("images/pages/slider/sos/ru/2.jpg"),
        asset("images/pages/slider/sos/ru/3.jpg"),
        asset("images/pages/slider/sos/ru/4.jpg"),
    ],
    'he' => [
        asset("images/pages/slider/sos/he/0.png"),
        asset("images/pages/slider/sos/he/1.jpg"),
        asset("images/pages/slider/sos/he/2.jpg"),
        asset("images/pages/slider/sos/he/3.jpg"),
        asset("images/pages/slider/sos/he/4.jpg"),
    ],
]

@endphp
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            @foreach($slides[$locale] as $image)
                <li><img src="{{$image}}" alt="Slide" title=""/></li>
            @endforeach
        </ul>
    </div>
    <div class="ws_shadow"></div>
</div>
