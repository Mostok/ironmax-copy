@if(isset($page_title) and $page_title=="Home")
    @if(isset($locale) and $locale == "ru")
        <title>Технологические решения для вашей компании - IRONMAX Technologies</title>
        <meta name="description" content="IRONMAX Technologies - это компания-разработчик программного обеспечения, предоставляющая передовые технологические решения для бизнеса. Программные продукты  IRONMAX Technologies повысят эффективность вашей компании и добавят новые инструменты для правильного мониторинга и управления вашим бизнесом.">
    @elseif(isset($locale) and $locale == "he")
        <title>פתרונות טכנולוגיים מתקדמים עבור הארגון שלך - IRONMAX Technologies</title>
        <meta name="description" content="IRONMAX Technologies הינה חברת תוכנה המספקת פתרונות טכנולוגיים לחברות וארגונים.מוצרי התוכנה של החברה ישדרגו ויוסיפו כלים למעקב וניהול נכון של העסק שלך.">
    @else
        <title>High-quality tech solutions for your company - IRONMAX Technologies</title>
        <meta name="description" content="IRONMAX Technologies is a software company that provides technological solutions to companies and organizations. The company's software products will get your business to the next level and add tools for proper monitoring and management of your business.">
    @endif
@endif

@if(isset($page_title) and $page_title=="Patrol")
    @if(isset($locale) and $locale == "ru")
        <title>Управления патрулями в режиме реального времени по привлекательной цене - IRONMAX Patrol</title>
        <meta name="description" content="IRONMAX Patrol - это современная эффективная  система мониторинга патрулей в режиме реального времени. Наша система контролирует патрули охранников с помощью специального приложения, что позволяет охранным компаниям и организациям эффективно управлять силами безопасности.">
    @elseif(isset($locale) and $locale == "he")
        <title>מערכת תוכנה למעקב אחר סיורי אבטחה במחיר אטרקטיבי IRONMAX PATROL</title>
        <meta name="description" content="IRONMAX Patrol הינה מערכת התוכנה המובילה לבקרה, בדיקה ומעקב אחר סיורי אבטחה.המערכת מתעדת את סיור המאבטח במתקן בזמן אמת בעזרת אפליקציה ייעודית ובכך מאפשרת לחברות האבטחה לנהל ביעילות את המתקנים והמאבטחים שברשותה.">
    @else
        <title>Realtime patrol management software at an attractive price - IRONMAX Patrol</title>
        <meta name="description" content="IRONMAX Patrol is the leading real time and on-line guard tour and patrol system. Our system monitors the security guard's patrol in real time with the help of a dedicated application, thus enabling security companies  and organizations to manage their guards efficiently.">
    @endif
@endif

@if(isset($page_title) and $page_title=="Miniguard")
    @if(isset($locale) and $locale == "ru")
        <title>Современная  система сигнализации, работающая в режиме реального времени - IRONMAX MiniGuard</title>
        <meta name="description" content="Маленькая и тихая система сигнализации, предупреждающая о взломе через специальное приложение смартфона.">
    @elseif(isset($locale) and $locale == "he")
        <title>IRONMAX MiniGuard - מערכת אזעקה חכמה</title>
        <meta name="description" content="IRONMAX Technologies הינה חברת תוכנה המספקת פתרונות טכנולוגיים לחברות וארגונים.מוצרי התוכנה של החברה ישדרגו ויוסיפו כלים למעקב וניהול נכון של העסק שלך.">
    @else
        <title>IRONMAX MiniGuard - Realtime & Smart alarm system</title>
        <meta name="description" content="IRONMAX Technologies is a software company that provides technological solutions to companies and organizations. The company's software products will get your business to the next level and add tools for proper monitoring and management of your business.">
    @endif
@endif

@if(isset($page_title) and $page_title=="Sos")
    @if(isset($locale) and $locale == "ru")
        <title>Кнопка экстренного оповещения о чрезвычайной ситуации через специальное приложение смартфона</title>
        <meta name="description" content="Кнопка экстренного оповещения о чрезвычайной ситуации через специальное приложение смартфона">
    @elseif(isset($locale) and $locale == "he")
        <title>לחצן התראת חירום באמצעות אפליקציית סמארטפון ייעודית</title>
        <meta name="description" content="מערכת אזעקה קטנה ושקטה המתריעה על פריצה ישירות לסמארטפון דרך אפליקציה ייעודית.">
    @else
        <title>Quick alert button for the emergency cases via dedicated smartphone app</title>
        <meta name="description" content="A tiny and quiet alarm system that alerts you about breaking via your smartphone's special app.">
    @endif
@endif

@if(isset($page_title) and $page_title=="Shifter")
    @if(isset($locale) and $locale == "ru")
        <title>Простой способ эффективного управления сменами Работников - IRONMAX Shifter</title>
        <meta name="description" content="Система для управления временем посещаемости и отслеживания смен сотрудников">
    @elseif(isset($locale) and $locale == "he")
        <title>IRONMAX Shifter - תוכנה לניהול משמרות עובדים באופן פשוט ויעיל</title>
        <meta name="description" content="מערכת תוכנה בשילוב עם אפליקציה לניהול שעון נוכחות ולמעקב אחר משמרות העובדים">
    @else
        <title>Simple way to manage work shifts efficiently - IRONMAX Shifter Software</title>
        <meta name="description" content="A software system combined with an application for managing attendance time and monitoring employee shifts">
    @endif
@endif

@if(isset($page_title) and $page_title=="Gatekeeper and Digital ID")
    @if(isset($locale) and $locale == "ru")
        <title>Эффективный способ управления записями посетителей - IRONMAX Gatekeeper</title>
        <meta name="description" content="Система для контроля и мониторинга входа посетителей и сотрудников на объект">
    @elseif(isset($locale) and $locale == "he")
        <title>IRONMAX Gatekeeper - תוכנה למעקב אחר כניסות מבקרים באופן פשוט ויעיל</title>
        <meta name="description" content="מערכת תוכנה בשילוב עם אפליקציה לבקרה ומעקב אחר כניסת מבקרים ועובדים למתקן">
    @else
        <title>Simple way to manage visitors entries efficiently - IRONMAX Gatekeeper Software</title>
        <meta name="description" content="A software system combined with an app for controlling and monitoring the entry of visitors and employees to the facility">
    @endif
@endif
