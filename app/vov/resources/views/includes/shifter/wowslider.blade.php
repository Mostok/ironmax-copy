@php
    $locale = app()->getLocale();
$slides = [
    'en' => [
        asset("images/pages/slider/shifter/en/0.png"),
        asset("images/pages/slider/shifter/en/1.png"),
        asset("images/pages/slider/shifter/en/2.png"),
        asset("images/pages/slider/shifter/en/3.png"),
        asset("images/pages/slider/shifter/en/4.png"),
        asset("images/pages/slider/shifter/en/5.png"),
        asset("images/pages/slider/shifter/en/6.png"),
        asset("images/pages/slider/shifter/en/7.png"),
    ],
    'ru' => [
        asset("images/pages/slider/shifter/ru/0.png"),
        asset("images/pages/slider/shifter/ru/1.png"),
        asset("images/pages/slider/shifter/ru/2.png"),
        asset("images/pages/slider/shifter/ru/3.png"),
        asset("images/pages/slider/shifter/ru/4.png"),
        asset("images/pages/slider/shifter/ru/5.png"),
        asset("images/pages/slider/shifter/ru/6.png"),
        asset("images/pages/slider/shifter/ru/7.png"),
    ],
    'he' => [
        asset("images/pages/slider/shifter/he/0.png"),
        asset("images/pages/slider/shifter/he/1.png"),
        asset("images/pages/slider/shifter/he/2.png"),
        asset("images/pages/slider/shifter/he/3.png"),
        asset("images/pages/slider/shifter/he/4.png"),
        asset("images/pages/slider/shifter/he/5.png"),
        asset("images/pages/slider/shifter/he/6.png"),
        asset("images/pages/slider/shifter/he/7.png"),
    ],
]

@endphp
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            @foreach($slides[$locale] as $image)
                <li><img src="{{$image}}" alt="Slide" title=""/></li>
            @endforeach
        </ul>
    </div>
    <div class="ws_shadow"></div>
</div>
