<div class="logotype">
    <img src="{{asset('images/pages/icons/shifter.png')}}"
         alt="{{__('pages/shifter.title')}}">
</div>

<div class="content_block {{(App::getLocale()=="he")?"rtl":""}} {{(App::getLocale()=="ru")?"ru":""}}">
    <ul class="features-list">
    @foreach(__('pages/shifter.points') as $item)
        <li>
            <p>{!! $item !!}</p>
        </li>
    @endforeach
    </ul>
</div>