<meta charset="UTF-8">
<!-- For IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- For Resposive Device -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap">
<link rel="preload" as="style" href="{{mix('css/fonts.css')}}">
<link rel="preload" as="style" href="{{mix('css/app.css')}}">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{mix('css/fonts.css')}}">
<link rel="stylesheet" type="text/css" href="{{mix('css/app.css')}}">
<link rel="icon" type="image/png" sizes="56x56" href="{{asset('images/fav-icon/icon1.png')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('includes.metadata',["page_title" => ($page_title ?? ''), "locale" => app()->getLocale()])
@if(app()->isLocale('he'))
    <style>
        input[type=text], input[type=password], input[type=email], textarea {
            direction: rtl;
        }

        .send-message .g-recaptcha {
            direction: rtl;
        }

        .send-message button {
            margin-left: auto;
        }

        .accordion--clear .accordion__child .accordion__toggle::before, .accordion--highlight .accordion__child .accordion__toggle::before {
            top: 10px;
            left: 0px;
            right: 10px;
            direction: rtl;
            float: right;
            position: relative;
        }

        .accordion__child .accordion__label {
            margin-right: 1.85714em;
            text-align: right;
        }

        .accordion-controller-wrapper .button {
            margin: 0.42857em 0 0.42857em 1.71429em;
        }

        .accordion-controller-wrapper {
            direction: rtl;
        }

        .accordion--clear .accordion__child .accordion__toggle:focus, .accordion--clear .accordion__child .accordion__toggle:hover, .accordion--highlight .accordion__child .accordion__toggle:focus, .accordion--highlight .accordion__child .accordion__toggle:hover {
            background-image: linear-gradient(270deg, #1382cf 0%, #1382cf 8px, transparent 8px, transparent 100%);
            text-decoration: none;
        }

        summary::before, .accordion__toggle::before {
            transform: rotate(-30deg);
        }

        .psw, .service-item, #contacts, .navbar-nav.top-menu {
            direction: rtl;
        }

        @media (max-width: 768px) {
            .navbar-nav .langdrop, .navbar-nav .login-item {
                margin-left: auto;
            }
        }

        .cfont {
            text-align: right !important;
            padding-right: 10px !important;
        }

        #pp_pages {
            direction: rtl;
        }
    </style>
@endif
