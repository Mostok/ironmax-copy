@isset($slides)
    <?php $locale = app()->getLocale(); ?>
    <div id="topslider" class="swiper-container swiper-scale-effect">
        <div class="swiper-wrapper"
             style="background-image:url('{{asset('/images/banner_img/background_ready/background_ready.png')}}'); background-position: bottom; background-size: cover;  background-attachment: fixed">

            @foreach ($slides as $slide)

                <div class="swiper-slide">
                    <div class="swiper-slide-cover">
                        <img src='/images/banner_img/{{$slide['image']}}' class="mobile-img-width" alt="{{$slide['alt']}}">
                    </div>
                    <div class="slide-content">
                        <div class="txt-wrap h1">
                            <h1 class="animated slideInUp">{!! __($slide['title']) !!}</h1>
                        </div>
                        <div class="txt-wrap">
                            <h5 class="animated slideInUp">{!! __($slide['subtitle']) !!}</h5>
                        </div>
                    </div>
                </div>

            @endforeach

        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <div class="arrowwrapper">
            <img class="moveup animated slideInUp" style="width: 30px; height: 26px;"
                 src="/images/logo/arrow.png" alt="arrow"/>
        </div>
    </div>

@endisset
