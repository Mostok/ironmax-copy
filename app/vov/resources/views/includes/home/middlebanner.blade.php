@php
$products = [
    ['sort' => 1, 'name' => 'patrol', 'link' => '/patrol', 'img_src' => 'patrol', 'alt_en' => 'Patrols monitoring app', 'alt_he' => 'אפליקציה לסיורי אבטחה', 'alt_ru' => 'Приложение по мониторингу патрулей'],
    ['sort' => 2, 'name' => 'sos', 'link' => '/sos', 'img_src' => 'sos', 'alt_en' => 'Smart emergency call button', 'alt_he' => 'לחצן מצוקה חכם', 'alt_ru' => 'Умная кнопка экстренного вызова'],
    ['sort' => 3, 'name' => 'miniguard', 'link' => '/miniguard', 'img_src' => 'miniguard', 'alt_en' => 'Smart alarm system', 'alt_he' => 'מערכת אזעקה חכמה', 'alt_ru' => 'Умная сигнализация'],
    ['sort' => 4, 'name' => 'shifter', 'link' => '/shifter', 'img_src' => 'shifter', 'alt_en' => 'Working time monitoring software', 'alt_he' => 'תוכנת שעון נוכחות', 'alt_ru' => 'Программное обеспечение для мониторинга рабочего времени'],
    ['sort' => 5, 'name' => 'gatekeeper', 'link' => '/gatekeeper_and_digital_id', 'img_src' => 'gatekeeper', 'alt_en' => 'Software for Monitoring Visitors ', 'alt_he' => 'תוכנה לבקרת כניסת מבקרים', 'alt_ru' => 'Программное обеспечение для мониторинга входа посетителей'],
    ['sort' => 6, 'name' => 'digital_id', 'link' => '/gatekeeper_and_digital_id', 'img_src' => 'digital_id', 'alt_en' => 'Software for Monitoring Visitors ', 'alt_he' => 'תוכנה לבקרת כניסת מבקרים', 'alt_ru' => 'Программное обеспечение для мониторинга входа посетителей'],
];
@endphp
<section style="background-attachment: fixed" id="products">
    <slot name="header">
        <h2>{{ __('header.IRONMAX PRODUCTS') }}</h2>
    </slot>
    <div class="row ">
        <div class="col-xl-10 offset-xl-1 col-md-12 offset-md-0">
            <div class="d-flex justify-content-around flex-wrap product-block-row">
                @foreach ($products as $product)
                    <div class="product-block col-md-6 col-xl-4">
                        <div class="pr-0-mobile h-100 d-flex align-content-end justify-content-end pr-5">
                            <a href="{{ $product['link'] }}" class="h-100 d-flex ">
                                <img src="{{ '/svg/front/' . app()->getLocale() . '/' . $product['img_src'] . '.svg' }}"
                                    class="product-icon back" title=""
                                    alt="{{ $product['alt_' . app()->getLocale()] }}">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

@section('js')
    <script>
        $(document).ready(function() {
            $('.product-block').hover(function(e) {
                if (e.type === 'mouseenter') {
                    e.currentTarget.querySelector('img').src = e.currentTarget.querySelector('img').src
                        .replace('front', 'back');
                } else if (e.type === 'mouseleave') {
                    e.currentTarget.querySelector('img').src = e.currentTarget.querySelector('img').src
                        .replace('back', 'front');
                }
            })
        });
    </script>
@endsection
