<div id="service-section" class="{{checkwebp()}}">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="service-item text-center"><a>
                        <img src="{{asset('images/fav-icon/11.png')}}" alt="" class="s-regular">
                        <img src="{{asset('images/fav-icon/1.png')}}" alt="" class="s-hover">
                        <dl>
                            <dt>{{__('header.30 Days Trial Free')}}</dt>
                            <dd>{{__('header.VAR1')}}</dd>
                        </dl>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="service-item text-center"><a id="setpadd">
                        <img src="{{asset('images/fav-icon/22.png')}}" alt="" class="s-regular">
                        <img src="{{asset('images/fav-icon/2.png')}}" alt="" class="s-hover">
                        <dl>
                            <dt>{{__('header.VAR18')}}</dt>
                            <dd>{{__('header.VAR2')}}</dd>
                        </dl>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="service-item text-center"><a>
                        <img src="{{asset('images/fav-icon/33.png')}}" alt="" class="s-regular">
                        <img src="{{asset('images/fav-icon/3.png')}}" alt="" class="s-hover">
                        <dl>
                            <dt>{{__('header.VAR19')}}</dt>
                            <dd>{{__('header.VAR3')}}</dd>
                        </dl>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
