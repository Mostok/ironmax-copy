<div class="num_count1" style="background-color: #000000">
    <h2 style="color:#fff;margin-bottom:50px">{!!__('header.IRONMAX IN NUMBERS')!!}</h2>
    <div class="row">
        <div class="col-md-3 numcount">
            <div class="counter" data-number="11"></div>
            <div class="counter-text">{{__('header.customers')}}</div>
        </div>
        <div class="col-md-3 numcount">
            <div class="counter" data-number="1327"></div>
            <div class="counter-text">{{__('header.patrols')}}</div>
        </div>
        <div class="col-md-3 numcount">
            <div class="counter" data-number="18578"></div>
            <div class="counter-text">{{__('header.barcodes')}}</div>
        </div>
        <div class="col-md-3 numcount">
            <div class="counter" data-number="347"></div>
            <div class="counter-text">{{__('header.events')}}</div>
        </div>
    </div>
</div>
