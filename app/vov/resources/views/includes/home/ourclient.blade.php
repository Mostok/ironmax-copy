<div id="our-client">
    <div class="container">
        <div class="client-slider" style="width:100%; ">
            <div class="item">
                <div class="row">
                    <div class="col-md-4 d-flex" style="">
                        <img class="img-m" style="border-radius: 15px 60px 150px 5px;"
                             src="{{asset('images/home/cartumizer.png')}}" alt="Cartomizer"
                        />
                    </div>
                    <div class="col-md-8">
                        <img style="width: 50px; height: 50px" src="{{asset('images/home/q1.png')}}" alt="quotes"/>
                        <div class="container7 d-flex align-items-center">
                            <div class="vertical-center d-flex align-items-center" style="height:100%">
                                <div class="inner-slide">
                                    <p style="direction:rtl;padding-left: 50px;text-align: right;letter-spacing: 3px; width: 100%;">
                                        אנו משתמשים באפליקציית IRONMAX Patrol על מנת להגביר את מערך האבטחה בבניין
                                        המשרדים שלנו בלילות.
                                        האפליקציה מאוד נוחה ומספקת המון כלים שימושיים למאבטח.
                                        בנוסף הדוחות שנשלחים לאחראי על האבטחה מפורטים ותורמים למעקב אחר הסיורים הליליים.
                                    </p>
                                    <h4>- Cartomizer -</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 d-flex" style="">
                        <img class="img-m" style="border-radius: 15px 60px 150px 5px;"
                             src="{{asset('images/home/codegasm.png')}}" alt="codegasm"
                        />
                    </div>
                    <div class="col-md-8">
                        <img style="width: 50px; height: 50px" src="{{asset('images/home/q1.png')}}" alt="quotes"/>
                        <div class="container7 d-flex align-items-center" style="height:100%">
                            <div class="vertical-center d-flex align-items-center" style="height:100%">
                                <div class="inner-slide">
                                    <p>
                                        Shifter application helps us to manage our workers shifts.<br>
                                        It’s easy to use and it saves us a lot of time.<br>
                                        Thanks!
                                    </p>
                                    <h4>- CodeGasm -</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 d-flex">
                        <img class="img-m" style="border-radius: 15px 60px 150px 5px;"
                             src="{{asset('images/home/motif.png')}}" alt="motif"
                        />
                    </div>
                    <div class="col-md-8">
                        <img style="width: 50px; height: 50px" src="{{asset('images/home/q1.png')}}" alt="quotes"/>
                        <div class="container7 d-flex align-items-center">
                            <div class="vertical-center d-flex align-items-center" style="height:100%">
                                <div class="inner-slide">
                                    <p>
                                        Great Application.<br>
                                        Keep our security officer connected to his guards. Highly recommended.
                                    </p>
                                    <h4>- Motif -</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 d-flex">
                        <img class="img-m" style="border-radius: 15px 60px 150px 5px;"
                             src="{{asset('images/home/shelfit.png')}}" alt="shelfit"
                        />
                    </div>
                    <div class="col-md-8">
                        <img style="width: 50px; height: 50px" src="{{asset('images/home/q1.png')}}" alt="quotes"/>
                        <div class="container7 d-flex align-items-center">
                            <div class="vertical-center d-flex align-items-center" style="height:100%">
                                <div class="inner-slide">
                                    <p>
                                        Great and inexpensive solution for SOS button.</p>
                                    <h4>- ShelfIt -</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /.client-slider -->
    </div> <!-- /.container -->
</div> <!-- /#our-client -->
