<div id="contacts">
    <div class="container">
        <div class="row contact-address-content">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="left-side {{(App::getLocale()=="he")?'rtl':''}}">
                    <h2>{{__('header.VAR8')}}</h2>
                    <ul>
                        <li class="d-flex">
                            <div class="icon tran3s round-border p-color-bg">
                                <i class="fas fa-map-marker-alt" aria-hidden="true"></i></div>
                            <div class="">
                                <h6>{{__('header.ADDRESS')}}</h6>
                                <p>{{__('header.address')}}</p>
                            </div>
                        </li>
                        <li class="d-flex">
                            <div class="icon tran3s round-border p-color-bg">
                                <i class="fas fa-phone-alt" aria-hidden="true"></i></div>
                            <div class="">
                                <h6>{{__('header.VAR5')}}</h6>
                                <p>{{__('header.phone1')}} @if (!App::isLocale('he') && !App::isLocale('ru')) / {{__('header.phone2')}} @endif </p>
                            </div>
                        </li>
                        <li class="d-flex">
                            <div class="icon tran3s round-border p-color-bg">
                                <i class="fas fa-envelope" aria-hidden="true"></i></div>
                            <div class="">
                                <h6>{{__('header.VAR6')}}</h6>
                                <p>{{__('header.email_target')}}</p>
                            </div>
                        </li>
                    </ul>
                </div> <!-- /.left-side -->
            </div> <!-- /.col- -->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="map-area">
                    <h2>{{__('header.VAR7')}}</h2>
                    <div id="map" style="height: 330px">
                        <iframe style="height: 330px;width: 100%" id="gmap_canvas"
                                loading="lazy"
                                src="https://maps.google.com/maps?q=Yiga'el%20Yadin%20St%2011%2C%20Holon%2C%20Israel&t=k&z=12&ie=UTF8&iwloc=&output=embed"></iframe>
                    </div>
                </div> <!-- /.map-area -->
            </div> <!-- /.col- -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- Contact Form -->
                <div class="send-message">
                    <h2>{{__('header.VAR4')}}</h2>
                    <form action="{{route('send_message')}}" class="form-validation" autocomplete="off" method="post"
                          id="contact-form">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12" style="width: 50%;    padding-right: 14px;">
                                <div class="single-input">
                                    <input type="text" placeholder="{{__('header.VAR10')}}" name="name" required>
                                </div> <!-- /.single-input -->
                            </div>
                            <div class="col-sm-6 col-xs-12" style="width: 50%; padding-left: 14px">
                                <div class="single-input">
                                    <input type="email" placeholder="{{__('header.VAR9')}}" name="email" required>
                                </div> <!-- /.single-input -->
                            </div>
                        </div> <!-- /.row -->
                        <div class="single-input">
                            <input type="text" placeholder="{{__('header.VAR11')}}" name="subject" required>
                        </div> <!-- /.single-input -->
                        <textarea placeholder="{{__('header.VAR12')}}" name="message" required></textarea>
                        <br/>
                        <div id="contactrecapture"></div>
                        <br/>
                        <button class="tran3s p-color-bg">{{__('header.Sendbutton')}}</button>
                    </form>
                    <div id="contact-success">
                        <p>{{__('header.Your message')}}</p>
                    </div>
                    <!-- Contact Form Validation Markup -->
                    <!-- Contact alert -->
                    <div class="alert-wrapper" id="alert-success">
                        <div id="success">
                            <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                            <div class="wrapper">
                                <p>Your message was sent successfully.</p>
                            </div>
                        </div>
                    </div> <!-- End of .alert_wrapper -->
                    <div class="alert-wrapper" id="alert-error">
                        <div id="error">
                            <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                            <div class="wrapper">
                                <p>Sorry!Something Went Wrong.</p>
                            </div>
                        </div>
                    </div> <!-- End of .alert_wrapper -->
                </div> <!-- /.send-message -->
            </div>
        </div>
    </div>
</div>
