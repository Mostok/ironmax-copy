<section id="about-us" class="{{(App::getLocale()=="he")?"rtl":""}} {{(App::getLocale()=="ru")?"ru":""}}">
    <div class="container">
        <h2>{{__('header.VAR17')}}</h2>
        <div class="row">
            <div class="col-md-6 pb-4 ch-text">
                <div class="check-mark">
                    <img style="display: inline;float: left;"
                         src="{{asset('images/logo/check-mark-smo25.png')}}" alt="check-mark"/>
                </div>
                <div class="cfont" style="width: auto; display: flex; text-align: left;font-size: 16px">
                    {{ __('header.ADV1') }}
                </div>
                <div class="check-mark">
                    <img style="display: inline;float: left;"
                         src="{{asset('images/logo/check-mark-smo25.png')}}" alt="check-mark"/>
                </div>
                <div class="cfont" style="width: auto;display: flex; text-align: left;font-size: 16px">
                    {{ __('header.ADV2') }}
                </div>
                <div class="check-mark">
                    <img style="display: inline;float: left;"
                         src="{{asset('images/logo/check-mark-smo25.png')}}" alt="check-mark"/>
                </div>
                <div class="cfont" style="width: auto;display: flex; text-align: left;font-size: 16px">
                    {{ __('header.ADV3') }}
                </div>
                <div class="check-mark">
                    <img style="display: inline;float: left;"
                         src="{{asset('images/logo/check-mark-smo25.png')}}" alt="check-mark"/>
                </div>
                <div class="cfont" style="width: auto; display: flex; text-align: left;font-size: 16px">
                    {{ __('header.ADV4') }}
                </div>
                <div class="check-mark">
                    <img style="display: inline;float: left;"
                         src="{{asset('images/logo/check-mark-smo25.png')}}" alt="check-mark"/>
                </div>
                <div class="cfont" style="width: auto;display: flex; text-align: left;font-size: 16px">
                    {{ __('header.ADV5') }}
                </div>
                <div class="check-mark">
                    <img style="display: inline;float: left;"
                         src="{{asset('images/logo/check-mark-smo25.png')}}" alt="check-mark"/>
                </div>
                <div class="cfont" style="width: auto;display: flex; text-align: left;font-size: 16px">
                    {{ __('header.ADV6') }}
                </div>
                <div class="check-mark">
                    <img style="display: inline;float: left;"
                         src="{{asset('images/logo/check-mark-smo25.png')}}" alt="check-mark"/>
                </div>
                <div class="cfont" style="width: auto; display: flex; text-align: left;font-size: 16px">
                    {{ __('header.ADV7') }}
                </div>
                <div class="check-mark">
                    <img style="display: inline;float: left;"
                         src="{{asset('images/logo/check-mark-smo25.png')}}" alt="check-mark"/>
                </div>
                <div class="cfont" style="width: auto;display: flex; text-align: left;font-size: 16px">
                    {{ __('header.ADV9') }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="containervid">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="853" height="480" src="https://www.youtube.com/embed/lsHCMHRrkpA"
                                srcdoc="<style>*{padding:0;margin:0;overflow:hidden}
                                    html,body{height:100%}
                                    img,span{position:absolute;width:100%;top:0;bottom:0;margin:auto}
                                    span{height:1.5em;text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}
                                    </style>
                                    <a href=https://www.youtube.com/embed/lsHCMHRrkpA?autoplay=1>
                                    <img src={{ asset('images/youtube_logo.jpg') }} alt='Demo video'>
                                    <span>▶</span>
                                    </a>"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /.container -->
</section> <!-- /#about-us -->
