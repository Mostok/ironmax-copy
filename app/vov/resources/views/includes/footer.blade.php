@php
    $lang = app()->getLocale();
@endphp
<footer>
    <div class="container">
        <a href="{{asset('/')}}" class="logo">
            <img src="{{asset('images/logo/logo4.png')}}" style="width: 65px"
                 alt="Logo">
        </a>
        <ul style="margin-bottom: 30px;">
            <li>
                <a href="https://www.facebook.com/Ironmax-Technologies-2070530093028121/" target="_blank"
                   class="tran3s round-border">
                    <i class="fab fa-facebook-f" aria-hidden="true">
                    </i>
                </a>
            </li>
            <li>
                <a href="skype:live:ironmaxtech?chat" class="tran3s round-border">
                    <i class="fab fa-skype" aria-hidden="true">
                    </i>
                </a>
            </li>
            <li>
                <a href="https://www.linkedin.com/in/iron-max-21aa5817b/" target="_blank" class="tran3s round-border">
                    <i
                        class="fab fa-linkedin-in" aria-hidden="true">
                    </i>
                </a>
            </li>
            <li>
                <a href="https://www.youtube.com/channel/UCDzflJRI2I6y7hEFmBGO4bw/" target="_blank"
                   class="tran3s round-border">
                    <i class="fab fa-youtube" aria-hidden="true">
                    </i>
                </a>
            </li>
        </ul>
        <p class="pp ppf {{($lang=="he")?"he":""}}">
            <a style="color: #ffffff"
               href="{{route('privacy_policy',['lang'=>$lang])}}">{{__('header.VAR13')}}</a>
        </p>
        <p class="pp {{($lang=="he")?"he":""}}">
            <a style="color: #ffffff;"
               href="{{route('termofuse',['lang'=>$lang])}}">{{__('header.VAR14')}}</a>
        </p>
        <p style="margin-top: 20px;">Copyright @ {{date("Y")}} IRONMAX TECHNOLOGIES</p>
    </div>
</footer>
<div style="display:none;" class="testfade">
    <button onclick="window.open('mailto:support@ironmax.tech','_blank')" style="display:block;background: #1f5bc1;"
            id="myBtn1" class="scroll-top tran3s">
        <i class="fas fa-envelope" aria-hidden="true">
        </i>
    </button>
    <button class="scroll-top tran3s p-color-bg" id="toTopBtn">
        <i class="fas fa-chevron-up" aria-hidden="true">
        </i>
    </button>
</div>
