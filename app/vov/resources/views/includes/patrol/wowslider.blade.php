@php
    $locale = app()->getLocale();
$slides = [
    'en' => [
        asset("images/pages/slider/patrol/en/0.png"),
        asset("images/pages/slider/patrol/en/1.png"),
        asset("images/pages/slider/patrol/en/2.jpg"),
        asset("images/pages/slider/patrol/en/3.jpg"),
        asset("images/pages/slider/patrol/en/4.png"),
        asset("images/pages/slider/patrol/en/5.jpg"),
        asset("images/pages/slider/patrol/en/6.jpg"),
        asset("images/pages/slider/patrol/en/7.jpg"),
        asset("images/pages/slider/patrol/en/8.jpg"),
    ],
    'ru' => [
        asset("images/pages/slider/patrol/ru/0.png"),
        asset("images/pages/slider/patrol/ru/1.jpg"),
        asset("images/pages/slider/patrol/ru/2.png"),
        asset("images/pages/slider/patrol/ru/3.jpg"),
        asset("images/pages/slider/patrol/ru/4.jpg"),
        asset("images/pages/slider/patrol/ru/5.jpg"),
        asset("images/pages/slider/patrol/ru/6.jpg"),
        asset("images/pages/slider/patrol/ru/7.jpg"),
        asset("images/pages/slider/patrol/ru/8.jpg"),
        asset("images/pages/slider/patrol/ru/9.jpg"),
    ],
    'he' => [
        asset("images/pages/slider/patrol/he/0.png"),
        asset("images/pages/slider/patrol/he/1.jpg"),
        asset("images/pages/slider/patrol/he/2.png"),
        asset("images/pages/slider/patrol/he/3.jpg"),
        asset("images/pages/slider/patrol/he/4.jpg"),
        asset("images/pages/slider/patrol/he/5.jpg"),
        asset("images/pages/slider/patrol/he/6.jpg"),
        asset("images/pages/slider/patrol/he/7.jpg"),
        asset("images/pages/slider/patrol/he/8.jpg"),
    ],
]

@endphp
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            @foreach($slides[$locale] as $image)
                <li><img src="{{$image}}" alt="Slide" title=""/></li>
            @endforeach
        </ul>
    </div>
    <div class="ws_shadow"></div>
</div>
