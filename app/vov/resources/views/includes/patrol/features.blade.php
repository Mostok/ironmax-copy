<div class="logotype">
    <img src="{{asset('images/product/ironmax-patrol-app.png')}}"
         alt="{{__('pages/patrol.title')}}">
</div>

<div class="container">
    <div class="row {{(App::getLocale()=="he")?"rtl":""}} {{(App::getLocale()=="ru")?"ru":""}}">
        <div class="col-md-6 pb-4 ch-text">
            <ul class="features-list">
                <li>
                    <div class="cfont" style="width: auto; display: flex; text-align: left;font-size: 16px">
                        {{ __('header.ADV1') }}
                    </div>
                </li>
                <li>
                    <div class="cfont" style="width: auto;display: flex; text-align: left;font-size: 16px">
                        {{ __('header.ADV2') }}
                    </div>
                </li>
                <li>
                    <div class="cfont" style="width: auto;display: flex; text-align: left;font-size: 16px">
                        {{ __('header.ADV3') }}
                    </div>
                </li>
                <li>
                    <div class="cfont" style="width: auto; display: flex; text-align: left;font-size: 16px">
                        {{ __('header.ADV4') }}
                    </div>
                </li>
                <li>
                    <div class="cfont" style="width: auto;display: flex; text-align: left;font-size: 16px">
                        {{ __('header.ADV5') }}
                    </div>
                </li>
                <li>
                    <div class="cfont" style="width: auto;display: flex; text-align: left;font-size: 16px">
                        {{ __('header.ADV6') }}
                    </div>
                </li>
                <li>
                    <div class="cfont" style="width: auto; display: flex; text-align: left;font-size: 16px">
                        {{ __('header.ADV7') }}
                    </div>
                </li>
                <li>
                    <div class="cfont" style="width: auto;display: flex; text-align: left;font-size: 16px">
                        {{ __('header.ADV9') }}
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-md-6">
            <div class="containervid">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="853" height="480" src="https://www.youtube.com/embed/lsHCMHRrkpA"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

