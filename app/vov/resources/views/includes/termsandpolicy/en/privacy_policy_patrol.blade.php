<div id="pp_pages">
    <h1>IRONMAX app privacy policy</h1>
    <hr>
    <p>IRONMAX Technologies (hereinafter: "Ironmax Technologies" and / or "Ironmax" and / or "the Company" and / or
        "We") respects the privacy of the users of its services. The manner in which Ironmax uses the information
        provided by the subscriber and / or the user (hereinafter: "the user") and the information collected about them
        when using the Ironmax services. Many literally. </p> <br/>
    <p>Ironmax makes available to you, the Ironmax website at <a href="https://ironmax.tech">ironmax.tech</a> (the "Site"), the "IRONMAX" application (the
        "App") and other / additional means and services, as specified in the terms of use of the Ironmax (hereinafter
        together, together with the site and the application, will be prepared "Ironmax Services").</p> <br/>
    <p>Some uses of Ironmax services require registration in which various details will be required. Therefore, you
        hereby declare that the personal details that are provided to the company during registration are done of your
        own free will and with your consent. In addition, the application requires sensitive permissions that include:
        access to the camera, access to the microphone, access to the location of the device and access to information
        stored on the device. When installing the application and / or using Ironmax services, you give your full
        consent to the terms of this privacy policy. If you do not agree to these terms, in whole or in part, you may
        not install the application or make any use of the Ironmax services and in the event that the application is
        installed, you are required to remove it immediately. It is also clarified that registration for Ironmax
        services by any means (including registration on the site and / or in the application and / or by telephone by
        the user) and / or use of Ironmax services, in whole or in part, constitutes the user's express consent to the
        terms of this privacy policy And even receive e-mails and / or text messages (SMS) and / or telephone calls from
        Ironmax, including service messages and service calls (such as telephone calls intended to alert you to
        emergency events in the facilities with which Ironmax works), messages related to the services provided by
        Ironmax, update regarding For Ironmax services, including additional services, details of the execution of a
        transaction, and messages that contain content that meets the definition of "advertisement thing", according to
        Amendment 40 to the Communications (Bezeq and Broadcasting) Law, 1982. Remove his address from the mailing list
        of recipients of "advertisement thing" using one of the alternatives specified in the "advertisement thing"
        message and / or by contacting the website. Receives messages of service, cease to make use of services a
        Ironmax and delete the user as a whole (removal and cancellation of the service as a whole). </p> <br/>
    <p>Ironmax reserves the right to change the terms of the Privacy Policy, from time to time and as necessary at its
        sole discretion and even without obligation to give notice, in advance and / or retrospectively. The changes
        that will be made, if they are made, will take effect immediately upon presentation of the updated privacy
        policy on the Ironmax Sub app. Therefore, Ironmax recommends reviewing this Privacy Policy from time to time.
        Your continued use of Ironmax Services after making such changes will indicate your agreement to these changes
        and the updated Privacy Policy. If you do not agree to the above changes, in whole or in part, please refrain
        from continuing to use Ironmax Services or contact us for assistance. It should be noted that in the event that
        changes are made which in our discretion will materially affect your rights, Ironmax will take reasonable
        measures to display a prominent notice in this regard on the Ironmax website or in the application and even via
        email alert. </p> <br/>
    <h2> 1. Agreement </h2>
    <p>Ironmax collects and stores in its databases, certain information in relation to the user in order to provide
        the Ironmax services. This information includes: </p>
    <p><b>Registration:</b> As part of the registration, the user is required to provide personal and identifying
        information, such as name, address and contact information.<br/> <b>Payment:</b> The clearing and payment will
        be made by a third party clearer and therefore the credit details or payments are not stored in the Ironmax
        repositories.<br/> <b>Verification:</b> Ironmax may be required to verify your identity based on the information
        provided by you. In addition, when registering you can choose a username and password, you are responsible for
        maintaining the confidentiality of the password and the user may not transfer or enable after using a username
        and password and / or the subscriber account. You agree to immediately report any unauthorized use of your
        account to Ironmax so that Ironmax can take appropriate action. The subscriber may not transfer and / or
        transfer his rights to another account.<br/> <b>Data storage:</b> <b>Anonymous data:</b> When using the Ironmax
        application, website and services, Ironmax collects anonymous information, without any connection to the use of
        a specific Ironmax service. This information includes technical and statistical information, such as: type of
        device, model, hardware, operating system, Internet service provider, statistical information related to the
        extent of use of the application, website and / or Ironmax services including date and time of use, duration of
        use, frequency of use and more. . Ironmax may collect and / or store this data, segment and analyze it,
        including for the purpose of providing services. You may at any time order the cancellation of the collection of
        anonymous data about you, either through the application interface or by contacting us as Which will be detailed
        below.<br/> <b>Data intended for the essential functioning of the application:</b> Information embedded within
        the encoder is scanned by the app user. This information contains the following information: the name of the
        security company of the facility, the name of the facility and the name of the location where the code was
        pasted. Device location also background device location and time when scanning the globe. Telephone number
        intended for answering emergency calls. The location of the device, time and photos that were attached to the
        report sent by the user when reporting an event through the app. Details of the security guards working in the
        facility linked to the application - the application allows you to save the following information about the
        security guards of the facility: first name, last name, mobile phone number and employee code. The user of the
        app must request permission from any security guard before entering his personal details listed above. The
        security guard will also be entitled to require the user to remove his details from the app records at any time,
        in which case the user must remove the security information From the database owned by Ironmax. In any case,
        entering personal details of the facility security guards will be the full responsibility of the user only.
        Procedures of the facility linked to the application - The user of the application must obtain approval for the
        publication of the facilities procedures from the person in charge at the workplace, any publication of content,
        whether confidential or not, will be the sole responsibility of the user. </p>
    <p>Ironmax may collect and / or store this data, segment and analyze it, including for the purpose of providing
        services. You may at any time order the cancellation of the collection of data about you, through the
        application interface or by contacting us as described below, if you decide to cancel the above collection of
        data which are intended for the vital functioning of the application, essential features of the application will
        not work as a result. Your right to complain about their irregularities to Ironmax. </p>
    <p><b>Internet Protocol Address ("IP"):</b> When using the Website and the application, the Ironmax will collect the
        IP address.<br/> <b>Device ID:</b> IMEI, Android ID and Advertising ID: When using the site and the application
        Ironmax will collect the IMEI (serial number of the mobile device) and Android ID. Also, third parties that
        provide services for Ironmax may collect the Advertising ID. This information may be information associated with
        your device. You can stop collecting Advertising ID information in your device settings at any time. It should
        be noted that Ironmax does not combine and / or connect the device ID listed above.<br/> <b>Location:</b> For
        the purpose of activating the application services, the geographical location of your device and the property
        you wish to secure is used through the application, this information is provided to Ironmax directly from the
        user, from his device automatically (by coordinates sent from the cell phone, GPS location services and
        more).<br/> Ironmax (such as: navigation apps and maps and technological components embedded in the app on
        behalf of third parties and more). It is clarified that Ironmax may collect additional information about your
        location. The location data and the scan times of the points at which you have chosen to place user barcodes are
        collected, stored and accumulated in the databases and database of Ironmax and / or the service provider. The
        places where the barcodes / minigards are installed are exposed to the knowledge of the subscriber (who pays for
        the service). The user hereby acknowledges that he is aware and clarifies that Ironmax uses this information,
        which includes non-anonymous data, and hereby gives his consent that Ironmax will use it either by itself or in
        collaboration with third parties.<br/> <b>Camera:</b> The Ironmax application uses the device's camera in one of
        two possible cases: <i>Barcode scanning:</i>When scanning the barcode, areas around the barcode location may be
        photographed by the user. Ironmax clarifies that it transmits and uses the information contained within the
        barcode only and does not refer to the background photographed around the barcode when it is scanned.<br/> <i>Event
            Reporting:</i> When the user decides to report an event through the app he is given the option to take a
        photo in order to better describe the event.<br/> <i>Usage history:</i> Ironmax holds in its database the
        details of use of the application and the website which in the services of Ironmax, the user gives his consent
        to the storage of the usage information in the database.<br/> <i>Crash Data:</i> If the application crashes
        while you are using it, the crash data will be collected by Ironmax and / or a third party (including
        "Crashlytics" on behalf of Google) which provides such reporting and analysis services for Ironmax, and these
        will be transferred to Ironmax for identification. The cause of the crash and in order to act to prevent a
        recurring malfunction, improve the application and services and implement required updates. In addition to the
        said crash data contain data regarding the status of the application, operating system, device, device ID and
        location at the time of the crash of the application, Ironmax may share with the service provider personal
        information regarding the user such as phone number, car number or email address to link the crash to the user
        and create Contact the user for troubleshooting, etc. For more information, see the Crashlytics Privacy Policy
        at: <a href="https://try.crashlytics.com/terms/privacy-policy.pdf">https://try.crashlytics.com/terms/privacy-policy.pdf</a>
        and at: <a
            href="https://answers.io/img/onepager/privacy.pdf">https://answers.io/img/onepager/privacy.pdf</a>.<br/> <i>Contact:</i>
        If you choose to contact us, you will be required to provide us with the means of contacting you, including
        name, e-mail address and telephone number. Ironmax may respond to your request through its representatives and /
        or through third parties who provide services for Ironmax , through various media.<br/> <i>Direct mail:</i> When
        registering, the user gives his consent to receive direct mail as defined in the Protection of Privacy Law,
        5741-1981 ("Direct mail") and to use his details for that purpose. Ironmax may use the information in its
        possession for direct mail and / or transfer information about The user to third parties as specified in this
        privacy policy and the terms of use for direct mail inquiries by him.If you wish to remove yourself from the
        mailing list after registration, you can easily do so in a return message as part of the direct mail as he does
        by electronic means and / or contact A customer service center Ironmax and in this case Ironmax will remove your
        details from the list of direct mail recipients used for direct mail. Do not remove yourself from the direct
        mailing list to deny receipt of other messages, such as service messages and / or advertisements. If you are
        interested in removing your information from the Ironmax customer list or refraining from direct mailing or
        receiving inquiries from Ironmax completely, you can do so by deleting the user as a whole (removing and
        canceling the service as a whole). It is hereby clarified that the user does not have any legal obligation to
        provide such information as above, and that the provision of such information depends on the will and consent of
        the user. However, filling in some of the fields in the registration process and collecting data such as
        location data may be a necessary condition for the use of Ironmax services. The user hereby declares and
        undertakes that all information provided and / or updated is correct, reliable and accurate and that the
        registration is personal and not by name and / or for third parties, except in cases where it has been expressly
        authorized to do so. </p> <br/>
    <p>In addition, to provide and improve the services as well as for authentication purposes, Ironmax may receive
        additional information from various third parties that provide services for Ironmax, including identifying
        information or identifying the user, which will also be stored in the Ironmax databases. This information will
        be collected, stored and used solely in accordance with the terms of the Terms of Use and this Privacy
        Policy. </p>
    <h2>2. How was the information collected?</h2>
    <p>Ironmax collects information regarding the user, which will be provided at the user's initiative and / or
        collected during the use he will make of the Ironmax services, in accordance with the provisions of these Terms
        of Use and / or the policy and in accordance with the provisions of any law. The information is collected and
        provided to the company as follows: </p> <br/>
    <p>Information provided will be aligned by the user voluntarily. Information transmitted to Ironmax by third
        parties (such as navigation applications, etc.). In addition, the Company may make use of third-party technology
        components adapted for mobile devices in order to obtain characterization analyzes and anonymous statistics
        about the uses of the users. Information transmitted automatically from the user's device. Use of technologies
        such as "cookies" and other technologies (such as Web Beacons, Pixels and more). A cookie is a small file that
        contains a string of characters, which is sent to your device when you visit a particular website. When you
        visit the site again, the cookie will allow the site to identify your browser. The cookies contain anonymous
        information and are used for the purpose of collecting statistics about the use of the application, the website
        and the services of Ironmax , to verify details, and to adjust the services to your personal preferences. You
        have the option to block and / or delete cookies at any time independently by changing the settings on your
        device and / or the browsers you use. You know that some of the services may not be accessible and / or the
        experience of using the services for you will be limited if you block and / or delete the cookies. For more
        information, see: <a href="https://www.allaboutcookies.org">www.allaboutcookies.org</a>. </p> <br/>
    <p>Third parties (for example, Google Analytics) may also use cookies, and similar technologies (such as SDK) This
        information does not include personal information, except device ID, IP address and Advertising ID which may be
        collected. To learn more about cookies and how Google uses data generated from the use of Google Analytics, we
        recommend that you review Google's policies and learn more about how Google uses cookies and data when you use
        the site or application. You may also want to review the current Google Analytics opt-out options at:
        https://tools.google.com/dlpage/gaoptout </p> <br/>
    <p>Ironmax uses third-party SDKs to process information, improve and tailor services. Among other things, the
        Company uses the SDKs of the third parties which will be detailed below, and which further information on how
        data is collected through the SDKs of these third parties, can be found in the privacy policy and terms of use
        on their behalf, which form part of this privacy policy, as follows: <br/><br/> mongoDB: Privacy Policy
        Available at: <a href="https://www.mongodb.com/legal/privacy-policy">https://www.mongodb.com/legal/privacy-policy</a><br/>
        Twilio: Privacy Policy Available at: <a href="https://www.salesforce.com/company/privacy/">https://www.salesforce.com/company/privacy/</a><br/>
        Heroku: Privacy Policy Available at: <a href="https://www.salesforce.com/company/privacy/">https://www.salesforce.com/company/privacy/</a><br/>
        And Terms of Use are available at: <a href="https://www.heroku.com/policy/salesforce-heroku-msa">https://www.heroku.com/policy/salesforce-heroku-msa</a><br/>
        Mailgun: Privacy Policy Available at: <a href="https://www.mailgun.com/privacy-policy">https://www.mailgun.com/privacy-policy</a><br/>
        Namecheap: Privacy Policy Available at: <a href="https://www.namecheap.com/legal/general/privacy-policy.aspx">https://www.namecheap.com/legal/general/privacy-policy.aspx</a><br/>
        AWS: The Privacy Policy is available at: <a
            href="https://aws.amazon.com/privacy">https://aws.amazon.com/privacy</a><br/><br/>
        It is clarified that Ironmax does not collect, directly or through the SDK, the full information specified in
        the privacy policy on behalf of the third parties, but only the information necessary for the provision and
        improvement of the services.
    </p>
    <h2>
        3. Use of information
    </h2>

    <p>
        Ironmax has a registered database in which the information collected and detailed above will be stored and
        secured. Ironmax uses the information for the following purposes:<br/><br/>


        To enable you to use the Ironmax services, the content contained therein and the services offered by Ironmax as
        well as the management and operation of the company's services, including those requested by you.<br/>
        To identify you during your re-entries to areas that require registration and to save you from having to enter
        your details each time.<br/>
        To improve and enrich the services and content on the site, including creating new services and content that
        meet the requirements of users and their expectations, as well as changing or canceling existing services and
        content. The information used by Ironmax for this purpose may be collected as complete information and / or
        statistical information, which does not personally identify you.<br/>
        To enable you to adjust the services on the site and / or in the application to your preferences.<br/>
        To send you from time to time information about the services and contents of the site, activities related to it
        and to Ironmax , surveys, information leaflets, etc. Such information may be public in nature and may be sent to
        you via text messages, SMS messages, or electronically Other.<br/>
        To contact you 21<br/>
        The Minister of Ironmax believes that there is a need for this, including updating, approving or notifying you
        of issues related to the Services, including the Services requested by you.<br/>
        For the purpose of analyzing, controlling and processing information, whether by Ironmax or by providing
        statistical information to third parties.<br/>
        For the purpose of improving the development and adaptation of services.<br/>
        For the purpose of providing information to third parties, as specified in the Terms of Use and this
        Policy.<br/>
        For the proper operation and development of Ironmax services as well as troubleshooting.<br/>
        For any other purpose, set forth in this Privacy Policy or the Terms of Use.
    </p>

    <h2>
        4. Providing information to third parties
    </h2>

    <p>
        We do not share personal information or identifying information collected from you with third parties, except in
        the following cases:<br/><br/>


        Collaborate with third parties who provide services for us (such as clearing service providers, marketing
        assistance, payment process, tracking, storage, servers, customer service, development functionality,
        improvement, service and support, etc.).<br/>
        At your request and / or with your consent and / or to provide you with a service that you have requested and /
        or registered for.<br/>
        In the event that the user has violated the terms of the privacy policy or the terms of use and / or in cases
        where he has performed or attempted to perform actions that are contrary to the provisions of any law.<br/>
        Sharing the information to the extent necessary to comply with the provisions of any law, regulation, legal
        proceeding or government order (such as a court order, various regulatory bodies and more).<br/>
        Due to a dispute, claim, lawsuit, demand and / or legal proceedings that will be conducted between you and / or
        anyone on your behalf and Ironmax and / or anyone on its behalf.<br/>
        In the event that Ironmax believes that the provision of the information is necessary in order to prevent
        serious damage to the property and / or bodies of third parties and / or other users and / or any of Ironmax
        employees and / or any of its customers, or to prevent harm or other damage at its discretion. Of Ironmax.<br/>
        In the event that Ironmax transfers and / or transfers to a third party its activities and / or its rights and
        obligations to third parties, provided that such third parties accept the provisions specified in these
        regulations in connection with the information.<br/>
        <br/><br/>You know and you hereby agree that the personal information you provide may pass to the various
        service providers depending on the services you request to join, for the purpose of providing the service,
        ongoing conduct, and monthly accounting for the various services.
        <br/><br/>Ironmax will not infringe on the subscriber's privacy and will comply in this matter with the
        provisions of the Privacy Protection Law, 5741-1981, and the provisions of any other law in the matter. For the
        avoidance of doubt, even in the absence of service requiring third parties, the above Third parties have
        aggregate information and / or which is not personal and / or which does not identify the user and / or infringe
        on the user's privacy.
        <br/><br/>Please note that when we share your information with a third party with your consent and / or request,
        the said information will be subject to the third party's privacy practices. It is emphasized that Ironmax is
        not responsible for any use made by third parties regarding the information provided to them by Ironmax in
        accordance with your registration for the various services and / or will be collected by you about you, as
        collected, including information transmitted and / or transmitted to them and / or Which was delivered to them
        directly by you.
    </p>
    <h2>
        5. User rights
    </h2>
    <p>
        The subscriber can at any time review the information he has actively provided (for example at the time of
        registration) and even information about the Ironmax services he has consumed, through the website or the app.
        It is also possible to update and / or change details through the application or website. In addition, if the
        information in the Ironmax repositories is used for personal contact with the subscriber or user, based on
        affiliation to a population group, determined by characterization of one or more persons whose names are
        included in the repository ("direct mail contact"), then you are entitled by law Privacy Protection, 1981 to
        request in writing that the information relating to you be deleted from the database (we may ask you to verify
        your identity before we can respond to your request). Management of its business - including documentation of
        commercial and other transactions you have performed in connection with Ironmax services or for the purpose of
        continuing to provide Ironmax services in the future - will continue to be maintained by the company by law, but
        will no longer be used for direct mail. We will also retain the information collected. In addition, as specified
        in these private countries and in the terms of use, for the purpose of providing some of Ironmax’s services,
        Ironmax allows third parties (eg, local authorities, car park operators and suppliers with whom Ironmax operates
        in connection with Subscriptions) and / Or the user, therefore, you Aware and hereby certify that, even if you
        ask us to delete the information about you, we will not be able to delete information transferred to third
        parties for the purpose of providing the services to which you are registered and which is held by those third
        parties.<br/><br/>
        Ironmax reserves the right to reject requests that have already been submitted an unreasonable number of times,
        requests that require exceptional technical effort (for example, developing a new system or a fundamental change
        to existing practices), requests that endanger the privacy of others or manifestly impractical requests (e.g.
        Located in backup systems).
    </p>
    <h2>
        6. Age restriction
    </h2>
    <p>
        Ironmax does not knowingly collect information about a child under the age of 14 without parental consent and
        even sensitive information from minors under the age of 18. In any case where Ironmax becomes aware of the
        collection of sensitive minor information or information from a child under 14 Ironmax will make every effort to
        delete this information immediately.
    </p>
    <h2>
        7. Saving information Information security
    </h2>
    <p>
        You know and you hereby give your consent to the storage of the information and / or its processing in a
        third-party cloud (storage that requires the transfer of information via the Internet) even outside the borders
        of the State of Israel. Ironmax operates in accordance with the relevant legal provisions to the best of its
        ability to maintain the confidentiality of subscription data and / or use advanced security methods approved for
        civilian use on the Internet. The information security is designed to ensure the user identification and
        encryption of the transmitted identification data. The information transmitted to Ironmax servers in the cloud
        is encrypted and secured. The purpose of making the secure connection is to prevent the transmitted information
        from being exposed to the eyes of other surfers and unauthorized persons on the Internet. Islands
        Ironmax uses advanced technologies provided by leading technology companies in their field such as Google and
        mongoDB. There is constant supervision and control over all communication between the Ironmax servers and the
        Internet. To this end, Ironmax implements information security systems and procedures in the Ironmax platforms.
        While these systems and procedures reduce the risk of unauthorized intrusion into Ironmax platforms and Ironmax
        servers, complete protection cannot be provided. Therefore, Ironmax does not warrant that the servers will be
        completely immune from unauthorized access to the information stored on them. Ironmax will not bear any
        responsibility in cases of disclosure and use of the above information resulting from unauthorized intrusion by
        others.
    </p>
    <h2>
        8. Contact
    </h2>
    <p>
        For any request, requirement or question please contact us by email: <a href="mailto:support@ironmax.tech">support@ironmax.tech</a>
        and we will make every effort to get back to you within a reasonable time.
    </p>
    <h2>
        9. Inaccuracies in the wording of this document
    </h2>
    <p>
        Grammatical inaccuracies in the wording of this document, such as incorrect use of masculine or feminine words,
        or inconsistencies in singular and plural definitions, are not material, will not constitute a basis for a claim
        or disagreement and will be interpreted according to the content and intent of the document.
    </p>
</div>
