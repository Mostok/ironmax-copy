@extends('emails.layouts.default',["page_title"=>__('pages/gatekeeper_and_digital_id.title'),])

@section('style')

    <!--[if mso]>
<xml>
<o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml>
<![endif]-->
<!--[if !mso]><!-->
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
<link
    href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap"
    rel="stylesheet" type="text/css">
<style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
    @import url(https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap);
</style>
<!--<![endif]-->

<style type="text/css">
    @php
        echo \Illuminate\Support\Facades\File::get(public_path('resources/views/emails/style.css'));
    @endphp
</style>

@endsection

@section('content')



    <!-- 1 Column Text + Button : BEGIN -->
    <tr class="events_report_on_end_day">
        <td>

            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" class="hello_block">
                <tr>
                    <td>
                        {!! $body !!}
                    </td>
                </tr>
            </table>

            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" class="facilities">
                @foreach ($facilities as $facility)
                    <tr>
                        <td class="facility_wrap">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">

                                <!-- Set facility name header: BEGIN-->
                                <thead class="facility_title">
                                <tr>
                                    <th colspan='3'>
                                        {{$facility['name']}}
                                    </th>
                                </tr>
                                </thead>
                                <!-- Set facility name header: END-->

                                <!-- Set subheader: BEGIN-->
                                <thead class='block_headers'>
                                <tr>
                                    <th colspan='2' class='description'>
                                        {{$table_header_exception}}
                                    </th>
                                    <th class='date_time'>
                                        {{$table_header_date_time}}
                                    </th>
                                </tr>
                                </thead>
                                <!-- Set subheader: END-->


                                <tbody>
                                @foreach ($facility['event_reports'] as $event)
                                    <tr>
                                        <td class='icon'>
                                            @switch($event['code'])
                                                @case('s_bc_ls')
                                                <img
                                                    src="{{ $message->embed(public_path('mail/icons/png/qr.svg.png')) }}"
                                                    class="icon qr"/> @break
                                                @case('s_ch') @case('s_tb_ls') @case('s_tb_em')
                                                <img
                                                    src="{{$message->embed(public_path('mail/icons/png/patrol.svg.png')) }}"
                                                    class="icon patrol"/> @break
                                                @case('s_em')
                                                <img
                                                    src="{{$message->embed(public_path('mail/icons/png/emergency.svg.png')) }}"
                                                    class="icon emergency"/> @break
                                                @case('s_in')
                                                <img
                                                    src="{{$message->embed(public_path('mail/icons/png/incedent.svg.png')) }}"
                                                    class="icon incedent"/> @break
                                            @endswitch
                                        </td>
                                        <td class='description'> {!! __('events.'.$event['code'],[ 'facility'=>$facility['name'], ], $lang) !!} </td>
                                        <td class='date_time'>
                                            {{date(__('datetime.full',[], $lang ), strtotime($event['created_at']))}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                @endforeach

            </table>

            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" class="invit_block">
                <tr>
                    <td>
                        {{$invitation_to_dashboard}}<br/>
                        <a target="_blank" href='{{$link_to_dashboard}}' class='clean'>{{$link_to_dashboard}}</a>
                    </td>
                </tr>
            </table>

            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" class="tnx_block">
                <tr>
                    <td>
                        {{$thank_you}}<br/> IRONMAX Technologies Team
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <!-- 1 Column Text + Button : END -->

@endsection
