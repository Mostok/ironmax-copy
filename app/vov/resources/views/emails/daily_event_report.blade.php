@php
    $rtl_special_style = ($direction === 'rtl') ? ';text-align: right;' : null;
    $rtl_special_header_border_stile = ($direction === 'rtl') ? ';border-right: 2px #ffffff solid;' : ';border-left: 2px #ffffff solid;';
    $rtl_special_row_border_stile = ($direction === 'rtl') ? ';border-right: 2px #E7EBEE solid;' : ';border-left: 2px #E7EBEE solid;';
@endphp
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="{{$lang}}" xmlns="http://www.w3.org/1999/xhtml"
      dir="{{$direction}}"
      style="width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
<head>
    <meta charset="UTF-8">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title>{{$title}}</title>
    <!--[if (mso 16)]>
    <style type="text/css">
    a {
        text-decoration: none;
    }
    </style>
    <![endif]-->
    <!--[if gte mso 9]>
    <style>sup {
        font-size: 100% !important;
    }</style><![endif]-->
    <!--[if gte mso 9]>
    <xml>
    <o:OfficeDocumentSettings>
        <o:AllowPNG></o:AllowPNG>
        <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if !mso]><!-- -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet">
    <!--<![endif]-->
    <style type="text/css">
        th.td_datetime {
            border: none;
        }

        td.td_datetime {
            border: none;
        }

        [dir=rtl] * {
            direction: rtl;
        }

        #outlook a {
            padding: 0;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        .es-button {
            mso-style-priority: 100 !important;
            text-decoration: none !important;
        }

        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        .es-desk-hidden {
            display: none;
            float: left;
            overflow: hidden;
            width: 0;
            max-height: 0;
            line-height: 0;
            mso-hide: all;
        }
    </style>
</head>
<body
    style="width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
<span
    style="display:none !important;font-size:0px;line-height:0;color:#FFFFFF;visibility:hidden;opacity:0;height:0;width:0;mso-hide:all">{{$preview}}</span>
<div class="es-wrapper-color" style="background-color:#FFFFFF">
    <!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" color="#ffffff"></v:fill>
    </v:background>
    <![endif]-->
    <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0"
           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
        <tr class="gmail-fix" height="0" style="border-collapse:collapse">
            <td style="padding:0;Margin:0">
                <table cellpadding="0" cellspacing="0" border="0" align="center"
                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:600px">
                    <tr style="border-collapse:collapse">
                        <td cellpadding="0" cellspacing="0" border="0" height="0"
                            style="padding:0;Margin:0;line-height:1px;min-width:600px"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="border-collapse:collapse">
            <td valign="top" style="padding:0;Margin:0">
                <table cellpadding="0" cellspacing="0" class="es-content" align="center"
                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
                    <tr style="border-collapse:collapse">
                        <!-- RTL -->
                        <td align="center" dir="{{$direction}}" style="padding:0;Margin:0;direction:{{$direction}}">
                            <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0"
                                   cellspacing="0"
                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
                                <tr style="border-collapse:collapse">
                                    <td align="left"
                                        style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px">
                                        <table cellpadding="0" cellspacing="0" width="100%"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td align="center" valign="top" style="padding:0;Margin:0;width:560px">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation"
                                                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                        <tr style="border-collapse:collapse">
                                                            <td align="center" style="padding:0;Margin:0;font-size:0px">
                                                                <a target="_blank" href="https://ironmax.tech/"
                                                                   style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:#5F5F5F"><img
                                                                        src="{{ $message->embed(public_path('mail/restore_password/NoPath@2x.png')) }}"
                                                                        alt="ironmax.tech"
                                                                        style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                        title="ironmax.tech" width="120"
                                                                        height="110"></a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left"
                                        style="padding:0;Margin:0;padding-left:20px;padding-right:20px;padding-top:35px">
                                        <table width="100%" cellspacing="0" cellpadding="0"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td class="es-m-p0r es-m-p20b" valign="top" align="center"
                                                    style="padding:0;Margin:0;width:560px">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation"
                                                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                        <tr style="border-collapse:collapse">
                                                            <td align="left" class="rtl_class"
                                                                style="padding:0;Margin:0"><p
                                                                    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#575757{{$rtl_special_style}}">
                                                                    {!! $body !!}</p></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left"
                                        style="padding:0;Margin:0;padding-left:20px;padding-right:20px;padding-top:30px">
                                        <table cellpadding="0" cellspacing="0" width="100%"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td align="center" valign="top" style="padding:0;Margin:0;width:560px">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation"
                                                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                        <tr style="border-collapse:collapse">
                                                            <td style="padding:0;Margin:0">
                                                                <table width="100%"
                                                                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:100%"
                                                                       border="0" role="presentation">
                                                                    <tr style="border-collapse:collapse">
                                                                        <td style="padding:0;Margin:0">

                                                                            @foreach ($facilities as $facility)

                                                                                <table width="100%"
                                                                                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:100%"
                                                                                       border="0" role="presentation">
                                                                                    <tr style="border-collapse:collapse">
                                                                                        <th colspan="3" height="45"
                                                                                            style="background-color:#E7EBEE;height:45px;border-bottom:2px #FFFFFF solid;line-height:19px;font-size:14px;color:#5F5F5F;font-weight:lighter">
                                                                                            {{$facility['name']}}
                                                                                        </th>
                                                                                    </tr>
                                                                                    <tr style="border-collapse:collapse">
                                                                                        <th colspan="2" height="45"
                                                                                            style="background-color:#E7EBEE;height:45px;line-height:19px;font-size:14px;color:#5F5F5F;font-weight:lighter">
                                                                                            {{$table_header_exception}}
                                                                                        </th>
                                                                                        <th colspan="1" width="150"
                                                                                            height="45"
                                                                                            style="background-color:#E7EBEE;height:45px;line-height:19px;font-size:14px;color:#5F5F5F;font-weight:lighter{{$rtl_special_header_border_stile}}"
                                                                                            class="td_datetime">
                                                                                            {{$table_header_date_time}}
                                                                                        </th>
                                                                                    </tr>

                                                                                    @foreach ($facility['event_reports'] as $event)


                                                                                        <tr style="border-collapse:collapse;border-bottom:2px #E7EBEE solid"
                                                                                            data-eid="{{ $event['id'] }}">
                                                                                            <td style="padding:0;Margin:0;min-height:60px;height:60px;text-align:center;vertical-align:middle;width:65px"
                                                                                                width="65"
                                                                                                align="center"
                                                                                                valign="middle">@switch($event['code'])
                                                                                                    @case('s_bc_ls')
                                                                                                    <img
                                                                                                        src="{{ $message->embed(public_path('mail/icons/png/qr.svg.png')) }}"
                                                                                                        style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;vertical-align:middle;line-height:0;max-height:30px;width:auto;margin:0 auto">
                                                                                                    @break
                                                                                                    @case('s_ch') @case('s_tb_ls') @case('s_tb_em')
                                                                                                    <img
                                                                                                        src="{{ $message->embed(public_path('mail/icons/png/patrol.svg.png')) }}"
                                                                                                        style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;vertical-align:middle;line-height:0;max-height:30px;width:auto;margin:0 auto">
                                                                                                    @break
                                                                                                    @case('s_em')
                                                                                                    <img
                                                                                                        src="{{ $message->embed(public_path('mail/icons/png/emergency.svg.png')) }}"
                                                                                                        style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;vertical-align:middle;line-height:0;max-height:30px;width:auto;margin:0 auto">
                                                                                                    @break
                                                                                                    @case('s_in')
                                                                                                    <img
                                                                                                        src="{{ $message->embed(public_path('mail/icons/png/incedent.svg.png')) }}"
                                                                                                        style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;vertical-align:middle;line-height:0;max-height:30px;width:auto;margin:0 auto">
                                                                                                    @break
                                                                                                @endswitch
                                                                                            </td>
                                                                                            <td style="padding:0;Margin:0;color:#5F5F5F;font-size:14px;line-height:19px">
                                                                                                @switch($event['code'])
                                                                                                    @case('s_tb_ls') @case('s_tb_em')

                                                                                                    {!! trans_choice('mails/events_report_on_end_day.events.s_tb_ls',
$event['expected_reports'] - count($event['reports']),
[ 'num'=>$event['expected_reports'] - count($event['reports']), 'facility'=>$facility['name'], ], $lang) !!}
                                                                                                    @break
                                                                                                    @default
                                                                                                    {!! __('events.'.$event['code'],[ 'facility'=>$facility['name'], ], $lang) !!}
                                                                                                    @break
                                                                                                @endswitch
                                                                                            </td>
                                                                                            <td style="padding:0;Margin:0;color:#5F5F5F;font-size:14px;line-height:19px{{$rtl_special_row_border_stile}}"
                                                                                                class="td_datetime"
                                                                                                align="center">
                                                                                                @switch($event['code'])
                                                                                                    @case('s_bc_ls') @case('s_in') @case('s_em') @case('s_ch')
                                                                                                    {{date(__('datetime.full',[], $lang ),
                                                                                                            strtotime( $event['reports'][0]['patrol_start_time'] ??
                                                                                                            $event['reports'][0]['created_at'] ))}}
                                                                                                    @break
                                                                                                    @case('s_tb_ls') @case('s_tb_em')
                                                                                                    {{date(__('datetime.date',[], $lang ), strtotime($event['date']))}}
                                                                                                    <br/>{{$event['short_time_group']}}
                                                                                                    @break
                                                                                                @endswitch
                                                                                            </td>
                                                                                        </tr>

                                                                                    @endforeach

                                                                                </table>

                                                                            @endforeach


                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left"
                                        style="padding:0;Margin:0;padding-left:20px;padding-right:20px;padding-top:35px">
                                        <table cellpadding="0" cellspacing="0" width="100%"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td align="center" valign="top" style="padding:0;Margin:0;width:560px">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation"
                                                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                        <tr style="border-collapse:collapse">
                                                            <td align="left" class="rtl_class"
                                                                style="padding:0;Margin:0"><p
                                                                    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#5F5F5F{{$rtl_special_style}}">
                                                                    {{$invitation_to_dashboard}}<br>
                                                                    {{$link_to_dashboard}}</p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left"
                                        style="Margin:0;padding-bottom:15px;padding-left:20px;padding-right:20px;padding-top:35px">
                                        <table cellpadding="0" cellspacing="0" width="100%"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td align="center" valign="top" style="padding:0;Margin:0;width:560px">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation"
                                                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                        <tr style="border-collapse:collapse">
                                                            <td align="left" class="rtl_class"
                                                                style="padding:0;Margin:0"><p
                                                                    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#5F5F5F{{$rtl_special_style}}">
                                                                    {{$thank_you}}<br/> IRONMAX Technologies Team</p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left"
                                        style="padding:0;Margin:0;padding-left:20px;padding-right:20px;padding-top:40px">
                                        <!--[if mso]>
                                        <table style="width:560px" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width:280px" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                                            <tr style="border-collapse:collapse">
                                                <td class="es-m-p20b" align="left"
                                                    style="padding:0;Margin:0;width:280px">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation"
                                                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                                        <tr style="border-collapse:collapse">
                                                            <td align="right"
                                                                style="padding:0;Margin:0;padding-right:20px;padding-top:25px;font-size:0px">
                                                                <a target="_blank" href="https://ironmax.tech/"
                                                                   style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:#5F5F5F"><img
                                                                        class="adapt-img"
                                                                        src="{{ $message->embed(public_path('mail/restore_password/Rectangle_322@2x.png')) }}"
                                                                        alt
                                                                        style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                                                        height="95" width="95"></a></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--[if mso]></td>
                                        <td style="width:0px"></td>
                                        <td style="width:280px" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-right" align="right"
                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
                                            <tr style="border-collapse:collapse">
                                                <td align="left" style="padding:0;Margin:0;width:280px">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;border-left:1px solid #040505" role="presentation">
                                                        <tr style="border-collapse:collapse">
                                                            <td align="left" style="padding:0;Margin:0;padding-top:34px;padding-left:20px">
                                                                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#5F5F5F">
                                                                    IRONMAX Technologies</p>
                                                            </td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:34px;padding-left:20px">
                                                                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#5F5F5F">
                                                                    support@ironmax.tech</p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]--></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
