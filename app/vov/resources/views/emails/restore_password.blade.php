<!doctype html>
<html lang="{{$lang}}" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <title>
    {{$title}}
  </title>
  <!--[if !mso]><!-- -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
    #outlook a {
      padding: 0;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }
  </style>
  <!--[if mso]>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
  <!--[if lte mso 11]>
        <style type="text/css">
          .mj-outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->
  <!--[if !mso]><!-->
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet" type="text/css">
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
    @import url(https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap);
  </style>
  <!--<![endif]-->
  <style type="text/css">
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100% !important;
        max-width: 100%;
      }

      .mj-column-per-25 {
        width: 25% !important;
        max-width: 25%;
      }
    }
  </style>
  <style type="text/css">
    @media only screen and (max-width:480px) {
      table.mj-full-width-mobile {
        width: 100% !important;
      }

      td.mj-full-width-mobile {
        width: auto !important;
      }
    }
  </style>
  <style type="text/css">
    @font-face {
      font-family: "Calisto MT";
      src: url("https://ironmax.tech/fonts/CALIST.ttf");
    }

    .footer .wrap {
      width: 15% !important;
      max-width: 15%;
    }

    .footer .icon {
      padding-left: 50px !important;
      background-repeat: no-repeat;
      background-size: 15px auto;
      background-position: 25px;
    }

    .footer .email {
      padding-top: 0px !important;
      background-image:
        url("https://ironmax.tech/mail/restore_password/Group 538@2x.png");
    }

    .footer .phone {
      background-image: url("https://ironmax.tech/mail/restore_password/Group 541@2x.png");
    }

    .footer .logo {
      width: 30% !important;
      max-width: 30%;
    }

    .footer .contacts {
      width: 40% !important;
      max-width: 40%;
    }

    .footer .contacts div {
      font-family: "Calisto MT" !important;
    }

    @media only screen and (max-width:480px) {
      .footer .logo {
        width: 40% !important;
        max-width: 40%;
      }

      .footer .contacts {
        width: 60% !important;
        max-width: 60%;
      }

      .footer .wrap {
        width: 0 !important;
        max-width: 0;
      }
    }

    @media only screen and (max-width:480px) {
      .footer .icon {
        background-image: none;
        padding: 5px 25px !important;
      }

      .footer .icon div {
        font-size: 12px !important;
      }

      .footer .company {
        padding-bottom: 10px !important;
      }
    }

    @media only screen and (max-width:480px) {
      .btn a {
        font-size: 14px !important;
      }
    }

    .rtl div,
    .rtl a {
      direction: rtl !important;
      text-align: right !important;
    }
  </style>
</head>

<body style="background-color:#F8F8F8;">
  <div style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
    {{$preview}}
  </div>
  <div style="background-color:#F8F8F8;">
    <!--[if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="{{$class_rtl}}-outlook" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
    <div class="{{$class_rtl}}" style="margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
              <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

        <tr>

            <td
               class="" style="vertical-align:top;width:600px;"
            >
          <![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                  <tr>
                    <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                        <tbody>
                          <tr>
                            <td style="width:155px;">
                              <a href="https://ironmax.tech/" target="_blank">
                                <img height="auto" src="https://ironmax.tech/mail/restore_password/NoPath@2x.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="155" />
                              </a>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size:0px;word-break:break-word;">
                      <!--[if mso | IE]>

        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="55" style="vertical-align:top;height:55px;">

    <![endif]-->
                      <div style="height:55px;"> &nbsp; </div>
                      <!--[if mso | IE]>

        </td></tr></table>

    <![endif]-->
                    </td>
                  </tr>
                  <tr>
                    <td align="left" class="{{$class_rtl}}" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                      <div style="font-family:Nunito Sans;font-size:16px;line-height:24px;text-align:left;color:#575757;">{!! $body !!}</div>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size:0px;word-break:break-word;">
                      <!--[if mso | IE]>

        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="30" style="vertical-align:top;height:30px;">

    <![endif]-->
                      <div style="height:30px;"> &nbsp; </div>
                      <!--[if mso | IE]>

        </td></tr></table>

    <![endif]-->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" vertical-align="middle" class="btn" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;width:100%;line-height:100%;">
                        <tr>
                          <td align="center" bgcolor="#707070" role="presentation" style="border:none;border-radius:10px;cursor:auto;mso-padding-alt:10px 25px;background:#707070;" valign="middle">
                            <a href="{{$reset_url}}" style="display:inline-block;background:#707070;color:#FFFFFF;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:20px;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:10px;" target="_blank">
                              {{$reset_btn_text}}
                            </a>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size:0px;word-break:break-word;">
                      <!--[if mso | IE]>

        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="30" style="vertical-align:top;height:30px;">

    <![endif]-->
                      <div style="height:30px;"> &nbsp; </div>
                      <!--[if mso | IE]>

        </td></tr></table>

    <![endif]-->
                    </td>
                  </tr>
                  <tr>
                    <td align="left" class="{{$class_rtl}}" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                      <div style="font-family:Nunito Sans;font-size:16px;line-height:24px;text-align:left;color:#575757;">{{$thank_you}}<br />IRONMAX Technologies Team</div>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size:0px;word-break:break-word;">
                      <!--[if mso | IE]>

        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="30" style="vertical-align:top;height:30px;">

    <![endif]-->
                      <div style="height:30px;"> &nbsp; </div>
                      <!--[if mso | IE]>

        </td></tr></table>

    <![endif]-->
                    </td>
                  </tr>
                </table>
              </div>
              <!--[if mso | IE]>
            </td>

        </tr>

                  </table>
                <![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]>
          </td>
        </tr>
      </table>

      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
    <div style="margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
              <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

        <tr>

            <td
               class="footer-outlook" style="width:600px;"
            >
          <![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix footer" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                <!--[if mso | IE]>
        <table
           border="0" cellpadding="0" cellspacing="0" role="presentation"
        >
          <tr>

              <td
                 style="vertical-align:top;width:150px;"
              >
              <![endif]-->
                <div class="mj-column-per-25 mj-outlook-group-fix wrap" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:25%;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                  </table>
                </div>
                <!--[if mso | IE]>
              </td>

              <td
                 style="vertical-align:top;width:150px;"
              >
              <![endif]-->
                <div class="mj-column-per-25 mj-outlook-group-fix logo" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:25%;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                    <tr>
                      <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                          <tbody>
                            <tr>
                              <td style="width:100px;">
                                <img height="auto" src="https://ironmax.tech/mail/restore_password/Rectangle_322@2x.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="100" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
                <!--[if mso | IE]>
              </td>

              <td
                 style="vertical-align:top;width:150px;"
              >
              <![endif]-->
                <div class="mj-column-per-25 mj-outlook-group-fix contacts" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:25%;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-left:1px solid #707070;vertical-align:top;" width="100%">
                    <tr>
                      <td align="left" class="company" style="font-size:0px;padding:10px 25px;padding-bottom:20px;word-break:break-word;">
                        <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;font-weight:bold;line-height:1;text-align:left;color:#000000;">IRONMAX Technologies</div>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" class="email icon" style="font-size:0px;padding:10px 25px;padding-bottom:0;word-break:break-word;">
                        <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:14px;line-height:18px;text-align:left;color:#000000;">{{config('mail.from.address')}}</div>
                      </td>
                    </tr>
                  </table>
                </div>
                <!--[if mso | IE]>
              </td>

              <td
                 style="vertical-align:top;width:150px;"
              >
              <![endif]-->
                <div class="mj-column-per-25 mj-outlook-group-fix wrap" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:25%;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                  </table>
                </div>
                <!--[if mso | IE]>
              </td>

          </tr>
          </table>
        <![endif]-->
              </div>
              <!--[if mso | IE]>
            </td>

        </tr>

                  </table>
                <![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
  </div>
</body>

</html>
