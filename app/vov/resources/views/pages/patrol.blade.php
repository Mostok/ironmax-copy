@extends('layouts.default',["page_title"=>"Patrol",])
@section('content')
    @include('includes/header')
    <div class="product_page">
        <div id="product">
            @include('includes/patrol/features')
            @include('includes/patrol/wowslider')
        </div>
        @include('includes/patrol/faq')
        @include('includes/loginpopup')
        @include('includes/footer',['product'=>"ironmaxpatrol"])
    </div>
@endsection
