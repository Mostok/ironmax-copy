@extends('layouts.default',["page_title"=>"Reset password",])
@section('content')
    @include('includes/header')

    @include('includes.reset_password_form')

    @include('includes/loginpopup')
    @include('includes/footer')
@endsection
