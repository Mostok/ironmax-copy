@extends('layouts.default',["page_title"=>__('pages/gatekeeper_and_digital_id.title'),])
@section('content')
    @include('includes/header')
    <div class="product_page">
        <div id="product">
            @include('includes/gatekeeper_and_digital_id/features')
            @include('includes/gatekeeper_and_digital_id/wowslider')
        </div>
        @include('includes/gatekeeper_and_digital_id/faq')
        @include('includes/loginpopup')
        @include('includes/footer',['product'=>"shifter"])
    </div>
@endsection
