@extends('layouts.default',["page_title"=>"Sos",])
@section('content')
    @include('includes/header')
    <div class="product_page">
        <div id="product">
            @include('includes/sos/features')
            @include('includes/sos/wowslider')
        </div>
        @include('includes/sos/faq')
        @include('includes/loginpopup')
        @include('includes/footer',['product'=>"sos"])
    </div>
@endsection
