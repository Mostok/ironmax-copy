@extends('layouts.default',["page_title"=>"Shifter",])
@section('content')
    @include('includes/header')
    <div class="product_page">
        <div id="product">
            @include('includes/shifter/features')
            @include('includes/shifter/wowslider')
        </div>
        @include('includes/shifter/faq')
        @include('includes/loginpopup')
        @include('includes/footer',['product'=>"shifter"])
    </div>
@endsection
