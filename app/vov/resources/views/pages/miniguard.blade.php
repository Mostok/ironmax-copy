@extends('layouts.default',["page_title"=>"Miniguard",])
@section('content')
    @include('includes/header')
    <div class="product_page">
        <div id="product">
            @include('includes/miniguard/features')
            @include('includes/miniguard/wowslider')
        </div>
        @include('includes/miniguard/faq')
        @include('includes/loginpopup')
        @include('includes/footer',['product'=>"miniguard"])
    </div>
@endsection
