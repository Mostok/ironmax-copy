@extends('layouts.default',["page_title"=>$title,])
@section('content')
    @include('includes/header')
    @if($type=="tos")
        @include('includes/termsandpolicy/termsofuse')
    @else

        @switch($lang)
            @case("en")
            @include("includes/termsandpolicy/{$lang}/privacy_policy".$type)
            @break
            @default
            @include('includes/termsandpolicy/privacy_policy'.$type)
        @endswitch

    @endif
    @include('includes/loginpopup')
    @include('includes/footer')
@endsection
