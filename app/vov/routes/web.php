<?php

use App\Notifications\DailyReport;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Route;

/**
 * Auth layout
 */
Route::post('/login', 'Auth\LoginController@loginUser')->name('login');
Route::post('/forgot', 'Auth\ForgotPasswordController@sendResetLinkEmail')
    // тут добавил мидл, потому что в контроллере есть вывод сообщений
    ->middleware('locale')
    ->name('forgot');
Route::get('/logout', 'Auth\LoginController@logoutUser')->name('logout');
/**
 * Admin layout
 */
// TODO[DaGlob] 27.06.2020 17:52: role:admin,big_boss,mini_boss - remove after start create admin board
Route::prefix('admin')->middleware(['auth', 'role:admin,big_boss,mini_boss', 'locale'])->group(function () {

    Route::any('/{any?}', 'Web\AdminController@index')->name('adminIndex')->where('any', '.*');
});
/**
 * Boss layout
 */
Route::prefix('dashboard')->middleware(['auth', 'role:admin,big_boss,mini_boss', 'locale'])->group(function () {
    Route::any('/{any?}', 'Web\AdminController@dashboard')->name('dashboardIndex')->where('any', '.*');
});



/*
 * Public zone
 */
/** config block / */
$langs = ['en', 'he', 'ru'];
$lang_rules = implode('|', $langs);
$pages = ['patrol', 'sos', 'miniguard', 'shifter', 'gatekeeper_and_digital_id'];
$pages_rules = implode('|', $pages);
/** / config block */
/** reset password block / */
Route::get('/reset_password/{token}/{email}/{lang?}', 'Auth\ResetPasswordController@showResetForm')
    ->middleware(['locale', 'guest'])
    ->where([
        'lang' => "({$lang_rules})",
    ])
    ->name('password.reset');
Route::post('/reset_password/', 'Auth\ResetPasswordController@reset')
    ->middleware('locale')
    ->where([
        'lang' => "({$lang_rules})",
    ])
    ->name('password.reset.post');
/** / reset password block */
Route::group(['middleware' => 'locale'], function () use ($lang_rules, $pages_rules) {

    Route::get('/{product_static_page}/{lang?}', 'Web\PagesController@product_static_page')
        ->where([
            'product_static_page' => "({$pages_rules})",
            'lang' => "({$lang_rules})",
        ])
        ->name('product_static_page');
    Route::get('/privacy/{product_privacy_page}/{lang?}', 'Web\PagesController@product_privacy_page')
        ->where([
            'product_privacy_page' => "({$pages_rules})",
            'lang' => "({$lang_rules})",
        ])
        ->name('product_privacy_page');
    /** ------------------------------------------- */
    Route::get('/privacy_policy/{lang?}', 'Web\PagesController@privacy_policy')
        ->where(['lang' => "({$lang_rules})"])
        ->name('privacy_policy');
    /** ------------------------------------------- */
    Route::get('/termofuse/{lang?}', 'Web\PagesController@termofuse')
        ->where(['lang' => "({$lang_rules})"])
        ->name('termofuse');
    /** ------------------------------------------- */
    Route::get('/{lang?}', 'Web\PagesController@index')
        ->where(['lang' => "({$lang_rules})"])
        ->name('index');
}
);
Route::group(['middleware' => 'auth'], function () {

    Route::any('/searchdata/{lang?}', 'Api\PdfController@search')->name('search.data');
    Route::get('/downloadPdf/{id}', 'Api\PdfController@downlaod')->name('downloadPdf');
    Route::get( '/qr/{id}', 'Web\QrController@index' )->where('id', '[0-9]+')->name('QR');
    Route::get( '/qr/current', 'Web\QrController@current' )->name( 'currentQR' );
    Route::get('/barcodes/{lang}/{download?}', 'Api\BarcodeController@indexPrint');
});

Route::post('/action/sendmessage', 'Web\ContactController@store')->name('send_message');

if (config('app.env') !== 'production') {
    /** developers tools */
    Route::get('/invoice/{id}', function ($id) {
        if (!$notify = DatabaseNotification::find($id)) {
            abort('404');
        }

        $user = $notify->notifiable()->first();
        return (new DailyReport($notify->data['from'], $notify->data['to'], $notify->data['data'], true))
            ->toMail($user);
    });
}

