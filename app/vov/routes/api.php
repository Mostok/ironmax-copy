<?php

use Illuminate\Support\Facades\Route;

/*
 * API zone
 */

Route::get('/make-barcodes', 'BarcodeController@makeBarcodes')->name('QR');

Route::middleware(['auth:api', 'role:admin,big_boss,mini_boss'])->group(function () {

    Route::prefix('barcode')->group(function () {
        Route::get('/', 'BarcodeController@index');
        Route::post('/', 'BarcodeController@store');
        Route::put('/current', 'BarcodeController@updateCurrent');
        Route::put('/{barcode}', 'BarcodeController@update');
        Route::get('/{id}', 'BarcodeController@show');
        Route::delete('/{id}', 'BarcodeController@delete');
    });

    Route::prefix('events')->group(function () {
        Route::get('/', 'EventsController@index')->name('EventsControllerIndex');
        Route::post('/read', 'EventsController@read')->name('EventsControllerRead');
        Route::get('/read_all', 'EventsController@read_all')->name('EventsControllerReadAll');
    });

    Route::prefix('dashboard')->group(function () {
        Route::get('missing_patrols', 'DashboardController@missing_patrols')->name('DashboardMissingPatrols');
        Route::get('emergency_events', 'DashboardController@emergency_events')->name('DashboardEmergencyEvents');
        Route::get('incident_reports', 'DashboardController@incident_reports')->name('DashboardIncidentReports');
        Route::get('missing_barcodes', 'DashboardController@missing_barcodes')->name('DashboardMissingBarcodes');
        Route::get('missing_inspector_barcodes', 'DashboardController@missing_inspector_barcodes')->name('DashboardMissingInspectorBarcodes');
    });

    Route::prefix('points')->group(function () {
        Route::get('/', 'PointsController@index')->name('pointsIndex');
    });

    Route::prefix('facilities')->group(function () {
        Route::get('/', 'FacilitiesController@index')->name('facilitiesIndex');
        Route::get('/barcodes', 'FacilitiesController@barcodes')->name('facilitiesBarcodes');
        Route::post('/complex', 'FacilitiesController@saveComplex')->name('facilitiesSaveComplex');
        Route::get('/{code}', 'FacilitiesController@show')->name('facilitiesShow');
        Route::post('/{code}', 'FacilitiesController@save')->name('facilitiesSave');
        Route::post('/{code}/points', 'FacilitiesController@savePoints')->name('facilitiesSavePoints');
    });

    Route::prefix('profile')->group(function () {
        Route::get('/', 'ProfileController@load')->name('profileLoad');
        Route::post('/', 'ProfileController@update')->name('profileUpdate');
        Route::post('/lang', 'ProfileController@lang')->name('profileLanguage');
        Route::post('/password', 'ProfileController@changePassword')->name('profileChangePassword');
        Route::get('/config', 'ProfileController@getConfig')->name('profileGetConfig');
        Route::post('/config', 'ProfileController@setConfig')->name('profileSetConfig');
    });

    Route::prefix('reports')->group(function () {
        Route::get('inspector', 'DashboardController@inspector')->name('reportsInspector');
        Route::get('emergency', 'DashboardController@emergency')->name('reportsEmergency');
        Route::get('incident', 'DashboardController@incident')->name('reportsIncident');
        Route::get('patrol', 'DashboardController@patrol')->name('reportsPatrol');
        Route::get('exceptions', 'DashboardController@exceptions')->name('reportsExceptions');
    });

    Route::prefix('files')->group(function () {
        Route::get('/{code}', 'FileController@index')->name('fileIndex');
    });

    Route::post('searchdata', 'PdfController@search')->name('searchData');
    Route::get('all_events', 'DashboardController@allEvents');
});
