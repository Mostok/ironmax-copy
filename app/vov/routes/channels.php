<?php
/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use App\Models\User;
use Illuminate\Support\Facades\Broadcast;

Broadcast::channel('App.User.{id}', function($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('chat', function($user) {
    return $user;
});

Broadcast::channel('facility.{id}', function(User $user, $id = null) {
    return (bool) $user->facilities()->where('owner_id', '=', $id)->count();
});

Broadcast::channel('dashboard.{user_id}', function($user, $user_id) {
    return (bool) $user->id == $user_id;
});
