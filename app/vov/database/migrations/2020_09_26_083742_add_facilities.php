<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFacilities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {

            $table->string( 'owner_id' )->storedAs( 'data ->> \'$.owner_id\'' );
            $table->string( 'mini_boss_group_id' )->storedAs( 'data ->> \'$.mini_boss_group_id\'' );
            $table->string( 'big_boss_group_id' )->storedAs( 'data ->> \'$.big_boss_group_id\'' );

            $table->string( 'facility_name' )->storedAs( 'data ->> \'$.facility_name\'' );
            $table->string( 'facility_language' )->storedAs( 'data ->> \'$.facility_language\'' );

            $table->string( 'country' )->storedAs( 'data ->> \'$.country\'' );
            $table->string( 'time_zone' )->storedAs( 'data ->> \'$.time_zone\'' );
            $table->string( 'manager_id' )->storedAs( 'data ->> \'$.manager_id\'' );

            $table->string( 'company_name' )->storedAs( 'data ->> \'$.company_name\'' );
            $table->string( 'company_pic' )->storedAs( 'data ->> \'$.company_pic\'' );
            $table->string( 'company_id' )->storedAs( 'data ->> \'$.company_id\'' );

            $table->string( 'is_on_barcode_mode' )->storedAs( 'data ->> \'$.is_on_barcode_mode\'' );
            $table->string( 'is_on_site' )->storedAs( 'data ->> \'$.is_on_site\'' );
            $table->string( 'is_emergency_service_paid' )->storedAs( 'data ->> \'$.is_emergency_service_paid\'' );
            $table->string( 'is_report_service_paid' )->storedAs( 'data ->> \'$.is_report_service_paid\'' );
            $table->string( 'is_sayar' )->storedAs( 'data ->> \'$.is_sayar\'' );

            $table->string( 'barcodes_num' )->storedAs( 'data ->> \'$.barcodes_num\'' );
            $table->string( 'total_req_patrols' )->storedAs( 'data ->> \'$.total_req_patrols\'' );

            $table->json( 'timetable' )->storedAs( 'data ->> \'$.timetable\'' );
            $table->json( 'facility_barcodes_list' )->storedAs( 'data ->> \'$.facility_barcodes_list\'' );

            $table->json( 'data' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facilities');
    }
}
