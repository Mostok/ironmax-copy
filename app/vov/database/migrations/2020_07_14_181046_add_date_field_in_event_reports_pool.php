<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateFieldInEventReportsPool extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_reports_pool', function (Blueprint $table) {
            $table->date('date')
                ->after('facility_code')
                ->nullable(true)
                ->comment('отправлять пользователю ежедневный отчет событий на подчиненных объектах');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_reports_pool', function (Blueprint $table) {

            $table->dropColumn('date');
        });
    }
}
