<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePdfDatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pdf_datas')) {
            Schema::create('pdf_datas', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('owner_id', 250)->nullable();
                $table->string('big_boss_group_id', 256)->nullable();
                $table->string('mini_boss_group_id', 256)->nullable();
                $table->string('type', 255)->nullable(false);
                $table->string('file', 250)->nullable(false);
                $table->string('drive_file_id', 255)->nullable(false);
                $table->dateTime('patrol_start_time')->nullable();
                $table->dateTime('patrol_end_time')->nullable();
                $table->tinyInteger('is_cheating')->nullable();
                $table->integer('num_of_patrols')->nullable();
                $table->integer('number_of_patrols_should_be_made')->nullable();
                $table->integer('barcodes_scanned')->nullable();
                $table->integer('barcodes_should_be_scanned')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdf_datas');
    }
}
