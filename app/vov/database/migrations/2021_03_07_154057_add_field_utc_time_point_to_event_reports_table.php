<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldUtcTimePointToEventReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_reports', function (Blueprint $table) {
            $table
                ->dateTimeTz('utc_time_point')
                ->nullable()
                ->after('id');
        });
        /* принудительно синхронизируем данные по объектам */
        \Illuminate\Support\Facades\Artisan::call('x-mongo-sync:facilities');
        /* перерабатываем старые данные */
        $sql = <<<SQL
update event_reports
set utc_time_point = case code
                         when 's_tb_em' then convert_tz(
                                 DATE_ADD(CONCAT(date, ' ', start), INTERVAL TIME_TO_SEC(duration) SECOND),
                                 (select time_zone from facilities where owner_id = facility_code), 'UTC')
                         when 's_tb_ls' then convert_tz(
                                 DATE_ADD(CONCAT(date, ' ', start), INTERVAL TIME_TO_SEC(duration) SECOND),
                                 (select time_zone from facilities where owner_id = facility_code), 'UTC')
                         when 's_bc_ls' then convert_tz(
                                 CONCAT(date, ' ', start),
                                 (select time_zone from facilities where owner_id = facility_code), 'UTC')
                         when 's_ch' then convert_tz(
                                 CONCAT(date, ' ', start),
                                 (select time_zone from facilities where owner_id = facility_code), 'UTC')

                         when 's_in' then convert_tz(
                                 CONCAT(date, ' ', start),
                                 (select time_zone from facilities where owner_id = facility_code), 'UTC')
                         when 's_em' then convert_tz(
                                 CONCAT(date, ' ', start),
                                 (select time_zone from facilities where owner_id = facility_code), 'UTC')
                         when 'er_pt_nsf' then null
                         when 'er_pt_ett' then null
                         when 's_mt_t' then null
                         when 's_mt_d' then null
                         when 'er_pt_uf' then null
                         when 'er_pt_ste' then null
    end;
SQL;

        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_reports', function (Blueprint $table) {
            $table->dropColumn('utc_time_point');
        });
    }
}
