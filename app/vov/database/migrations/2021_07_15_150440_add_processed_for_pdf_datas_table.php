<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\pdfData;
use Illuminate\Support\Facades\DB;
use App\Models\MongoFacilitiesModel;

class AddProcessedForPdfDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::table('pdf_datas', function (Blueprint $table) {
                $table->tinyInteger('processed')->nullable(true);
            });

            foreach (pdfData::all() as $pdf_data) {
                $pdf_data->processed = true;
                $pdf_data->save();
            }

            foreach (MongoFacilitiesModel::all() as $mongoFacilitiesModel) {
                foreach (DB::select("show events where name like '{$mongoFacilitiesModel->getKey()}%';") as $event) {
                    DB::connection()->getpdo()->exec("DROP EVENT IF EXISTS {$event->Name};");
                }
            }

            DB::connection()->getpdo()->exec('DROP TRIGGER IF EXISTS REPORTS;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pdf_datas', function (Blueprint $table) {
            $table
                ->dropColumn('processed');
        });
    }
}
