<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldConfigUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->addColumn('json', 'config', ['nullable' => true, 'default' => null])
                ->comment('Конфигурация профиля');
        });

        \App\Models\User::query()->update(['config' => [
            "ui" => [
                "events" => [
                    "auto_open" => true
                ]
            ]
        ]]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('config');
        });
    }
}
