<?php

use App\Models\MongoFacilitiesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTriggersAndEvents extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        /**
         * @see
         *
         * SET GLOBAL log_bin_trust_function_creators = 1;
         * SELECT @@log_bin_trust_function_creators;
         *
         * SET sql_mode = (SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));
         * SELECT @@sql_mode;
         */
        foreach ( MongoFacilitiesModel::all() as $facillity ) {
            event( 'eloquent.updated: App\Models\MongoFacilitiesModel', $facillity );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        foreach ( MongoFacilitiesModel::all() as $facillity ) {
            event( 'eloquent.deleted: App\Models\MongoFacilitiesModel', $facillity );
        }
    }
}
