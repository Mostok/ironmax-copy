<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEventEmailFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // send_out_events
        Schema::table('users', function (Blueprint $table) {
            $table->addColumn('boolean', 'send_out_events', ['nullable' => true, 'default' => false])
                ->comment('отправлять пользователю ежедневный отчет событий на подчиненных объектах');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('send_out_events');
        });
    }
}
