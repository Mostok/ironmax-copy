<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EventReportsPoolUserStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create( 'event_reports_pool_user_status', function( Blueprint $table ) {

			$table->unsignedBigInteger( 'event_reports_pool_id' );
			$table->unsignedBigInteger( 'user_id' );
			$table->timestamps();
			$table->foreign( 'event_reports_pool_id' )
			      ->references( 'id' )
			      ->on( 'event_reports_pool' )
			      ->onDelete( 'cascade' );
			$table->foreign( 'user_id' )
			      ->references( 'id' )
			      ->on( 'users' )
			      ->onDelete( 'cascade' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::table( 'event_reports_pool_user_status', function( $table ) {

			$table->dropForeign( [ 'event_reports_pool_id' ] );
			$table->dropForeign( [ 'user_id' ] );
		} );
		Schema::dropIfExists( 'event_reports_pool_user_status' );
	}
}
