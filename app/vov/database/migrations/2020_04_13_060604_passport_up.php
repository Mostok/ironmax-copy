<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PassportUp extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::table( 'oauth_clients', static function( Blueprint $table ) {

			$table->string( 'secret', 100 )
			      ->nullable( true )
			      ->comment( 'secret key' )
			      ->change();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		// nothing
	}
}
