<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersTz extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::table( 'users', function( Blueprint $table ) {

			$table->addColumn( 'string', 'timezone', [ 'length' => 255, 'nullable' => true, 'default' => null ] )
			      ->comment( 'Временная зона пользователя; указывается в профиле' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::table( 'users', function( Blueprint $table ) {

			$table->dropColumn( 'timezone' );
		} );
	}
}
