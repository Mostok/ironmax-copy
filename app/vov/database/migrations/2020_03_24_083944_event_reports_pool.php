<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EventReportsPool extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create( 'event_reports_pool', function( Blueprint $table ) {

            $table->bigIncrements( 'id' );
            $table->bigInteger( 'report_id' )->storedAs( 'event_data ->> \'$.report.id\'' )->nullable( true );
            $table->string( 'facility_code' )->storedAs( 'event_data ->> \'$.facility.owner_id\'' )->nullable( true );
            $table->string( 'time_group' )->storedAs( 'event_data ->> \'$.time_group\'' )->nullable( true );
            $table->string( 'code' )->storedAs( 'event_data ->> \'$.code\'' );
            $table->string( 'type' )->nullable( true );
            $table->json( 'event_data' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::dropIfExists( 'event_reports_pool' );
    }
}
