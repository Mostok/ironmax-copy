<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMissedPointsFieldPdfDatesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        // missed_points
        Schema::table( 'pdf_datas', function( Blueprint $table ) {

            // Длинна выбрана по причине: https://support.telesign.com/s/article/what-is-the-maximum-length-of-any-phone-number
            $table->json( 'missed_points' )
                  ->nullable( true )
                  ->default( null )
                  ->comment( 'Список необработанных точек' )
                  ->after( 'is_cheating' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::table( 'pdf_datas', function( Blueprint $table ) {

            $table->dropColumn( 'missed_points' );
        } );
    }
}
