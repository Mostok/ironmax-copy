<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldEventDatetimeOnTableEventReportsPool extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_reports_pool', function (Blueprint $table) {
            $table
                ->dropColumn('event_datetime');
        });
        Schema::table('event_reports_pool', function (Blueprint $table) {
            $table
                ->dateTimeTz('event_datetime')
                ->storedAs("IF(event_data ->> '$.utc_time_point', event_data ->> '$.utc_time_point', event_data ->> '$.created_at')")
                ->after('event_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_reports_pool', function (Blueprint $table) {
            $table
                ->dropColumn('event_datetime');
        });

        Schema::table('event_reports_pool', function (Blueprint $table) {
            $table
                ->dateTimeTz('event_datetime')
                ->storedAs('event_data ->> \'$.created_at\'')
                ->after('event_data');
        });
    }
}
