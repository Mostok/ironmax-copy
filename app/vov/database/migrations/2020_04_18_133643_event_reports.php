<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class EventReports extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create( 'event_reports', function( Blueprint $table ) {

            $table->bigIncrements( 'id' );
            $table->unsignedInteger( 'report_id' )->nullable( true );
            $table->string( 'facility_code' )->nullable( true ); // owner_id
            $table->date( 'date' )->nullable( true );
            $table->string( 'time_group' )->nullable( true )->default( null );
            $table->json( 'reports_ids' )->nullable( true )->default( null );
            $table->integer( 'expected_reports' )->nullable( true )->default( null );
            $table->time( 'start' )->nullable( true );
            $table->time( 'duration' )->nullable( true );
            $table->string( 'code' );
            $table->tinyInteger( 'processed' )->nullable( true );
            $table->timestamp( 'created_at' )->default( DB::raw( 'CURRENT_TIMESTAMP' ) );
            $table->timestamp( 'updated_at' )->default( DB::raw( 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ) );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::dropIfExists( 'event_reports' );
    }
}
