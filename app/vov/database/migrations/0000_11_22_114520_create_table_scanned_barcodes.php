<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableScannedBarcodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('scanned_barcodes')) {
            Schema::create('scanned_barcodes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('owner_id', 25)->nullable();
                $table->string('big_boss_group_id', 256)->nullable();
                $table->string('mini_boss_group_id', 256)->nullable();
                $table->string('code', 256)->nullable();
                $table->string('location', 64)->nullable();
                $table->dateTime('scanned_time')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scanned_barcodes');
    }
}
