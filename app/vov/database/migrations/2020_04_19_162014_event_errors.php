<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class EventErrors extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create( 'event_errors', function( Blueprint $table ) {

            $table->bigIncrements( 'id' );
            $table->string( 'code', 30 );
            $table->text( 'message' );
            $table->string( 'query_type', 50 );
            $table->bigInteger( 'record_id' );
            $table->string( 'on_db', 50 );
            $table->string( 'on_table', 50 );
            $table->tinyInteger( 'emailed' );
            $table->timestamp( 'created_at' )->default( DB::raw( 'CURRENT_TIMESTAMP' ) );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::dropIfExists( 'event_errors' );
    }
}
