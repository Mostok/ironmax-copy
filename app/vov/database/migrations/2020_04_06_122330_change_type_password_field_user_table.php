<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTypePasswordFieldUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::table( 'users', function( Blueprint $table ) {

			$table->text( 'password' )
			      ->nullable( false )
			      ->comment( 'Пароль пользователя в закодированном виде' )
			      ->change();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

//		Schema::table( 'users', function( Blueprint $table ) {
//
//			$table->string( 'password', 255 )
//			      ->nullable( false )
//			      ->comment( 'Пароль пользователя в закодированном виде' )
//			      ->change();
//		} );
	}
}
