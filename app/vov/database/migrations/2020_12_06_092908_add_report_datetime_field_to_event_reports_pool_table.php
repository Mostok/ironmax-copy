<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReportDatetimeFieldToEventReportsPoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_reports_pool', function (Blueprint $table) {
            $table
                ->dateTimeTz('report_datetime')
                ->storedAs('IF(event_data ->> \'$.code\' IN(\'s_em\',\'s_in\'), event_data ->> \'$.created_at\',IF(event_data ->> \'$.report.patrol_start_time\' = \'null\', null, event_data ->> \'$.report.patrol_start_time\'))')
                ->nullable()
                ->after('event_datetime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_reports_pool', function (Blueprint $table) {
            $table->dropColumn('report_datetime');
        });
    }
}
