<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneFieldUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::table( 'users', function( Blueprint $table ) {

			// Длинна выбрана по причине: https://support.telesign.com/s/article/what-is-the-maximum-length-of-any-phone-number
			$table->addColumn( 'string', 'phone', [ 'length' => 15, 'nullable' => true, 'default' => null ] )
			      ->comment( 'Номер телефона профиля' )->after( 'email' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::table( 'users', function( Blueprint $table ) {

			$table->dropColumn( 'phone' );
		} );
	}
}
