<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RunFacilitiesUpdateCommands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Artisan::call('x-mongo:adjusting_time_table_facilities');
        Artisan::call('x-mongo-sync:facilities');
        Artisan::call('service:reset_facility_db_structure');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {}
}
