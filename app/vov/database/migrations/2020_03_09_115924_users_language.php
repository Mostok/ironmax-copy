<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersLanguage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::table( 'users', function( Blueprint $table ) {

			$table->addColumn( 'string', 'language', [ 'length' => 255, 'nullable' => true, 'default' => null ] )
			      ->comment( 'Предпочтительный язык для пользовтеля' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::table( 'users', function( Blueprint $table ) {

			$table->dropColumn( 'language' );
		} );
	}
}
