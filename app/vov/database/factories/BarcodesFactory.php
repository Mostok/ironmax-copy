<?php
/** @var Factory $factory */

use App\Models\ScannedBarcodes;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;

$factory->define(ScannedBarcodes::class, function (Faker $faker) {
    $user = User::whereEmail('ironmaxtech@gmail.com')->firstOrFail();
    $facilities = array_column($user->localFacilities()->get(['owner_id'])
        ->toArray(), 'owner_id');
    $owner_id = $faker->randomElement($facilities);

    $lat = $faker->randomFloat(2, 31, 32);
    $lon = $faker->randomFloat(2, 34.1, 34.7);

    $mb = $user->miniBosses()->first();

    return [
        'owner_id' => $owner_id,
        'big_boss_group_id' => $user->big_boss_group_id,
        'mini_boss_group_id' => $mb ? $mb->mini_boss_group_id : null,
        'code' => Str::random(10),
        'location' => "{$lat},{$lon}",
        'scanned_time' => now()->timezone($user->timezone)->format('Y-m-d H:i:s'),
    ];
});
