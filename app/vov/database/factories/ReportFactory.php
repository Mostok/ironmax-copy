<?php
/** @var Factory $factory */

use App\Models\Facility;
use App\Models\Report;
use App\Models\User;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;

$factory->define(Report::class, function (Faker $faker) {
    $user = User::whereEmail('ironmaxtech@gmail.com')->firstOrFail();
    /** @var Facility $facility */
    $facility = $user->localFacilities()->get()->random()->toArray();
    $owner_id = $facility['owner_id'];
    $type = $faker->optional(0.4, 'patrol_report')->randomElement(
        [
            'patrol_report',
            'inspector_report',
            'emergency_report',
            'incident_report',
        ]);
    $date = Carbon::instance(new DateTime('today'));
    $time = now($user->timezone);
    $start = $date->setTimeFromTimeString($time);
    $end = $start->clone()->addMinutes(random_int(0, 360));
    $num_of_patrols = null;
    $barcodes_scanned = null;
    $barcodes_should_be_scanned = null;
    $is_cheating = null;
    $missed_points = null;
    if (in_array($type, [
        'patrol_report',
        'inspector_report',
    ])) {
        $facility_barcodes_list = json_decode($facility['facility_barcodes_list'], true);
        $num_of_patrols = random_int(1, 20);
        $barcodes_should_be_scanned = count($facility_barcodes_list) ? random_int(1,
            count($facility_barcodes_list)) : 0;
        $barcodes_scanned = random_int(round($barcodes_should_be_scanned / 2), $barcodes_should_be_scanned);
        $missed_points = $barcodes_scanned < $barcodes_should_be_scanned ?
            $faker->randomElements($facility_barcodes_list,
                $barcodes_should_be_scanned - $barcodes_scanned
            ) ?? null :
            null;
        $is_cheating = $faker->optional(0.2, null)->randomElement([1, 0]);
    }

    return [
        'big_boss_group_id' => $user->big_boss_group_id,
        'owner_id' => $owner_id,
        'type' => $type,
        'mini_boss_group_id' => $user->mini_boss_group_id,
        'is_cheating' => $is_cheating,
        'num_of_patrols' => $num_of_patrols,
        'patrol_start_time' => $start->format('Y-m-d H:i:s'),
        'patrol_end_time' => $end->format('Y-m-d H:i:s'),
        'drive_file_id' => Str::random('32'),
        'file' => 'test',
        'barcodes_scanned' => $barcodes_scanned,
        'barcodes_should_be_scanned' => $barcodes_should_be_scanned,
        'missed_points' => $missed_points
    ];
});
