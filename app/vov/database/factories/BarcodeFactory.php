<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Models\Barcode;
use App\Models\Facility;

$factory->define(Barcode::class, function (Faker $faker) {

    $facility = Facility::query()->inRandomOrder()->first();

    return [
        'name' => $faker->word(),
        'location' => $faker->word(),
        'user_id' => 76,
        'facility_id' => $facility->owner_id,
        'additional' => '[]',
        'start_patrol_name' => 0,
    ];
});
