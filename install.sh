printf "\nHost ironmax\nUser fastuser\nHostName 159.253.18.247\nPort 22\n" >~/.ssh/config &&
  ./create_struct.sh &&
  docker-compose build --pull --force-rm &&
  docker-compose up -d &&
  docker-compose exec php-fpm composer install --no-interaction &&
  docker-compose restart php-fpm &&
  docker-compose exec php-fpm ./artisan migrate &&
  ./run_server.sh
